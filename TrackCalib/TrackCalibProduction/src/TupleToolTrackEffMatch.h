#ifndef TUPLETOOLTRACKEFFMATCH_H 
#define TUPLETOOLTRACKEFFMATCH_H 1

// Include files
// from Gaudi
#include "Kernel/IParticleTupleTool.h"
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Math/Boost.h"
#include "GaudiKernel/Vector4DTypes.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "Event/CaloCluster.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/IHitExpectation.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "Kernel/IParticle2MCAssociator.h"
#include "Kernel/LHCbID.h"


/** @class TupleToolTrackEffMatch TupleToolTrackEffMatch.h
 *  
 *  Fill tracking efficiency information: Check if probe track is matched to a long track and save additional information of the matched track
 *
 * - head_Assoc: Is probe track matched to a long track from the overlap check in the trigger?
 * - head_Overlap_XFraction: Overlap of probe with matched long track in subdetector X (T, TT, IT, OT, Velo, Muon)
 * - head_Matched_StateCTB_tx: tx of state closest to beam of matched long track 
 * - head_Matched_StateCTB_ty: ty of state closest to beam of matched long track 
 * - head_Matched_StateCTB_x: x of state closest to beam of matched long track 
 * - head_Matched_StateCTB_y: y of state closest to beam of matched long track 
 * - head_Matched_StateCTB_z: z of state closest to beam of matched long track 
 * - head_Matched_Px: x momentum component of matched long track
 * - head_Matched_Py: y momentum component of matched long track
 * - head_Matched_Pz: z momentum component of matched long track
 * - head_Matched_P: momentum of matched long track
 * - head_Matched_dP: momentum uncertainty of matched long track
 * - head_Matched_probChi2: track fit chisquare of matched long track
 * - head_Matched_Chi2NDoF: track fit chisquare per degree of freedom of matched long track
 * - head_Matched_VeloChi2: Velo track fit chisquare of matched long track
 * - head_Matched_VeloNDoF: Velo track fit chisquare per degree of freedom of matched long track
 * - head_DeltaR: difference in R between probe and matched long track
 * - head_DeltaP: difference in P between probe and matched long track
 * - head_Matched_SameMCPart: checks if probe and matched long track originate from same MC particle
 * - head_MCPart: checks if probe has an associated MC particle
 * - head_Matched_MCPart: checks if matched long track has an associated MC particle
 *
 * \sa TupleToolTrackEffMatch, DecayTreeTuple, MCDecayTreeTuple
 * 
 *  @author Michael Kolpin
 *  @date   2017-07-26
 */

class TupleToolTrackEffMatch : public TupleToolBase, virtual public IParticleTupleTool {


public: 
  /// Standard constructor
  TupleToolTrackEffMatch( const std::string& type, 
                      const std::string& name,
                      const IInterface* parent);

  virtual ~TupleToolTrackEffMatch( ); ///< Destructor
  virtual StatusCode initialize();
  virtual StatusCode fill( const LHCb::Particle*
               , const LHCb::Particle*
               , const std::string&
               , Tuples::Tuple& );
 

protected:

private:
  
  const LHCb::MCParticle* assocMCPart( const LHCb::Track* assocTrack );
  void commonLHCbIDs(const LHCb::Track* longTrack, const LHCb::Track* muonTTTrack, const LHCb::MuonPID* longMuPID, double& muFraction, double& TTFraction, double& TFraction, double& ITFraction, double& OTFraction, double& VeloFraction);
  size_t nOverlap(const std::vector<LHCb::LHCbID> list1, const std::vector<LHCb::LHCbID> list2, bool verb);
  
  std::string m_matchedLocation1;
  std::string m_matchedLocation2;
  bool m_MC;
  
  IParticle2MCAssociator* m_p2mcAssoc;

  
  

};
#endif // TUPLETOOLTRACKEFFMATCH_H

