################################################################################
# Package: ErasmusSys
################################################################################
gaudi_subdir(ErasmusSys v15r2)

gaudi_depends_on_subdirs(Hlt/HltPython
                         Math/SomeUtils
                         MCTools/BkgTreePlotter
                         Phys/VertexMixing
                         Phys/B2XTauNuTools
                         Phys/B2KShh
                         Phys/Xib2Lchh
                         Phys/Xib2pKK
                         Phys/BenderAlgo
                         Phys/AToMuMuTuples
                         Phys/Ks2Pi0MuMuTuples
                         PIDCalib/PidCalibProduction
                         Tr/TrackingScripts
                         TrackCalib/TrackCalibProduction)

