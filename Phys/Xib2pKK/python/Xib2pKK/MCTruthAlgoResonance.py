#!/usr/bin/env python

from Bender.Main import *
from Bender.MainMC import *
from ROOT import TLorentzVector
from B2KShh.ThreeBodyKinematics import ThreeBodyKinematics


# Begin Xib2hhpMCTruthResonance Class Definition

class Xib2hhpMCTruthResonance(AlgoMC) :

    """
    Algorithm to perform ntupling of MC truth information for the Xib2hhp analyses.
    """

    def __init__( self, name, Btype, daug1type, daug2type, restype, resdaug1type, resdaug2type, isXGen, **kwargs ) :
        super(Xib2hhpMCTruthResonance,self).__init__( name, **kwargs )
        self.set_types( Btype, daug1type, daug2type, restype, resdaug1type, resdaug2type, isXGen)

    def set_types( self, Btype, daug1type, daug2type, restype, resdaug1type, resdaug2type , isXGen) :
        """
        Define the decay mode of the MC being run over:
        Btype = Xib- or Omegab-
        restype = K*(892)-, rho(770)-, N(1520)+, Delta+
        daug1type = pi, K, p
        daug2type = pi, K, p
        resdaug1type = pi, K, p, pi0 
        resdaug2type = pi, K, p, pi0 
        isXGen = true if xgen is supposed to be analyzed
        One (and only one) of daug1type, daug2type , resDaug1type and resDaug2type must be a proton and a pi0.
        daug3 will be stored as proton.
        """

        self.xgen = isXGen
        self.parentID = None
        self.daug1ID = None
        self.daug2ID = None
        self.daug3ID = None
        self.resonanceID = None
        self.resdaug1ID = None
        self.resdaug2ID = None
        self.resName = None

        self.names = {}

        # set the particle types
        if Btype == 'Xib':
            self.parentID = LHCb.ParticleID(5132)
            self.names[5132] = 'Xib'
            self.names[-5132] = 'Xib'
        elif Btype == 'Omegab':
            self.parentID = LHCb.ParticleID(5332)
            self.names[ 5332] = 'Omegab'
            self.names[-5332] = 'Omegab'
        else:
            self.Warning( 'B type ('+Btype+') not recognised, exiting.' , SUCCESS )
            exit()

        # determine the proton
        daug_list = []
        daug_list.append( daug1type )
        daug_list.append( daug2type )
        daug_list.append( resdaug1type )
        daug_list.append( resdaug2type )

        nP = daug_list.count('p+')
        npi0 = daug_list.count('pi0')
        if (1 != nP) and (1 != npi0):
            self.Error( 'Wrong number of pi0 and proton particles' )
            return

        if nP == 1 :
            self.daug3ID = LHCb.ParticleID(2212)
            self.names[2212]   = 'p'
            self.names[-2212]  = 'p'
            daug_list.remove( 'p+' )

        if npi0 == 1 :
            self.names[111]  = 'pi0'
            self.names[-111] = 'pi0'
            daug_list.remove( 'pi0' )

        # determine the charge daughter types
        daug_list[0] = daug_list[0].replace('+','')
        daug_list[0] = daug_list[0].replace('-','')
        daug_list[0] = daug_list[0].replace('~','')
        daug_list[1] = daug_list[1].replace('+','')
        daug_list[1] = daug_list[1].replace('-','')
        daug_list[1] = daug_list[1].replace('~','')

        # set the daug1 and daug2 types
        if daug_list[0] == 'pi' and daug_list[1] == 'pi':
            self.daug1ID = LHCb.ParticleID(-211)
            self.daug2ID = LHCb.ParticleID(-211)
            self.names[-211] = 'h1'
            self.names[211]  = 'h1'
        elif daug_list[0] == 'K' and daug_list[1] == 'K':
            self.daug1ID = LHCb.ParticleID(-321)
            self.daug2ID = LHCb.ParticleID(-321)
            self.names[-321] = 'h1'
            self.names[321]  = 'h1'
        elif daug_list[0] == 'pi' and daug_list[1] == 'K':
            self.daug1ID = LHCb.ParticleID(-211)
            self.daug2ID = LHCb.ParticleID(-321)
            self.names[-211] = 'h1'
            self.names[-321] = 'h2'
            self.names[211]  = 'h1'
            self.names[321]  = 'h2'
        elif daug_list[0] == 'K' and daug_list[1] == 'pi':
            self.daug1ID = LHCb.ParticleID(-321)
            self.daug2ID = LHCb.ParticleID(-211)
            self.names[-321] = 'h1'
            self.names[-211] = 'h2'
            self.names[321]  = 'h1'
            self.names[211]  = 'h2'
        else :
            self.Error( 'hh types ('+daug_list[0]+','+daug_list[1]+') not recognised, exiting.' , FAILURE )
            exit()

        # determine the resonance type
        if restype == 'K*(892)-' :
            self.resonanceID = LHCb.ParticleID(-323)
        elif restype == 'rho(770)-' :
            self.resonanceID = LHCb.ParticleID(-213)
        elif restype == 'Delta+' :
            self.resonanceID = LHCb.ParticleID(2214)
        elif restype == 'N(1520)+' :
            self.resonanceID = LHCb.ParticleID(2124)
        else :
            self.Warning( 'Resonance type ('+restype+') not recognised, exiting.' , FAILURE )
            exit()

        # determine the resonance daughter types
        if resdaug1type == 'pi+' :
            self.resdaug1ID = LHCb.ParticleID(211)
        elif resdaug1type == 'K+' :
            self.resdaug1ID = LHCb.ParticleID(321)
        elif resdaug1type == 'p+' or resdaug1type == 'p':
            self.resdaug1ID = LHCb.ParticleID(2212)
        elif resdaug1type == 'pi-' or resdaug1type == 'pi':
            self.resdaug1ID = LHCb.ParticleID(-211)
        elif resdaug1type == 'K-' or resdaug1type == 'K':
            self.resdaug1ID = LHCb.ParticleID(-321)
        elif resdaug1type == 'p-':
            self.resdaug1ID = LHCb.ParticleID(-2212)
        elif resdaug1type == 'pi0' :
            self.resdaug1ID = LHCb.ParticleID(111)
        else :
            self.Warning( 'Resonance daughter 1 type ('+resdaug1type+') not recognised, exiting.' , FAILURE )
            exit()

        if resdaug2type == 'pi+' :
            self.resdaug2ID = LHCb.ParticleID(211)
        elif resdaug2type == 'K+' :
            self.resdaug2ID = LHCb.ParticleID(321)
        elif resdaug2type == 'p+' or resdaug2type == 'p':
            self.resdaug2ID = LHCb.ParticleID(2212)
        elif resdaug2type == 'pi-' or resdaug2type == 'pi':
            self.resdaug2ID = LHCb.ParticleID(-211)
        elif resdaug2type == 'K-' or resdaug2type == 'K':
            self.resdaug2ID = LHCb.ParticleID(-321)
        elif resdaug2type == 'p-':
            self.resdaug2ID = LHCb.ParticleID(-2212)
        elif resdaug2type == 'pi0' :
            self.resdaug2ID = LHCb.ParticleID(111)
        else :
            self.Warning( 'Resonance daughter 1 type ('+resdaug2type+') not recognised, exiting.' , FAILURE )
            exit()

    def check_types( self ) :

        initB = 0
        if self.parentID.isBaryon() :
            initB = 1

        finalB = 0
        for id in ( self.daug1ID, self.daug2ID, self.daug3ID ) :
            if id.isBaryon() :
                if id.pid() > 0 :
                    finalB += 1
                else :
                    finalB -= 1

        if initB != finalB :
            self.Error( 'Initial and final state baryon numbers do not match, '+str(initB)+' != '+str(finalB) )
            return FAILURE

        daug1Mass = self.partpropsvc.find( self.daug1ID ).mass()
        daug2Mass = self.partpropsvc.find( self.daug2ID ).mass()
        daug3Mass = self.partpropsvc.find( self.daug3ID ).mass()
        parentMass = self.partpropsvc.find( self.parentID ).mass()

        if parentMass < ( daug1Mass + daug2Mass + daug3Mass ) :
            return FAILURE

        self.kinematics = ThreeBodyKinematics( daug1Mass, daug2Mass, daug3Mass, parentMass )

        return SUCCESS

    def form_decay_descriptor( self ) :

        parName = self.partpropsvc.find( self.parentID ).name()
        daug1Name = self.partpropsvc.find( self.daug1ID ).name()
        daug2Name = self.partpropsvc.find( self.daug2ID ).name()
        daug3Name = self.partpropsvc.find( self.daug3ID ).name()
        resName = self.partpropsvc.find( self.resonanceID ).name()
        resdaug1Name = self.partpropsvc.find( self.resdaug1ID ).name()
        resdaug2Name = self.partpropsvc.find( self.resdaug2ID ).name()

        if resName == 'K*(892)-' or resName == 'rho(770)-':
            self.decay_descriptor = '[ '
            self.decay_descriptor += parName
            self.decay_descriptor += ' => ( '
            self.decay_descriptor += resName
            self.decay_descriptor += ' => '
            self.decay_descriptor += resdaug1Name
            self.decay_descriptor += ' '
            self.decay_descriptor += resdaug2Name
            self.decay_descriptor += ' ) '
            self.decay_descriptor += daug1Name
            self.decay_descriptor += ' '
            self.decay_descriptor += daug3Name
            self.decay_descriptor += ' ]CC'
        else:
            self.decay_descriptor = '[ '
            self.decay_descriptor += parName
            self.decay_descriptor += ' => ( '
            self.decay_descriptor += resName
            self.decay_descriptor += ' => '
            self.decay_descriptor += resdaug1Name
            self.decay_descriptor += ' '
            self.decay_descriptor += resdaug2Name
            self.decay_descriptor += ' ) '
            self.decay_descriptor += daug1Name
            self.decay_descriptor += ' '
            self.decay_descriptor += daug2Name
            self.decay_descriptor += ' ]CC'

        self.Info( 'Will use the decay descriptor '+self.decay_descriptor )


    def initialize( self ) :

        sc = AlgoMC.initialize( self )
        if sc.isFailure() :
            return sc

        # set up the kinematics object based on the decay
        self.partpropsvc = self.ppSvc()

        # check the validity of the decay
        sc = self.check_types()
        if sc.isFailure() :
            return sc

        # form the decay descriptor
        self.form_decay_descriptor()

        # set up the reconstrucible/reconstructed tools
        if not self.xgen : 
            self.recible = self.tool( cpp.IMCReconstructible, 'MCReconstructible' )
            self.rected  = self.tool( cpp.IMCReconstructed,   'MCReconstructed'   )

        return SUCCESS

    def reco_status_tuple( self, tuple, mcparticle, name ) :
        """
        Store the reconstructible/reconstructed status of an MC particle
        """

        if not mcparticle or self.xgen :
            tuple.column_int( name + '_Reconstructible', -1 )
            tuple.column_int( name + '_Reconstructed',   -1 )
            return

        cat_ible = self.recible.reconstructible( mcparticle )
        cat_ted  = self.rected.reconstructed( mcparticle )

        tuple.column_int( name + '_Reconstructible', int(cat_ible ))
        tuple.column_int( name + '_Reconstructed',   int(cat_ted  ))


    def mc_p4_tuple( self, tuple, mcparticle, name ) :
        """
        Store the id, charge, 4-momentum, mass, p_t and eta of an MC particle
        """

        if not mcparticle :
            tuple.column_int( name + '_TRUEID',  -1 )
            tuple.column_int( name + '_TRUEQ',   -1 )
            tuple.column_double( name + '_TRUEP',   -1.1 )
            tuple.column_double( name + '_TRUEPE',  -1.1 )
            tuple.column_double( name + '_TRUEPX',  -1.1 )
            tuple.column_double( name + '_TRUEPY',  -1.1 )
            tuple.column_double( name + '_TRUEPZ',  -1.1 )
            tuple.column_double( name + '_TRUEPT',  -1.1 )
            tuple.column_double( name + '_TRUEETA', -1.1 )
            tuple.column_double( name + '_TRUEPHI',   -1.1 )
            tuple.column_double( name + '_TRUETHETA', -1.1 )
            tuple.column_double( name + '_TRUEM',   -1.1 )
            tuple.column_int( name + '_OSCIL',   -1   )
            return

        tuple.column_int(    name + '_TRUEID',    int(MCID(mcparticle))         )
        tuple.column_int(    name + '_TRUEQ',     int(MC3Q(mcparticle)/3)       )
        tuple.column_double( name + '_TRUEP',   MCP(mcparticle)          )
        tuple.column_double( name + '_TRUEPE',  MCE(mcparticle)          )
        tuple.column_double( name + '_TRUEPX',  MCPX(mcparticle)         )
        tuple.column_double( name + '_TRUEPY',  MCPY(mcparticle)         )
        tuple.column_double( name + '_TRUEPZ',  MCPZ(mcparticle)         )
        tuple.column_double( name + '_TRUEPT',  MCPT(mcparticle)         )
        tuple.column_double( name + '_TRUEETA', MCETA(mcparticle)        )
        tuple.column_double( name + '_TRUEPHI',   MCPHI(mcparticle)             )
        tuple.column_double( name + '_TRUETHETA', MCTHETA(mcparticle)           )
        tuple.column_double( name + '_TRUEM',   MCM(mcparticle)          )
        tuple.column_int(    name + '_OSCIL',     int(MCOSCILLATED(mcparticle)) )

    def mc_vtx_tuple( self, tuple, mcparticle, name ) :
        """
        Store vertex and lifetime info for the MC particle
        """

        if not mcparticle :
            tuple.column_double( name + '_TRUEORIGINVERTEX_X', -1.1 )
            tuple.column_double( name + '_TRUEORIGINVERTEX_Y', -1.1 )
            tuple.column_double( name + '_TRUEORIGINVERTEX_Z', -1.1 )
            tuple.column_double( name + '_TRUECTAU'          , -1.1 )
            return

        tuple.column_double( name + '_TRUEORIGINVERTEX_X', MCVFASPF(MCVX)(mcparticle)   )
        tuple.column_double( name + '_TRUEORIGINVERTEX_Y', MCVFASPF(MCVY)(mcparticle)   )
        tuple.column_double( name + '_TRUEORIGINVERTEX_Z', MCVFASPF(MCVZ)(mcparticle)   )
        tuple.column_double( name + '_TRUECTAU'          , MCCTAU(mcparticle)           )

    def mc_dp_tuple( self, tuple, mcparent ) :
        """
        Store the MC truth DP co-ordinates
        """

        # loop through the B daughters and store their 4-momenta treating the
        # expected 3 daughters and PHOTOS photons separately

        gammaB_p4 = []
        gammaR_p4 = []
        resdaug_id = [0,0]
        resdaug_p4 = [ TLorentzVector(), TLorentzVector() ]
        daugNonRes_id = [ 0, 0]
        daugNonRes_p4 = [ TLorentzVector(), TLorentzVector()]

        parID = mcparent.particleID().pid()

        for daug in mcparent.children( True ) :

            daugID = daug.particleID()

            px = MCPX( daug )
            py = MCPY( daug )
            pz = MCPZ( daug )
            pe = MCE ( daug )

            p4 = TLorentzVector( px, py, pz, pe )

            if 22 == daugID.pid() :
                gammaB_p4.append( p4 )
            elif daugID.pid() not in self.names :

                for resdaug in daug.children( True ) :

                    resdaugID = resdaug.particleID()

                    px = MCPX( resdaug )
                    py = MCPY( resdaug )
                    pz = MCPZ( resdaug )
                    pe = MCE ( resdaug )

                    p4 = TLorentzVector( px, py, pz, pe )

                    if 22 == resdaugID.pid() :
                        gammaR_p4.append( p4 )
                    elif resdaugID.abspid() == self.resdaug1ID.abspid() and resdaug_id[0] == 0:
                        resdaug_id[0] = resdaugID.pid()
                        resdaug_p4[0] = p4
                    else:
                        resdaug_id[1] = resdaugID.pid()
                        resdaug_p4[1] = p4
            elif daugNonRes_id[0] == 0:
                daugNonRes_id[0] = daugID.pid()
                daugNonRes_p4[0] = p4
            else :
                daugNonRes_id[1] = daugID.pid()
                daugNonRes_p4[1] = p4

        ngammaB = len(gammaB_p4)
        ngammaR = len(gammaR_p4)
        tuple.column_int( 'nPHOTOS', ngammaB+ngammaR )

        if 0 != ngammaB :
            for gamma in gammaB_p4 :
                minangle = 1000.0
                mindaug = -1
                for daug in daugNonRes_p4 :
                    angle = gamma.Angle( daug.Vect() )
                    if abs(angle) < minangle :
                        minangle = angle
                        mindaug = daugNonRes_p4.index(daug)

                daugNonRes_p4[ mindaug ] += gamma

        if 0 != ngammaR :
            for gamma in gammaR_p4 :
                minangle = 1000.0
                mindaug = -1
                for daug in resdaug_p4 :
                    angle = gamma.Angle( daug.Vect() )
                    if abs(angle) < minangle :
                        minangle = angle
                        mindaug = resdaug_p4.index(daug)

                resdaug_p4[ mindaug ] += gamma

        unsorted_daug_id = daugNonRes_id + resdaug_id
        unsorted_daug_p4 = daugNonRes_p4 + resdaug_p4

        #print(unsorted_daug_id)
        #print(len(unsorted_daug_p4))

        daug_id = [ 0, 0, 0 ]
        daug_p4 = [ TLorentzVector(), TLorentzVector(), TLorentzVector() ]

        for i in range(len(unsorted_daug_p4)):
            id = unsorted_daug_id[i]
            if self.names[ id ] == 'p' :
                daug_p4[2] = unsorted_daug_p4[i]
                daug_id[2] = id
            elif self.names[ id ] == 'h1' and daug_id[0] == 0:
                daug_p4[0] = unsorted_daug_p4[i]
                daug_id[0] = id
            elif self.names[ id ] == 'pi0':
                continue
            else :
                daug_p4[1] = unsorted_daug_p4[i]
                daug_id[1] = id

        p12 = daug_p4[0] + daug_p4[1]
        p13 = daug_p4[0] + daug_p4[2]
        p23 = daug_p4[1] + daug_p4[2]

        #print(daug_id)
        #print(daug_p4)
        #print(len(daug_p4))

        daug_name = [ 'h1', 'h2', 'p' ]

        for i in range(len(daug_p4)) :
            tuple.column_double( daug_name[i] + '_CORRPE',  daug_p4[i].E()  )
            tuple.column_double( daug_name[i] + '_CORRPX',  daug_p4[i].Px() )
            tuple.column_double( daug_name[i] + '_CORRPY',  daug_p4[i].Py() )
            tuple.column_double( daug_name[i] + '_CORRPZ',  daug_p4[i].Pz() )

        m12Sq = p12.M2()
        m13Sq = p13.M2()
        m23Sq = p23.M2()

        mPrime = -1.1
        thPrime = -1.1

        if self.kinematics.withinDPLimits( m13Sq, m23Sq ) :
            self.kinematics.updateKinematics( m13Sq, m23Sq )
            mPrime = self.kinematics.mPrime
            thPrime = self.kinematics.thPrime

        if m12Sq<0 :     m12Sq = -1.1
        if m12Sq>100e6 : m12Sq = -1.1
        if m13Sq<0 :     m13Sq = -1.1
        if m13Sq>100e6 : m13Sq = -1.1
        if m23Sq<0 :     m23Sq = -1.1
        if m23Sq>100e6 : m23Sq = -1.1

        tuple.column_double( 'm12Sq_MC', m12Sq )
        tuple.column_double( 'm13Sq_MC', m13Sq )
        tuple.column_double( 'm23Sq_MC', m23Sq )

        tuple.column_double( 'mPrime_MC', mPrime )
        tuple.column_double( 'thPrime_MC', thPrime )


    def analyse( self ) :
        """
        The method called in the event loop
        """

        # select MC particles
        cands  = self.mcselect('cands',  self.decay_descriptor)
        ncands = cands.size()

        if 0 == ncands :
            self.Warning( 'No MC B candidates found in this event', SUCCESS )
            return SUCCESS

        # get the event header
        if not self.xgen:
            evthdr = self.get( '/Event/Rec/Header' )
        else :
            evthdr = self.get( '/Event/Gen/Header' )

        # create the ntuple
        tuple = self.nTuple( 'tupleMCTruth' )

        # loop through the candidates
        for cand in cands :

            # fill event information
            tuple.column_int( 'runNumber', evthdr.runNumber() )
            tuple.column_int( 'evtNumber', evthdr.evtNumber() )
            tuple.column_int( 'nCands',    ncands             )

            # get the ID and hance name of the parent
            candID = cand.particleID().pid()
            candname = self.names[ candID ]
            candcharge = cand.particleID().threeCharge()

            # store parent information
            self.mc_p4_tuple( tuple, cand, candname )
            self.mc_vtx_tuple( tuple, cand, candname )
            self.reco_status_tuple( tuple, cand, candname )

            # store DP information
            self.mc_dp_tuple( tuple, cand )

            # find the 4 final-state particles
            daug_list = []
            for daug in cand.children( True ) :
                daugID = daug.particleID().pid()
                if 22 == daugID :
                    continue
                elif daugID not in self.names :
                    for resdaug in daug.children( True ) :
                        resdaugID = resdaug.particleID().pid()
                        if 22 == resdaugID :
                            continue
                        daug_list.append( resdaug )
                else :
                    daug_list.append( daug )

            #print("The list of dogs %i" %(len(daug_list)))

            fillh1 = True
            # loop through the final state particles and store their information
            for daug in daug_list :

                daugID = daug.particleID().pid()
                daugname = None

                if self.names[ daugID ] == 'p' :
                    daugname = 'p'
                    #print('I am here 1')
                elif self.names[ daugID ] == 'h1' and fillh1:
                    daugname = 'h1'
                    fillh1 = False
                    #print('I am here 2')
                elif self.names[ daugID ] == 'pi0':
                    daugname = 'pi0'
                    #print('I am here 3')
                else :
                    daugname = 'h2'
                    #print('I am here 4')

                self.mc_p4_tuple( tuple, daug, daugname )
                self.mc_vtx_tuple( tuple, daug, daugname )
                self.reco_status_tuple( tuple, daug, daugname )

                ## if this is the V0 then also find and store information for it's daughters
                #if abs(daugID) == self.daug3ID.abspid() :

                #    tuple.column_double( daugname+'_NDAUG', daug.nChildren() )

                #    gdaug1_name = ''
                #    gdaug2_name = ''

                #    for gdaug in daug.children( True ) :
                #        gdaugID = gdaug.particleID().pid()
                #        if gdaugID not in self.gdaugnames :
                #            continue

                #        gdaugname = self.gdaugnames[gdaugID]
                #        if gdaug1_name == '' :
                #            gdaug1_name = gdaugname
                #        elif gdaug2_name == '' :
                #            gdaug2_name = gdaugname
                #        else :
                #            self.Warning('Unexpected extra daughter of '+daugname, SUCCESS)
                #            continue

                #        self.mc_p4_tuple( tuple, gdaug, gdaugname )
                #        self.mc_vtx_tuple( tuple, gdaug, gdaugname )
                #        self.reco_status_tuple( tuple, gdaug, gdaugname )

                #    gdaugnames = self.gdaugnames.values()
                #    if gdaug1_name in gdaugnames :
                #        gdaugnames.remove( gdaug1_name )
                #    if gdaug2_name in gdaugnames :
                #        gdaugnames.remove( gdaug2_name )

                #    for name in gdaugnames :
                #        self.mc_p4_tuple( tuple, None, name )
                #        self.mc_vtx_tuple( tuple, None, name )
                #        self.reco_status_tuple( tuple, None, name )
            tuple.write()
            fillh1 = True

        return SUCCESS

# End of Xib2hhpMCTruthResonance Class Definition

