#!/usr/bin/env python

import math
from ROOT import ( TMath, TRandom3 )

# Begin ThreeBodyKinematics Class Definition

class ThreeBodyKinematics(object) :
    """
    3-body kinematics
    """

    def __init__( self, m1, m2, m3, mParent ) :
        self._mP = mParent
        self._m = [ m1, m2, m3 ]

        self._mPSq = mParent*mParent
        self._mSq = [ m1*m1, m2*m2, m3*m3 ]

        self._mDTot = m1 + m2 + m3
        self._mSqDTot = m1*m1 + m2*m2 + m3*m3

        self._mMin = [ self._mDTot - self._m[i] for i in range(3) ]
        self._mMax = [ self._mP - self._m[i] for i in range(3) ]
        self._mDiff = [ self._mMax[i] - self._mMin[i] for i in range(3) ]

        self._mSqMin = [ self._mMin[i] * self._mMin[i] for i in range(3) ]
        self._mSqMax = [ self._mMax[i] * self._mMax[i] for i in range(3) ]
        self._mSqDiff = [ self._mSqMax[i] - self._mSqMin[i] for i in range(3) ]

        self._mij = [ 0.0, 0.0, 0.0 ]
        self._mijSq = [ 0.0, 0.0, 0.0 ]
        self._cij = [ 0.0, 0.0, 0.0 ]

        self._mPrime = 0.0
        self._thPrime = 0.0

        self._random = TRandom3(1234)

    @property
    def m12Sq( self ) :
        return self._mijSq[2]

    @property
    def m13Sq( self ) :
        return self._mijSq[1]

    @property
    def m23Sq( self ) :
        return self._mijSq[0]

    @property
    def mPrime( self ) :
        return self._mPrime

    @property
    def thPrime( self ) :
        return self._thPrime

    def genFlatPhaseSpace( self ) :
        self.updateKinematics( *self._genFlatPoint() )
        while not self._withinDPLimits() :
            self.updateKinematics( *self._genFlatPoint() )

    def _genFlatPoint( self ) :
        m13Sq = self._mSqMin[1] + self._random.Rndm()*self._mSqDiff[1]
        m23Sq = self._mSqMin[0] + self._random.Rndm()*self._mSqDiff[0]
        return m13Sq, m23Sq

    def updateKinematics( self, m13Sq, m23Sq ) :
        self._updateMassSquares( m13Sq , m23Sq )
        self._calcHelicities()
        self._calcSqDPVars()

    def updateKinematicsSqDP( self, mPrime, thPrime ) :
        self._updateMassSquaresSqDP( mPrime , thPrime )
        self._calcHelicities()

    def _withinDPLimits( self ) :
        return self.withinDPLimits( self._mijSq[1], self._mijSq[0] )

    def withinDPLimits( self, m13Sq, m23Sq ) :
        if m13Sq < self._mSqMin[1] or m13Sq > self._mSqMax[1] :
            return False

        if m23Sq < self._mSqMin[0] or m23Sq > self._mSqMax[0] :
            return False

        m13 = TMath.Sqrt( m13Sq )

        e3CMS13 = ( m13Sq - self._mSq[0] + self._mSq[2] ) / (2.0*m13)
        p3CMS13 = self._pCalc( e3CMS13, self._mSq[2] )

        e2CMS13 = ( self._mPSq - m13Sq - self._mSq[1] ) / (2.0*m13)
        p2CMS13 = self._pCalc( e2CMS13, self._mSq[1] )

        term1 = 2.0 * e2CMS13 * e3CMS13 + self._mSq[1] + self._mSq[2]
        term2 = 2.0 * p2CMS13 * p3CMS13

        m23SqLocMin = term1 - term2
        m23SqLocMax = term1 + term2

        withinDP = False
        if m23Sq > m23SqLocMin and m23Sq < m23SqLocMax :
            withinDP = True

        return withinDP

    def calcSqDPJacobian( self ) :
        e1CMS12 = ( self._mijSq[2] - self._mSq[1] + self._mSq[0] ) / (2.0*self._mij[2])
        e3CMS12 = ( self._mPSq - self._mijSq[2] - self._mSq[2] ) / (2.0*self._mij[2])

        p1CMS12 = self._pCalc( e1CMS12, self._mSq[0] )
        p3CMS12 = self._pCalc( e3CMS12, self._mSq[2] )

        deriv1 = TMath.PiOver2() * self._mDiff[2] * TMath.Sin( TMath.Pi() * self._mPrime )
        deriv2 = TMath.Pi() * TMath.Sin( TMath.Pi() * self._thPrime )

        jacobian = 4.0 * p1CMS12 * p3CMS12 * self._mij[2] * deriv1 * deriv2

        return jacobian

    def _calcSqDPVars( self ) :
        value = 2.0*(self._mij[2] - self._mMin[2])/self._mDiff[2] - 1.0
        self._mPrime = TMath.InvPi() * TMath.ACos( value )
        self._thPrime = TMath.InvPi() * TMath.ACos( self._cij[2] )

    def _updateMassSquares( self, m13Sq, m23Sq ) :
        m12Sq = self._calcThirdMassSq( m13Sq, m23Sq )

        self._mijSq[1] = m13Sq
        self._mij[1] = TMath.Sqrt( m13Sq )

        self._mijSq[0] = m23Sq
        self._mij[0] = TMath.Sqrt( m23Sq )

        self._mijSq[2] = m12Sq
        self._mij[2] = TMath.Sqrt( m12Sq )

    def _updateMassSquaresSqDP( self, mPrime, thPrime ) :
        self._mPrime = mPrime
        self._thPrime = thPrime

        m12 = 0.5 * self._mDiff[2] * ( 1.0 + TMath.Cos( TMath.Pi() * mPrime ) ) + self._mMin[2]
        c12 = TMath.Cos( TMath.Pi() * thPrime )

        self._updateMassSquares_SqDP12( m12, c12 )

    def _updateMassSquares_SqDP12( self, m12, c12 ) :
        self._mij[2] = m12
        self._mijSq[2] = m12*m12
        self._cij[2] = c12

        self._mijSq[1] = self._mFromC( 0, 1, 2 )
        self._mij[1] = TMath.Sqrt( self._mijSq[1] )

        self._mijSq[0] = self._calcThirdMassSq( self._mijSq[2], self._mijSq[1] )
        self._mij[0] = TMath.Sqrt( self._mijSq[0] )

    def _calcHelicities( self ) :
        self._cij[2] = self._cFromM( 0, 1, 2 )
        self._cij[0] = self._cFromM( 1, 2, 0 )
        self._cij[1] = self._cFromM( 2, 0, 1 )

    def _cFromM( self, i, j, k ) :
        eiCMSij = self._eiCMSij( i, j, k )
        ekCMSij = self._ekCMSij( i, j, k )

        qi = self._pCalc( eiCMSij, self._mSq[i] )
        qk = self._pCalc( ekCMSij, self._mSq[k] )

        cFromM = -1.0 * ( self._mijSq[j] - self._mSq[i] - self._mSq[k] - 2.0*eiCMSij*ekCMSij ) / (2.0*qi*qk)

        if i == 1 :
            cFromM = -1.0 * cFromM

        return cFromM

    def _mFromC( self, i, j, k ) :
        eiCMSij = self._eiCMSij( i, j, k )
        ekCMSij = self._ekCMSij( i, j, k )

        qi = self._pCalc( eiCMSij, self._mSq[i] )
        qk = self._pCalc( ekCMSij, self._mSq[k] )

        mFromC = self._mSq[i] + self._mSq[k] + 2.0*eiCMSij*ekCMSij - 2.0*qi*qk*self._cij[k]

        return mFromC

    def _eiCMSij( self, i, j, k ) :
        return ( self._mijSq[k] - self._mSq[j] + self._mSq[i] ) / (2.0 * self._mij[k])

    def _ekCMSij( self, i, j, k ) :
        return ( self._mPSq - self._mijSq[k] - self._mSq[k] ) / (2.0 * self._mij[k])

    def _calcThirdMassSq( self, firstMassSq, secondMassSq ) :
        return self._mPSq + self._mSqDTot - firstMassSq - secondMassSq

    def _pCalc( self, energy, massSq ) :
        return TMath.Sqrt( energy*energy - massSq )

# End of ThreeBodyKinematics Class Definition

