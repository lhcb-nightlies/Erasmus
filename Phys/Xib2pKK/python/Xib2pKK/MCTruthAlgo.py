#!/usr/bin/env python

from Bender.Main import *
from Bender.MainMC import *
from ROOT import ( TLorentzVector, TVector3, TRotation, TLorentzRotation, TMath )
from Xib2pKK.ThreeBodyKinematics import ThreeBodyKinematics


# Begin Xib2pKKMCTruth Class Definition

class Xib2pKKMCTruth(AlgoMC) :

    """
    Algorithm to perform ntupling of MC truth information for the Xib2pKK analyses.
    """

    def __init__( self, name, Btype, h1type, h2type, h3type, isXGen, **kwargs ) :
        super(Xib2pKKMCTruth,self).__init__( name, **kwargs )
        self.set_types( Btype, h1type, h2type, h3type, isXGen )

    def set_types( self, Btype, h1type, h2type, h3type, isXGen ):
        """
        Define the decay mode of the MC being run over:
        Btype = Xib, Omegab, Bu
        h1type = pi, K
        h2type = pi, K
        h3type = must be p for Xib & Omegab, but for Bu it can be pi,K
        isXGen = true if xgen is supposed to be analyzed
        """

        self.xgen = isXGen
        self.names = {}

        # set the particle types
        if Btype == 'Xib':
            self.parentID = LHCb.ParticleID(5132)
            self.names[5132] = 'Xib'
            self.names[-5132] = 'Xib'
        elif Btype == 'Omegab':
            self.parentID = LHCb.ParticleID(5332)
            self.names[ 5332] = 'Omegab'
            self.names[-5332] = 'Omegab'
        elif Btype == 'Bu':
            self.parentID = LHCb.ParticleID(-521)
            self.names[ 521] = 'Bu'
            self.names[-521] = 'Bu'
        else:
            self.Warning( 'B type ('+Btype+') not recognised, exiting.' , SUCCESS )
            exit()

        self.daug1ID = None
        self.daug2ID = None
        self.daug3ID = None
        
        if h1type == 'pi' and h2type == 'pi' and h3type == 'p':
            self.daug1ID = LHCb.ParticleID(-211)
            self.daug2ID = LHCb.ParticleID(-211)
            self.daug3ID = LHCb.ParticleID(2212)
        elif h1type == 'K' and h2type == 'K' and h3type == 'p':
            self.daug1ID = LHCb.ParticleID(-321)
            self.daug2ID = LHCb.ParticleID(-321)
            self.daug3ID = LHCb.ParticleID(2212)
        elif ( h1type == 'pi' and h2type == 'K' and h3type == 'p') or (h1type == 'K' and h2type == 'pi' and h3type == 'p') :
            self.daug1ID = LHCb.ParticleID(-321)
            self.daug2ID = LHCb.ParticleID(-211)
            self.daug3ID = LHCb.ParticleID(2212)
        elif h1type == 'K' and h2type == 'K' and h3type == 'K':
            self.daug1ID = LHCb.ParticleID(-321)
            self.daug2ID = LHCb.ParticleID(-321)
            self.daug3ID = LHCb.ParticleID(321)
        elif ( h1type == 'K' and h2type == 'pi' and h3type == 'K') or (h1type == 'pi' and h2type == 'K' and h3type == 'K') or (h1type == 'K' and h2type == 'K' and h3type == 'pi'):
            self.daug1ID = LHCb.ParticleID(-321)
            self.daug2ID = LHCb.ParticleID(-211)
            self.daug3ID = LHCb.ParticleID(321)
        elif (h1type == 'K' and h2type == 'pi' and h3type == 'pi') or (h1type == 'pi' and h2type == 'pi' and h3type == 'K') or ( h1type == 'pi' and h2type == 'K' and h3type == 'pi'):
            self.daug1ID = LHCb.ParticleID(-321)
            self.daug2ID = LHCb.ParticleID(-211)
            self.daug3ID = LHCb.ParticleID(211)
        elif h1type == 'pi' and h2type == 'pi' and h3type == 'pi':
            self.daug1ID = LHCb.ParticleID(-211)
            self.daug2ID = LHCb.ParticleID(-211)
            self.daug3ID = LHCb.ParticleID(211)
        else :
            self.Error( 'h1h2p types ('+h1type+','+h2type+'and'+h3type+') not recognised, exiting.' , FAILURE )


    def check_types( self ) :

        initB = 0
        if self.parentID.isBaryon() :
            initB = 1

        finalB = 0
        for id in ( self.daug1ID, self.daug2ID, self.daug3ID ) :
            if id.isBaryon() :
                if id.pid() > 0 :
                    finalB += 1
                else :
                    finalB -= 1

        if initB != finalB :
            self.Error( 'Initial and final state baryon numbers do not match, '+str(initB)+' != '+str(finalB) )
            return FAILURE

        daug1Mass = self.partpropsvc.find( self.daug1ID ).mass()
        daug2Mass = self.partpropsvc.find( self.daug2ID ).mass()
        daug3Mass = self.partpropsvc.find( self.daug3ID ).mass()
        parentMass = self.partpropsvc.find( self.parentID ).mass()

        if parentMass < ( daug1Mass + daug2Mass + daug3Mass ) :
            return FAILURE

        self.kinematics = ThreeBodyKinematics( daug1Mass, daug2Mass, daug3Mass, parentMass )

        return SUCCESS


    def form_decay_descriptor( self) :

        par_name   = self.partpropsvc.find( self.parentID ).name()
        daug1_name = self.partpropsvc.find( self.daug1ID ).name()
        daug2_name = self.partpropsvc.find( self.daug2ID ).name()
        daug3_name = self.partpropsvc.find( self.daug3ID ).name()

        self.decay_descriptor = '[ '+par_name+' => '+daug1_name+' '+daug2_name+' '+daug3_name+' ]CC'

        self.Info( 'Will use the decay descriptor '+self.decay_descriptor )


    def initialize( self ) :

        sc = AlgoMC.initialize( self )
        if sc.isFailure() :
            return sc

        # get the particle property service
        self.partpropsvc = self.ppSvc()

        # check the validity of the decay
        sc = self.check_types()
        if sc.isFailure() :
            return sc

        # form the decay descriptor
        self.form_decay_descriptor()

        # set up the reconstrucible/reconstructed tools
        if not self.xgen : 
            self.recible = self.tool( cpp.IMCReconstructible, 'MCReconstructible' )
            self.rected  = self.tool( cpp.IMCReconstructed,   'MCReconstructed'   )

        return SUCCESS


    def reco_status_tuple( self, tuple, mcparticle, name ) :
        """
        Store the reconstructible/reconstructed status of an MC particle
        """

        if not mcparticle or self.xgen :
            tuple.column_int( name + '_Reconstructible', -1 )
            tuple.column_int( name + '_Reconstructed',   -1 )
            return

        cat_ible = self.recible.reconstructible( mcparticle )
        cat_ted  = self.rected.reconstructed( mcparticle )

        tuple.column_int( name + '_Reconstructible', cat_ible )
        tuple.column_int( name + '_Reconstructed',   cat_ted  )


    def angvariables( self, XibP4, h1P4, h2P4, pP4 ) :
        """
        Calculate and returns the angular variables related to the decay
        """

        beam = TVector3(0, 0, 1).Unit()

        XibP3 = XibP4.Vect()
        h1P3 = h1P4.Vect()
        h2P3 = h2P4.Vect()
        pP3 = pP4.Vect()
    
        XibRest = XibP4.BoostVector() # for boosting into Lb rest frame
    
        x = XibP3.Unit()
        z = beam.Cross(x).Unit()
        y = z.Cross(x).Unit() # -beam
    
        # Boost into Xib rest frame
    
        pP4.Boost(-XibRest)
        h1P4.Boost(-XibRest)
        h2P4.Boost(-XibRest)
    
        rot = TRotation().RotateAxes(x, y, z)
    
        rot4 = TLorentzRotation(rot)
        rot4.Invert()
    
        XibP4 = rot4 * XibP4
        pP4 = rot4 * pP4
        h1P4 = rot4 * h1P4
        h2P4 = rot4 * h2P4
    
        # 4-vectors are now in the correct reference frame (I hope)
    
        cThetap = pP4.Vect().CosTheta()
        phip = pP4.Vect().Phi()
    
        hhNormal = h1P4.Vect().Cross(h2P4.Vect()).Unit()
        lPolNormal = pP4.Vect().Cross(z).Unit()
    
        # Make it signed
        if lPolNormal.Cross(hhNormal).Z() > 0:
            phihh = hhNormal.Angle(lPolNormal)
        else:
            phihh = -hhNormal.Angle(lPolNormal)
    
        return cThetap, phip, phihh
    
    def mc_p4_tuple( self, tuple, mcparticle, name ) :
        """
        Store the id, charge, 4-momentum, mass, p_t and eta of an MC particle
        """

        if not mcparticle :
            tuple.column_int( name + '_TRUEID',  -1 )
            tuple.column_int( name + '_TRUEQ',   -1 )
            tuple.column_double( name + '_TRUEP',   -1.1 )
            tuple.column_double( name + '_TRUEPE',  -1.1 )
            tuple.column_double( name + '_TRUEPX',  -1.1 )
            tuple.column_double( name + '_TRUEPY',  -1.1 )
            tuple.column_double( name + '_TRUEPZ',  -1.1 )
            tuple.column_double( name + '_TRUEPT',  -1.1 )
            tuple.column_double( name + '_TRUEETA', -1.1 )
            tuple.column_double( name + '_TRUEPHI',   -1.1 )
            tuple.column_double( name + '_TRUETHETA', -1.1 )
            tuple.column_double( name + '_TRUEM',   -1.1 )
            tuple.column_int( name + '_OSCIL',   -1   )
            return

        tuple.column_int( name + '_TRUEID',  int(MCID(mcparticle)         ))
        tuple.column_int( name + '_TRUEQ',   int(MC3Q(mcparticle)/3       ))
        tuple.column_double( name + '_TRUEP',   MCP(mcparticle)          )
        tuple.column_double( name + '_TRUEPE',  MCE(mcparticle)          )
        tuple.column_double( name + '_TRUEPX',  MCPX(mcparticle)         )
        tuple.column_double( name + '_TRUEPY',  MCPY(mcparticle)         )
        tuple.column_double( name + '_TRUEPZ',  MCPZ(mcparticle)         )
        tuple.column_double( name + '_TRUEPT',  MCPT(mcparticle)         )
        tuple.column_double( name + '_TRUEETA', MCETA(mcparticle)        )
        tuple.column_double( name + '_TRUEPHI',   MCPHI(mcparticle)             )
        tuple.column_double( name + '_TRUETHETA', MCTHETA(mcparticle)           )
        tuple.column_double( name + '_TRUEM',   MCM(mcparticle)          )
        tuple.column_int( name + '_OSCIL',   int(MCOSCILLATED(mcparticle) ))

    def mc_vtx_tuple( self, tuple, mcparticle, name ) :
        """
        Store vertex and lifetime info for the MC particle
        """

        if not mcparticle :
            tuple.column_double( name + '_TRUEORIGINVERTEX_X', -1.1 )
            tuple.column_double( name + '_TRUEORIGINVERTEX_Y', -1.1 )
            tuple.column_double( name + '_TRUEORIGINVERTEX_Z', -1.1 )
            tuple.column_double( name + '_TRUECTAU'          , -1.1 )
            return

        tuple.column_double( name + '_TRUEORIGINVERTEX_X', MCVFASPF(MCVX)(mcparticle)   )
        tuple.column_double( name + '_TRUEORIGINVERTEX_Y', MCVFASPF(MCVY)(mcparticle)   )
        tuple.column_double( name + '_TRUEORIGINVERTEX_Z', MCVFASPF(MCVZ)(mcparticle)   )
        tuple.column_double( name + '_TRUECTAU'          , MCCTAU(mcparticle)           )


    def mc_dp_tuple( self, tuple, mcparent ) :
        """
        Store the MC truth DP co-ordinates
        """

        # loop through the daughters and store their 4-momenta treating the
        # expected 3 daughters and PHOTOS photons separately

        daug_id = [ 0, 0, 0 ]
        daug_p4 = [ TLorentzVector(), TLorentzVector(), TLorentzVector() ]
        daug_name = [ 'h1', 'h2', 'p' ]
        gamma_p4 = []

        parID = mcparent.particleID().pid()
        mcparentcharge = mcparent.particleID().threeCharge()

        mcparentp4 = TLorentzVector( MCPX(mcparent), MCPY(mcparent), MCPZ(mcparent), MCE(mcparent) )
        
        for daug in mcparent.children( True ) :

            daugID = daug.particleID()
            daugcharge = daugID.threeCharge()

            px = MCPX( daug )
            py = MCPY( daug )
            pz = MCPZ( daug )
            pe = MCE ( daug )

            p4 = TLorentzVector( px, py, pz, pe )

            if daugID.pid() == 22 :
                gamma_p4.append( p4 )
            else :
                if daugID.abspid() == self.daug3ID.abspid() and daugcharge != mcparentcharge:
                    daug_id[2] = daugID.pid()
                    daug_p4[2] = p4
                elif daugID.abspid() == self.daug1ID.abspid() and daug_id[0] == 0:
                    daug_id[0] = daugID.pid()
                    daug_p4[0] = p4
                else :
                    daug_id[1] = daugID.pid()
                    daug_p4[1] = p4

        ngamma = len(gamma_p4)
        tuple.column_int( 'nPHOTOS', ngamma )

        if 0 != ngamma :

            for gamma in gamma_p4 :
                minangle = 1000.0
                mindaug = -1
                for daug in daug_p4 :
                    angle = gamma.Angle( daug.Vect() )
                    if abs(angle) < minangle :
                        minangle = angle
                        mindaug = daug_p4.index(daug)

                daug_p4[ mindaug ] += gamma

        p12 = daug_p4[0] + daug_p4[1]
        p13 = daug_p4[0] + daug_p4[2]
        p23 = daug_p4[1] + daug_p4[2]

        for i in range(3) :
            tuple.column_double( daug_name[i] + '_CORRPE',  daug_p4[i].E()  )
            tuple.column_double( daug_name[i] + '_CORRPX',  daug_p4[i].Px() )
            tuple.column_double( daug_name[i] + '_CORRPY',  daug_p4[i].Py() )
            tuple.column_double( daug_name[i] + '_CORRPZ',  daug_p4[i].Pz() )

        m12Sq = p12.M2()
        m13Sq = p13.M2()
        m23Sq = p23.M2()

        mPrime = -1.1
        thPrime = -1.1

        cThetap = -1.1
        phip = -1.1
        phihh = -1.1
        
        #get the angular variables
        cThetap, phip, phihh = self.angvariables(mcparentp4, daug_p4[0], daug_p4[1], daug_p4[2])

        tuple.column_double( 'cThetap_MC', cThetap )
        tuple.column_double( 'phip_MC', phip )
        tuple.column_double( 'phihh_MC', phihh )
        
        if self.kinematics.withinDPLimits( m13Sq, m23Sq ) :
            self.kinematics.updateKinematics( m13Sq, m23Sq )
            mPrime = self.kinematics.mPrime
            thPrime = self.kinematics.thPrime

        if m12Sq<0 :     m12Sq = -1.1
        if m12Sq>100e6 : m12Sq = -1.1
        if m13Sq<0 :     m13Sq = -1.1
        if m13Sq>100e6 : m13Sq = -1.1
        if m23Sq<0 :     m23Sq = -1.1
        if m23Sq>100e6 : m23Sq = -1.1

        tuple.column_double( 'm12Sq_MC', m12Sq )
        tuple.column_double( 'm13Sq_MC', m13Sq )
        tuple.column_double( 'm23Sq_MC', m23Sq )

        tuple.column_double( 'mPrime_MC', mPrime )
        tuple.column_double( 'thPrime_MC', thPrime )


    def analyse( self ) :
        """
        The method called in the event loop
        """

        # select MC particles
        cands  = self.mcselect('cands',  self.decay_descriptor)
        ncands = cands.size()

        if 0 == ncands :
            self.Warning( 'No MC candidates found in this event', SUCCESS )
            return SUCCESS

        # get the event header
        if not self.xgen:
            evthdr = self.get( '/Event/Rec/Header' )
        else :
            evthdr = self.get( '/Event/Gen/Header' )
            
        # create the ntuple
        tuple = self.nTuple( 'tupleMCTruth' )

        # loop through the candidates
        for cand in cands :

            # fill event information
            tuple.column_int( 'runNumber', evthdr.runNumber() )
            tuple.column_int( 'evtNumber', evthdr.evtNumber() )
            tuple.column_int( 'nCands',    ncands             )
            # get the ID and hance name of the parent
            candID = cand.particleID().pid()
            candname = self.names[candID] 
            candcharge = cand.particleID().threeCharge()

            # store parent information
            self.mc_p4_tuple( tuple, cand, candname )
            self.mc_vtx_tuple( tuple, cand, candname )
            self.reco_status_tuple( tuple, cand, candname )

            # store DP information
            self.mc_dp_tuple( tuple, cand )

            # loop through the daughters and store their information
            doneh1 = False
            for daug in cand.children( True ) :

                daugID = daug.particleID().pid()
                if 22 == daugID :
                    continue
                daugcharge = daug.particleID().threeCharge()
                if daugcharge != candcharge :
                    daugname = 'p'
                elif not doneh1 and daugcharge == candcharge and daug.particleID().abspid() == self.daug1ID.abspid():
                    daugname = 'h1'
                    doneh1 = True
                else :
                    daugname = 'h2'

                self.mc_p4_tuple( tuple, daug, daugname )
                self.mc_vtx_tuple( tuple, daug, daugname )
                self.reco_status_tuple( tuple, daug, daugname )

            tuple.write()


        return SUCCESS

# End of Xib2pKKMCTruth Class Definition

