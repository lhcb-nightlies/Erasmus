
import os
import re

path = os.getcwd()
pathList = path.split('/')

match1 = re.search( '/Bender_(v\d+r\d+[p(?=\d)]?[(?<=p)\d]*)/', path )
match2 = re.search( 'cmtuser', path )

if not path.endswith('job') or 'Xib2pKK' not in pathList or not match1 or not match2 :
    e = Exception('You do not appear to be in the \'job\' directory of the Xib2pKK package within a Bender project')
    raise e

benderVersion = match1.groups()[0]

modulePath = path.replace('job','options')

userReleaseArea = path[:match2.end()]

mode = 'Xib2KKp'
datatype = '2012'
magtype = 'MagDown'
stripping = 'Stripping20'



params = {}
params = {}
params['btype']     = 'Xib'
params['track1']    = 'K'
params['track2']    = 'K'
params['track3']    = 'p'
params['whichMC']   = datatype
params['stripping'] = stripping
params['dddbtag']   = 'Sim08-20130503-1'
params['conddbtag'] = 'Sim08-20130503-1-vc-md100'

moduleFile = modulePath+'/bender_signalMC_Bu.py'

nsubjobs = 97
datapath = '/data/lhcb/phsdba/Xib2pKK/MC/'
filename = '00034598_00000021_1.allstreams.dst'
ds = LHCbDataset()
#for i in range(97) :
pfn = PhysicalFile( datapath+filename )
ds.files.append( pfn )

b = Bender(version=benderVersion)
b.user_release_area = userReleaseArea
b.module = File(moduleFile)
b.params = params

j=Job()
j.name = mode+'_MC_'+datatype+'_'+magtype+'_'+stripping
j.application = b
j.backend = LSF( queue = 'short' )
j.inputdata = ds

# NB remember to change the tuple name in the Bender script to match this!
tupleFile = mode+'-MC-'+datatype+'-'+magtype+'-'+stripping+'-withMCtruth.root'

# can pick if you want the ntuple returned to you immediately (SandboxFile) or stored on the Grid (DiracFile)
j.outputfiles = [SandboxFile(tupleFile)]
#j.outputfiles = [DiracFile(tupleFile)]

# can tweak the Dirac options if you like
#j.backend.settings['CPUTime'] = 10000 
#j.backend.settings['Destination'] = 'LCG.CERN.ch' 

# can change here the number of files you want to run over per job
j.splitter = SplitByFiles( filesPerJob = 5 ) 

#j.submit()

