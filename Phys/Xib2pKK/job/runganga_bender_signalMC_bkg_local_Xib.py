import os
import re

path = os.getcwd()
pathList = path.split('/')

match1 = re.search( '/Bender_(v\d+r\d+[p(?=\d)]?[(?<=p)\d]*)/', path )
match2 = re.search( 'cmtuser', path )

if not path.endswith('job') or 'Xib2pKK' not in pathList or not match1 or not match2 :
    e = Exception('You do not appear to be in the \'job\' directory of the Xib2pKK package within a Bender project')
    raise e

benderVersion = match1.groups()[0]

modulePath = path.replace('job','options')

userReleaseArea = path[:match2.end()]

datatypes = ['2011','2012']
magtypes = [ 'MagDown', 'MagUp' ]
strippings = [ 'Stripping20r1', 'Stripping20' ]
eventtypes = []
eventtypes.append('Xib2Delta+KK_KKp_ppi0_phsp_DecProdCut')
eventtypes.append('Xib2K*(892)-Kp_KKp_Kpi0_phsp_DecProdCut')
#eventtypes.append('Omegab2Delta+KK_KKp_ppi0_phsp_DecProdCut')
#eventtypes.append('Omegab2K*(892)-Kp_KKp_Kpi0_phsp_DecProdCut')

knownMagTypes = [ 'MagDown', 'MagUp' ]

known2011StrippingVersions = [ 'Stripping20r1' ]
known2012StrippingVersions = [ 'Stripping20' ]

nativeStrippingVersion = {}
nativeStrippingVersion['2011'] = 'Stripping20r1'
nativeStrippingVersion['2012'] = 'Stripping20'

filenames = {} 
filenames['Xib2DeltaKK_MC_2011_MagDown_Stripping20r1_Pythia8'] = [
                                                                    '00046501_00000011_2.AllStreams.dst',
                                                                    '00046501_00000018_2.AllStreams.dst',
                                                                    '00046501_00000030_2.AllStreams.dst',
                                                                    '00046501_00000024_2.AllStreams.dst',
                                                                    '00046501_00000005_2.AllStreams.dst'
                                                                ]
filenames['Xib2DeltaKK_MC_2011_MagUp_Stripping20r1_Pythia8'] = [
                                                                    '00046495_00000009_2.AllStreams.dst',
                                                                    '00046495_00000006_2.AllStreams.dst',
                                                                    '00046495_00000023_2.AllStreams.dst',
                                                                    '00046495_00000008_2.AllStreams.dst',
                                                                    '00046495_00000012_2.AllStreams.dst',
                                                                    '00046495_00000017_2.AllStreams.dst',
                                                                    '00046495_00000010_2.AllStreams.dst',
                                                                    '00046495_00000019_2.AllStreams.dst'
                                                                ]
filenames['Xib2DeltaKK_MC_2012_MagDown_Stripping20_Pythia8'] = [
                                                                    '00046523_00000021_2.AllStreams.dst',
                                                                    '00046523_00000034_2.AllStreams.dst',
                                                                    '00046523_00000014_2.AllStreams.dst',
                                                                    '00046523_00000026_2.AllStreams.dst',
                                                                    '00046523_00000029_2.AllStreams.dst',
                                                                    '00046523_00000023_2.AllStreams.dst',
                                                                    '00046523_00000033_2.AllStreams.dst',
                                                                    '00046523_00000011_2.AllStreams.dst',
                                                                    '00046523_00000044_2.AllStreams.dst',
                                                                    '00046523_00000032_2.AllStreams.dst',
                                                                    '00046523_00000027_2.AllStreams.dst'
                                                                ]
filenames['Xib2DeltaKK_MC_2012_MagUp_Stripping20_Pythia8'] = [
                                                                    '00046513_00000006_2.AllStreams.dst',
                                                                    '00046513_00000034_2.AllStreams.dst',
                                                                    '00046513_00000011_2.AllStreams.dst',
                                                                    '00046513_00000022_2.AllStreams.dst',
                                                                    '00046513_00000020_2.AllStreams.dst',
                                                                    '00046513_00000019_2.AllStreams.dst',
                                                                    '00046513_00000042_2.AllStreams.dst',
                                                                    '00046513_00000025_2.AllStreams.dst',
                                                                    '00046513_00000015_2.AllStreams.dst',
                                                                    '00046513_00000039_2.AllStreams.dst'
                                                                ]
filenames['Xib2KstKp_MC_2011_MagDown_Stripping20r1_Pythia8'] = [
                                                                '00046505_00000002_2.AllStreams.dst',
                                                                '00046505_00000029_2.AllStreams.dst'
                                                                ]
filenames['Xib2KstKp_MC_2011_MagUp_Stripping20r1_Pythia8'] = [
                                                                '00046493_00000010_2.AllStreams.dst',
                                                                '00046493_00000014_2.AllStreams.dst',
                                                                '00046493_00000001_2.AllStreams.dst',
                                                                '00046493_00000003_2.AllStreams.dst',
                                                                '00046493_00000016_2.AllStreams.dst',
                                                                '00046493_00000015_2.AllStreams.dst'
                                                                ]
filenames['Xib2KstKp_MC_2012_MagDown_Stripping20_Pythia8'] = [
                                                                '00046525_00000041_2.AllStreams.dst',
                                                                '00046525_00000001_2.AllStreams.dst',
                                                                '00046525_00000028_2.AllStreams.dst',
                                                                '00046525_00000013_2.AllStreams.dst',
                                                                '00046525_00000026_2.AllStreams.dst',
                                                                '00046525_00000027_2.AllStreams.dst',
                                                                '00046525_00000034_2.AllStreams.dst',
                                                                '00046525_00000046_2.AllStreams.dst',
                                                                '00046525_00000042_2.AllStreams.dst',
                                                                '00046525_00000047_2.AllStreams.dst',
                                                                '00046525_00000002_2.AllStreams.dst',
                                                                '00046525_00000039_2.AllStreams.dst',
                                                                '00046525_00000024_2.AllStreams.dst',
                                                                '00046525_00000031_2.AllStreams.dst',
                                                                '00046525_00000016_2.AllStreams.dst',
                                                                '00046525_00000015_2.AllStreams.dst'
                                                                ]
filenames['Xib2KstKp_MC_2012_MagUp_Stripping20_Pythia8'] = [
                                                                '00046515_00000035_2.AllStreams.dst',
                                                                '00046515_00000006_2.AllStreams.dst',
                                                                '00046515_00000021_2.AllStreams.dst',
                                                                '00046515_00000005_2.AllStreams.dst',
                                                                '00046515_00000040_2.AllStreams.dst',
                                                                '00046515_00000017_2.AllStreams.dst',
                                                                '00046515_00000009_2.AllStreams.dst',
                                                                '00046515_00000031_2.AllStreams.dst',
                                                                '00046515_00000028_2.AllStreams.dst',
                                                                '00046515_00000027_2.AllStreams.dst',
                                                                '00046515_00000029_2.AllStreams.dst',
                                                                '00046515_00000024_2.AllStreams.dst'
                                                                ]

for eventtype in eventtypes :

    print 'Creating job(s) for event type: ' + eventtype

    mode = eventtype.split('_')[0]
    btype = mode.split('2')[0]
    restracks = re.findall( 'Delta\+|K\*\(892\)\-|rho\(770\)\-|N\(1520\)\+|K(?!S)|pi|p(?!i)', mode )
    resname = restracks[0]
    tracks = re.findall( 'Delta\+|K\*\(892\)\-|rho\(770\)\-|N\(1520\)\+|K(?!S)|pi|p(?!i)', eventtype.split('_')[1] )
    track1 = tracks[0]
    track2 = tracks[1]
    track3 = tracks[2]

    if 'K*(892)-' in mode:
        mode = mode.replace("K*(892)-", "Kst")
    elif 'rho(770)-' in mode:
        mode = mode.replace("rho(770)-", "rho")
    elif 'Delta+' in mode:
        mode = mode.replace("Delta+", "Delta")
    else:
        mode = mode.replace("N(1520)+", "N")
        
    for stripping in strippings :

        print 'With stripping: ' + stripping

        datatype = ''
        if stripping in known2011StrippingVersions:
            datatype = '2011'
        elif stripping in known2012StrippingVersions:
            datatype = '2012'
        else :
            e = Exception('Unsupported Stripping version: ' + stripping)
            raise e

        for magtype in magtypes :

            print 'With magnet setting: ' + magtype

            if magtype not in knownMagTypes :
                e = Exception('Unsupported magnet setting: ' + magtype)
                raise e

            for pythiaversion in [ 8 ] :

                print 'With Pythia version: %d' % pythiaversion

                params = {}

                params['btype']     = 'Xib'
                params['track1']    = track1
                params['track2']    = track2
                params['track3']    = track3
                params['resname']   = resname
                params['mode']   = mode
                params['whichMC']   = datatype
                params['stripping'] = stripping
                params['dddbtag']   = 'Sim08-20130503-1'
                if magtype=='MagUp':
                    params['conddbtag'] = 'Sim08-20130503-1-vc-mu100'
                else:
                    params['conddbtag'] = 'Sim08-20130503-1-vc-md100'

                moduleFile = modulePath+'/bender_signalMC_bkg.py'
                
                datapath = '/data/lhcb/phrjgz/PartialReco_Xib/'+mode+'_MC_'+datatype+'_'+magtype+'_'+stripping+'_Pythia'+str(pythiaversion)+'/'
                ds = LHCbDataset()
                files = filenames[mode+'_MC_'+datatype+'_'+magtype+'_'+stripping+'_Pythia'+str(pythiaversion)]
                #for i in range(97) :
                for file in files:
                    pfn = PhysicalFile( datapath+file)
                    ds.files.append( pfn )

                b = Bender(version=benderVersion)
                b.user_release_area = userReleaseArea
                b.module = File(moduleFile)
                b.params = params
                
                j=Job()
                j.name = mode+'_MC_'+datatype+'_'+magtype+'_'+stripping+'_Pythia'+str(pythiaversion)
                j.application = b
                j.backend = LSF( queue = 'medium' )
                j.inputdata = ds
                
                # NB remember to change the tuple name in the Bender script to match this!
                tupleFile = mode+'-MC-'+datatype+'-'+magtype+'-'+stripping+'-withMCtruth.root'
                
                # can pick if you want the ntuple returned to you immediately (SandboxFile) or stored on the Grid (DiracFile)
                j.outputfiles = [LocalFile(tupleFile)]
                #j.outputfiles = [DiracFile(tupleFile)]
                
                # can tweak the Dirac options if you like
                #j.backend.settings['CPUTime'] = 10000 
                #j.backend.settings['Destination'] = 'LCG.CERN.ch' 
                
                # can change here the number of files you want to run over per job
                j.splitter = SplitByFiles( filesPerJob = 5 ) 
                
                #j.submit()
                
