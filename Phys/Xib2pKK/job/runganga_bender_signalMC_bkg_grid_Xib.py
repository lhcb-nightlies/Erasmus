eventTypeDict = {
  #  'Xib2KKp-sqDalitz-DecProdCut'                   : 16103030
  #, 'Xib2Kpip-sqDalitz-DecProdCut'                  : 16103031
  #, 'Xib2pipip-sqDalitz-DecProdCut'                 : 16103032
  #, 'Omegab2KKp-sqDalitz-DecProdCut'                : 16103050
  #, 'Omegab2Kpip-sqDalitz-DecProdCut'               : 16103051
  #, 'Omegab2pipip-sqDalitz-DecProdCut'              : 16103033
  #, 'Xib2Delta+KK_KKp_ppi0_phsp_DecProdCut'         : 16103430
  #  'Xib2K*(892)-Kp_KKp_Kpi0_phsp_DecProdCut'       : [16103431,(8,),('Sim08h',)]
  #, 'Omegab2Delta+KK_KKp_ppi0_phsp_DecProdCut'      : 16103432
  #, 'Omegab2K*(892)-Kp_KKp_Kpi0_phsp_DecProdCut'    : 16103433

  #For KKp
    'Xib2Delta+KK_KKp_ppi0_phsp_DecProdCut'          : [16303430,(8,),('Sim08h',)]
  , 'Xib2K*(892)-Kp_KKp_Kpi0_phsp_DecProdCut'        : [16303431,(8,),('Sim08h',)]
  , 'Xib2N(1520)+KK_KKp_ppi0_phsp_DecProdCut'        : [16303432,(8,),('Sim08h',)]
  , 'Omegab2Delta+KK_KKp_ppi0_phsp_DecProdCut'       : [16303434,(8,),('Sim08h',)]
  , 'Omegab2K*(892)-Kp_KKp_Kpi0_phsp_DecProdCut'     : [16303435,(8,),('Sim08h',)]
  , 'Omegab2N(1520)+KK_KKp_ppi0_phsp_DecProdCut'     : [16303436,(8,),('Sim08h',)]
  #For Kpip
  , 'Xib2Delta+Kpi_Kpip_ppi0_phsp_DecProdCut'        : [16303430,(8,),('Sim08h',)]
  , 'Xib2K*(892)-pip_Kpip_Kpi0_phsp_DecProdCut'      : [16303431,(8,),('Sim08h',)]
  , 'Xib2N(1520)+Kpi_Kpip_ppi0_phsp_DecProdCut'      : [16303432,(8,),('Sim08h',)]
  , 'Xib2rho(770)-Kp_Kpip_pipi0_phsp_DecProdCut'     : [16303433,(8,),('Sim08h',)]
  , 'Omegab2Delta+Kpi_Kpip_ppi0_phsp_DecProdCut'     : [16303434,(8,),('Sim08h',)]
  , 'Omegab2K*(892)-pip_Kpip_Kpi0_phsp_DecProdCut'   : [16303435,(8,),('Sim08h',)]
  , 'Omegab2N(1520)+Kpi_Kpip_ppi0_phsp_DecProdCut'   : [16303436,(8,),('Sim08h',)]
  , 'Omegab2rho(770)-Kp_Kpip_pipi0_phsp_DecProdCut'  : [16303437,(8,),('Sim08h',)]
  #For pipip
  , 'Xib2Delta+pipi_pipip_ppi0_phsp_DecProdCut'      : [16303430,(8,),('Sim08h',)]
  , 'Xib2N(1520)+pipi_pipip_ppi0_phsp_DecProdCut'    : [16303432,(8,),('Sim08h',)]
  , 'Xib2rho(770)-pip_pipip_pipi0_phsp_DecProdCut'   : [16303433,(8,),('Sim08h',)]
  , 'Omegab2Delta+pipi_pipip_ppi0_phsp_DecProdCut'   : [16303434,(8,),('Sim08h',)]
  , 'Omegab2N(1520)+pipi_pipip_ppi0_phsp_DecProdCut' : [16303436,(8,),('Sim08h',)]
  , 'Omegab2rho(770)-pip_pipip_pipi0_phsp_DecProdCut': [16303437,(8,),('Sim08h',)]
}
# *************************************************************************************** #
# pick magnet polarities, stripping versions and an event type from the known types above #
# *************************************************************************************** #
magtypes = [ 'MagDown', 'MagUp' ]
strippings = [ 'Stripping20r1', 'Stripping20' ]
eventtypes = []
#For KKp
eventtypes.append('Xib2Delta+KK_KKp_ppi0_phsp_DecProdCut')
eventtypes.append('Xib2K*(892)-Kp_KKp_Kpi0_phsp_DecProdCut')
eventtypes.append('Xib2N(1520)+KK_KKp_ppi0_phsp_DecProdCut')
eventtypes.append('Omegab2Delta+KK_KKp_ppi0_phsp_DecProdCut')
eventtypes.append('Omegab2K*(892)-Kp_KKp_Kpi0_phsp_DecProdCut')
eventtypes.append('Omegab2N(1520)+KK_KKp_ppi0_phsp_DecProdCut')
#For Kpip
eventtypes.append('Xib2rho(770)-Kp_Kpip_pipi0_phsp_DecProdCut')
eventtypes.append('Omegab2rho(770)-Kp_Kpip_pipi0_phsp_DecProdCut')
eventtypes.append('Xib2K*(892)-pip_Kpip_Kpi0_phsp_DecProdCut')
eventtypes.append('Xib2Delta+Kpi_Kpip_ppi0_phsp_DecProdCut')
eventtypes.append('Xib2N(1520)+Kpi_Kpip_ppi0_phsp_DecProdCut')
eventtypes.append('Omegab2K*(892)-pip_Kpip_Kpi0_phsp_DecProdCut')
eventtypes.append('Omegab2Delta+Kpi_Kpip_ppi0_phsp_DecProdCut')
eventtypes.append('Omegab2N(1520)+Kpi_Kpip_ppi0_phsp_DecProdCut')
#For pipip
eventtypes.append('Xib2Delta+pipi_pipip_ppi0_phsp_DecProdCut')
eventtypes.append('Xib2N(1520)+pipi_pipip_ppi0_phsp_DecProdCut')
eventtypes.append('Xib2rho(770)-pip_pipip_pipi0_phsp_DecProdCut')
eventtypes.append('Omegab2Delta+pipi_pipip_ppi0_phsp_DecProdCut')
eventtypes.append('Omegab2N(1520)+pipi_pipip_ppi0_phsp_DecProdCut')
eventtypes.append('Omegab2rho(770)-pip_pipip_pipi0_phsp_DecProdCut')
#eventtypes = eventTypeDict.keys()
# *************************************************************************************** #

import os
import re

knownMagTypes = [ 'MagDown', 'MagUp' ]

known2011StrippingVersions = [ 'Stripping20r1' ]
known2012StrippingVersions = [ 'Stripping20' ]

nativeStrippingVersion = {}
nativeStrippingVersion['2011'] = 'Stripping20r1'
nativeStrippingVersion['2012'] = 'Stripping20'

beamEnergy = {}
beamEnergy['2011'] = '3500'
beamEnergy['2012'] = '4000'

nuValue = {}
nuValue['2011'] = '2'
nuValue['2012'] = '2.5'

tck = {}
tck['2011'] = '0x40760037'
tck['2012'] = '0x409f0045'

path = os.getcwd()
pathList = path.split('/')

match1 = re.search( '/Bender_(v\d+r\d+[p(?=\d)]?[(?<=p)\d]*)/', path )
match2 = re.search( 'cmtuser', path )

if not path.endswith('job') or 'Xib2pKK' not in pathList or not match1 or not match2 :
    e = Exception('You do not appear to be in the \'job\' directory of the Xib2pKK package within a Bender project')
    raise e

benderVersion = match1.groups()[0]

modulePath = path.replace('job','options')

userReleaseArea = path[:match2.end()]

for eventtype in eventtypes :

    print 'Creating job(s) for event type: ' + eventtype

    if eventtype not in eventTypeDict.keys() :
        e = Exception('Unknown event type: ' + eventtype )
        raise e

    evtID = eventTypeDict[ eventtype ][0]
    pythiaversions = eventTypeDict[ eventtype ][1]
    simversions = eventTypeDict[ eventtype ][2]

    bkinfo = getBKInfo( evtID )
    bkpaths = bkinfo.keys()
    #print bkpaths

    #mode = eventtype.split('-')[0]
    mode = eventtype.split('_')[0]
    btype = mode.split('2')[0]
    restracks = re.findall( 'Delta\+|K\*\(892\)\-|rho\(770\)\-|N\(1520\)\+', mode )
    resname = restracks[0]
    tracks = re.findall( 'Delta\+|K\*\(892\)\-|rho\(770\)\-|N\(1520\)\+|K(?!S)|pi|p(?!i)', eventtype.split('_')[1] )
    track1 = tracks[0]
    track2 = tracks[1]
    track3 = tracks[2]

    if 'K*(892)-' in mode:
        mode = mode.replace("K*(892)-", "Kst")
    elif 'rho(770)-' in mode:
        mode = mode.replace("rho(770)-", "rho")
    elif 'Delta+' in mode:
        mode = mode.replace("Delta+", "Delta")
    else:
        mode = mode.replace("N(1520)+", "N")
        
    for stripping in strippings :

        print 'With stripping: ' + stripping

        datatype = ''
        if stripping in known2011StrippingVersions:
            datatype = '2011'
        elif stripping in known2012StrippingVersions:
            datatype = '2012'
        else :
            e = Exception('Unsupported Stripping version: ' + stripping)
            raise e

        restrip = False
        if stripping != nativeStrippingVersion[datatype] :
            restrip = True

        for magtype in magtypes :

            print 'With magnet setting: ' + magtype

            if magtype not in knownMagTypes :
                e = Exception('Unsupported magnet setting: ' + magtype)
                raise e

            for pythiaversion in pythiaversions:

                print 'With Pythia version: %d' % pythiaversion

                datasets = []
                dddbtags = set()
                condtags = set()
                for simversion in simversions:
                    bkpath = '/MC/%s/Beam%sGeV-%s-%s-Nu%s-Pythia%d/%s/Digi13/Trig%s/Reco14c/%sNoPrescalingFlagged/%d/ALLSTREAMS.DST' % ( datatype, beamEnergy[datatype], datatype, magtype, nuValue[datatype], pythiaversion, simversion, tck[datatype], nativeStrippingVersion[datatype], evtID )
                    #print bkpath
                    if bkpath not in bkpaths :
                        continue
                    print 'Trying BK path: ' + bkpath,
                    bkq = BKQuery( type='Path', dqflag='OK', path=bkpath )
                    dstmp = bkq.getDataset()
                    if len(dstmp) != 0 :
                        print ' - found dataset'
                        datasets.append( dstmp )
                        dddbtags.add( bkinfo[bkpath][0] )
                        condtags.add( bkinfo[bkpath][1] )
                    else :
                        print ' - nothing found'

                    if len( datasets ) == 0 :
                        print 'Could not find any valid data!!  Skipping this configuration!!'
                        continue
                    elif len(datasets) == 1 :
                        ds = datasets[0]
                    elif len(datasets) == 2 :
                        ds = datasets[0].union( datasets[1] )
                    else :
                        e = Exception('Found more than two datasets!')
                        raise e

                    if len( ds.files ) == 0 :
                        e = Exception('Zero files in this dataset!')
                        raise e

                    if len( dddbtags ) != 1 or len( condtags ) != 1 :
                        e = Exception('Found multiple DB tags')
                        raise e

                    # uncomment this if you want to run a quick test on the CERN batch
                    #reduced_ds = LHCbDataset()
                    #for file in ds.files :
                        #sites = file.getReplicas().keys()
                        #for site in sites :
                            #if 'CERN' in site :
                                #reduced_ds.files.append( file )
                                #break
                        #if len(reduced_ds.files) > 0 :
                            #break

                    print "btype: {0}, track1: {1}, track2: {2}, track3: {3}, resname: {4}, mode: {5}".format(btype, track1, track2, track3, resname, mode)
                    params = {}

                    params['btype']     = 'Xib'
                    params['track1']    = track1
                    params['track2']    = track2
                    params['track3']    = track3
                    params['resname']   = resname
                    params['mode']   = mode
                    params['whichMC']   = datatype
                    params['stripping'] = stripping
                    params['dddbtag']   = dddbtags.pop()
                    params['conddbtag'] = condtags.pop()

                    moduleFile = modulePath+'/bender_signalMC_bkg_Xib.py'
                    
                    #if restrip :
                        #moduleFile = modulePath+'/bender_stripping_signalMC.py'

                    b = Bender(version=benderVersion)
                    b.user_release_area = userReleaseArea
                    b.module = File(moduleFile)
                    b.params = params

                    j=Job()
                    j.name = mode+'_MC_'+datatype+'_'+magtype+'_'+stripping+'_Pythia'+str(pythiaversion)
                    j.application = b
                    j.backend = Dirac()
                    j.inputdata = ds

                    # uncomment this if you want to run a quick test on the CERN batch
                    #j.backend = LSF( queue = '8nh' ) 
                    #j.inputdata = reduced_ds

                    # NB remember to change the tuple name in the Bender script to match this!
                    tupleFile = mode+'-MC-'+datatype+'-'+magtype+'-'+stripping+'-withMCtruth.root'

                    # can pick if you want the ntuple returned to you immediately (SandboxFile) or stored on the Grid (DiracFile)
                    #j.outputfiles = [LocalFile(tupleFile)]
                    j.outputfiles = [DiracFile(tupleFile)]

                    # can tweak the Dirac options if you like
                    #j.backend.settings['CPUTime'] = 10000 
                    #j.backend.settings['Destination'] = 'LCG.CERN.ch' 

                    # can change here the number of files you want to run over per job
                    j.splitter = SplitByFiles( filesPerJob = 5 ) 

                    #j.submit()
