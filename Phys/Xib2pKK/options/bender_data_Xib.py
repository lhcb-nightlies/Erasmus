#!/usr/bin/env python

"""
Bender module to run the following sequence over Bu2hhh stripped data samples:
- run an algorithm to store the reco info for all Stripped candidates
"""

from Bender.Main import *

def configure( datafiles, catalogues = [], params = {}, castor = False ) :

    #======= B->KShh Configuration and Setup =========#
    whichStripping = params.get( 'stripping', 'Stripping20' )
    magtype        = params.get( 'magtype', 'MagDown' )
    #=================================================#

    #reco_daughters = [ 'K', 'K', 'K' ]

    knownMagTypes = [ 'MagDown', 'MagUp' ]

    if magtype not in knownMagTypes :
        e = Exception('Unsupported magnet setting: ' + magtype)
        raise e

    known2011StrippingVersions = ['Stripping20r1']
    known2012StrippingVersions = ['Stripping20']

    whichData = ''
    if whichStripping in known2011StrippingVersions :
        whichData = '2011'
    elif whichStripping in known2012StrippingVersions :
        whichData = '2012'
    else :
        e = Exception('Unsupported Stripping version: ' + whichStripping)
        raise e


    # Configuration of DaVinci
    from Configurables import DaVinci

    daVinci = DaVinci()
    daVinci.DataType        = whichData
    daVinci.Simulation      = False
    daVinci.Lumi            = True
    daVinci.InputType       = "MDST"
    daVinci.EvtMax          = -1
    daVinci.PrintFreq       = 1000
    daVinci.TupleFile       = 'Xib2hhp-Collision'+whichData[2:]+'-'+magtype+'-'+whichStripping+'.root'
    ######daVinci.IgnoreDQFlags   = True #for data file flagged as bad
    daVinci.IgnoreDQFlags   = True #for data file flagged as bad

    from Configurables import CondDB
    CondDB().LatestGlobalTagByDataType = whichData

    stream = 'Bhadron'
    teslocation = '/Event/'+stream
    daVinci.RootInTES = teslocation

    inputLocation = 'Phys/B2hhh_KKK_inclLine/Particles'
    #inputLocation = 'Phys/B2hhh_pph_inclLine/Particles'

    from PhysConf.Filters import LoKi_Filters
    filterStrip = "HLT_PASS_RE('StrippingB2hhh_KKK_inclLineDecision')"
    filterVoid = "EXISTS('/Event/Strip/Phys/DecReports')"
    filterAlg = LoKi_Filters(VOID_Code = filterVoid, STRIP_Code = filterStrip)
    #filterAlg = LoKi_Filters(STRIP_Code = filterStrip)
    filters = filterAlg.filters('Filters')
    filters.reverse()
    daVinci.EventPreFilters = filters

    alg_name = 'Xib2hhpReco'
    daVinci.UserAlgorithms += [ alg_name ]

    setData( datafiles, catalogues, castor )

    from Xib2pKK.RecoAlgo import Xib2pKKReco

    gaudi = appMgr()

    algXib2pKK = Xib2pKKReco(
            alg_name,
            #reco_daughters,
            #extended_hypos,
            False, False,
            RootInTES = teslocation,
            Inputs = [ inputLocation ]
            )

    #userSeq = gaudi.algorithm('GaudiSequencer/DaVinciUserSequence' , True )
    #userSeq.Members += [ algXib2pKK.name() ]

    return SUCCESS

#############

if '__main__' == __name__ :

    datafiles = [
            '/data/lhcb/phrjgz/test_mdstfile/Xib2pKK/data/00020350_00007564_1.bhadron.mdst'
    ]

    pars = {}
    pars['stripping' ] = 'Stripping20'
    pars['magtype' ]   = 'MagDown'

    configure( datafiles, params = pars, castor=False )

    run(-1)

#############

