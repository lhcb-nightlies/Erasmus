#!/usr/bin/env python

"""
Bender module to run the following sequence over B2KShh signal MC samples:
- run an algorithm to store the MC truth DP position (and other info) for all generated events
- run an algorithm to store the reco and MC truth-matched info for all Stripped candidates
"""

from Bender.Main import *

def configure( datafiles, catalogues = [], params = {}, castor = False ) :

    #======= B->KShh Configuration and Setup =========#
    btype  = params.get( 'btype',  'Bu' )
    track1 = params.get( 'track1', 'pi'  )
    track2 = params.get( 'track2', 'pi'  )
    track3 = params.get( 'track3', 'pi' )
    whichMC = params.get( 'whichMC', '2012' )
    whichStripping = params.get( 'stripping', 'Stripping20' )
    isXGen = params.get( 'isXGen', False )
    printFreq = params.get( 'printFreq', 1000 )
    dddbtag = params.get( 'dddbtag', '' )
    conddbtag = params.get( 'conddbtag', '' )
    #extended_hypos = params.get( 'extended_hypos', False )
    #=================================================#

    mode = btype+'2'+track1+track2+track3
    mc_daughters = [ track1, track2, track3 ]
    print 'MODE: ', mode
    # decay descriptors can be provided if running over resonant signal MC, e.g. B0 -> K*+ pi-; K*+ -> KS0 pi+
    mc_decay_descriptors = []

    reco_daughters = [ 'K', 'K', 'K' ]

    knownMCTypes = [ '2011', '2012' ]

    if whichMC not in knownMCTypes :
        e = Exception('Unsupported MC version')
        raise e

    nativeStrippingVersion = {}
    nativeStrippingVersion['2011'] = 'Stripping21r1'
    nativeStrippingVersion['2012'] = 'Stripping21'

    if whichStripping != nativeStrippingVersion[ whichMC ] :
        e = Exception('Requested stripping version %s is not the native version for this MC %s, you need to use the version of the script that will first re-strip the MC.' % (whichStripping, whichMC) )
        raise e

    inputLocation = '/Event/B2HHH.StripTrigger/Phys/B2hhh_KKK_inclLine/Particles'


    # Configuration of RelatedInfo tools - needed when running over MC filtered with S21
    from Configurables import AddRelatedInfo, RelInfoConeVariables, RelInfoConeVariablesForEW

    coneangles = [ 0.5, 1.0, 1.5, 2.0 ]

    for idx, angle in enumerate( coneangles ) :
        angle_name = '%.1f' % angle
        angle_name = angle_name.replace('.','')

        alg_name = 'RelatedInfo%d_Bu2hhh' % (idx+1)
        tool_name = 'RelInfoConeVariables%d_Bu2hhh' % (idx+1)

        relinfo = AddRelatedInfo( alg_name )
        relinfo.Inputs = [ inputLocation ]
        relinfo.addTool( RelInfoConeVariables, tool_name )
        cv = getattr( relinfo, tool_name )
        cv.ConeAngle = angle
        cv.Variables = ['CONEANGLE', 'CONEMULT', 'CONEPASYM', 'CONEPTASYM']
        relinfo.Tool = 'RelInfoConeVariables/'+tool_name
        relinfo.Location = 'ConeIso%sB' % angle_name
        relinfo.DaughterLocations = {
                "[B+ -> ^K+ K+ K-]CC" : "ConeIso%sh1" % angle_name,
                "[B+ -> K+ ^K+ K-]CC" : "ConeIso%sh2" % angle_name,
                "[B+ -> K+ K+ ^K-]CC" : "ConeIso%sh3" % angle_name,
                }

        ew_alg_name = 'RelatedInfo%d_Bu2hhh' % (len(coneangles)+idx+1)
        ew_tool_name = 'RelInfoConeVariablesForEW%d_Bu2hhh' % (idx+1)

        ew_relinfo = AddRelatedInfo( ew_alg_name )
        ew_relinfo.Inputs = [ inputLocation ]
        ew_relinfo.addTool( RelInfoConeVariablesForEW, ew_tool_name )
        ew_cv = getattr( ew_relinfo, ew_tool_name )
        ew_cv.ConeAngle = angle
        ew_cv.Variables = ['EWCONEANGLE','EWCONENPX','EWCONENPY','EWCONENPZ','EWCONENMULT', 'EWCONENVPT', 'EWCONENSPT', 'EWCONENVP', 'EWCONENSP']
        ew_relinfo.Tool = 'RelInfoConeVariablesForEW/'+ew_tool_name
        ew_relinfo.Location = 'NConeIso%sB' % angle_name
        ew_relinfo.DaughterLocations = {
                "[B+ -> ^K+ K+ K-]CC" : "NConeIso%sh1" % angle_name,
                "[B+ -> K+ ^K+ K-]CC" : "NConeIso%sh2" % angle_name,
                "[B+ -> K+ K+ ^K-]CC" : "NConeIso%sh3" % angle_name,
                }


    # for debugging - try and pring the contents of the TES
    #from Configurables import StoreExplorerAlg
    #storeExp = StoreExplorerAlg("PrintTES")
    #storeExp.Load = 1
    #storeExp.PrintFreq = 1.0
    #storeExp.OutputLevel = 1


    # Configuration of DaVinci
    from Configurables import DaVinci

    daVinci = DaVinci()
    daVinci.DataType        = whichMC
    daVinci.Simulation      = True
    daVinci.Lumi            = False
    daVinci.InputType       = "DST"
    daVinci.EvtMax          = -1
    daVinci.PrintFreq       = printFreq

    ## try to get the tags from Rec/Header
    if dddbtag != '' and conddbtag != '' :
        daVinci.DDDBtag = dddbtag
        daVinci.CondDBtag = conddbtag
    else :
        from BenderTools.GetDBtags import getDBTags
        tags = getDBTags ( datafiles[0] , castor  )

        logger.info ( 'Extract tags from DATA : %s' % tags )
        if tags.has_key ( 'DDDB' ) and tags ['DDDB'] :
            daVinci.DDDBtag   = tags['DDDB'  ]
            logger.info ( 'Set DDDB    %s ' % daVinci.DDDBtag   )
        if tags.has_key ( 'CONDDB' ) and tags ['CONDDB'] :
            daVinci.CondDBtag = tags['CONDDB']
            logger.info ( 'Set CONDDB  %s ' % daVinci.CondDBtag )
        if tags.has_key ( 'SIMCOND' ) and tags ['SIMCOND'] :
            daVinci.CondDBtag = tags['SIMCOND']
            logger.info ( 'Set SIMCOND %s ' % daVinci.CondDBtag )

    magtype = "MagUp"
    if "md" in daVinci.CondDBtag :
        magtype = "MagDown"

    daVinci.TupleFile = mode+'-MC-'+whichMC+'-'+magtype+'-'+whichStripping+'-withMCtruth.root'

    from Gaudi.Configuration import FileCatalog
    from GaudiConf import IOHelper
    IOHelper('ROOT').inputFiles([], clear = True )
    FileCatalog().Catalogs = []

    setData( datafiles, catalogues )

    gaudi = appMgr()

    from Xib2pKK.MCTruthAlgo import Xib2pKKMCTruth
    from Xib2pKK.RecoAlgo import Xib2pKKReco

    algGenMCTruth = Xib2pKKMCTruth( mode, btype, track1, track2, track3, isXGen )

    algXib2pKK = Xib2pKKReco(
            'Bu2hhhReco',
#            reco_daughters,
            #extended_hypos,
            True, True, sameSign = False,
            mc_daughters = mc_daughters,
            mc_decay_descriptors = mc_decay_descriptors,
            isbbsack = False,
            strippingVersion = '21',
            PP2MCs = [ 'Relations/Rec/ProtoP/Charged'] ,
            Inputs = [ inputLocation ],
            )

    userSeq = gaudi.algorithm('GaudiSequencer/DaVinciUserSequence' , True )
    for i in range( 2*len(coneangles) ) :
        userSeq.Members += [ "AddRelatedInfo/RelatedInfo%d_Bu2hhh" % (i+1) ]
    #userSeq.Members += [ "StoreExplorerAlg/PrintTES" ]
    userSeq.Members += [ algGenMCTruth.name() ]
    userSeq.Members += [ algXib2pKK.name() ]
    
    return SUCCESS

#############

if '__main__' == __name__ :

#    datafiles = [
#            '/data/lhcb/phrjgz/test_mdstfile/Bu2pipipi/MC/00034832_00000027_1.allstreams.dst'
#    ]

    import os

#    datafiles = map(lambda x : '/data/lhcb/phrnas/B3pi/dsts/filtered/' + x,  os.listdir('/data/lhcb/phrnas/B3pi/dsts/filtered'))

    #datafiles = ['/data/lhcb/phrnas/B3pi/dsts/filtered/00045916_00000059_1.b2hhh.striptrigger.dst']
    datafiles = ['/data/lhcb/phsdba/Bu2hhh-testDST/00045954_00000057_1.b2hhh.striptrigger.dst']

    pars = {}
    pars[ 'btype' ]     = 'Bu'
    pars[ 'track1' ]    = 'K'
    pars[ 'track2' ]    = 'K'
    pars[ 'track3' ]    = 'K'
    pars[ 'whichMC' ]   = '2012'
    pars[ 'stripping' ] = 'Stripping21'
    pars[ 'dddbtag' ]   = 'dddb-20130929-1'
    pars[ 'conddbtag' ] = 'sim-20130522-1-vc-md100'

    configure( datafiles, params = pars, castor=False )

    run(10)

#############

