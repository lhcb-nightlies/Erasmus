#!/usr/bin/env python

"""
Bender module to run the following sequence over B2KShh signal MC samples:
- run an algorithm to store the MC truth DP position (and other info) for all generated events
- run an algorithm to store the reco and MC truth-matched info for all Stripped candidates
"""

from Bender.Main import *

def configure( datafiles, catalogues = [], params = {}, castor = False ) :

    #======= B->KShh Configuration and Setup =========#
    btype  = params.get( 'btype',  'Bs' )
    track1 = params.get( 'track1', 'K'  )
    track2 = params.get( 'track2', 'pi'  )
    track3 = params.get( 'track3', 'K' )
    resname = params.get( 'resname', 'K*(892)~0' )
    resname2 = params.get( 'resname2', 'K*(892)0' )
    mode = params.get( 'mode', 'Bs2KstKst' )
    whichMC = params.get( 'whichMC', '2012' )
    whichStripping = params.get( 'stripping', 'Stripping20' )
    isXGen = params.get( 'isXGen', False )
    printFreq = params.get( 'printFreq', 1000 )
    dddbtag = params.get( 'dddbtag', '' )
    conddbtag = params.get( 'conddbtag', '' )
    #extended_hypos = params.get( 'extended_hypos', False )
    #=================================================#

    #mode = btype+'2'+track1+track2+track3
    mc_daughters = [ track1, track2, track3 ]
    daug1name = track1+'-'
    daug2name = track2+'-'
    daug3name = track3+'+'

    # decay descriptors can be provided if running over resonant signal MC, e.g. B0 -> K*+ pi-; K*+ -> KS0 pi+
    mc_decay_descriptors = []

    if (btype == 'Bs' or btype == 'Bd') and (resname == 'K*(892)~0' or resname == 'K*(892)0' or resname == 'f_0(980)') and (resname2 == 'phi(1020)' or resname2 == 'K*(892)0'):
        mc_decay_descriptors.append('[ ( Xb & X0 ) => ( %s =>  %s pi+ ) ( %s =>  %s  %s ) ]CC'  % (resname, daug1name, resname2, daug2name, daug3name))
        mc_decay_descriptors.append('[ ( Xb & X0 ) => ( %s => ^%s pi+ ) ( %s =>  %s  %s ) ]CC'  % (resname, daug1name, resname2, daug2name, daug3name))
        mc_decay_descriptors.append('[ ( Xb & X0 ) => ( %s =>  %s pi+ ) ( %s => ^%s  %s ) ]CC'  % (resname, daug1name, resname2, daug2name, daug3name))
        mc_decay_descriptors.append('[ ( Xb & X0 ) => ( %s =>  %s pi+ ) ( %s =>  %s ^%s ) ]CC'  % (resname, daug1name, resname2, daug2name, daug3name))
    else:
        if resname == 'K*(892)-' and resname2 == 'nores':
            mc_decay_descriptors.append('[ ( Xb & X- ) => ( %s =>  %s pi0 )  %s  %s ]CC'  % (resname, daug1name, daug2name, daug3name))
            mc_decay_descriptors.append('[ ( Xb & X- ) => ( %s => ^%s pi0 )  %s  %s ]CC'  % (resname, daug1name, daug2name, daug3name))
            mc_decay_descriptors.append('[ ( Xb & X- ) => ( %s =>  %s pi0 ) ^%s  %s ]CC'  % (resname, daug1name, daug2name, daug3name))
            mc_decay_descriptors.append('[ ( Xb & X- ) => ( %s =>  %s pi0 )  %s ^%s ]CC'  % (resname, daug1name, daug2name, daug3name))
        elif resname == 'nores' and resname2 == 'nores' and (btype == 'Xib0' or btype == 'Lb'):
            mc_decay_descriptors.append('[ ( Xb & X0 ) =>  %s  %s  %s pi+ ]CC'  % (daug1name, daug2name, daug3name))
            mc_decay_descriptors.append('[ ( Xb & X0 ) => ^%s  %s  %s pi+ ]CC'  % (daug1name, daug2name, daug3name))
            mc_decay_descriptors.append('[ ( Xb & X0 ) =>  %s ^%s  %s pi+ ]CC'  % (daug1name, daug2name, daug3name))
            mc_decay_descriptors.append('[ ( Xb & X0 ) =>  %s  %s ^%s pi+ ]CC'  % (daug1name, daug2name, daug3name))
        elif resname == 'K*(892)-' and resname2 == 'phi(1020)':
            mc_decay_descriptors.append('[ ( Xb & X- ) => ( %s =>  %s pi0 ) ( %s =>  %s  %s ) ]CC'  % (resname, daug1name, resname2, daug2name, daug3name))
            mc_decay_descriptors.append('[ ( Xb & X- ) => ( %s => ^%s pi0 ) ( %s =>  %s  %s ) ]CC'  % (resname, daug1name, resname2, daug2name, daug3name))
            mc_decay_descriptors.append('[ ( Xb & X- ) => ( %s =>  %s pi0 ) ( %s => ^%s  %s ) ]CC'  % (resname, daug1name, resname2, daug2name, daug3name))
            mc_decay_descriptors.append('[ ( Xb & X- ) => ( %s =>  %s pi0 ) ( %s =>  %s ^%s ) ]CC'  % (resname, daug1name, resname2, daug2name, daug3name))
        #else if resname == 'D0' or resname == 'D~0':
        #    mc_decay_descriptors.append('[ ( Xb & X- ) => ( %s =>  %s pi+ pi0 )  %s ]CC'  % (resname, daug1name, daug2name))
        #    mc_decay_descriptors.append('[ ( Xb & X- ) => ( %s => ^%s pi+ pi0 )  %s ]CC'  % (resname, daug1name, daug2name))
        #    mc_decay_descriptors.append('[ ( Xb & X- ) => ( %s =>  %s pi+ pi0 ) ^%s ]CC'  % (resname, daug1name, daug2name))
        #    mc_decay_descriptors.append('[ ( Xb & X- ) => ( %s =>  %s pi+ pi0 )  %s ]CC'  % (resname, daug1name, daug2name))

    #reco_daughters = [ 'K', 'K', 'K' ]

    knownMCTypes = [ '2011', '2012' ]

    if whichMC not in knownMCTypes :
        e = Exception('Unsupported MC version')
        raise e

    nativeStrippingVersion = {}
    nativeStrippingVersion['2011'] = 'Stripping20r1'
    nativeStrippingVersion['2012'] = 'Stripping20'

    if whichStripping != nativeStrippingVersion[ whichMC ] :
        e = Exception('Requested stripping version %s is not the native version for this MC %s, you need to use the version of the script that will first re-strip the MC.' % (whichStripping, whichMC) )
        raise e

    inputLocation = '/Event/AllStreams/Phys/B2hhh_KKK_inclLine/Particles'
    #inputLocation = '/Event/AllStreams/Phys/B2hhh_pph_inclLine/Particles'

    #################################
    from Configurables import AddExtraInfo, ConeVariables
    #for pt asym
    extra = AddExtraInfo('BhadronExtraInfo')
    extra.Inputs = [ '/Event/AllStreams/Phys/B2hhh_KKK_inclLine/Particles' ]
    cv1   = ConeVariables('BhadronExtraInfo.ConeVariables1', ConeAngle = 1.5, ConeNumber = 1,
                           Variables = ['angle', 'mult', 'ptasy'] )
    cv2   = ConeVariables('BhadronExtraInfo.ConeVariables2', ConeAngle = 1.7, ConeNumber = 2,
                           Variables = ['angle', 'mult', 'ptasy'] )
    cv3   = ConeVariables('BhadronExtraInfo.ConeVariables3', ConeAngle = 1.0, ConeNumber = 3,
                           Variables = ['angle', 'mult', 'ptasy'] )
    
    extra.addTool( cv1 , 'BhadronExtraInfo.ConeVariables1')
    extra.addTool( cv2 , 'BhadronExtraInfo.ConeVariables2')
    extra.addTool( cv3 , 'BhadronExtraInfo.ConeVariables3')

    extra.Tools = [ 'ConeVariables/BhadronExtraInfo.ConeVariables1',
                            'ConeVariables/BhadronExtraInfo.ConeVariables2',
                            'ConeVariables/BhadronExtraInfo.ConeVariables3',
                          ]
    #################################

    # Configuration of DaVinci
    from Configurables import DaVinci

    daVinci = DaVinci()
    daVinci.DataType        = whichMC
    daVinci.Simulation      = True
    daVinci.Lumi            = False
    daVinci.InputType       = "DST"
    daVinci.EvtMax          = -1
    daVinci.PrintFreq       = printFreq

    ## try to get the tags from Rec/Header
    if dddbtag != '' and conddbtag != '' :
        daVinci.DDDBtag = dddbtag
        daVinci.CondDBtag = conddbtag
    else :
        from BenderTools.GetDBtags import getDBTags
        tags = getDBTags ( datafiles[0] , castor  )

        logger.info ( 'Extract tags from DATA : %s' % tags )
        if tags.has_key ( 'DDDB' ) and tags ['DDDB'] :
            daVinci.DDDBtag   = tags['DDDB'  ]
            logger.info ( 'Set DDDB    %s ' % daVinci.DDDBtag   )
        if tags.has_key ( 'CONDDB' ) and tags ['CONDDB'] :
            daVinci.CondDBtag = tags['CONDDB']
            logger.info ( 'Set CONDDB  %s ' % daVinci.CondDBtag )
        if tags.has_key ( 'SIMCOND' ) and tags ['SIMCOND'] :
            daVinci.CondDBtag = tags['SIMCOND']
            logger.info ( 'Set SIMCOND %s ' % daVinci.CondDBtag )

    magtype = "MagUp"
    if "md" in daVinci.CondDBtag :
        magtype = "MagDown"

    daVinci.TupleFile = mode+'-MC-'+whichMC+'-'+magtype+'-'+whichStripping+'-withMCtruth.root'

    from Gaudi.Configuration import FileCatalog
    from GaudiConf import IOHelper
    IOHelper('ROOT').inputFiles([], clear = True )
    FileCatalog().Catalogs = []

    setData( datafiles, catalogues )

    gaudi = appMgr()

    #from Xib2pKK.MCTruthAlgo import Xib2pKKMCTruth
    from Xib2pKK.RecoAlgo import Xib2pKKReco

    #algGenMCTruth = Xib2pKKMCTruth( mode, btype, track1, track2, track3, isXGen )
    #if resname == 'K*(892)-' or resname == 'rho(770)-':
    #    algGenMCTruthResonance = Xib2hhpMCTruthResonance( mode, btype, daug1name, daug3name, resname, daug2name, 'pi0', isXGen )
    #else:
    #    algGenMCTruthResonance = Xib2hhpMCTruthResonance( mode, btype, daug1name, daug2name, resname, daug3name, 'pi0', isXGen )

    algXib2pKK = Xib2pKKReco(
            'Bu2hhhReco',
            #reco_daughters,
            #extended_hypos,
            True, True,
            mc_daughters,
            mc_decay_descriptors,
            True,
            PP2MCs = [ 'Relations/Rec/ProtoP/Charged'] ,
            Inputs = [ inputLocation ]
            )

    userSeq = gaudi.algorithm('GaudiSequencer/DaVinciUserSequence' , True )
    #userSeq.Members += [ algGenMCTruth.name() ]
    #userSeq.Members += [ algGenMCTruthResonance.name() ]
    userSeq.Members += [ "AddExtraInfo/BhadronExtraInfo" ]
    userSeq.Members += [ algXib2pKK.name() ]

    return SUCCESS
#############

if '__main__' == __name__ :

    datafiles = [
            #Bs2KstKst
            '/data/lhcb/phrjgz/test_mdstfile/Bu2KKK/MC/00034922_00000001_1.allstreams.dst'
            #Bs2KstPhi
            #'/data/lhcb/phrjgz/test_mdstfile/Bu2KKK/MC/00041453_00000026_2.AllStreams.dst'
            #Bu2KstPhi
            #'/data/lhcb/phrjgz/test_mdstfile/Bu2KKK/MC/00034914_00000001_1.allstreams.dst'
            #Bu2KstKK
            #'/data/lhcb/phrjgz/test_mdstfile/Bu2KKK/MC/00026127_00000015_1.allstreams.dst'
    ]

    pars = {}
    pars[ 'btype' ]     = 'Bs'
    pars[ 'track1' ]    = 'K'
    pars[ 'track2' ]    = 'pi'
    pars[ 'track3' ]    = 'K'
    pars[ 'resname']    = 'K*(892)~0'
    pars[ 'resname2']   = 'K*(892)0'
    pars[ 'mode']       = 'Bs2KstKst'
    pars[ 'whichMC' ]   = '2011'
    pars[ 'stripping' ] = 'Stripping20r1'
    pars[ 'dddbtag' ]   = 'Sim08-20130503-1'
    pars[ 'conddbtag' ] = 'Sim08-20130503-1-vc-md100'

    configure( datafiles, params = pars, castor=False )

    run(-1)

#############
