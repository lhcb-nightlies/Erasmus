#!/usr/bin/env python

"""
Bender module to run the following sequence over Bu2hhh stripped data samples:
- run an algorithm to store the reco info for all Stripped candidates
"""

from Bender.Main import *

def configure( datafiles, catalogues = [], params = {}, castor = False ) :

    #======= B->KShh Configuration and Setup =========#
    whichStripping = params.get( 'stripping', 'Stripping20' )
    magtype        = params.get( 'magtype', 'MagDown' )
    #=================================================#

    #reco_daughters = [ 'K', 'K', 'K' ]

    knownMagTypes = [ 'MagDown', 'MagUp' ]

    if magtype not in knownMagTypes :
        e = Exception('Unsupported magnet setting: ' + magtype)
        raise e

    known2011StrippingVersions = [ 'Stripping21r1', 'Stripping20r1', 'Stripping21r1p1a' ]
    known2012StrippingVersions = [ 'Stripping21', 'Stripping20', 'Stripping21r0p1a' ]

    whichData = ''
    if whichStripping in known2011StrippingVersions :
        whichData = '2011'
    elif whichStripping in known2012StrippingVersions :
        whichData = '2012'
    else :
        e = Exception('Unsupported Stripping version: ' + whichStripping)
        raise e


    # Configuration of DaVinci
    from Configurables import DaVinci

    daVinci = DaVinci()
    daVinci.DataType        = whichData
    daVinci.Simulation      = False
    daVinci.Lumi            = True
    daVinci.InputType       = "MDST"
    daVinci.EvtMax          = 1000
    daVinci.PrintFreq       = 1000
    daVinci.TupleFile       = 'Bu2hhh-Collision'+whichData[2:]+'-'+magtype+'-'+whichStripping + '.root'

    #######daVinci.IgnoreDQFlags   = True #for data file flagged as bad

    from Configurables import CondDB
    CondDB().LatestGlobalTagByDataType = whichData

    stream = 'Bhadron'
    teslocation = '/Event/'+stream
    daVinci.RootInTES = teslocation

    from Configurables import TrackScaleState as SCALER
    scaler = SCALER( 'Scaler' )

    daVinci.UserAlgorithms = [ scaler ]

    inputLocation = 'Phys/Bu2hhh_KKK_inclLine/Particles'
    #inputLocation = 'Phys/B2hhh_pph_inclLine/Particles'

    from Configurables import LHCbApp

    #write out a summary file called summary.xml
    LHCbApp().XMLSummary="summary.xml"

    from PhysConf.Filters import LoKi_Filters
    filterStrip = "HLT_PASS_RE('StrippingBu2hhh_KKK_inclLineDecision')"
    filterVoid = "EXISTS('/Event/Strip/Phys/DecReports')"
    filterAlg = LoKi_Filters(VOID_Code = filterVoid, STRIP_Code = filterStrip)
    #filterAlg = LoKi_Filters(STRIP_Code = filterStrip)
    filters = filterAlg.filters('Filters')
    filters.reverse()
    daVinci.EventPreFilters = filters

    alg_name = 'Bu2hhhReco'
    daVinci.UserAlgorithms += [ alg_name ]

    from Gaudi.Configuration import FileCatalog
    from GaudiConf import IOHelper
    IOHelper('ROOT').inputFiles([], clear = True )
    FileCatalog().Catalogs = []

    setData( datafiles, catalogues, castor )

    from Xib2pKK.RecoAlgo import Xib2pKKReco

    gaudi = appMgr()

    algXib2pKK = Xib2pKKReco(
            alg_name,
            #reco_daughters,
            #extended_hypos,
            False, False, strippingVersion = '21', sameSign = False,
            RootInTES = teslocation,
            Inputs = [ inputLocation ]
            )

    userSeq = gaudi.algorithm('GaudiSequencer/DaVinciUserSequence' , True )
    userSeq.Members += [ algXib2pKK.name() ]

    return SUCCESS

#############                                                                                                                                                                                                   

if '__main__' == __name__ :

    import os
    import sys

#    datafiles = map(lambda x : '/data/lhcb/phrnas/B3pi/dsts/' + x,  os.listdir('/data/lhcb/phrnas/B3pi/dsts'))[int(sys.argv[1])]                                                                               

#    datafiles = ['/data/lhcb/phrnas/B3pi/dsts/00041838_00000589_1.bhadron.mdst']

#    datafiles = ['/data/lhcb/phulfy/test_s21/00050733_00018986_1.bhadron.mdst']

    datafiles = ['/data/lhcb/phrnas/00050877_00019016_1.bhadron.mdst']

    pars = {}
    pars[ 'btype' ]     = 'Bu'
    pars[ 'track1' ]    = 'pi'
    pars[ 'track2' ]    = 'pi'
    pars[ 'track3' ]    = 'pi'
    pars[ 'whichMC' ]   = '2011'
    pars[ 'stripping' ] = 'Stripping21r1'
    pars[ 'dddbtag' ]   = 'Sim08-20130503-1'
    pars[ 'conddbtag' ] = 'Sim08-20130503-1-vc-md100'
    pars['magtype' ]   = 'MagUp'
    configure( datafiles, params = pars, castor=False )

    run(1000)

#############       
