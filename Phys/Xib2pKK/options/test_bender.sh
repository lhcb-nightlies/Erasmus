#!/bin/bash

bsub -q short -oo log/Bu2KpiK.log ". SetupProject.sh Bender v25r7p4;python bender_signalMC_Bu2KpiK.py"
bsub -q short -oo log/Bs2KstKst.log ". SetupProject.sh Bender v25r7p4;python bender_signalMC_bkg_Bu.py"

bsub -q short -oo log/Omegab2pipip.log ". SetupProject.sh Bender v25r7p4;python bender_signalMC_Omegab2pipip.py"
bsub -q short -oo log/Omegab2Kpip.log ". SetupProject.sh Bender v25r7p4;python bender_signalMC_Omegab2Kpip.py"
bsub -q short -oo log/Xib2rhoKp.log ". SetupProject.sh Bender v25r7p4;python bender_signalMC_bkg_Xib.py"
