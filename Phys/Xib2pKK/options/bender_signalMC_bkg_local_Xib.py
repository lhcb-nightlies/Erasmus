#!/usr/bin/env python

import sys

"""
Bender module to run the following sequence over B2KShh signal MC samples:
- run an algorithm to store the MC truth DP position (and other info) for all generated events
- run an algorithm to store the reco and MC truth-matched info for all Stripped candidates
"""

from Bender.Main import *

def configure( datafiles, catalogues = [], params = {}, castor = False ) :

    #======= B->KShh Configuration and Setup =========#
    btype  = params.get( 'btype',  'Xib' )
    track1 = params.get( 'track1', 'K'  )
    track2 = params.get( 'track2', 'K'  )
    track3 = params.get( 'track3', 'p' )
    resname = params.get( 'resname', 'K*(892)-' )
    mode = params.get( 'mode', 'Xib2KstKp' )
    whichMC = params.get( 'whichMC', '2012' )
    whichStripping = params.get( 'stripping', 'Stripping20' )
    isXGen = params.get( 'isXGen', False )
    printFreq = params.get( 'printFreq', 1000 )
    dddbtag = params.get( 'dddbtag', '' )
    conddbtag = params.get( 'conddbtag', '' )
    #extended_hypos = params.get( 'extended_hypos', False )
    #=================================================#

    #mode = btype+'2'+track1+track2+track3
    mc_daughters = [ track1, track2, track3 ]
    daug1name = track1+'-'
    daug2name = track2+'-'
    daug3name = track3+'+'

    # decay descriptors can be provided if running over resonant signal MC, e.g. B0 -> K*+ pi-; K*+ -> KS0 pi+
    mc_decay_descriptors = []

    if resname == 'K*(892)-' or resname == 'rho(770)-':
        mc_decay_descriptors.append('[ ( Xb & X- ) =>  %s ( %s => pi0  %s )  %s ]CC'  % (daug1name, resname, daug2name, daug3name))
        mc_decay_descriptors.append('[ ( Xb & X- ) => ^%s ( %s => pi0  %s )  %s ]CC'  % (daug1name, resname, daug2name, daug3name))
        mc_decay_descriptors.append('[ ( Xb & X- ) =>  %s ( %s => pi0 ^%s )  %s ]CC'  % (daug1name, resname, daug2name, daug3name))
        mc_decay_descriptors.append('[ ( Xb & X- ) =>  %s ( %s => pi0  %s ) ^%s ]CC'  % (daug1name, resname, daug2name, daug3name))
    else:
        mc_decay_descriptors.append('[ ( Xb & X- ) =>  %s  %s ( %s => pi0  %s ) ]CC'  % (daug1name, daug2name, resname, daug3name))
        mc_decay_descriptors.append('[ ( Xb & X- ) => ^%s  %s ( %s => pi0  %s ) ]CC'  % (daug1name, daug2name, resname, daug3name))
        mc_decay_descriptors.append('[ ( Xb & X- ) =>  %s ^%s ( %s => pi0  %s ) ]CC'  % (daug1name, daug2name, resname, daug3name))
        mc_decay_descriptors.append('[ ( Xb & X- ) =>  %s  %s ( %s => pi0 ^%s ) ]CC'  % (daug1name, daug2name, resname, daug3name))

    #reco_daughters = [ 'K', 'K', 'K' ]

    knownMCTypes = [ '2011', '2012' ]

    if whichMC not in knownMCTypes :
        e = Exception('Unsupported MC version')
        raise e

    nativeStrippingVersion = {}
    nativeStrippingVersion['2011'] = 'Stripping20r1'
    nativeStrippingVersion['2012'] = 'Stripping20'

    if whichStripping != nativeStrippingVersion[ whichMC ] :
        e = Exception('Requested stripping version %s is not the native version for this MC %s, you need to use the version of the script that will first re-strip the MC.' % (whichStripping, whichMC) )
        raise e

    inputLocation = '/Event/AllStreams/Phys/B2hhh_KKK_inclLine/Particles'
    #inputLocation = '/Event/AllStreams/Phys/B2hhh_pph_inclLine/Particles'


    # Configuration of DaVinci
    from Configurables import DaVinci

    daVinci = DaVinci()
    daVinci.DataType        = whichMC
    daVinci.Simulation      = True
    daVinci.Lumi            = False
    daVinci.InputType       = "DST"
    daVinci.EvtMax          = -1
    daVinci.PrintFreq       = printFreq

    ## try to get the tags from Rec/Header
    if dddbtag != '' and conddbtag != '' :
        daVinci.DDDBtag = dddbtag
        daVinci.CondDBtag = conddbtag
    else :
        from BenderTools.GetDBtags import getDBTags
        tags = getDBTags ( datafiles[0] , castor  )

        logger.info ( 'Extract tags from DATA : %s' % tags )
        if tags.has_key ( 'DDDB' ) and tags ['DDDB'] :
            daVinci.DDDBtag   = tags['DDDB'  ]
            logger.info ( 'Set DDDB    %s ' % daVinci.DDDBtag   )
        if tags.has_key ( 'CONDDB' ) and tags ['CONDDB'] :
            daVinci.CondDBtag = tags['CONDDB']
            logger.info ( 'Set CONDDB  %s ' % daVinci.CondDBtag )
        if tags.has_key ( 'SIMCOND' ) and tags ['SIMCOND'] :
            daVinci.CondDBtag = tags['SIMCOND']
            logger.info ( 'Set SIMCOND %s ' % daVinci.CondDBtag )

    magtype = "MagUp"
    if "md" in daVinci.CondDBtag :
        magtype = "MagDown"

    daVinci.TupleFile = mode+'-MC-'+whichMC+'-'+magtype+'-'+whichStripping+'-withMCtruth.root'

    setData( datafiles, catalogues )

    gaudi = appMgr()

    #from Xib2pKK.MCTruthAlgo import Xib2pKKMCTruth
    from Xib2pKK.MCTruthAlgoResonance import Xib2hhpMCTruthResonance
    from Xib2pKK.RecoAlgo import Xib2pKKReco

    #algGenMCTruth = Xib2pKKMCTruth( mode, btype, track1, track2, track3, isXGen )
    if resname == 'K*(892)-' or resname == 'rho(770)-':
        algGenMCTruthResonance = Xib2hhpMCTruthResonance( mode, btype, daug1name, daug3name, resname, daug2name, 'pi0', isXGen )
    else:
        algGenMCTruthResonance = Xib2hhpMCTruthResonance( mode, btype, daug1name, daug2name, resname, daug3name, 'pi0', isXGen )

    algXib2pKK = Xib2pKKReco(
            'Xib2KKpReco',
            #reco_daughters,
            #extended_hypos,
            True, True,
            mc_daughters,
            mc_decay_descriptors,
            PP2MCs = [ 'Relations/Rec/ProtoP/Charged'] ,
            Inputs = [ inputLocation ]
            )

    userSeq = gaudi.algorithm('GaudiSequencer/DaVinciUserSequence' , True )
    #userSeq.Members += [ algGenMCTruth.name() ]
    userSeq.Members += [ algGenMCTruthResonance.name() ]
    userSeq.Members += [ algXib2pKK.name() ]

    return SUCCESS

#############

if '__main__' == __name__ :

    if (len(sys.argv) < 4):
        print("Usage: python bender_signalMC_bkg_local.py <mode> <magtype> <stripping>.")
        sys.exit(2)

    knownMagTypes = [ 'MagDown', 'MagUp' ]
    known2011StrippingVersions = [ 'Stripping20r1' ]
    known2012StrippingVersions = [ 'Stripping20' ]

    Mode = sys.argv[1]
    magtype = sys.argv[2]
    stripping = sys.argv[3]
    pythiaversion = 8

    eventtype = ""
    if Mode == "Xib2KstKp":
        eventtype = "Xib2K*(892)-Kp_KKp_Kpi0_phsp_DecProdCut" 
    elif Mode == "Xib2DeltaKK":
        eventtype = "Xib2Delta+KK_KKp_ppi0_phsp_DecProdCut"
    elif Mode == "Omegab2KstKp":
        eventtype = "Omegab2K*(892)-Kp_KKp_Kpi0_phsp_DecProdCut"
    elif Mode == "Omegab2DeltaKK":
        eventtype = "Omegab2Delta+KK_KKp_ppi0_phsp_DecProdCut"

    filenames = {} 
    filenames['Xib2DeltaKK_MC_2011_MagDown_Stripping20r1_Pythia8'] = [
                                                                        '00046501_00000011_2.AllStreams.dst',
                                                                        '00046501_00000018_2.AllStreams.dst',
                                                                        '00046501_00000030_2.AllStreams.dst',
                                                                        '00046501_00000024_2.AllStreams.dst',
                                                                        '00046501_00000005_2.AllStreams.dst'
                                                                    ]
    filenames['Xib2DeltaKK_MC_2011_MagUp_Stripping20r1_Pythia8'] = [
                                                                        '00046495_00000009_2.AllStreams.dst',
                                                                        '00046495_00000006_2.AllStreams.dst',
                                                                        '00046495_00000023_2.AllStreams.dst',
                                                                        '00046495_00000008_2.AllStreams.dst',
                                                                        '00046495_00000012_2.AllStreams.dst',
                                                                        '00046495_00000017_2.AllStreams.dst',
                                                                        '00046495_00000010_2.AllStreams.dst',
                                                                        '00046495_00000019_2.AllStreams.dst'
                                                                    ]
    filenames['Xib2DeltaKK_MC_2012_MagDown_Stripping20_Pythia8'] = [
                                                                        '00046523_00000021_2.AllStreams.dst',
                                                                        '00046523_00000034_2.AllStreams.dst',
                                                                        '00046523_00000014_2.AllStreams.dst',
                                                                        '00046523_00000026_2.AllStreams.dst',
                                                                        '00046523_00000029_2.AllStreams.dst',
                                                                        '00046523_00000023_2.AllStreams.dst',
                                                                        '00046523_00000033_2.AllStreams.dst',
                                                                        '00046523_00000011_2.AllStreams.dst',
                                                                        '00046523_00000044_2.AllStreams.dst',
                                                                        '00046523_00000032_2.AllStreams.dst',
                                                                        '00046523_00000027_2.AllStreams.dst',
                                                                        #not included as subjobs(4) completed successfully
                                                                        #'00046523_00000016_2.AllStreams.dst',
                                                                        #'00046523_00000048_2.AllStreams.dst'
                                                                    ]
    filenames['Xib2DeltaKK_MC_2012_MagUp_Stripping20_Pythia8'] = [
                                                                        '00046513_00000006_2.AllStreams.dst',
                                                                        '00046513_00000034_2.AllStreams.dst',
                                                                        '00046513_00000011_2.AllStreams.dst',
                                                                        '00046513_00000022_2.AllStreams.dst',
                                                                        '00046513_00000020_2.AllStreams.dst',
                                                                        '00046513_00000019_2.AllStreams.dst',
                                                                        '00046513_00000042_2.AllStreams.dst',
                                                                        '00046513_00000025_2.AllStreams.dst',
                                                                        '00046513_00000015_2.AllStreams.dst',
                                                                        '00046513_00000039_2.AllStreams.dst'
                                                                    ]
    filenames['Xib2KstKp_MC_2011_MagDown_Stripping20r1_Pythia8'] = [
                                                                    '00046505_00000002_2.AllStreams.dst',
                                                                    '00046505_00000029_2.AllStreams.dst'
                                                                    ]
    filenames['Xib2KstKp_MC_2011_MagUp_Stripping20r1_Pythia8'] = [
                                                                    '00046493_00000010_2.AllStreams.dst',
                                                                    '00046493_00000014_2.AllStreams.dst',
                                                                    '00046493_00000001_2.AllStreams.dst',
                                                                    '00046493_00000003_2.AllStreams.dst',
                                                                    '00046493_00000016_2.AllStreams.dst',
                                                                    '00046493_00000015_2.AllStreams.dst'
                                                                    ]
    filenames['Xib2KstKp_MC_2012_MagDown_Stripping20_Pythia8'] = [
                                                                    '00046525_00000041_2.AllStreams.dst',
                                                                    '00046525_00000001_2.AllStreams.dst',
                                                                    '00046525_00000028_2.AllStreams.dst',
                                                                    '00046525_00000013_2.AllStreams.dst',
                                                                    '00046525_00000026_2.AllStreams.dst',
                                                                    '00046525_00000027_2.AllStreams.dst',
                                                                    '00046525_00000034_2.AllStreams.dst',
                                                                    '00046525_00000046_2.AllStreams.dst',
                                                                    '00046525_00000042_2.AllStreams.dst',
                                                                    '00046525_00000047_2.AllStreams.dst',
                                                                    '00046525_00000002_2.AllStreams.dst',
                                                                    '00046525_00000039_2.AllStreams.dst',
                                                                    '00046525_00000024_2.AllStreams.dst',
                                                                    '00046525_00000031_2.AllStreams.dst',
                                                                    '00046525_00000016_2.AllStreams.dst',
                                                                    '00046525_00000015_2.AllStreams.dst'
                                                                    ]
    filenames['Xib2KstKp_MC_2012_MagUp_Stripping20_Pythia8'] = [
                                                                    '00046515_00000035_2.AllStreams.dst',
                                                                    '00046515_00000006_2.AllStreams.dst',
                                                                    '00046515_00000021_2.AllStreams.dst',
                                                                    '00046515_00000005_2.AllStreams.dst',
                                                                    '00046515_00000040_2.AllStreams.dst',
                                                                    '00046515_00000017_2.AllStreams.dst',
                                                                    '00046515_00000009_2.AllStreams.dst',
                                                                    '00046515_00000031_2.AllStreams.dst',
                                                                    '00046515_00000028_2.AllStreams.dst',
                                                                    '00046515_00000027_2.AllStreams.dst',
                                                                    '00046515_00000029_2.AllStreams.dst',
                                                                    '00046515_00000024_2.AllStreams.dst'
                                                                    ]
    filenames['Omegab2DeltaKK_MC_2011_MagDown_Stripping20r1_Pythia8'] = [
                                                                        '00046507_00000022_2.AllStreams.dst',
                                                                        '00046507_00000004_2.AllStreams.dst',
                                                                        '00046507_00000018_2.AllStreams.dst',
                                                                        '00046507_00000001_2.AllStreams.dst',
                                                                        '00046507_00000016_2.AllStreams.dst'
                                                                    ]
    filenames['Omegab2DeltaKK_MC_2011_MagUp_Stripping20r1_Pythia8'] = [
                                                                        '00046499_00000025_2.AllStreams.dst',
                                                                        '00046499_00000027_2.AllStreams.dst',
                                                                        '00046499_00000018_2.AllStreams.dst',
                                                                        '00046499_00000032_2.AllStreams.dst',
                                                                        '00046499_00000010_2.AllStreams.dst',
                                                                        '00046499_00000002_2.AllStreams.dst',
                                                                        '00046499_00000015_2.AllStreams.dst',
                                                                        '00046499_00000005_2.AllStreams.dst',
                                                                        '00046499_00000017_2.AllStreams.dst',
                                                                        '00046499_00000021_2.AllStreams.dst'
                                                                    ]
    filenames['Omegab2DeltaKK_MC_2012_MagDown_Stripping20_Pythia8'] = [
                                                                        '00046527_00000003_2.AllStreams.dst',
                                                                        '00046527_00000007_2.AllStreams.dst',
                                                                        '00046527_00000018_2.AllStreams.dst',
                                                                        '00046527_00000004_2.AllStreams.dst',
                                                                        '00046527_00000048_2.AllStreams.dst',
                                                                        '00046527_00000034_2.AllStreams.dst',
                                                                        '00046527_00000037_2.AllStreams.dst',
                                                                        '00046527_00000029_2.AllStreams.dst',
                                                                        '00046527_00000016_2.AllStreams.dst',
                                                                        '00046527_00000032_2.AllStreams.dst',
                                                                        '00046527_00000025_2.AllStreams.dst',
                                                                        '00046527_00000039_2.AllStreams.dst',
                                                                        '00046527_00000011_2.AllStreams.dst'
                                                                    ]
    filenames['Omegab2DeltaKK_MC_2012_MagUp_Stripping20_Pythia8'] = [
                                                                        '00046517_00000004_2.AllStreams.dst',
                                                                        '00046517_00000013_2.AllStreams.dst',
                                                                        '00046517_00000043_2.AllStreams.dst',
                                                                        '00046517_00000026_2.AllStreams.dst',
                                                                        '00046517_00000031_2.AllStreams.dst',
                                                                        '00046517_00000049_2.AllStreams.dst',
                                                                        '00046517_00000047_2.AllStreams.dst',
                                                                        '00046517_00000027_2.AllStreams.dst',
                                                                        '00046517_00000040_2.AllStreams.dst',
                                                                        '00046517_00000006_2.AllStreams.dst',
                                                                        '00046517_00000029_2.AllStreams.dst',
                                                                        '00046517_00000016_2.AllStreams.dst',
                                                                        '00046517_00000009_2.AllStreams.dst',
                                                                        '00046517_00000035_2.AllStreams.dst',
                                                                        '00046517_00000022_2.AllStreams.dst',
                                                                        '00046517_00000037_2.AllStreams.dst',
                                                                        '00046517_00000023_2.AllStreams.dst'
                                                                    ]
    filenames['Omegab2KstKp_MC_2011_MagDown_Stripping20r1_Pythia8'] = [
                                                                    '00046503_00000029_2.AllStreams.dst',
                                                                    '00046503_00000020_2.AllStreams.dst',
                                                                    '00046503_00000013_2.AllStreams.dst',
                                                                    '00046503_00000032_2.AllStreams.dst',
                                                                    '00046503_00000024_2.AllStreams.dst',
                                                                    '00046503_00000023_2.AllStreams.dst',
                                                                    '00046503_00000003_2.AllStreams.dst',
                                                                    '00046503_00000021_2.AllStreams.dst',
                                                                    '00046503_00000009_2.AllStreams.dst',
                                                                    '00046503_00000002_2.AllStreams.dst'
                                                                    ]
    filenames['Omegab2KstKp_MC_2011_MagUp_Stripping20r1_Pythia8'] = [
                                                                    '00046497_00000007_2.AllStreams.dst',
                                                                    '00046497_00000006_2.AllStreams.dst',
                                                                    '00046497_00000031_2.AllStreams.dst',
                                                                    '00046497_00000002_2.AllStreams.dst',
                                                                    '00046497_00000023_2.AllStreams.dst',
                                                                    '00046497_00000021_2.AllStreams.dst',
                                                                    '00046497_00000011_2.AllStreams.dst'
                                                                    ]
    filenames['Omegab2KstKp_MC_2012_MagDown_Stripping20_Pythia8'] = [
                                                                    '00046529_00000010_2.AllStreams.dst',
                                                                    '00046529_00000001_2.AllStreams.dst',
                                                                    '00046529_00000005_2.AllStreams.dst',
                                                                    '00046529_00000020_2.AllStreams.dst',
                                                                    '00046529_00000037_2.AllStreams.dst',
                                                                    '00046529_00000031_2.AllStreams.dst',
                                                                    '00046529_00000025_2.AllStreams.dst',
                                                                    '00046529_00000043_2.AllStreams.dst',
                                                                    '00046529_00000007_2.AllStreams.dst',
                                                                    '00046529_00000038_2.AllStreams.dst'
                                                                    ]
    filenames['Omegab2KstKp_MC_2012_MagUp_Stripping20_Pythia8'] = [
                                                                    '00046519_00000020_2.AllStreams.dst',
                                                                    '00046519_00000032_2.AllStreams.dst',
                                                                    '00046519_00000001_2.AllStreams.dst',
                                                                    '00046519_00000030_2.AllStreams.dst',
                                                                    '00046519_00000033_2.AllStreams.dst',
                                                                    '00046519_00000011_2.AllStreams.dst',
                                                                    '00046519_00000014_2.AllStreams.dst',
                                                                    '00046519_00000037_2.AllStreams.dst',
                                                                    '00046519_00000025_2.AllStreams.dst',
                                                                    '00046519_00000039_2.AllStreams.dst'
                                                                    ]
    import re
    
    print 'Runing event type: ' + eventtype
    
    #set all the names up
    mode = eventtype.split('_')[0]
    btype = mode.split('2')[0]
    restracks = re.findall( 'Delta\+|K\*\(892\)\-|rho\(770\)\-|N\(1520\)\+|K(?!S)|pi|p(?!i)', mode )
    resname = restracks[0]
    tracks = re.findall( 'Delta\+|K\*\(892\)\-|rho\(770\)\-|N\(1520\)\+|K(?!S)|pi|p(?!i)', eventtype.split('_')[1] )
    track1 = tracks[0]
    track2 = tracks[1]
    track3 = tracks[2]
    
    print 'With stripping: ' + stripping
    
    datatype = ''
    if stripping in known2011StrippingVersions:
        datatype = '2011'
    elif stripping in known2012StrippingVersions:
        datatype = '2012'
    else :
        e = Exception('Unsupported Stripping version: ' + stripping)
        raise e
    
    print 'With magnet setting: ' + magtype
    
    if magtype not in knownMagTypes :
        e = Exception('Unsupported magnet setting: ' + magtype)
        raise e

    print 'With Pythia version: %d' % pythiaversion
    
    #set things up
    pars = {}
    pars['btype']     = 'Xib'
    pars['track1']    = track1
    pars['track2']    = track2
    pars['track3']    = track3
    pars['resname']   = resname
    pars['mode']      = Mode
    pars['whichMC']   = datatype
    pars['stripping'] = stripping
    pars['dddbtag']   = 'Sim08-20130503-1'
    if magtype=='MagUp':
        pars['conddbtag'] = 'Sim08-20130503-1-vc-mu100'
    else:
        pars['conddbtag'] = 'Sim08-20130503-1-vc-md100'
    
    #build correct file path
    datafiles = []
    files = filenames[Mode+'_MC_'+datatype+'_'+magtype+'_'+stripping+'_Pythia'+str(pythiaversion)]
    datapath = '/data/lhcb/phrjgz/PartialReco_Xib/'+Mode+'_MC_'+datatype+'_'+magtype+'_'+stripping+'_Pythia'+str(pythiaversion)+'/'
    for file in files:
        datafile = datapath+file 
        datafiles.append(datafile)
    
    #configure things 
    configure( datafiles, params = pars, castor=False )
    
    #run things
    run(-1)
    
    #############
    
