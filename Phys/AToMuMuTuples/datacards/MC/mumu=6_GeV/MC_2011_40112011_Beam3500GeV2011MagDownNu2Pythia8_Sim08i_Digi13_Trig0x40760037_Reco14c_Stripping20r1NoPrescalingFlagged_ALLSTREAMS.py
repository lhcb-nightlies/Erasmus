#-- GAUDI jobOptions generated on Sat May 21 10:26:36 2016
#-- Contains event types : 
#--   40112011 - 9 files - 57725 events - 13.42 GBytes


#--  Extra information about the data processing phases:


#--  Processing Pass Step-127970 

#--  StepId : 127970 
#--  StepName : Reco14c for MC - 2011 
#--  ApplicationName : Brunel 
#--  ApplicationVersion : v43r2p11 
#--  OptionFiles : $APPCONFIGOPTS/Brunel/DataType-2011.py;$APPCONFIGOPTS/Brunel/MC-WithTruth.py;$APPCONFIGOPTS/Persistency/DST-multipleTCK-2011.py;$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r222 
#--  Visible : Y 


#--  Processing Pass Step-126731 

#--  StepId : 126731 
#--  StepName : Stripping20r1-NoPrescalingFlagged for Sim08 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v32r2p3 
#--  OptionFiles : $APPCONFIGOPTS/DaVinci/DV-Stripping20r1-Stripping-MC-NoPrescaling.py;$APPCONFIGOPTS/DaVinci/DataType-2011.py;$APPCONFIGOPTS/DaVinci/InputType-DST.py;$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r171 
#--  Visible : Y 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(['LFN:/lhcb/MC/2011/ALLSTREAMS.DST/00051466/0000/00051466_00000001_2.AllStreams.dst',
'LFN:/lhcb/MC/2011/ALLSTREAMS.DST/00051466/0000/00051466_00000002_2.AllStreams.dst',
'LFN:/lhcb/MC/2011/ALLSTREAMS.DST/00051466/0000/00051466_00000003_2.AllStreams.dst',
'LFN:/lhcb/MC/2011/ALLSTREAMS.DST/00051466/0000/00051466_00000004_2.AllStreams.dst',
'LFN:/lhcb/MC/2011/ALLSTREAMS.DST/00051466/0000/00051466_00000005_2.AllStreams.dst',
'LFN:/lhcb/MC/2011/ALLSTREAMS.DST/00051466/0000/00051466_00000006_2.AllStreams.dst',
'LFN:/lhcb/MC/2011/ALLSTREAMS.DST/00051466/0000/00051466_00000007_2.AllStreams.dst',
'LFN:/lhcb/MC/2011/ALLSTREAMS.DST/00051466/0000/00051466_00000008_2.AllStreams.dst',
'LFN:/lhcb/MC/2011/ALLSTREAMS.DST/00051466/0000/00051466_00000009_2.AllStreams.dst'
], clear=True)
