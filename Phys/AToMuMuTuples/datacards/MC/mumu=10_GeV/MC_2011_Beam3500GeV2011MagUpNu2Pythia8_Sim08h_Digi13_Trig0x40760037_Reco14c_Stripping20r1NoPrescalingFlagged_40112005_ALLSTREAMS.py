#-- GAUDI jobOptions generated on Tue Oct 27 08:55:12 2015
#-- Contains event types : 
#--   40112005 - 8 files - 60365 events - 15.08 GBytes


#--  Extra information about the data processing phases:

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(['LFN:/lhcb/MC/2011/ALLSTREAMS.DST/00048287/0000/00048287_00000001_2.AllStreams.dst',
'LFN:/lhcb/MC/2011/ALLSTREAMS.DST/00048287/0000/00048287_00000002_2.AllStreams.dst',
'LFN:/lhcb/MC/2011/ALLSTREAMS.DST/00048287/0000/00048287_00000003_2.AllStreams.dst',
'LFN:/lhcb/MC/2011/ALLSTREAMS.DST/00048287/0000/00048287_00000004_2.AllStreams.dst',
'LFN:/lhcb/MC/2011/ALLSTREAMS.DST/00048287/0000/00048287_00000005_2.AllStreams.dst',
'LFN:/lhcb/MC/2011/ALLSTREAMS.DST/00048287/0000/00048287_00000006_2.AllStreams.dst',
'LFN:/lhcb/MC/2011/ALLSTREAMS.DST/00048287/0000/00048287_00000007_2.AllStreams.dst',
'LFN:/lhcb/MC/2011/ALLSTREAMS.DST/00048287/0000/00048287_00000008_2.AllStreams.dst'
], clear=True)
