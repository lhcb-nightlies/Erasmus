#-- GAUDI jobOptions generated on Wed Nov  4 12:05:35 2015
#-- Contains event types : 
#--   40112003 - 9 files - 58103 events - 25.78 GBytes


#--  Extra information about the data processing phases:

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(['LFN:/lhcb/MC/2012/ALLSTREAMS.DST/00048273/0000/00048273_00000002_2.AllStreams.dst',
'LFN:/lhcb/MC/2012/ALLSTREAMS.DST/00048273/0000/00048273_00000004_2.AllStreams.dst',
'LFN:/lhcb/MC/2012/ALLSTREAMS.DST/00048273/0000/00048273_00000005_2.AllStreams.dst',
'LFN:/lhcb/MC/2012/ALLSTREAMS.DST/00048273/0000/00048273_00000008_2.AllStreams.dst'], clear=True)
