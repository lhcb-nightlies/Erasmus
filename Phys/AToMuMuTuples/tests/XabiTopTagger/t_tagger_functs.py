import GaudiPython
from math import *
from Bender.MainMC import *

isDEBUG = 0

#hard-coded ordered list of the extraInfos provided by BDTTagJets
TagDir = ["JetTag_extrainfo", "JetTag_absQSum", "JetTag_backwards", "JetTag_bdt0", "JetTag_bdt1", "JetTag_drSvrJet", "JetTag_fdChi2", "JetTag_fdrMin", "JetTag_ipChi2Sum", "JetTag_m", "JetTag_mCor", "JetTag_nTrk", "JetTag_nTrkJet", "JetTag_pass", "JetTag_pt", "JetTag_ptSvrJet", "JetTag_tau", "JetTag_z"]
JetTagStartNum = 5200; # this is just an arbitrary number, the same has set in FilterJet



######################## getting easy kinematics stuff #####################
trpx    = switch(TrLONG,TrPX,0)
trpy    = switch(TrLONG,TrPY,0)
trpt    = switch(TrLONG,TrPT,0)
trpxsum = RV_TrSUM(trpx,-1)
trpysum = RV_TrSUM(trpy,-1)
trptsum = RV_TrSUM(trpt,-1)

def sumpx(particle):
    pv = getBestPV(particle)
    if not pv: return -1.
    return trpxsum(pv)

def sumpy(particle):
    pv = getBestPV(particle)
    if not pv: return -1.
    return trpysum(pv)

def sumpt(particle):
    pv = getBestPV(particle)
    if not pv: return -1.
    return trptsum(pv)-particle.pt()

def misspt(mypar):
    return sqrt(sumpx(mypar)**2 + sumpy(mypar)**2)

def fillKin(pref,infos,par): # Fill in kinematics
    infos[pref+"_px"]=par.momentum().x()
    infos[pref+"_py"]=par.momentum().y()
    infos[pref+"_pz"]=par.momentum().z()
    infos[pref+"_p"]=par.p()
    infos[pref+"_pt"]=par.pt()
    infos[pref+"_eta"]=par.momentum().eta()
    infos[pref+"_phi"]=par.momentum().phi()
    infos[pref+"_key"]=float(par.key())
    infos[pref+"_vtx_totpt"]=float(sumpt(par))
    infos[pref+"_vtx_misspt"]=float(misspt(par))
    as_vtx = getBestPV(par)
    infos[pref+"_vtx_key"]=float(as_vtx.key())

def inv_mass(par1,par2,par3=0):
    px1=par1.momentum().x()
    py1=par1.momentum().y()
    pz1=par1.momentum().z()
    p1 = sqrt(px1**2+py1**2+pz1**2)
    px2=par2.momentum().x()
    py2=par2.momentum().y()
    pz2=par2.momentum().z()
    p2 = sqrt(px2**2+py2**2+pz2**2)
    # assume massless pars:
    if not par3:
        mass = 2*p1*p2-2*(px1*px2+py1*py2+pz1*pz2)
        if mass>0: return sqrt(mass)
        return -1 ## <- non physical value
    px3=par3.momentum().x()
    py3=par3.momentum().y()
    pz3=par3.momentum().z()
    p3 = sqrt(px3**2+py3**2+pz3**2)
    # assume massless pars:
    mass = 2*(p1*p2+p1*p3+p2*p3)-2*((px1*px2+py1*py2+pz1*pz2)+
                                    (px1*px3+py1*py3+pz1*pz3)+
                                    (px2*px3+py2*py3+pz2*pz3))
    if mass>0: return sqrt(mass)
    return -1. ## non physical value


ptCone = SUMCONE(0.5 ,PT,'/Event/Phys/StdAllNoPIDsPions/Particles')




#################### get bestPV for any type of particle ############
def getBestPV(particle0):
    gaudi = GaudiPython.AppMgr()
    TES = gaudi.evtsvc()
    if abs(particle0.particleID().pid())==11: # 11 are electrons
        particle = particle0
        p2pvTable = TES["Phys/StdAllLooseElectrons/Particle2VertexRelations"]
    elif abs(particle0.particleID().pid())==13: # 13 are muons
        ## This should not be needed now, we have ensured the leptons to be from IsMuonLoose container.
        ## Workaround for whenever we get the muons from A0 stored in the dst...
        pkey = particle0.proto().track().key()
        if not TES["Phys/StdAllLooseMuons/Particles"]: return -1.
        ml = filter(lambda x: x.proto().track().key()==pkey,TES["Phys/StdAllLooseMuons/Particles"])
        if not len(ml): return -1.
        particle = ml[0]
        p2pvTable = TES["Phys/StdAllLooseMuons/Particle2VertexRelations"]
    elif abs(particle0.particleID().pid())==211: # 211 are charged pions
        print "computing bestPV for a pion!"
        particle = particle0
        p2pvTable = TES["Phys/StdAllNoPIDsPions/Particle2VertexRelations"]
    else:
        pkey = particle0.key()
        stdJets2 = TES["Phys/StdJets2/Particles"]
        particle = stdJets2[pkey]
        #particle = filter(lambda x: x.key()==pkey, TES["Phys/StdJets2/Particles"])
        #get the corresponding StdJets2 because sometimes using the BDTTagJet makes the extraction of the PV to crash
        #p2pvTable = TES["Phys/BDTTagJets/Particle2VertexRelations"]
        p2pvTable = TES["Phys/StdJets2/Particle2VertexRelations"]
    if not p2pvTable:
        return -1.
    if not "relations" in dir(p2pvTable):
        return -1.
    relPVs = p2pvTable.relations(particle)
    return relPVs.back().to()


######################### Fill extrainfo from JetTagging ##############
#def fillJetTag(pref,infos,jet):
#    try: #FIXME sometimes it crashes
#        jet.extraInfo().get(JetTagStartNum) #make exception because sometimes (rarely) it crashes
#        if jet.extraInfo().get(JetTagStartNum) > 0. :
#            for tag in TagDir:
#                infos[pref+"_"+tag] = jet.extraInfo().get(JetTagStartNum+TagDir.index(tag))
#        else:
#            for tag in TagDir:
#                infos[pref+"_"+tag]=-1.
#    except:
#        print "getting the jetTag info caused the crash"
#        for tag in TagDir:
#            infos[pref+"_"+tag]=-1.


def fillJetTag(pref,infos,jet):
        if jet.extraInfo().get(JetTagStartNum) > 0. :
            for tag in TagDir:
                infos[pref+"_"+tag] = jet.extraInfo().get(JetTagStartNum+TagDir.index(tag))
        else:
            for tag in TagDir:
                infos[pref+"_"+tag]=-1.


#################### main loop to get fill jet info while banning muons from A0 #################
def topBDTInfo_ban(infos,extrainfo):
    cats,group = extrainfo
    gaudi=GaudiPython.AppMgr()
    TES=gaudi.evtsvc()
    ## in principle, maximum is three (1 b jets, 2 light jets from W)
    njets = 3
    bdtname = "topBDT"
    jetbranches = ["px","py","pz","phi","eta","pt","p","key","vtx_misspt","vtx_totpt","vtx_key"]
    for branch in TagDir: jetbranches.append(branch)
    for var in jetbranches:
        for i in range(njets):
            infos[bdtname+"jet"+str(i)+"_"+var]=-1.
    if isDEBUG: print "tt1"
    for var in ["px","py","pz","phi","eta","pt","p","key","iso","vtx_misspt","vtx_totpt","vtx_key"]:
        for i in range(2):
            infos[bdtname+"el"+str(i)+"_"+var]=-1.
            infos[bdtname+"mu"+str(i)+"_"+var]=-1.
    infos[bdtname+"jet_wmass"]=-1.
    infos[bdtname+"jet_tmass"]=-1.
    if isDEBUG: print "tt2"
    skiplepton = group["leptons"]
    skipbjet,skipwjets  = 0,0
    ## this is the PV that will be used for the topBDT variables
    mypv = getBestPV(group["leptons"][0])
    if isDEBUG: print "the PV related to the first muon is: ", mypv
    infos[bdtname+"weird_event"]=0
    ## protect against events in which the PV association did not work
    if not mypv:
        mypv = TES["Rec/Vertex/Primary"][0]
        infos[bdtname+"weird_event"]=1 #events are classified as weird when the PV of the A0 muons is not found
    if isDEBUG: print "tt3"
    ## Forcing jets from same PV than A1MuMu candidate!
    #stdjets0 = TES['Phys/TopoTagJets/Particles']
    stdjets0 = TES['Phys/BDTTagJets/Particles']
    if isDEBUG: print "tt3.1"
    if len(stdjets0):
        stdjets0 = filter(lambda x: getBestPV(x)==mypv,stdjets0) # filter out all jets not having the same PV as the first muon
        if isDEBUG: print "tt3.2"
        if skipwjets: stdjets0 = filter(lambda x: not (x in skipwjets),stdjets0)
        if skipbjet: stdjets0 = filter(lambda x: x != skipbjet, stdjets0)
        stdjets = map(lambda x: [x.pt(),x],stdjets0) #map first object to jet pt...easier ordering later
        if isDEBUG: print "tt3.3"
        if isDEBUG: print "stdjets object is", stdjets
        stdjets.sort(reverse=True)
        if isDEBUG: print "tt3.4"
        mw,mt = [],[]
        for i in range(min(njets,len(stdjets))):
            jet1 = stdjets[i][1] # object 0 is pt, 1 is the jet itself
            if isDEBUG: print "JET1 is :", jet1
            fillKin(bdtname+"jet"+str(i),infos,jet1)
            #if isDEBUG: print "after fillKin"
            #infos[bdtname+"jet"+str(i)+"_maxTopoBDT"]=getTopoBDT(jet1)
            fillJetTag(bdtname+"jet"+str(i),infos,jet1)
            for j in range(min(njets,len(stdjets))):
                jet2 = stdjets[j][1]
                mass = inv_mass(jet1,jet2)
                mw.append([abs(mass-80e3),mass])
                if isDEBUG: print "2-jets mass is: ", mass
                for w in range(min(njets,len(stdjets))):
                    jet3 = stdjets[w][1]
                    mass = inv_mass(jet1,jet2,jet3)
                    mt.append([abs(mass-173e3),mass])
                    if isDEBUG: print "3-jets mass is: ", mass
        mw.sort()
        if len(mw): infos[bdtname+"jet_wmass"]=float(mw[0][1])
        mt.sort()
        if len(mt): infos[bdtname+"jet_tmass"]=float(mt[0][1])
        if isDEBUG: print "tt4"
    else:
        if isDEBUG: print "NO JET FOUND"

    ## Forcing electrons from same PV as A1MuMu candidate!
    els0 = TES["Phys/StdAllLooseElectrons/Particles"]
    els0 = filter(lambda x: getBestPV(x)==mypv,els0)
    if isDEBUG: print "tt5"

    if skiplepton: els0 = filter(lambda x: not (x in skiplepton), els0)
    els = map(lambda x: [x.pt(),x],els0)
    els.sort(reverse=True)
    for i in range(min(2,len(els))):
        el = els[i][1]
        fillKin(bdtname+"el"+str(i),infos,el)
        infos[bdtname+"el"+str(i)+"_iso"]=float(ptCone(el)-el.pt())
    ## Forcing muons from same PV as A1MuMu candidate!
    mus0 = TES["Phys/StdAllLooseMuons/Particles"]
    mus0 = filter(lambda x: getBestPV(x)==mypv,mus0)
    if isDEBUG: print "tt6"
    if skiplepton: mus0 = filter(lambda x: not (x in skiplepton), mus0)
    mus = map(lambda x: [x.pt(),x],mus0)
    mus.sort(reverse=True)
    for i in range(min(2,len(mus))):
        mu = mus[i][1]
        fillKin(bdtname+"mu"+str(i),infos,mu)
        infos[bdtname+"mu"+str(i)+"_iso"]=float(ptCone(mu)-mu.pt())
    if isDEBUG: print "tt7"
    if isDEBUG: print infos

