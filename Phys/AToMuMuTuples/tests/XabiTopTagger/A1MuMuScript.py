#! /usr/bin/env python
from ROOT import * ### Keep this at the top, to overwrite the shit that DV does to displays
import sys, os
from Gaudi.Configuration import NTupleSvc
from Configurables import GaudiSequencer, DaVinci, L0Conf, SelDSTWriter, FilterDesktop
from triggerDecisionLists import *
from BenderAlgo.BenderV0 import *
from BenderAlgo.extrafunctions import *
bsmumuCounterKeys = ["MuMuCouplesAnalyzed","EVT", "weird"]

################################### Some General Options. Remember to check it

ISGRID = 0
INTERACTIVE = 10000*(not ISGRID)
SIMULATION = 1
MC_INFO = 1 # if 1 this BREAKS LoKi, check WHY!

TUP = 1
TRIGGER = 1
DebugAlgos = 0

# This is old and probably not needed anymore #
dataFormats = {}
dataFormats[0] = "MDST"
dataFormats[1] = "DST"
dataFormat = dataFormats[SIMULATION]
RootsInTES = {}
RootsInTES["MDST"] = "/Event/Leptonic/"
RootsInTES["DST"] = False
RootInTES = RootsInTES[dataFormat]
###############################################

DataType = "2012" # Christian 8 GeV MC
magnetPolarity = 'md' # md or mu, depends on simulation conditions, for Christian MC DST, DB Tags are not set
tagsDic = { "2011" : {}, "2012" : {}} # only Simulation
tagsDic["2011"] = ('Sim08-20130503','Sim08-20130503-vc-'+magnetPolarity+'100') # tags for S20r1 Simulation. Change for S21r1.
tagsDic["2012"] = ('Sim08-20130503-1','Sim08-20130503-1-vc-'+magnetPolarity+'100') # tags for S20 Simulation. Change for S21.

#########################
### ALGORITHMS TO ADD ###
#########################

TUPLE_FILE = "A0mumu_"+DataType+"_data_ntuples" + "MC"*SIMULATION +".root" # propagate to REAL DATA MAKERS
HISTO_FILE = "A0mumu_"+DataType+"_data_histos" + "MC"*SIMULATION +".root" # propagate to REAL DATA MAKERS

if TUP: NTupleSvc( Output =["T DATAFILE='"+ TUPLE_FILE + "' TYP='ROOT' OPT='NEW'"] )

#################
### DATACARDS ###
#################

from PhysSelPython.Wrappers import SelectionSequence
import GaudiPython

import Gaudi
from Gaudi.Configuration import *

#importOptions("/afs/cern.ch/work/c/cvazquez/A0mumu/MC_A1MuMu/A1_MC_truth_8GeV.py")
#if not ISGRID: Eostize(EventSelector()) 
DaVinci().Input = ['PFN:/afs/cern.ch/work/c/cvazquez/A0mumu/trying_Xabier/dst/00048273_00000001_2.AllStreams.dst']

#Locations = {"Upsilon":"Dimuon/Phys/FullDSTDiMuonDiMuonHighMassLine",
#             "A1":"Dimuon/Phys/A1MuMuLine",
#             "A1SS":"Dimuon/Phys/A1MuMuSameSignLine"}

Locations = {"A1" : "AllStreams/Phys/A1MuMuA1MuMuLine/Particles"}

def configure():

    DaVinci().EvtMax = 0    
    DaVinci().DataType = DataType
    DaVinci().Simulation = SIMULATION
    DaVinci().HistogramFile = HISTO_FILE
   
    if not SIMULATION: # propagate to REAL DATA MAKERS!
      from Configurables import CondDB
      CondDB( LatestGlobalTagByDataType = DataType ) # latest tag from CondDB
    #else:
      ###DaVinci().DDDBtag = tagsDic[SIMULATION][DataType][0]
      ###DaVinci().CondDBtag = tagsDic[SIMULATION][DataType][1]
      #DaVinci().DDDBtag = tagsDic[DataType][0] # not needed for Christian MC
      #DaVinci().CondDBtag = tagsDic[DataType][1] # not needed for Christian MC
    DaVinci().Lumi = bool(SIMULATION) # Simulation does not allow Lumi tool!
    
    ################## END IMPORTING OPTIONS

    DaVinci().applyConf()   
    gaudi = appMgr()
    algos = []
    
    ###################### BENDER ALGORITHMS
    ## A1

    A1 = B2QQ('A1')
    A1.LookIn = Locations["A1"]
    A1.decayname = "A1-->mumu"
    A1.COUNTER = {}
    A1.COUNTER["Bender(evts) " + A1.decayname] = 0
    
    A1.Sel_Above_GL = 0
    A1.extraFunctions = [trackHits, subdetectorDLLs, more_muon_things, globalEvtVars, maxEventPT, maxEventPTMuon]
    
    if SIMULATION*MC_INFO: A1.extraFunctions += [BQQMCtruth, mc_geometry]
    for key in bsmumuCounterKeys:
        A1.COUNTER[key] = 0
    algos.append(A1)
    gaudi.addAlgorithm(A1)

    ## A1SS

    #A1SS = B2QQ('A1SS')
    #A1SS.LookIn = Locations["A1SS"]
    #A1SS.decayname = "A1SS-->mumu"
    #A1SS.COUNTER = {}
    #A1SS.COUNTER["Bender(evts) " + A1SS.decayname] = 0
    
    #A1SS.Sel_Above_GL = 0
    #A1SS.extraFunctions = [trackHits, subdetectorDLLs, more_muon_things, globalEvtVars, maxEventPT, maxEventPTMuon]
    
    #if SIMULATION*MC_INFO: A1SS.extraFunctions += [BQQMCtruth, mc_geometry]
    #for key in bsmumuCounterKeys:
    #    A1SS.COUNTER[key] = 0
    #algos.append(A1SS)
    #gaudi.addAlgorithm(A1SS)
                
    ## Upsilon

    #Ups = B2QQ('Upsilon')
    #Ups.LookIn = Locations["Upsilon"]
    #Ups.decayname = "Upsilon-->mumu"
    #Ups.COUNTER = {}
    #Ups.COUNTER["Bender(evts) " + Ups.decayname] = 0
    
    #Ups.Sel_Above_GL = 0
    #Ups.extraFunctions = [trackHits, subdetectorDLLs, more_muon_things, globalEvtVars, maxEventPT, maxEventPTMuon]
    
    #if SIMULATION*MC_INFO: Ups.extraFunctions += [BQQMCtruth, mc_geometry]
    #for key in bsmumuCounterKeys:
    #    Ups.COUNTER[key] = 0
    #algos.append(Ups)
    #gaudi.addAlgorithm(Ups)

    gaudi.initialize()
    from BenderAlgo import PIDcalTools
 
    ########################
    ### COMMON ATRIBUTES ###
    ########################
   
    for algo in algos:
        algo.MC_INFO = SIMULATION*MC_INFO
        algo.TRIGGER = TRIGGER
        algo.PIDcalTools = PIDcalTools
        algo.NTupleLUN = "T"
        algo.addedKeys = []
        algo.DST = False
        algo.TUP = TUP
        algo.COUNTER['negSq'] = 0
        algo.COUNTER["weird"] = 0
        algo.DEBUG = DebugAlgos ### Do not define global DEBUG
       
        algo.evt_of = 0

        algo.l0BankDecoder=algo.tool(cpp.IL0DUFromRawTool,'L0DUFromRawTool')
        algo.rawBankDecoder=algo.tool(cpp.IOTRawBankDecoder,'OTRawBankDecoder')

        algo.PVRefitter = algo.tool(cpp.IPVReFitter,"AdaptivePVReFitter") # DEPRECATED, CHANGE!
        algo.LifeFitter = algo.tool(cpp.ILifetimeFitter,"PropertimeFitter") # DEPRECATED, CHANGE!
        algo.Geom =  algo.tool(cpp.IDistanceCalculator,"LoKi::DistanceCalculator")
        algo.l0List = l0List_2011
        algo.hlt1List = hlt1List_2011
        algo.hlt2List = hlt1List_2011
        if algo.MC_INFO :algo.extraFunctions += [KsPi0MuMuMCtruth]
        if RootInTES: algo.RootInTES = RootInTES
       
configure()
gaudi = appMgr()

if INTERACTIVE:
    for i in range(INTERACTIVE):
        gaudi.run(1)
        TES = gaudi.evtsvc()
else:
    gaudi.run(-1)
    gaudi.stop()
    gaudi.finalize()

