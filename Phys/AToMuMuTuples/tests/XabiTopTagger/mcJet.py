import GaudiPython
from math import *

LHCbParticle=GaudiPython.gbl.LHCb.Particle
LorentzVector=GaudiPython.gbl.Gaudi.LorentzVector
ParticleID=GaudiPython.gbl.LHCb.ParticleID
vector=GaudiPython.gbl.std.vector

MaxRdistance = 0.5

class irange(object):
    def __init__(self,b,e):
        self.begin, self.end = b, e
    def __iter__(self):
        it = self.begin
        while it != self.end:
            yield it.__deref__()
            it.__postinc__(1)

SLIST=[23,24,25,5,6]

## Get the Vector Bosons W/Z, H, top and bottom events:

def get_vbosons():
    gaudi = GaudiPython.AppMgr()
    TES = gaudi.evtsvc()
    hepmc = TES['Gen/HepMCEvents']
    VBosonEvent = 0
    if not hepmc: return
    for i in hepmc:
        genevent = i.pGenEvt()
        for particle in irange(genevent.particles_begin(),
                               genevent.particles_end()):
            if abs(particle.pdg_id()) in SLIST: VBosonEvent=particle.parent_event()
    return VBosonEvent

PLIST=[1,2,3,4,5,21]

## Get the partons of the events with a cut of 5 GeV on the PT:

def get_partons(pTcut=5000):
    gaudi = GaudiPython.AppMgr()
    TES = gaudi.evtsvc()
    hepmc = TES['Gen/HepMCEvents']
    if not hepmc: return
    GPartons = []
    for i in hepmc:
        genevent = i.pGenEvt()
        for particle in irange(genevent.particles_begin(),
                               genevent.particles_end()):
            if (abs(particle.pdg_id()) in PLIST) and (particle.momentum().pt()>pTcut): GPartons.append(particle)
    return GPartons

## Get the bhadrons and chadrons of the events with a cut of 5 GeV on the PT:

def get_bhadrons(pTcut=5000):
    gaudi = GaudiPython.AppMgr()
    TES = gaudi.evtsvc()
    hepmc = TES['Gen/HepMCEvents']
    if not hepmc: return
    GHadrons = []
    for i in hepmc:
        genevent = i.pGenEvt()
        for particle in irange(genevent.particles_begin(),
                               genevent.particles_end()):
            if (particle.momentum().pt() < pTcut): continue
            myParticleID = ParticleID(particle.pdg_id())
            if myParticleID.hasBottom() and myParticleID.isHadron(): GHadrons.append(particle)
    return GHadrons

def get_chadrons(pTcut=5000):
    gaudi = GaudiPython.AppMgr()
    TES = gaudi.evtsvc()
    hepmc = TES['Gen/HepMCEvents']
    if not hepmc: return
    GHadrons = []
    for i in hepmc:
        genevent = i.pGenEvt()
        for particle in irange(genevent.particles_begin(),
                               genevent.particles_end()):
            if (particle.momentum().pt() < pTcut): continue
            myParticleID = ParticleID(particle.pdg_id())
            if myParticleID.hasCharm() and myParticleID.isHadron(): GHadrons.append(particle)
    return GHadrons

## Get HepMC Particles with the final status (1 or 999). Ban neutrinos:

SLIST=[1,999]
BLIST=[12,14,16]

def get_final():
    gaudi = GaudiPython.AppMgr()
    TES = gaudi.evtsvc()
    hepmc = TES['Gen/HepMCEvents']
    if not hepmc: return
    GFinal = []
    for i in hepmc:
        genevent = i.pGenEvt()
        for particle in irange(genevent.particles_begin(),
                               genevent.particles_end()):
            if (abs(particle.pdg_id()) in BLIST): continue
            if (particle.status() in SLIST): GFinal.append(particle)
    return GFinal

## Get all stable particles from PV to reconstruct MC Jets:

def get_stable():  
  vbpvgenp4jets = []
  GFinal = get_final()
  VBosonEvent = get_vbosons()
  if not VBosonEvent: return vbpvgenp4jets
  for gen in GFinal:
      mycond = False
      if VBosonEvent.signal_process_id() == gen.parent_event().signal_process_id():
          part = LHCbParticle() 
          gen4vectortmp = LorentzVector(gen.momentum().px(),gen.momentum().py(),gen.momentum().pz(),gen.momentum().e())
          part.setMomentum(gen4vectortmp)
          part.setParticleID(ParticleID(gen.pdg_id()))
          vbpvgenp4jets.append(part)
  return vbpvgenp4jets

## Get all partons that match with one MCJet:

def partons_from_jet(jet,pTCut = 5000):
  vbpvjetpartons = []
  GPartons = get_partons(pTCut)
  for quark in GPartons:
      deltaPhi = abs(jet.momentum().phi() - quark.momentum().phi()) ### INCLUDE EXPLICITLY AS VARIABLE IN THE TUPLE?
      if (deltaPhi > acos(-1)): deltaPhi = 2*acos(-1) - deltaPhi
      deltaEta = jet.momentum().eta() - quark.momentum().eta() 
      if sqrt( deltaPhi**2 + deltaEta**2) < MaxRdistance and quark.momentum().pt()>5000:
        ## Check for partons inside the jet with a R distance lower than MaxRdistance
        vbpvjetpartons.append(quark)
  return vbpvjetpartons      


## Get all bhadrons that match with one MCJet:

def bhadrons_from_jet(jet,pTCut = 5000):
  vbpvjetpartons = []
  GHadrons = get_bhadrons(pTCut)
  for hadron in GHadrons:
      deltaPhi = abs(jet.momentum().phi() - hadron.momentum().phi()) ### INCLUDE EXPLICITLY AS VARIABLE IN THE TUPLE?
      if (deltaPhi > acos(-1)): deltaPhi = 2*acos(-1) - deltaPhi
      deltaEta = jet.momentum().eta() - hadron.momentum().eta() 
      if sqrt( deltaPhi**2 + deltaEta**2) < MaxRdistance and hadron.momentum().pt()>5000:
        ## Check for partons inside the jet with a R distance lower than MaxRdistance
        vbpvjetpartons.append(hadron)
  return vbpvjetpartons      

def chadrons_from_jet(jet,pTCut = 5000):
  vbpvjetpartons = []
  GHadrons = get_chadrons(pTCut)
  for hadron in GHadrons:
      deltaPhi = abs(jet.momentum().phi() - hadron.momentum().phi()) ### INCLUDE EXPLICITLY AS VARIABLE IN THE TUPLE?
      if (deltaPhi > acos(-1)): deltaPhi = 2*acos(-1) - deltaPhi
      deltaEta = jet.momentum().eta() - hadron.momentum().eta() 
      if sqrt( deltaPhi**2 + deltaEta**2) < MaxRdistance and hadron.momentum().pt()>5000:
        ## Check for partons inside the jet with a R distance lower than MaxRdistance
        vbpvjetpartons.append(hadron)
  return vbpvjetpartons      

## Defining the JetMaker to use with MC:

def make_jets_cpp(pTcut = 5000, had_par = "had"):
    gaudi = GaudiPython.AppMgr()
    TES = gaudi.evtsvc()
    vbpvhepgenjets = TES["Phys/StdMCJets/Particles"]
    outjets = []
    for jet in vbpvhepgenjets:
        if had_par == "par": mypars = partons_from_jet(jet,pTcut)
        else: mypars = bhadrons_from_jet(jet,pTcut) # get all b-hadrons that match with one MCJet
        outjets.append([mypars,LHCbParticle(jet)])
    return outjets

def make_jets_cpp_forCharm(pTcut = 5000, had_par = "had"):
    gaudi=GaudiPython.AppMgr()
    TES=gaudi.evtsvc()
    vbpvhepgenjets = TES["Phys/StdMCJets/Particles"]
    outjets = []
    for jet in vbpvhepgenjets:
        if had_par == "par": mypars = partons_from_jet(jet,pTcut)
        else: mypars = chadrons_from_jet(jet,pTcut) # get all c-hadrons that match with one MCJet
        outjets.append([mypars,LHCbParticle(jet)])
    return outjets

## Defining the JetMaker to use with MC:

def make_jets(jetMaker): # this function makes jets with a certain maker
    stable=get_stable()
    vbpvgenp4jets= vector('const LHCb::Particle*')()
    for st in stable: vbpvgenp4jets.push_back(st)
    vbpvhepgenjets = vector('LHCb::Particle*')()    
    sc = jetMaker.makeJets ( vbpvgenp4jets , vbpvhepgenjets ) 
    if sc.isFailure(): return []
    outjets=[]
    for jet in vbpvhepgenjets:
        mypartons=partons_from_jet(jet)
        outjets.append([mypartons,LHCbParticle(jet)])
    return outjets

## Match reconstructed and MC Jets using the R (maxdistance=0.5):

def match_true_rec_jets(jet1,jet2,maxdistance=0.5):
    deltaPhi = abs(jet1.momentum().phi() - jet2.momentum().phi())
    if (deltaPhi > acos(-1)): deltaPhi = 2*acos(-1) - deltaPhi
    deltaEta = jet1.momentum().eta() - jet2.momentum().eta()
    if sqrt( deltaPhi**2 + deltaEta**2) < maxdistance: return True
    return False

