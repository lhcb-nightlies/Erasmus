import os,GaudiPython, sys
from t_tagger_functs import *
from putInTES import *
from Gaudi.Configuration import *
from Bender.MainMC import *
from HltPython.PyTree import *
from JetAccessories.ParticleFlow_Config import ParticleFlowConf
from JetAccessories.JetMaker_Config import JetMakerConf
from ROOT import *

from StrippingConf.StrippingStream import StrippingStream
from StrippingConf.Configuration import StrippingConf

from Configurables import DaVinci, CombineParticles, LoKi__FastJetMaker
import readGen as rg

from LoKiTracks.decorators import *
from LoKiCore.functions import sum

LHCbParticle = GaudiPython.gbl.LHCb.Particle

TRIGGER = True ## TAKE A LOOK
isSIMULATION = TRUE
isGRID = 0
isDEBUG = 0
#optionsFile = "/afs/cern.ch/work/c/cvazquez/cmtuser/Erasmus_v10r2/Phys/AToMuMuTuples/datacards/real_data/LHCb_Collision12_Beam4000GeVVeloClosedMagDown_Real Data_Reco14_Stripping21_90000000_DIMUON.DST.py"
optionsFile = "/afs/cern.ch/user/m/mborsato/cmtuser/Erasmus_v10r2/Phys/AToMuMuTuples/datacards/MC/mumu=8_GeV/MC_2011_Beam3500GeV2011MagUpNu2Pythia8_Sim08h_Digi13_Trig0x40760037_Reco14c_Stripping20r1NoPrescalingFlagged_40112003_ALLSTREAMS.py"

### Define locations inside the DST:
#A1Location = "/Event/Dimuon/Phys/A1MuMuLine/Particles" # real data
A1Location = "/Event/AllStreams/Phys/A1MuMuA1MuMuLine/Particles" # MC
stripline  = "A1"


### Ban from the jets and leptons the muons from the A1mumu candidates:
#pflow = ParticleFlowConf("PF2" ,  _params = {"CandidateToBanLocation":[A1MuonsLocation]})
pflow = ParticleFlowConf("PF2")
DaVinci().UserAlgorithms += pflow.algorithms
jetmaker = JetMakerConf("StdJets2", Inputs = ['Phys/PF2/Particles'], listOfParticlesToBan = [A1Location])
DaVinci().UserAlgorithms += jetmaker.algorithms


### b-tagging:
from Configurables import FilterJet
filterJet0 = FilterJet('BDTTagJets')
filterJet0.Inputs = ["Phys/StdJets2/Particles"] # HERE: StdJets or StdJets2?
filterJet0.tagToolName = 'LoKi::BDTTag'
filterJet0.setNewPID = "b"
filterJet0.saveAllJetWithInfo = True
filterJet0.saveTaggerInfo = True
filterJet0.startNumberForAddInfo = 5200

### DaVinci conditions, to be updated:
magnetPolarity = "md"
dataYear = "2012"
tagsDic = {}
tagsDic["2012"] = ['Sim08-20130503-1','Sim08-20130503-1-vc-'+magnetPolarity+'100'] #FIXME tags for S20 Simulation. Change for S21.

DaVinci().DataType = dataYear
DaVinci().Simulation = isSIMULATION
if not isSIMULATION:
    DaVinci().DDDBtag = tagsDic[dataYear][0] # only for real data
    DaVinci().CondDBtag = tagsDic[dataYear][1] # only for real data
from Configurables import CondDB
CondDB( LatestGlobalTagByDataType = dataYear )
DaVinci().Lumi = not isSIMULATION # True for real data
DaVinci().TupleFile = "DVnTuples.root"
DaVinci().UserAlgorithms += [filterJet0]

if TRIGGER: # L0 trigger, this is not part of the original maker!
    L0SelRepSeq = GaudiSequencer("L0SelRepSeq")
    L0SelRepSeq.MeasureTime = True
    from Configurables import L0SelReportsMaker, L0DecReportsMaker
    L0SelRepSeq.Members += [ L0DecReportsMaker(), L0SelReportsMaker() ]
    DaVinci().UserAlgorithms += [ L0SelRepSeq ]

###############################################

def Eostize(thingie):
    x = []
    for thing in thingie.Input:
        x.append(thing.replace("LFN:", "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod"))
    thingie.Input = x

importOptions(optionsFile)
if not isGRID: Eostize(EventSelector())

gaudi = GaudiPython.AppMgr()
gaudi.initialize()

TES = gaudi.evtsvc()

if TRIGGER:
    from BenderAlgo.extrafunctions import *#triggerBlock, triggerBlockDaughters # triggerBlock is deprecated
    trigger_algo=AlgoMC("trigger")
    trigger_algo.tistostool = trigger_algo.tool( cpp.ITriggerTisTos , 'TriggerTisTos' )
    trigger_algo.tisTosToolL0= trigger_algo.tool( cpp.ITriggerTisTos , 'L0TriggerTisTos' )

    trigger_algo.l0List = []
    trigger_algo.hlt1List = []
    trigger_algo.hlt2List = []
    trigger_algo.DEBUG = False

###############################################




def fill_ntuple(tup,infos):
    def tup_started(tup_): return len(map(lambda x: x.GetName(),tup_.GetListOfBranches())) # gets len of list of names of tuple branches

    if not tup_started(tup): # if there is no tuple
        for el in infos: # infos is a dict of branches ([name] = value) for a new tuple
            if type(infos[el])==int or type(infos[el])==long:
                tup.book(el+"/I")
            elif type(infos[el])==float:
                tup.book(el+"/F")
            else:
                leni=len(infos[el])
                if type(infos[el][0])==int or type(infos[el][0])==long:
                    tup.book(el+"["+str(leni)+"]/I")
                if type(infos[el][0])==float:
                    tup.book(el+"["+str(leni)+"]/F")
    for el in infos: # set new branches and fill tuple
        tup.set(el,infos[el])
    tup.Fill()


##def fill_trigger_info(infos): # triggerBlock is deprecated!
##    mylist = ["L0Decision","Hlt1Decision","Hlt2Decision"]
##    infos_ = {}
##    Done = {"TES":TES,"Candidate":LHCbParticle()}
##    triggerBlock(trigger_algo,infos_, Done) # where is this defined?
##    for el in infos_:
##        if not (el in mylist): continue
##        infos[el]=int(infos_[el])


def toIsMuonLoosePar(particle0):
    pkey = particle0.proto().track().key()
    if not TES["Phys/StdAllLooseMuons/Particles"]: return 0
    ml =  filter(lambda x: x.proto().track().key()==pkey, TES["Phys/StdAllLooseMuons/Particles"])
    if isDEBUG: print "[DEBUG] toIsMuonLoosePar"
    if not len(ml): return 0
    return ml[0]


def sumpx(particle):
    pv = getBestPV(particle)
    if not pv: return -1.
    return trpxsum(pv)

def sumpy(particle):
    pv = getBestPV(particle)
    if not pv: return -1.
    return trpysum(pv)

def sumpt(particle):
    pv = getBestPV(particle)
    if not pv: return -1.
    return trptsum(pv)-particle.pt()

def misspt(mypar):
    return sqrt(sumpx(mypar)**2 + sumpy(mypar)**2)


def getBestPV(particle0): ### need to add tau leptons?
    if abs(particle0.particleID().pid())==11: # electrons
        particle = particle0
        p2pvTable = TES["Phys/StdAllNoPIDsElectrons/Particle2VertexRelations"]
    elif abs(particle0.particleID().pid())==13: # muons
        particle = toIsMuonLoosePar(particle0)
        if isDEBUG: print "[DEBUG] getBestPV: muons"
        if not particle: return 0
        if isDEBUG: print "[DEBUG] getBestPV: muons, after check if not particle"
        p2pvTable = TES["Phys/StdAllLooseMuons/Particle2VertexRelations"]
    if not p2pvTable:
        return 0
    if not "relations" in dir(p2pvTable):
        return 0
    relPVs = p2pvTable.relations(particle)
    if not relPVs:
        return 0
    if not relPVs.back():
        return 0
    mypv = relPVs.back().to()
    return mypv



def fill_stripping_lines(cand,infos):
    mu1,mu2 = cand.daughters()[0],cand.daughters()[1]
    infos[stripline+"_A1mass"] = cand.measuredMass()
    infos[stripline+"_d1_pt"] = mu1.pt()
    infos[stripline+"_d2_pt"] = mu2.pt()
    infos[stripline+"_d1_key"] = mu1.proto().track().key()
    infos[stripline+"_d2_key"] = mu2.proto().track().key()
    infos[stripline+"_d1_iso"] = float(ptCone(mu1)-mu1.pt())
    infos[stripline+"_d2_iso"] = float(ptCone(mu2)-mu2.pt())
    if isDEBUG: print "got iso"
    infos[stripline+"_d1_sumpt"] = float(sumpt(mu1))
    infos[stripline+"_d1_misspt"] = float(misspt(mu1))
    if isDEBUG: print "got misspt"
    # mu1 and mu2 are later used to require jets to have the same BestPV as them
    mymu1 = toIsMuonLoosePar(mu1)
    mymu2 = toIsMuonLoosePar(mu2)
    return mymu1,mymu2

def fill_gen_event(infos): # is this compatible with Bender?
    infos["evtNum"] = float(TES["Rec/Header"].evtNumber())
    infos["runNum"] = float(TES["Rec/Header"].runNumber())
    infos["nPVs"] = int(TES["Rec/Vertex/Primary"].size())
    infos["nTrs"] = int(TES["Rec/Track/Best"].size())
    infos["nTrsLong"] = int(len(filter(lambda x: x.type()==3,TES["Rec/Track/Best"])))

def GECs(infos): # I do not know what TrSOURCE does
    sumpt  = TrSOURCE( 'Rec/Track/Best', TrLONG) >> sum(TrPT)
    sumpx  = TrSOURCE( 'Rec/Track/Best', TrLONG) >> sum(TrPX)
    sumpy  = TrSOURCE( 'Rec/Track/Best', TrLONG) >> sum(TrPY)
    infos["GEC_sumpt_allpvs"] = sumpt()
    infos["GEC_misspt_allpvs"] = sumpx()**2 + sumpy()**2



def go(n):
    tup_mct=PyTree("mct_jets_tuple","mct_jets_tuple")
    tup=PyTree("jets_tuple","jets_tuple")
    for i in range(n):
        c1=gaudi.run(1)
        if isDEBUG: print "EVENT ", i
        if not TES["Rec/Track/Best"]: break
        pvs = TES["Rec/Vertex/Primary"]
        if isDEBUG: print "PVs are: ", pvs
        if not (pvs and pvs.size()): continue
        pars = TES[A1Location]
        if isDEBUG: print "A0 particles are: ", pars
        if not (pars and pars.size()): continue
        for par in pars:
            infos = {}
            mu1,mu2 = fill_stripping_lines(par,infos)
            ## ban A1MuMu location
            if isDEBUG: print "after stripping"
            extrainfo = [[],{"leptons":[mu1,mu2]}]
            topBDTInfo_ban(infos,extrainfo)
            if isDEBUG: print "after topBDT"
            GECs(infos)
            if isDEBUG: print "after GECs"
            fill_gen_event(infos)
            if isDEBUG: print "after gen"
            #fill_trigger_info(infos)
            #if isDEBUG: print "after trigger"
            fill_ntuple(tup,infos)
            if isDEBUG: print "after fill"
    return tup

def finalize():
    gaudi.stop()
    gaudi.finalize()

if isGRID: mytup=go(10000)
else:
    mytup=go(50000)
    fnew=TFile("MC_top_tagger_tup.root","RECREATE")
    mytupw=mytup.CopyTree("1")
    mytupw.Write()
    fnew.Close()
    finalize()




