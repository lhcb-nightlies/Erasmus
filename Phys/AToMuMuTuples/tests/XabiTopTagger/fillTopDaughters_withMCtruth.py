import readGen as rg
import mcJet as mcj
import GaudiPython
from math import *
import HltPython.ghostsClassifyTools as ghc
from Bender.MainMC import *


isDEBUGMC = 0

def has_eve(par,id):
    mom = rg.mother(par)
    if not mom: return 0
    if abs(mom.pdg_id())==id: return mom
    return has_eve(mom,id)

def has_eve_t(par):
    return has_eve(par,6)

##########################

def get_genpar(lmcpar):
    mytool = GaudiPython.AppMgr().toolsvc().create("LoKi::HepMC2MC",interface="IHepMC2MC")
    rels = mytool.mc2HepMC().relations(lmcpar)
    if not rels.size(): return
    return rels[0].to()

############# High-pT leptons from W (no tau lepton included):

def leptons_from_w():
    gaudi = GaudiPython.AppMgr()
    TES = gaudi.evtsvc()
    allpars = TES["Phys/StdAllNoPIDsPions/Particles"]
    mus, els = [],[]
    for ltr in allpars:
        p1 = ghc.track_to_mcp(TES,ltr.proto().track())
        if not p1: continue
        if abs(p1.particleID().pid())==13: # 13 is muon
            if not p1.mother(): continue
            g1 = get_genpar(p1)
            if not g1: continue
            mom = rg.mother(g1)
            if not mom: continue
            if abs(mom.pdg_id())==24: mus.append([ltr,g1]) # 24 is W, asking for mother
        if abs(p1.particleID().pid())==11: # 11 is electron
            if not p1.mother(): continue
            g1 = get_genpar(p1)
            if not g1: continue
            mom = rg.mother(g1)
            if not mom: continue
            if abs(mom.pdg_id())==24: els.append([ltr,g1]) # 24 is W, asking for mother
    leptons=mus
    leptons.extend(els)
    return leptons








############# Jets from W (5 GeV cut on pT by default, MaxRdistance = 0.5):
def jets_from_w():
    mcjets = mcj.make_jets_cpp() # uses JetMaker, defined in mcJet.py, make_jets_cpp(pTcut = 5000, had_par = "had")
    gaudi = GaudiPython.AppMgr()
    TES = gaudi.evtsvc()
    #stdjets = TES['Phys/TopoTagJets/Particles']
    stdjets = TES['Phys/BDTTagJets/Particles']
    out = {}
    for mcjet in mcjets:
        for stdjet in stdjets:
            if not mcj.match_true_rec_jets(mcjet[1],stdjet): continue
            if isDEBUGMC: print "the BDTTagJet is found to match with MC truth"
            skey = stdjet.key()
            for parton in mcjet[0]:
                if abs(rg.mother(parton).pdg_id())==24: # 24 is W
                    if skey in out: out[skey].append(parton) # out is empty? wtf
                    else: out[skey]=[stdjet,parton]
    return out

############# Jets from top (5 GeV cut on pT by default, MaxRdistance = 0.5):
def jets_from_t():
    mcjets = mcj.make_jets_cpp()
    gaudi = GaudiPython.AppMgr()
    TES = gaudi.evtsvc()
    #stdjets = TES['Phys/TopoTagJets/Particles']
    stdjets = TES['Phys/BDTTagJets/Particles']
    out = {}
    for mcjet in mcjets:
        for stdjet in stdjets:
            if not mcj.match_true_rec_jets(mcjet[1],stdjet): continue
            if isDEBUGMC: "the BDTTagJet is found to match a t-jet"
            skey = stdjet.key()
            for parton in mcjet[0]:
                mom = rg.mother(parton)
                if not mom: continue
                if abs(mom.pdg_id())==6: ## looks for a top-quark mother
                    if skey in out: out[skey].append(parton)
                    else: out[skey]=[stdjet,parton]
    return out




def get_t_daughters():
    jetsw = jets_from_w()
    jetst = jets_from_t()
    leptons = leptons_from_w()
    group = {}
    for jkey in jetsw:
        jet = jetsw[jkey][0]
        for parton in jetsw[jkey][1:]:
            w = rg.mother(parton)
            t = has_eve_t(w)
            if not t: continue
            bc = t.barcode()
            if bc in group:
                if "jetsw" in group[bc]: group[bc]["jetsw"].append(jet)
                else: group[bc]["jetsw"]=[jet]
            else: group[bc]={"jetsw":[jet]}
            break
    for jkey in jetst:
        jet = jetst[jkey][0]
        for parton in jetst[jkey][1:]:
            t = rg.mother(parton)
            bc = t.barcode()
            ## by definition, these jets should come from a t! (jets_from_t)
            if bc in group: group[bc]["bjet"] = jet
            else: group[bc]={"bjet":jet}
            break
    for lepton1 in leptons:
        lepton,mclepton = lepton1
        w = rg.mother(mclepton)
        t = has_eve_t(w)
        if not t: continue
        if abs(t.pdg_id())!=6: continue
        bc = t.barcode()
        if bc in group: group[bc]["lepton"]=lepton1
        else: group[bc]={"lepton":lepton1}
    return group

##########################




def define_t_cat(): # I do not know what this does.
    def cat(myd):
        if ("lepton" in myd):
            if "bjet" in myd: return 1
            return 5
        elif ("jetsw" in myd):
            lenjw = len(myd["jetsw"])
            if "bjet" in myd:
                if lenjw==2: return 2
                if lenjw==1: return 3
                return 1000+lenjw
            if lenjw==2: return 6
            if lenjw==1: return 7
            return 2000+lenjw
        elif ("bjet" in myd): return 4
        return 0
    group = get_t_daughters()
    out = {}
    for bc in group: out[bc] = cat(group[bc])
    return out,group



ptCone = SUMCONE(0.5 ,PT,'/Event/Phys/StdAllNoPIDsPions/Particles')


def fillTopDaughters(infos,ret=0):
    jetbranches = ["px","py","pz","phi","eta","pt","p","key","vtx_misspt","vtx_totpt","vtx_key"]
    for branch in TagDir: jetbranches.append(branch)
    for var in jetbranches:
        infos["top1_bjet_"+var]=-1.
        infos["top1_wjet1_"+var]=-1.
        infos["top1_wjet2_"+var]=-1.
        infos["top2_bjet_"+var]=-1.
        infos["top2_wjet1_"+var]=-1.
        infos["top2_wjet2_"+var]=-1.
    for var in ["px","py","pz","phi","eta","pt","p","key","iso","vtx_misspt","vtx_totpt","vtx_key"]:
        infos["top1_lepton_"+var]=-1.
        infos["top1_lepton_id"]=-1
        infos["top2_lepton_"+var]=-1.
        infos["top2_lepton_id"]=-1
    infos["top1_mass"]=-1.
    infos["top1_wmass"]=-1.
    infos["top1_tag"]=0
    infos["top2_mass"]=-1.
    infos["top2_wmass"]=-1.
    infos["top2_tag"]=0
    cats,group = define_t_cat()
    i=1
    for bc in group:
        itop = "top"+str(i)
        infos[itop+"_tag"]=cats[bc]
        if "bjet" in group[bc]:
            fillKin(itop+"_bjet",infos,group[bc]["bjet"])
            fillJetTag(itop+"_bjet",infos,group[bc]["bjet"])
            #infos[itop+"_bjet_maxTopoBDT"]=getTopoBDT(group[bc]["bjet"])
        if "jetsw" in group[bc]:
            j = 1
            for jet in group[bc]["jetsw"]:
                fillKin(itop+"_wjet"+str(j),infos,jet)
                fillJetTag(itop+"_wjet"+str(j),infos,jet)
                #infos[itop+"_wjet"+str(j)+"_maxTopoBDT"]=getTopoBDT(jet)
                if j==2: break
                j+=1
            if len(group[bc]["jetsw"])==2:
                infos[itop+"_wmass"] = inv_mass(group[bc]["jetsw"][0],group[bc]["jetsw"][1])
                if "bjet" in group[bc]:
                    infos[itop+"_mass"] = inv_mass(group[bc]["jetsw"][0],group[bc]["jetsw"][1],group[bc]["bjet"])
        if "lepton" in group[bc]:
            fillKin(itop+"_lepton",infos,group[bc]["lepton"][0])
            infos[itop+"_lepton_iso"]=float(ptCone(group[bc]["lepton"][0])-group[bc]["lepton"][0].pt())
            infos[itop+"_lepton_id"]=group[bc]["lepton"][1].pdg_id()
        i+=1
    if ret: return cats,group


