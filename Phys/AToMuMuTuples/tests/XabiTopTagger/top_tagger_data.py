import os,GaudiPython, sys
from t_tagger_functs import *
from putInTES import *
from Gaudi.Configuration import *
from Bender.MainMC import *
from HltPython.PyTree import *
from JetAccessories.ParticleFlow_Config import ParticleFlowConf
from JetAccessories.JetMaker_Config import JetMakerConf
from ROOT import *

from StrippingConf.StrippingStream import StrippingStream
from StrippingConf.Configuration import StrippingConf

from Configurables import DaVinci, CombineParticles, LoKi__FastJetMaker
import readGen as rg

from LoKiTracks.decorators import *
from LoKiCore.functions import sum

LHCbParticle = GaudiPython.gbl.LHCb.Particle

TRIGGER = True ## TAKE A LOOK
ISGRID = 0
isDEBUG = 0

### Define locations inside the DST:
A1Location = "/Event/Dimuon/Phys/A1MuMuLine/Particles" # real data
#A1Location = "/Event/AllStreams/Phys/A1MuMuA1MuMuLine/Particles" # MC
A1MuonsLocationName = "A1Daughters"
A1MuonsLocation = "/Event/Phys/"+A1MuonsLocationName+"/Particles"

myPyAlgo = "setInTES" # what does this do?
myAlgoLocation = putInTES(myPyAlgo)
myAlgoLocation.A1Location = A1Location
myAlgoLocation.A1MuonsLocation = A1MuonsLocation
DaVinci().UserAlgorithms += [ "PyAlgorithm/"+myPyAlgo ]

### Ban from the jets and leptons the muons from the A1mumu candidates:
pflow = ParticleFlowConf("PF" , _MCCor = False, _params = {"CandidateToBanLocation":[A1MuonsLocation]})    
# for DATA one should put _MCCor = False, this sets up the E/p version
DaVinci().UserAlgorithms += pflow.algorithms

jetmaker = JetMakerConf("StdJets")
DaVinci().UserAlgorithms += jetmaker.algorithms

#from JetTagging.PseudoTopoCandidates_Config import PseudoTopoCandidatesConf
#pseudoTopo = PseudoTopoCandidatesConf("PseudoTopoCands")
#DaVinci().UserAlgorithms += pseudoTopo.algorithms

### b-tagging:
from Configurables import FilterJet
filterJet0 = FilterJet('TopoTagJets')
filterJet0.Inputs = ["Phys/StdJets/Particles"]
filterJet0.tagToolName = 'LoKi__TopoTagDir'
filterJet0.setNewPID = "b"
filterJet0.saveAllJetWithInfo = True

### DaVinci conditions, to be updated:
magnetPolarity = "md"
dataYear = "2012"
tagsDic = {}
tagsDic["2012"] = ('Sim08-20130503-1','Sim08-20130503-1-vc-'+magnetPolarity+'100') # tags for S20 Simulation. Change for S21.

DaVinci().DataType = dataYear
DaVinci().Simulation   = False
#DaVinci().DDDBtag = tagsDic[dataYear][0]
#DaVinci().CondDBtag = tagsDic[dataYear][1]
from Configurables import CondDB
CondDB( LatestGlobalTagByDataType = dataYear ) 
DaVinci().Lumi = True
DaVinci().TupleFile = "DVnTuples.root"
DaVinci().UserAlgorithms += [filterJet0]

if TRIGGER: # L0 trigger, this is not part of the original maker!
    L0SelRepSeq = GaudiSequencer("L0SelRepSeq")
    L0SelRepSeq.MeasureTime = True
    from Configurables import L0SelReportsMaker, L0DecReportsMaker
    L0SelRepSeq.Members += [ L0DecReportsMaker(), L0SelReportsMaker() ]
    DaVinci().UserAlgorithms += [ L0SelRepSeq ]

###############################################

#if not ISGRID:
#     DaVinci().Input = ['PFN:/tmp/cvazquez/00041836_00000007_1.dimuon.dst','PFN:/tmp/cvazquez/00041836_00000021_1.dimuon.dst','PFN:/tmp/cvazquez/00041836_00000035_1.dimuon.dst','PFN:/tmp/cvazquez/00041836_00000049_1.dimuon.dst','PFN:/tmp/cvazquez/00041836_00000063_1.dimuon.dst','PFN:/tmp/cvazquez/00041836_00000080_1.dimuon.dst','PFN:/tmp/cvazquez/00041836_00000095_1.dimuon.dst']
    #DaVinci().Input = ['PFN:/afs/cern.ch/work/c/cvazquez/A0mumu/trying_Xabier/dst/00048273_00000001_2.AllStreams.dst']
    #DaVinci().Input=['LFN:root://eoslhcb.cern.ch//eos//lhcb/MC/2012/ALLSTREAMS.DST/00048273/0000/00048273_00000001_2.AllStreams.dst']
    #DaVinci().Input=['LFN:/lhcb/MC/2012/ALLSTREAMS.DST/00048273/0000/00048273_00000001_2.AllStreams.dst']
def Eostize(thingie):
    x = []#.replace("LFN:", "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod")
    for thing in thingie.Input:
        x.append(thing.replace("LFN:", "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod"))
    thingie.Input = x
importOptions("/afs/cern.ch/work/c/cvazquez/cmtuser/Erasmus_v10r2/Phys/AToMuMuTuples/datacards/real_data/LHCb_Collision12_Beam4000GeVVeloClosedMagDown_Real Data_Reco14_Stripping21_90000000_DIMUON.DST.py")
if not ISGRID: Eostize(EventSelector())

gaudi = GaudiPython.AppMgr()
myAlgo = myAlgoMC(A1MuonsLocationName) # myAlgoMC defined inside putInTES
gaudi.addAlgorithm(myAlgo) # prepare an empty location in TES
gaudi.initialize()

## HACK: SWITCH THE ORDER OF THE ALGORITHMS
topalg = gaudi.TopAlg # what the fuck is TopAlg
topalg.remove(A1MuonsLocationName)
topalg.insert(0,A1MuonsLocationName) # ensure this empty location goes first!!
gaudi.TopAlg = topalg

TES = gaudi.evtsvc()

if TRIGGER:
    from BenderAlgo.extrafunctions import triggerBlock, triggerBlockDaughters
    trigger_algo=AlgoMC("trigger")
    trigger_algo.tistostool = trigger_algo.tool( cpp.ITriggerTisTos , 'TriggerTisTos' )
    trigger_algo.tisTosToolL0= trigger_algo.tool( cpp.ITriggerTisTos , 'L0TriggerTisTos' )

    trigger_algo.l0List = []
    trigger_algo.hlt1List = []
    trigger_algo.hlt2List = []
    trigger_algo.DEBUG = False

###############################################

def fill_ntuple(tup,infos): # I guess infos are the new branches from the tagging?
    def tup_started(tup_): return len(map(lambda x: x.GetName(),tup_.GetListOfBranches())) # gets len of list of names of tuple branches

    if not tup_started(tup): # if there is no tuple
        for el in infos: # infos is a dict of branches ([name] = value) for a new tuple, I guess?
            if type(infos[el])==int or type(infos[el])==long: tup.book(el+"/I")
            elif type(infos[el])==float: tup.book(el+"/F")
            else:
                leni=len(infos[el])
                if type(infos[el][0])==int or type(infos[el][0])==long: tup.book(el+"["+str(leni)+"]/I")
                if type(infos[el][0])==float: tup.book(el+"["+str(leni)+"]/F")
    for el in infos: # set new branches and fill tuple
        tup.set(el,infos[el])
    tup.Fill()

def GECs(infos): # I do not know what TrSOURCE does
    sumpt  = TrSOURCE( 'Rec/Track/Best', TrLONG) >> sum(TrPT)
    sumpx  = TrSOURCE( 'Rec/Track/Best', TrLONG) >> sum(TrPX)
    sumpy  = TrSOURCE( 'Rec/Track/Best', TrLONG) >> sum(TrPY)
    infos["GEC_sumpt_allpvs"] = sumpt()
    infos["GEC_misspt_allpvs"] = sumpx()**2 + sumpy()**2

def fill_trigger_info(infos):
    mylist = ["L0Decision","Hlt1Decision","Hlt2Decision"]
    infos_ = {}
    Done = {"TES":TES,"Candidate":LHCbParticle()}
    triggerBlock(trigger_algo,infos_, Done) # where is this defined?
    for el in infos_:
        if not (el in mylist): continue
        infos[el]=int(infos_[el])

stripline  = "A1"
striplines = {"A1":A1Location}

def toIsMuonLoosePar(particle0):
    pkey = particle0.proto().track().key()
    if not TES["Phys/StdAllLooseMuons/Particles"]: return 0
    ml =  filter(lambda x: x.proto().track().key()==pkey, TES["Phys/StdAllLooseMuons/Particles"])
    if isDEBUG: print "[DEBUG] toIsMuonLoosePar"
    if not len(ml): return 0
    return ml[0]

stabs=[11,13,211,321,2212] # 11 is electron, 13 is muon, 211 is charged pion, 321 is charged kaon, 2212 is proton.

def sumpx(particle):
    pv = getBestPV(particle)
    if not pv: return -1.
    return trpxsum(pv)

def sumpy(particle):
    pv = getBestPV(particle)
    if not pv: return -1.
    return trpysum(pv)

def sumpt(particle):
    pv = getBestPV(particle)
    if not pv: return -1.
    return trptsum(pv)-particle.pt()

def getBestPV(particle0): ### need to add tau leptons?
    if abs(particle0.particleID().pid())==11: # electrons
        particle = particle0
        p2pvTable = TES["Phys/StdAllNoPIDsElectrons/Particle2VertexRelations"]
    elif abs(particle0.particleID().pid())==13: # muons
        particle = toIsMuonLoosePar(particle0)
        if isDEBUG: print "[DEBUG] getBestPV: muons"
        if not particle: return 0
        if isDEBUG: print "[DEBUG] getBestPV: muons, after check if not particle"
        p2pvTable = TES["Phys/StdAllLooseMuons/Particle2VertexRelations"]
    if not p2pvTable: return 0
    if not "relations" in dir(p2pvTable): return 0
    if isDEBUG: print "[DEBUG] getBestPV: after check not relations"
    relPVs = p2pvTable.relations(particle)
    if not relPVs: return 0
    if not relPVs.back(): return 0
    if isDEBUG: print "[DEBUG] getBestPV: after check relPVs"
    mypv = relPVs.back().to()
    if isDEBUG: print "[DEBUG] getBestPV: end"
    return mypv

def misspt(mypar):
    return sqrt(sumpx(mypar)**2 + sumpy(mypar)**2)

def fill_stripping_lines(cand,infos):
    mu1,mu2 = cand.daughters()[0],cand.daughters()[1]
    infos[stripline+"_A1mass"] = cand.measuredMass()
    infos[stripline+"_d1_pt"] = mu1.pt()
    infos[stripline+"_d2_pt"] = mu2.pt()
    infos[stripline+"_d1_key"] = mu1.proto().track().key()
    infos[stripline+"_d2_key"] = mu2.proto().track().key()
    infos[stripline+"_d1_iso"] = float(ptCone(mu1)-mu1.pt())
    infos[stripline+"_d2_iso"] = float(ptCone(mu2)-mu2.pt())
    if isDEBUG: print "got iso"
    infos[stripline+"_d1_sumpt"] = float(sumpt(mu1))
    infos[stripline+"_d1_misspt"] = float(misspt(mu1))
    if isDEBUG: print "got misspt"
    # return the muons in the right format to be banned later...
    mymu1 = toIsMuonLoosePar(mu1)
    mymu2 = toIsMuonLoosePar(mu2)
    return mymu1,mymu2

def fill_gen_event(infos): # is this compatible with Bender?
    infos["evtNum"] = float(TES["Rec/Header"].evtNumber())
    infos["runNum"] = float(TES["Rec/Header"].runNumber())
    infos["nPVs"] = int(TES["Rec/Vertex/Primary"].size())
    infos["nTrs"] = int(TES["Rec/Track/Best"].size())
    infos["nTrsLong"] = int(len(filter(lambda x: x.type()==3,TES["Rec/Track/Best"])))

def go(n):
    tup_mct=PyTree("mct_jets_tuple","mct_jets_tuple")
    tup=PyTree("jets_tuple","jets_tuple")
    for i in range(n):
        c1=gaudi.run(1)
        if not TES["Rec/Track/Best"]: break
        pvs = TES["Rec/Vertex/Primary"]
        if not (pvs and pvs.size()): continue
        pars = TES[A1Location]
        if not (pars and pars.size()): continue
        for par in pars:
            infos = {}
            mu1,mu2 = fill_stripping_lines(par,infos)
            ## ban A1MuMu location
            if isDEBUG: print "after stripping"
            extrainfo = [[],{"leptons":[mu1,mu2]}]
            topBDTInfo_ban(infos,extrainfo)
            if isDEBUG: print "after topBDT"
            GECs(infos)
            if isDEBUG: print "after GECs"
            fill_gen_event(infos)
            if isDEBUG: print "after gen"
            fill_trigger_info(infos)
            if isDEBUG: print "after trigger"
            fill_ntuple(tup,infos)
            if isDEBUG: print "after fill"
    return tup

def finalize():
    gaudi.stop()
    gaudi.finalize()

fera=TFile("eraseme.root","RECREATE")
if ISGRID: mytup=go(10000000)
else: mytup=go(25000)

if not ISGRID:
    fnew=TFile("top_tagger_tup.root","RECREATE")
    mytupw=mytup.CopyTree("1")
    mytupw.Write()
    fnew.Close()
    finalize()

def search_case(n=1000):
    c1 = 0
    for i in range(n):
        bla = gaudi.run(1)
        pars = TES[A1Location]
        if not (pars and pars.size()): continue
        cbreak = 0
        par = pars[0]
        stdjets = TES["Phys/TopoTagJets/Particles"]
        if stdjets and stdjets.size(): c1+=1
        mu1k,mu2k = map(lambda x: x.proto().track().key(),par.daughters())
        for jet in stdjets:
            muj=filter(lambda x: x.proto(), jet.daughters())
            muj=filter(lambda x: x.proto().track(), muj)
            mujk = map(lambda x: x.proto().track().key(),muj)
            if (mu1k in mujk) or (mu2k in mujk):
                print "FOUND"
                cbreak = 1
        if cbreak: break
    print c1
