import GaudiPython

# Necessary to use HepMC iterators
class irange(object):
    def __init__(self,b,e):
        self.begin, self.end = b, e
    def __iter__(self):
        it = self.begin
        while it != self.end:
            yield it.__deref__()
            it.__postinc__(1)

def daughters(particle):
    out=[]
    if not particle.end_vertex(): return out
    it = particle.end_vertex().particles_out_const_begin()
    for i in range(particle.end_vertex().particles_out_size()):
        out.append(it.__deref__())
        it.__postinc__(1)
    return out

def prod_vertex(particle):
    out={"in":[],"out":[]}
    if not particle.production_vertex(): return out
    it = particle.production_vertex().particles_out_const_begin()
    for i in range(particle.production_vertex().particles_out_size()):
        mypar = it.__deref__()
        out["out"].append([mypar.pdg_id(),mypar.barcode()])
        it.__postinc__(1)

    it = particle.production_vertex().particles_in_const_begin()
    for i in range(particle.production_vertex().particles_in_size()):
        mypar = it.__deref__()
        out["in"].append([mypar.pdg_id(),mypar.barcode()])
        it.__postinc__(1)
    return out

def mother(particle):
    if not particle.production_vertex(): return
    if not particle.production_vertex().particles_in_const_begin(): return
    mymother=particle.production_vertex().particles_in_const_begin().__deref__()
    if (not mymother): return
    if mymother.pdg_id()==92: return mother(mymother)
    if mymother.pdg_id()==particle.pdg_id(): return mother(mymother)
    return mymother

def rmother(particle):
    mymother=mother(particle)
    if not mymother: return particle
    return rmother(mymother)

def get_signal(pdg_id):
    TES=GaudiPython.AppMgr().evtsvc()
    hepmc = TES['Gen/HepMCEvents']
    if not hepmc: return
    myparticles=[]
    for i in hepmc:
        genevent = i.pGenEvt()
        # Iterate over all particles in event
        for particle in irange(genevent.particles_begin(),
                               genevent.particles_end()):
            # PDG Id
            pdg=abs(particle.pdg_id())
            if pdg==pdg_id: myparticles.append(particle)
    return myparticles

def get_genpar_v0(lmcpar):
    def myform(numb):
        return "%.1f" %(round(numb,1))
    
    TES=GaudiPython.AppMgr().evtsvc()
    hepmc = TES['Gen/HepMCEvents']
    if not hepmc: return
    myparticles=[]
    for i in hepmc:
        genevent = i.pGenEvt()
        # Iterate over all particles in event
        for particle in irange(genevent.particles_begin(),
                               genevent.particles_end()):
            # PDG ID
            if myform(particle.momentum().pz())==myform(lmcpar.momentum().pz())\
               and myform(particle.momentum().pt())==myform(lmcpar.pt()): return particle
    return 0

def get_genpar(lmcpar):
    mytool=GaudiPython.AppMgr().toolsvc().create("LoKi::HepMC2MC",interface="IHepMC2MC")
    rels=mytool.mc2HepMC().relations(lmcpar)
    if not rels.size(): return
    return rels[0].to()

def get_mcpar(genpar):
    TES=GaudiPython.AppMgr().evtsvc()
    mypid = genpar.pdg_id()
    l0 = filter(lambda x: x.particleID().pid()==mypid,TES["MC/Particles"])    
    for el in l0:
        if get_genpar(el)==genpar: return el
    return 0
