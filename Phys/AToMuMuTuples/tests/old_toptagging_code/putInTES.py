#! /usr/bin/env python
import GaudiPython
from Bender.MainMC import *

class myAlgoMC(AlgoMC):
    def analyse(self):
        return SUCCESS

class putInTES(GaudiPython.PyAlgorithm):
    def isInTES(self,par):
        TES = GaudiPython.AppMgr().evtsvc()
        mypars = TES[self.A1MuonsLocation]
        if not mypars: return 0
        if not mypars.size(): return 0
        for mypar in mypars:
            if mypar.key()==par.key(): return 1
        return 0
    def execute(self): 
        debug = False
        if debug: print "ready to write"
        if not ("A1Location" in dir(self)): raise Exception("A1 location not defined")
        if not ("A1MuonsLocation" in dir(self)): raise Exception("A1 muons location not defined")
        TES = GaudiPython.AppMgr().evtsvc()
        A1s = TES[self.A1Location]
        if not (A1s and A1s.size()): return SUCCESS
        for A1 in A1s:
            for par in A1.daughters():
                if not isinstance(par,GaudiPython.gbl.LHCb.Particle):
                    print "ERROR: element not LHCbParticle"
                    continue
                if not self.isInTES(par): TES[self.A1MuonsLocation].add(par)

        return SUCCESS

