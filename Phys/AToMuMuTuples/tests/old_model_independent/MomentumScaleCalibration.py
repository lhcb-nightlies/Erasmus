"""
Configurable to set up momentum scale calibration
"""
__author__ = "Pieter David <pieter.david@cern.ch>"

from LHCbKernel.Configuration import LHCbConfigurableUser
from Configurables import TrackScaleState, SubstitutePID

class MomentumScaleCalibration(LHCbConfigurableUser):
    """
    Configurable to set up momentum scale calibration
    """
    __slots__ = {
        # for TrackScaleState
          "TrackLocation"    : "Rec/Track/Best"
        , "DeltaScale"       : 0.0
        # for FitDecayTrees/SubstitutePID
        , "Inputs"           : []
        , "Output"           : ""
        , "RootInTES"        : ""
        , "Preambulo"        : []
        , "Code"             : ""
        , "UsePVConstraints" : False
        , "MassConstraints"  : []
        , "Substitutions"    : { "J/psi(1S) -> mu+ mu-" : "KS0" }
        , "MaxChi2PerDoF"    : 10.
        }
    _propertyDocDct = {
        # for TrackScaleState
          "TrackLocation"    : "TES location of tracks to rescale"
        , "DeltaScale"       : "DeltaScale for TrackScaleState"
        # for FitDecayTrees/SubstitutePID
        , "Inputs"           : "Property passed on to FitDecayTrees/SubstitutePID"
        , "Output"           : "Property passed on to FitDecayTrees/SubstitutePID"
        , "RootInTES"        : "Property passed on to FitDecayTrees/SubstitutePID"
        , "Preambulo"        : "Property passed on to FitDecayTrees/SubstitutePID"
        , "Code"             : "Property passed on to FitDecayTrees/SubstitutePID"
        , "UsePVConstraints" : "Property passed on to FitDecayTrees/SubstitutePID"
        , "MassConstraints"  : "Property passed on to FitDecayTrees/SubstitutePID"
        , "Substitutions"    : "Property passed on to FitDecayTrees/SubstitutePID"
        , "MaxChi2PerDoF"    : "Property passed on to FitDecayTrees/SubstitutePID"
        }

    __used_configurables__ = [ ( TrackScaleState, None )
                             , ( SubstitutePID  , None ) ]

    def getUsedConf(self, type):
        return type(self._instanceName(type))

    def initialize(self):
        super( MomentumScaleCalibration, self ).initialize()

        self += self.getUsedConf( TrackScaleState )
        self += self.getUsedConf( SubstitutePID )

    def __apply_configuration__(self):

        scaler = self.getUsedConf( TrackScaleState )

        scaler.Input = self.getProp("TrackLocation")
        scaler.DeltaScale    = self.getProp("DeltaScale")


        refitter = self.getUsedConf( SubstitutePID )

        self.setOtherProps(refitter, [ "Inputs", "Output", "RootInTES", "Preambulo", "Code", "UsePVConstraints", "MassConstraints", "Substitutions", "MaxChi2PerDoF" ])
