"""
Configurable J/Psi (Upsilon, A0, Z etc) DecayTreeTuple setup
"""
__author__ = "Pieter David <pieter.david@cern.ch>"

from LHCbKernel.Configuration import LHCbConfigurableUser, INFO, DEBUG, VERBOSE
from Configurables import GaudiSequencer, FilterDesktop, DecayTreeTuple, DaVinci, LoKi__Hybrid__MCTupleTool, AddExtraInfo, ConeVariablesForEW, TrackMasterFitter, LoKi__Hybrid__TupleTool
from DecayTreeTuple.Configuration import *
from PFJetIsolationConf import PFJetIsolationConf
from itertools import ifilter, imap

def makeRelInfoLoc(candLoc, toolLoc):
    if candLoc.endswith("Particles"):
        return candLoc.replace("Particles", toolLoc)
    else:
        return "/".join(chain(candLoc.split("/"), (toolLoc,)))

class JPsiTupleConf(LHCbConfigurableUser):
    """
    Configurable J/Psi (Upsilon, A0, Z etc) DecayTreeTuple setup
    """
    __slots__ = {
          "MicroDST"             : False
        , "Inputs"               : []
        , "InputPrimaryVertices" : "Rec/Vertex/Primary"
        , "RootInTES"            : ""
        , "RightSign"            : True
        , "MC"                   : False
        , "MotherName"           : "J/psi(1S)"
        , "IsolationConeSizes"   : [ 0.5, 0.7 ]
        , "RelatedInfoTools"     : [ { "Type"           : "RelInfoBs2MuMuBIsolations"
                                     , "Location"       : "BSMUMUVARIABLES"
                                     , "RecursionLevel" : 0
                                     , "Variables"      : ['BSMUMUCDFISO', 'BSMUMUOTHERBMAG', 'BSMUMUOTHERBANGLE', 'BSMUMUOTHERBBOOSTMAG', 'BSMUMUOTHERBBOOSTANGLE', 'BSMUMUOTHERBTRACKS', 'BSMUMUPARTID', 'BSMUMUTOPID'] }
                                   , { "Type"           : "RelInfoBs2MuMuTrackIsolations"
                                     , "Locations"      : { 'Phys/UpsilonCalibrated/Particles' :  ['Muon1iso', 'Muon2iso'] }
                                     , "RecursionLevel" : 1
                                     , "Variables"      : [ 'BSMUMUTRACKPLUSISO', 'BSMUMUTRACKPLUSISOTWO', 'BSMUMUTRACKID', 'BSMUMUTRACKTOPID', 'ISOTWOBODYQPLUS', 'ISOTWOBODYMASSISOPLUS', 'ISOTWOBODYCHI2ISOPLUS', 'ISOTWOBODYISO5PLUS' ] }
                                   ]
        , "NTupleDir"            : "DefaultName"
        }

    _propertyDocDct = {
          "MicroDST"             : "Set to True for running on MicroDST"
        , "Inputs"               : "Input TES location"
        , "InputPrimaryVertices" : "PV location"
        , "RootInTES"            : "root TES location (for microDST)"
        , "RightSign"            : "Set to False for running on same-sign muon combinations"
        , "MC"                   : "Set to True when running on MC"
        , "MotherName"           : "Mother particle name for the DecayTreeTuple decay descriptor (for Z or upsilon MC)"
        }

    __used_configurables__ = [ (FilterDesktop , None)
                             , (DecayTreeTuple, None)
                             , (FilterDesktop , "PFChargedFilter")
                             , (FilterDesktop , "PFNeutralFilter")
                             , (AddExtraInfo  , None)
                             , (GaudiSequencer, None)
                             , (PFJetIsolationConf, "PFJetIsolation") ]

    def getUsedConf(self, type):
        return type(self._instanceName(type))

    vertexVarNamesAndFunctors_ALL = [
              ("X"        , "VX"                  )
            , ("Y"        , "VY"                  )
            , ("Z"        , "VZ"                  )
            , ("RHO"      , "VX_BEAMSPOTRHO(1.0)" )
            , ("CHI2"     , "VCHI2"               )
            , ("NDOF"     , "VDOF"                )
            , ("PCHI2"    , "VPCHI2"              )
            # covariance matrix
            , ("VARX"     , "VCOV2(0,0)"          )
            , ("COVXY"    , "VCOV2(0,1)"          )
            , ("COVXZ"    , "VCOV2(0,2)"          )
            , ("VARY"     , "VCOV2(1,1)"          )
            , ("COVYZ"    , "VCOV2(1,2)"          )
            , ("VARZ"     , "VCOV2(2,2)"          )
            ]
    vertexVarNamesAndFunctors_PV = [
              ("NTRACKS"  , "RV_TrNUM(TrALL)"     )
            , ("NBWTRACKS", "RV_TrNUM(TrBACKWARD)")
            ]

    def addTupleToolKinematic(self, branch):
        """
        LoKi-based Kinematic tuple tool
        """
        ##################################################
        #                   KINEMATICS                   #
        ##################################################
        kinematic = branch.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Kinematic")
        kinematic.Variables = {
              "P"              : "P"
            , "PT"             : "PT"
            , "PE"             : "E"
            , "PX"             : "PX"
            , "PY"             : "PY"
            , "PZ"             : "PZ"
            , "PERR2"          : "PERR2"
            , "PTERR2"         : "PTERR2"
            , "ETA"            : "ETA"
            , "THETA"          : "atan ( (PT/PZ))"
            , "PHI"            : "PHI"
            }

    def addTupleToolMass(self, branch):
        """
        LoKi-based Mass tuple tool
        """
        ##################################################
        #                      MASS                      #
        ##################################################
        mass = branch.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Mass")
        mass.Variables = {
              "MM"             : "MM"
            , "M"              : "M"
            , "M2ERR2"         : "M2ERR2"
            }

    def addTupleToolTrackInfo(self, branch):
        """
        LoKi-based TrackInfo tuple tool
        """
        trackInfo = branch.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_TrackInfo")
        trackInfo.Preambulo = [ "from LoKiTracks.decorators import *" ]
        trackInfo.Variables = {
            # in standard TupleToolTrackInfo
            #, "TRCHI2DOF"      : "TRCHI2DOF"
              "TRTYPE"         : "TRTYPE"
            , "TRPCHI2"        : "TRPCHI2"
            , "TRGHOSTPROB"    : "TRGHOSTPROB"
            , "TRHISTORY"      : "TRHISTORY"
            # in verbose TupleToolTrackInfo
            , "TRCHI2"         : "TRCHI2"
            , "TRNDOF"         : "TRCHI2/TRCHI2DOF"
            #, "TRVELOCHI2DOF"  : "TRFUN( TrFITVELOCHI2 / TrFITVELONDOF )"
            #, "TRTCHI2DOF"     : "TRFUN( TrFITTCHI2 / TrFITTNDOF )"
            # added / replacement
            , "TRFITVELOCHI2"  : "TRFUN( TrFITVELOCHI2 )"
            , "TRFITVELONDOF"  : "TRFUN( TrFITVELONDOF )"
            , "TRFITTCHI2"     : "TRFUN( TrFITTCHI2 )"
            , "TRFITTNDOF"     : "TRFUN( TrFITTNDOF )"
            , "TRFITMATCHCHI2" : "TRFUN( TrFITMATCHCHI2 )"
            , "TRFIRSTHITZ"    : "TRFUN( TrFIRSTHITZ )"
            , "TRKLDIST"       : "CLONEDIST"
            , "TRLIKELIHOOD"   : "TRLIKELIHOOD"
            }

    def addTupleToolTrackIsolation(self, branch):
        """
        Track isolation tupletool
        """
        trackIsolation = branch.addTupleTool("TupleToolTrackIsolation/MuonIsolation")
        trackIsolation.MinConeAngle = 0.50
        trackIsolation.MaxConeAngle = 1.50
        trackIsolation.StepSize     = 0.25

    def addTupleToolEWCone(self, branch):
        """
        LoKi-based tuple tool to get the EW cone variables from the extra info
        """
        coneSizeAlg = self.getUsedConf( AddExtraInfo )
        if len(coneSizeAlg.getProp("Tools")) > 0:
            ewCone = branch.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_EWCone")
            for coneToolFullName in coneSizeAlg.getProp("Tools"):
                shortToolName = coneToolFullName.split("/")[1]
                coneTool = getattr(coneSizeAlg, shortToolName)
                coneNumber = coneTool.getProp("ConeNumber")
                ewCone.Variables.update(dict(
                        ( "_".join((shortToolName, name))
                        , "INFO(LHCb.Particle.EWCone{0:d}Index+{1:d}, {2:s})".format(coneNumber, j, defaultValue)
                        ) for j, (name, defaultValue) in enumerate([
                                ("angle"  , "-1.")
                              , ("mult"   , "-1.")
                              , ("px"     , "-1.e16")
                              , ("py"     , "-1.e16")
                              , ("pz"     , "-1.e16")
                              , ("vp"     , "-1.e16")
                              , ("vpt"    , "-1.e16")
                              , ("sp"     , "-1.e16")
                              , ("spt"    , "-1.e16")
                              , ("tp"     , "-1.e16")
                              , ("tpt"    , "-1.e16")
                              , ("minpte" , "-1.e16")
                              , ("maxpte" , "-1.e16")
                              , ("minptmu", "-1.e16")
                              , ("maxptmu", "-1.e16")
                              , ("nmult"  , "-1.")
                              , ("npx"    , "-1.e16")
                              , ("npy"    , "-1.e16")
                              , ("npz"    , "-1.e16")
                              , ("nvp"    , "-1.e16")
                              , ("nvpt"   , "-1.e16")
                              , ("nsp"    , "-1.e16")
                              , ("nspt"   , "-1.e16")
                              # prepend 0 to name to disable a branch (without breaking the j count)
                              ]) if not name.startswith("0")
                    ))

    def addTupleToolPID(self, branch):
        """
        LoKi-based PID tuple tool
        """
        pid = branch.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_PID")
        pid.Variables = {
              "HASMUON"        : "switch(HASMUON, 1., 0.)"
            , "ISMUON"         : "switch(ISMUON , 1., 0.)"
            # PID likelihood variables
            , "PIDe"           : "PIDe"
            , "PIDmu"          : "PIDmu"
            , "PIDk"           : "PIDK"
            , "PIDpi"          : "PIDpi"
            , "PIDp"           : "PIDp"
            # neural network variables
            , "PROBNNe"        : "PROBNNe"
            , "PROBNNmu"       : "PROBNNmu"
            , "PROBNNk"        : "PROBNNk"
            , "PROBNNpi"       : "PROBNNpi"
            , "PROBNNp"        : "PROBNNp"
            , "PROBNNghost"    : "PROBNNghost"
            # for ghost studies: some calorimeter inputs to PID
            , "CaloEcalE" : "PPFUN(PP_CaloEcalE)"
            , "CaloHcalE" : "PPFUN(PP_CaloHcalE)"
            , "CaloPrsE"  : "PPFUN(PP_CaloPrsE )"
            , "CaloSpdE"  : "PPFUN(PP_CaloSpdE )"
            , "EcalPIDmu" : "PPFUN(PP_EcalPIDmu)"
            , "HcalPIDmu" : "PPFUN(PP_HcalPIDmu)"
            }
        if not self.getProp("RightSign"):
            pid.Variables["Q"] = "Q"

    def addTupleToolMuonIP(self, branch):
        """
        Muon IP to primary vertices
        """
        muonip = branch.addTupleTool("TrackGeometryTupleTool")
        muonip.BasicParticle = True
        muonip.DistanceCalculator = "LoKi::DistanceCalculator"

    def addTupleToolTISTOS(self, branch, triggers):
        """
        TISTOS tuple tool
        """
        tistos = branch.addTupleTool("TupleToolTISTOS/TISTOSJPsi")
        tistos.Verbose = True
        tistos.TriggerList = triggers

    def addTupleToolGeometryInfo(self, branch, trackOverlap=False):
        """
        LoKi-based GeometryInfo tuple tool, and a small custom C++ TupleTool
        """
        geo = branch.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Geometry")
        geo.Variables = {
              "IP"             : "BPVIP()"
            , "IPCHI2"         : "BPVIPCHI2()"

            , "BPV_VD"         : "BPVVD"
            , "BPV_VDCHI2"     : "BPVVDCHI2"
            , "BPV_VDRHO"      : "BPVVDRHO"
            , "BPV_VDZ"        : "BPVVDZ"
            , "BPV_DIRA"       : "BPVDIRA"

            , "BPV_DLS"        : "BPVDLS"
            , "BPV_LT"         : "BPVLT()"
            , "BPV_LTSIGNCHI2" : "BPVLTSIGNCHI2()"
            , "BPV_LTFITCHI2"  : "BPVLTFITCHI2()"

            , "DTF_CHI2NDOF"   : "DTF_CHI2NDOF(True)"
            }
        def addVertexInfo( variablesDict, leafPrefix, adapter, varsAndFunctors ):
            variablesDict.update( dict( ( "%s_%s" % (leafPrefix, varName), "%s(%s)" % (adapter, varFunctor) ) for varName, varFunctor in varsAndFunctors ) )

        addVertexInfo( geo.Variables, "BPV"  , "BPV"   , JPsiTupleConf.vertexVarNamesAndFunctors_ALL+JPsiTupleConf.vertexVarNamesAndFunctors_PV )
        addVertexInfo( geo.Variables, "ENDVX", "VFASPF", JPsiTupleConf.vertexVarNamesAndFunctors_ALL )

        geo2 = branch.addTupleTool("TrackGeometryTupleTool")
        geo2.BasicParticle = False
        geo2.DistanceCalculator = "LoKi::DistanceCalculator"
        geo2.DaughterNames = [ "MuP", "MuM" ]

    def addTupleToolTrackOverlap(self, branch):
        """
        A custom C++ TupleTool to study overlapping hits, split tracks etc.
        """
        overlap = branch.addTupleTool("TwoTrackOverlapTupleTool")
        overlap.Verbose = False
        overlap.DaughterNames = [ "MuP", "MuM" ]
        masterFitName = "MasterFitter"
        overlap.addTool( TrackMasterFitter, masterFitName )
        masterFit = getattr(overlap, masterFitName)
        from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter
        masterFit = ConfiguredMasterFitter(masterFit, MaxNumberOutliers=0)
        overlap.CombinedVeloFitter = masterFit.getTitleName()
        overlap.FitAllCombinedVeloSegments = True
        overlap.DetermineSegmentCurvatures = True
        overlap.TrackFitter = masterFit.getTitleName()
        overlap.VeloSegmentFitter = masterFit.getTitleName()
        overlap.TSegmentFitter = masterFit.getTitleName()
        #overlap.OutputLevel = DEBUG
        #masterFit.OutputLevel = INFO

    def makeMCTupleToolMotherID(self, mcTool):
        import re
        motherNameForLeaf = re.compile("[\W_]+").sub("", self.getProp("MotherName"))
        toolName = "MotherID"
        mcTool.addTool( LoKi__Hybrid__MCTupleTool, name=toolName )
        tool = getattr(mcTool, toolName)
        tool.Preambulo = [ "from LoKiCore.functions import *" ]
        tool.Variables = {
                "MOTHERID"                   : "MCMOTHER( MCID, 0 )"
              , "MOTHERCTAU"                 : "MCMOTHER( MCCTAU, 0 )"
              , "MOTHERPT"                   : "MCMOTHER( MCPT, 0 )"
              , "MOTHERETA"                  : "MCMOTHER( MCETA, 0 )"
              , "FROM%s" % motherNameForLeaf : "switch( MCINANCESTORS(MCABSID == \"%s\"), 1., 0.)" % self.getProp("MotherName")
              }
        return tool.getFullName()

    def addTupleToolMCTruthMuon(self, branch):
        """
        MCTruth tuple tool for the muons
        """
        mcTruth = branch.addTupleTool("TupleToolMCTruth/TruthMuon")
        mcTruth.ToolList = [
              self.makeMCTupleToolMotherID(mcTruth)
            ##  makeMCTupleToolKinematic()
              ]

    def addTupleToolAllPVCombinations(self, branch):
        allPVComb = branch.addTupleTool("AllPVCombinationsTupleTool")

    def addTupleToolPFJetIsolation(self, branch, seededJetsLocation, bannedJetsLocation):
        pfJetTrackIsolation = branch.addTupleTool("PFJetTrackIsolationTupleTool")
        #pfJetTrackIsolation.OutputLevel = DEBUG
        pfJetTrackIsolation.AssociateToPV = False
        pfJetTrackIsolation.SeededJets = seededJetsLocation
        pfJetTrackIsolation.BannedJets = bannedJetsLocation
        varsWithSeed = {
                    "PT"          : "switch(VALID, PT , -1.)"
                  , "ETA"         : "switch(VALID, ETA, -100.)"
                  , "PHI"         : "switch(VALID, PHI, -100.)"
                  # JetID
                  , "NTRACKS"     : "JNTRACKS"
                  , "NWITHPVINFO" : "JNWITHPVINFO"
                  , "NSATCALO"    : "JNSATCALO"
                  , "CPF"         : "JCPF"
                  , "WIDTH"       : "JWIDTH"
                  }
        bannedJetVarToolName = "JetVarsBanned"
        pfJetTrackIsolation.addTool(LoKi__Hybrid__TupleTool, bannedJetVarToolName)
        bannedJetVarTool = getattr(pfJetTrackIsolation, bannedJetVarToolName)
        pfJetTrackIsolation.SeededJetVariableTool = bannedJetVarTool.getTitleName()
        bannedJetVarTool.Preambulo.append("from DisplVertices.JetFunctions import *")
        bannedJetVarTool.Variables = varsWithSeed
        varsWithoutSeed = dict(varsWithSeed)
        varsWithoutSeed.update({
                  # JetID
                    "N90"         : "JN90"
                  , "MTF"         : "JMTF"
                  , "MPT"         : "JMPT"
                  , "MNF"         : "INFO(LHCb.JetIDInfo.MNF, -1.)"
                  })
        jetVarToolName = "JetVars"
        pfJetTrackIsolation.addTool(LoKi__Hybrid__TupleTool, jetVarToolName)
        jetVarTool = getattr(pfJetTrackIsolation, jetVarToolName)
        pfJetTrackIsolation.JetVariableTool = jetVarTool.getTitleName()
        jetVarTool.Preambulo.append("from DisplVertices.JetFunctions import *")
        jetVarTool.Variables = varsWithoutSeed

    def addTupleToolB2MuMuVariables(self, branch):
        candLoc = self.getUsedConf(FilterDesktop).getProp("Output")
        b2mumuTool = branch.addTupleTool("LoKi__Hybrid__TupleTool/B2MuMu_B")
        bInfoLoc = makeRelInfoLoc(candLoc, "BSMUMUVARIABLES")
        bVars = ("CDFISO", "OTHERBMAG", "OTHERBANGLE", "OTHERBBOOSTMAG", "OTHERBBOOSTANGLE", "OTHERBTRACKS")
        muVars = { "ISOGIAMPI" : "PLUSISO", "ISONEW2" : "PLUSISOTWO" }
        b2mumuTool.Variables = dict(imap( lambda v : ("B2MuMu_{0}".format(v), "RELINFO('{loc}', 'BSMUMU{0}', -1.)".format(v,loc=bInfoLoc)), bVars))
        makeMuTrackVar = lambda v,pid : "switch( RELINFO('{loc1}', 'BSMUMUTRACKID', 0.) == {pid:d}, RELINFO('{loc1}', 'BSMUMUTRACK{0}', -1.), RELINFO('{loc2}', 'BSMUMUTRACK{0}', -1.) )".format(v, loc1=makeRelInfoLoc(candLoc,"Muon1iso"), loc2=makeRelInfoLoc(candLoc,"Muon2iso"), pid=pid)
        b2mumuTool.Variables.update(imap( lambda (k,v) : ("MuP_B2MuMu_{0}".format(k), makeMuTrackVar(v, -13)), muVars.iteritems()))
        b2mumuTool.Variables.update(imap( lambda (k,v) : ("MuM_B2MuMu_{0}".format(k), makeMuTrackVar(v,  13)), muVars.iteritems()))

    def allConfTupleTools(self, branch):
        """
        IParticleTupleTools for all branches

        Kinematic
        """
        self.addTupleToolKinematic(branch)

    def muonConfTupleTools(self, branch):
        """
        IParticleTupleTools for muon branches

        TrackInfo, PID, TISTOS and MCTruth
        """

        self.addTupleToolTrackInfo(branch)

        self.addTupleToolPID(branch)

        self.addTupleToolTISTOS(branch, [
              "L0MuonDecision"
            , "L0DiMuonDecision"

            , "Hlt1SingleMuonHighPTDecision"
            , "Hlt1TrackMuonDecision"
            , "Hlt1TrackAllL0Decision"
            , "Hlt1VertexDisplVertexDecision"

            , "Hlt2SingleMuonHighPTDecision"
            , "Hlt2SingleMuonDecision"
            , "Hlt2TopoMu.*SimpleDecision"
            , "Hlt2TopoMu.*BBDTDecision"
            ] )

        ##self.addTupleToolTrackIsolation(branch)
        self.addTupleToolEWCone(branch)

        self.addTupleToolMuonIP(branch)

        if self.getProp("MC"):
            self.addTupleToolMCTruthMuon(branch)

        self.addTupleToolAllPVCombinations(branch)

        pfJetIsoConf = PFJetIsolationConf("PFJetIsolation")
        self.addTupleToolPFJetIsolation(branch, pfJetIsoConf.getProp("OutputJetsConstituents"), pfJetIsoConf.getProp("OutputJetsBanned"))

    def motherConfTupleTools(self, branch):
        """
        IParticleTupleTools for the dimuon combination

        Geometry and TISTOS
        """

        self.addTupleToolGeometryInfo(branch)
        self.addTupleToolTrackOverlap(branch)

        self.addTupleToolTISTOS(branch, [
              "L0MuonDecision"
            , "L0DiMuonDecision"

            , "Hlt1SingleMuon.*Decision"
            , "Hlt1DiMuon.*Decision"
            , "Hlt1DiMuonHighMassDecision"

            , "Hlt2DiMuonBDecision"
            , "Hlt2DiMuon.*Decision"
            ] )

        self.addTupleToolMass(branch)

        self.addTupleToolEWCone(branch)

        self.addTupleToolAllPVCombinations(branch)

        pfJetIsoConf = PFJetIsolationConf("PFJetIsolation")
        self.addTupleToolPFJetIsolation(branch, pfJetIsoConf.getProp("OutputJetsComposite"), pfJetIsoConf.getProp("OutputJetsBanned"))

        self.addTupleToolB2MuMuVariables(branch)

    def initialize(self):
        super( JPsiTupleConf, self ).initialize()

        self += self.getUsedConf( FilterDesktop )
        self += FilterDesktop("PFChargedFilter")
        self += FilterDesktop("PFNeutralFilter")
        self += self.getUsedConf( GaudiSequencer ) ## for AddExtraInfo and AddRelatedInfo tools
        for conf in PFJetIsolationConf("PFJetIsolation").__used_instances__:
            self += conf
        self += self.getUsedConf( DecayTreeTuple )

    def __apply_configuration__(self):

        # common settings for the DaVinciAlgorithms

        presel = self.getUsedConf( FilterDesktop )
        tup = self.getUsedConf( DecayTreeTuple )
        for dvAlg in presel, tup:
            if self.getProp("MicroDST"):
                self.setOtherProp(presel, "RootInTES")

        ### Pre-selection

        self.setOtherProp(presel, "Inputs")
        presel.Code = "DECTREE('{0}')".format("X -> mu+ mu-" if self.getProp("RightSign") else "[ X -> mu- mu- ]CC"
)
        presel.CloneFilteredParticles = False
        presel.Output = "Phys/UpsilonPreselected/Particles"

        ### Add EW cone variables to extra info

        coneSizeAlg = self.getUsedConf( AddExtraInfo )
        self.setOtherProp(coneSizeAlg, "Inputs") ## presel.Output are shared J/psi, so the extra info gets picked up correctly for all
        for i, coneSize in enumerate(self.getProp("IsolationConeSizes")[:4], 1):
            coneToolName = ("EWCone%.2f" % coneSize).replace(".", "")
            coneSizeAlg.addTool( ConeVariablesForEW, name=coneToolName )
            coneTool = getattr( coneSizeAlg, coneToolName )
            coneSizeAlg.getProp("Tools").append(coneTool.getFullName())
            coneTool.ConeNumber = i
            coneTool.ConeAngle = coneSize
        self.getUsedConf(GaudiSequencer).Members.append(coneSizeAlg)

        # Add PFParticle cone (if there are extrainfo fields left)
        pfChargedLoc, pfNeutralLoc = "Phys/ChargedPFParticles/Particles", "Phys/NeutralPFParticles/Particles"
        pfChargedFilter = FilterDesktop("PFChargedFilter")
        pfNeutralFilter = FilterDesktop("PFNeutralFilter")
        for pfFilter in (pfChargedFilter, pfNeutralFilter):
            pfFilter.Inputs = [ "Phys/PFParticles/Particles" ]
            pfFilter.Preambulo.append("from DisplVertices.JetFunctions import *")
            pfFilter.CloneFilteredParticles = False
            pfFilter.UseP2PVRelations = False
            pfFilter.WriteP2PVRelations = False
            pfFilter.ForceP2PVBuild = False
            #pfFilter.OutputLevel = DEBUG
            #pfFilter.PropertiesPrint = True
        pfChargedFilter.Code = "in_list( PFTYPE, [ {0} ] )".format(", ".join("LHCb.ParticleFlowType.{0}".format(tp) for tp in ("ChargedHadron", "Muon", "Electron", "Charged0Momentum")))
        pfChargedFilter.Output = pfChargedLoc
        pfNeutralFilter.Code = "in_list( PFTYPE, [ {0} ] )".format(", ".join("LHCb.ParticleFlowType.{0}".format(tp) for tp in ("Photon", "MergedPi0", "ResolvedPi0", "NeutralHadron", "NeutralRecovery", "V0", "BadPhoton", "IsolatedPhoton")))
        pfNeutralFilter.Output = pfNeutralLoc

        if len(self.getProp("IsolationConeSizes")) < 4:
            coneToolName = "PFCone07"
            coneSizeAlg.addTool( ConeVariablesForEW, name=coneToolName )
            coneTool = getattr( coneSizeAlg, coneToolName )
            coneSizeAlg.getProp("Tools").append(coneTool.getFullName())
            coneTool.ConeNumber = 4
            coneTool.ConeAngle = 0.7
            coneTool.TrackType = 3
            coneTool.ExtreParticlesLocation = pfChargedLoc.split("/")[1]
            coneTool.ExtrePhotonsLocation = pfNeutralLoc.split("/")[1]

        ### Add PFJet isolation
        pfJetIsoConf = PFJetIsolationConf("PFJetIsolation")
        pfJetIsoConf.Inputs = [ presel.getProp("Output") ]
        self.setOtherProp(pfJetIsoConf, "InputPrimaryVertices")

        ### Add B2MuMu RelatedInfo tools
        from Configurables import AddRelatedInfo
        for i, toolConf in enumerate(self.getProp("RelatedInfoTools")):
            outLoc = presel.getProp("Output")

            relatedInfoAlg = AddRelatedInfo("{0}_RelatedInfo{1:d}".format(self.name(), i))
            relatedInfoAlg.Inputs = [ outLoc ]

            if ( "Location" in toolConf ) == ( "Locations" in toolConf):
                raise Exception("Either one of 'Locations' and 'Location' should be specified, not both")
            relatedInfoAlg.InfoLocations = toolConf.get("Locations", { outLoc : [ toolConf.get("Location", None) ] })
            relatedInfoAlg.MaxLevel = ( toolConf.get("RecursionLevel", 1) if "Locations" in toolConf else 0 )

            import Configurables
            tNm = "Tool{0}".format(i)
            relatedInfoAlg.addTool(getattr(Configurables, toolConf["Type"]), name=tNm)
            tInst = getattr(relatedInfoAlg, tNm)
            relatedInfoAlg.Tool = tInst.getFullName()
            for k,v in toolConf.iteritems():
                if k not in ("Type", "Location", "Locations", "RecursionLevel", "TopSelection"):
                    tInst.setProp(k, v)
            self.getUsedConf(GaudiSequencer).Members.append(relatedInfoAlg)

        ### DecayTreeTuple setup

        if self.getProp("MicroDST"):
            self.setOtherProp(tup, "RootInTES")

        self.setOtherProps(tup, ["InputPrimaryVertices", "NTupleDir"])
        tup.Inputs = [ presel.getProp("Output") ]

        tup.ReFitPVs = True

        if self.getProp("RightSign"):
            tup.Decay = "X -> ^mu+ ^mu-"
            tup.addBranches({
                  "MuP"  : "X -> ^mu+ mu-"
                , "MuM"  : "X ->  mu+^mu-"
                , "Jpsi" : "X ->  mu+ mu-"
                })
        else:
            tup.Decay = "[ X -> ^mu- ^mu- ]CC"
            tup.addBranches({
                  "MuP"  : "[ X -> ^mu- mu- ]CC"
                , "MuM"  : "[ X ->  mu-^mu- ]CC"
                , "Jpsi" : "[ X ->  mu- mu- ]CC"
                })

        # remove default tuple tools
        tup.ToolList = []

        #
        # Particle tuple tools
        #

        self.allConfTupleTools(tup.MuP)
        self.allConfTupleTools(tup.MuM)
        self.allConfTupleTools(tup.Jpsi)

        self.muonConfTupleTools(tup.MuP)
        self.muonConfTupleTools(tup.MuM)

        self.motherConfTupleTools(tup.Jpsi)

        #
        # Event tuple tools
        #

        tup.ToolList += [
              "TupleToolEventInfo"
            , "TupleToolRecoStats"
            ]

        # Most upstream PV Z and highest muon PT in the event
        globalEventVars = tup.addTupleTool("LoKi__Hybrid__EvtTupleTool/TupleToolMostUpstreamPV")
        globalEventVars.Preambulo = [
                  "from LoKiPhys.decorators import *"
                , "from LoKiCore.functions  import *"
                , "from LoKiTracks.functions  import *"
                ]
        # Most upstream PV variables
        globalEventVars.VOID_Variables = dict( ( "UPPV_%s" % varName, "VSOURCEDESKTOP() >> min_element( VZ ) >> fetch( %s, 0, -1000000. )" % varFunctor ) for varName, varFunctor in JPsiTupleConf.vertexVarNamesAndFunctors_ALL+JPsiTupleConf.vertexVarNamesAndFunctors_PV )
        globalEventVars.VOID_Variables.update({ "HIGHESTMUONPT" : "SOURCE('Phys/StdAllLooseMuons/Particles') >> max_value(PT)" })
