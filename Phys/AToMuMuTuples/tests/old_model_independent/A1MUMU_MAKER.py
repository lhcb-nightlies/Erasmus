#! /usr/bin/env python
from ROOT import * ### Keep this at the top, to overwrite the shit that DV does to displays!
import sys, os
from Gaudi.Configuration import NTupleSvc
from Configurables import GaudiSequencer, DaVinci, L0Conf, SelDSTWriter, FilterDesktop
from triggerDecisionLists import *
from BenderAlgo.BenderV0 import *
from BenderAlgo.extrafunctions import *

bsmumuCounterKeys = [ "MuMuCouplesAnalyzed", "EVT", "weird" ]

#########################
#### GENERAL OPTIONS ####
#########################

ISGRID = 0 # Remember to change in case you send this to grid.
SIMULATION = 1

nEvents = 1000 # Number of events to run over in interactive.
INTERACTIVE = nEvents*(not ISGRID)
DataType = "2011"
MagnetType = "mu" # Magnet polarity: md, mu.
MassPoint = 8 # 5, 8, 10, 12 GeV, only for simulation.

MC_INFO = 1 # This is suppossed to be OK with the new DSTs.
TUP = 1
TRIGGER = 1
DebugAlgos = 1
if not SIMULATION:
  MC_INFO = 0

#########################
### ALGORITHMS TO ADD ###
#########################

TUPLE_FILE = "A0mumu_"+DataType+"_data_ntuples" + "_MC"*SIMULATION +".root"
HISTO_FILE = "A0mumu_"+DataType+"_data_histos" + "_MC"*SIMULATION +".root"

if TUP: NTupleSvc( Output =["T DATAFILE='"+ TUPLE_FILE + "' TYP='ROOT' OPT='NEW'"] )

#################
### DATACARDS ###
#################

from PhysSelPython.Wrappers import SelectionSequence
import GaudiPython

import Gaudi
from Gaudi.Configuration import *

dataDic = {0:{"2011":{}, "2012":{}}, 1:{"2011":{}, "2012":{}}}

dataDic[0]["2011"]['md'] = "real_data/LHCb_Collision11_Beam3500GeVVeloClosedMagDown_Real-Data_Reco14_Stripping21r1_90000000_DIMUON.DST.py"
dataDic[0]["2011"]['mu'] = "real_data/LHCb_Collision11_Beam3500GeVVeloClosedMagUp_Real-Data_Reco14_Stripping21r1_90000000_DIMUON.DST.py"
dataDic[0]["2012"]['md'] = "real_data/LHCb_Collision12_Beam4000GeVVeloClosedMagDown_Real-Data_Reco14_Stripping21_90000000_DIMUON.DST.py"
dataDic[0]["2012"]['mu'] = "real_data/LHCb_Collision12_Beam4000GeVVeloClosedMagUp_Real-Data_Reco14_Stripping21_90000000_DIMUON.DST.py"

h_id_dict = { 5:0, 8:3, 10:5, 12:7 }
h_id = h_id_dict[MassPoint]
pdim = 'MC/mumu='+str(5+h_id)+'_GeV/'
dataDic[1]["2011"]['md'] = pdim+'MC_2011_Beam3500GeV2011MagDownNu2Pythia8_Sim08h_Digi13_Trig0x40760037_Reco14c_Stripping20r1NoPrescalingFlagged_4011200'+str(h_id)+'_ALLSTREAMS.py'
dataDic[1]["2011"]['mu'] = pdim+'MC_2011_Beam3500GeV2011MagUpNu2Pythia8_Sim08h_Digi13_Trig0x40760037_Reco14c_Stripping20r1NoPrescalingFlagged_4011200'+str(h_id)+'_ALLSTREAMS.py'
dataDic[1]["2012"]['md'] = pdim+'MC_2012_Beam4000GeV2012MagDownNu2.5Pythia8_Sim08h_Digi13_Trig0x409f0045_Reco14c_Stripping20NoPrescalingFlagged_4011200'+str(h_id)+'_ALLSTREAMS.py'
dataDic[1]["2012"]['mu'] = pdim+'MC_2012_Beam4000GeV2012MagUpNu2.5Pythia8_Sim08h_Digi13_Trig0x409f0045_Reco14c_Stripping20NoPrescalingFlagged_4011200'+str(h_id)+'_ALLSTREAMS.py'

if not ISGRID:
  importOptions("$ATOMUMUTUPLESROOT/datacards/" + dataDic[SIMULATION][DataType][MagnetType])
  Eostize(EventSelector()) 

if SIMULATION:
  TES_dst_path = "AllStreams"
  pre_A1 = "A1MuMu"
else:
  TES_dst_path = "Dimuon"
  pre_A1 = ""

Locations = {"Upsilon": TES_dst_path + "/Phys/FullDSTDiMuonDiMuonHighMassLine",
             "A1": TES_dst_path + "/Phys/"+pre_A1+"A1MuMuLine",
             "A1SS": TES_dst_path + "/Phys/A1MuMuA1MuMuSameSignLine"}

def configure():

    DaVinci().EvtMax = 0 # This is not relevant.    
    DaVinci().DataType = DataType
    DaVinci().Simulation = SIMULATION
    DaVinci().HistogramFile = HISTO_FILE
   
    if SIMULATION:
      DDDB_dict = { "2011" : "dddb-20150522-1", "2012" : "dddb-20150928" }
      CondDB_dict = { "2011" : "sim-20150813-1-vc-"+MagnetType+"100" , "2012" : "sim-20150813-2-vc-"+MagnetType+"100" }
      DaVinci().DDDBtag   = DDDB_dict[DataType]
      DaVinci().CondDBtag   = CondDB_dict[DataType]
    else:
      from Configurables import CondDB
      CondDB( LatestGlobalTagByDataType = DataType )

    DaVinci().Lumi = bool(not SIMULATION)
    
    DaVinci().applyConf()   
    gaudi = appMgr()
        
    algos = []
    
    ##########################################

    ## A1
    A1 = B2QQ('A1')
    A1.LookIn = Locations["A1"]
    A1.decayname = "A1-->mumu"
    A1.COUNTER = {}
    A1.Sel_Above_GL = 0
    A1.extraFunctions = [trackHits, subdetectorDLLs, more_muon_things, globalEvtVars, maxEventPT, maxEventPTMuon]
    
    for key in bsmumuCounterKeys:
        A1.COUNTER[key] = 0
    algos.append(A1)
    gaudi.addAlgorithm(A1)

    ## A1SS
    A1SS = B2QQ('A1SS')
    A1SS.LookIn = Locations["A1SS"]
    A1SS.decayname = "A1SS-->mumu"
    A1SS.COUNTER = {}
    
    A1SS.Sel_Above_GL = 0
    A1SS.extraFunctions = [trackHits, subdetectorDLLs, more_muon_things, globalEvtVars, maxEventPT, maxEventPTMuon]
    
    for key in bsmumuCounterKeys:
        A1SS.COUNTER[key] = 0
    algos.append(A1SS)
    gaudi.addAlgorithm(A1SS)
    
    A1SS.RandomMuonOrder = 1

    ## Upsilon
    Ups = B2QQ('Upsilon')
    Ups.LookIn = Locations["Upsilon"]
    Ups.decayname = "Upsilon-->mumu"
    Ups.COUNTER = {}
    
    Ups.Sel_Above_GL = 0
    Ups.extraFunctions = [trackHits, subdetectorDLLs, more_muon_things, globalEvtVars, maxEventPT, maxEventPTMuon]
    
    for key in bsmumuCounterKeys:
        Ups.COUNTER[key] = 0
    algos.append(Ups)
    gaudi.addAlgorithm(Ups)

    gaudi.initialize()
    from BenderAlgo import PIDcalTools
 
    ########################
    ### COMMON ATRIBUTES ###
    ########################
   
    for algo in algos:
        algo.MC_INFO = SIMULATION*MC_INFO
        algo.TRIGGER = TRIGGER
        algo.PIDcalTools = PIDcalTools
        algo.NTupleLUN = "T"
        algo.addedKeys = []
        algo.DST = False
        algo.TUP = TUP
        algo.COUNTER['negSq'] = 0
        algo.COUNTER["weird"] = 0
        algo.PROMPT_KS_COUNTER = False
        algo.DEBUG = DebugAlgos ### Do not define global DEBUG!
       
        algo.evt_of = 0

        algo.l0BankDecoder = algo.tool(cpp.IL0DUFromRawTool,'L0DUFromRawTool')
        algo.rawBankDecoder = algo.tool(cpp.IOTRawBankDecoder,'OTRawBankDecoder')

        algo.PVRefitter = algo.tool(cpp.IPVReFitter,"LoKi::PVReFitter")
        #algo.LifeFitter = algo.tool(cpp.ILifetimeFitter,"PropertimeFitter")
        algo.LifeFitter = algo.tool(cpp.ILifetimeFitter,"LoKi::LifetimeFitter")
        algo.Geom =  algo.tool(cpp.IDistanceCalculator,"LoKi::DistanceCalculator")
        algo.l0List = l0List_2011
        algo.hlt1List = hlt1List_2011
        algo.hlt2List = hlt1List_2011
        if algo.MC_INFO: 
          algo.matcher = gaudi.toolsvc().create("MCMatchObjP2MCRelator",interface="IP2MCP")
          algo.extraFunctions += [BQQMCtruth, mc_geometry]
       
configure()
gaudi = appMgr()

if INTERACTIVE:
    for i in range(INTERACTIVE):
        gaudi.run(1)
        TES = gaudi.evtsvc()
       
else:
    gaudi.run(-1)
    gaudi.stop()
    gaudi.finalize()

