"""
Sequence for PFJet isolation variables
"""
__author__ = "Pieter David <pieter.david@nikhef.nl>"

from LHCbKernel.Configuration import LHCbConfigurableUser, DEBUG, VERBOSE
from Configurables import LoKi__PFJetMaker, FilterInTrees

class PFJetIsolationConf(LHCbConfigurableUser):
    """
    Sequence for PFJet isolation variables
    """
    __slots__ = {
          "Inputs"                 : []
        , "InputPrimaryVertices"   : "Rec/Vertex/Primary"
        , "OutputJetsBanned"       : "Phys/PFIsolationBanned/Particles"
        , "OutputJetsConstituents" : "Phys/PFIsolationConstituents/Particles"
        , "OutputJetsComposite"    : "Phys/PFIsolationComposite/Particles"
        }

    __used_configurables__ = [ (LoKi__PFJetMaker, "PFJetIsolation_composite")
                             , (LoKi__PFJetMaker, "PFJetIsolation_banned")
                             , (FilterInTrees   , "PFJetIsolation_getMuons")
                             , (LoKi__PFJetMaker, "PFJetIsolation_constituents")
                             ]

    def initialize(self):
        super( PFJetIsolationConf, self ).initialize()

        self += LoKi__PFJetMaker("PFJetIsolation_composite")
        self += LoKi__PFJetMaker("PFJetIsolation_banned")
        self += FilterInTrees("PFJetIsolation_getMuons")
        self += LoKi__PFJetMaker("PFJetIsolation_constituents")

    def __apply_configuration__(self):

        ## make sure the muons enter the jet reco - WARNING: this does not work correctly for hadrons, electrons or photons (due to neutral recovery)
        muonLoc = "Phys/PFJetIsolation_muons/Particles"
        muonFilter = FilterInTrees("PFJetIsolation_getMuons")
        self.setOtherProp(muonFilter, "Inputs")
        muonFilter.Output = muonLoc
        muonFilter.Code = "HASPROTO & ( ABSID == 'mu+' )"

        from JetAccessories.JetMaker_Config import JetMakerConf

        commonJetOpts = { "R" : 0.5
                        , "AssociateWithPV" : False
                        }

        JetMakerConf("PFJetIsolation_constituents",
                     Inputs = [ muonLoc, "Phys/PFParticles/Particles" ],
                     JetIDCut = False,
                     **commonJetOpts)
        LoKi__PFJetMaker("PFJetIsolation_constituents").Output = self.getProp("OutputJetsConstituents")

        JetMakerConf("PFJetIsolation_composite",
                     Inputs = ["Phys/PFParticles/Particles"]+self.getProp("Inputs"),
                     JetIDCut = False,
                     **commonJetOpts)
        LoKi__PFJetMaker("PFJetIsolation_composite").Output = self.getProp("OutputJetsComposite")

        JetMakerConf("PFJetIsolation_banned",
                     listOfParticlesToBan = self.getProp("Inputs"),
                     JetIDCut = True,
                     **commonJetOpts)
        LoKi__PFJetMaker("PFJetIsolation_banned").Output = self.getProp("OutputJetsBanned")
