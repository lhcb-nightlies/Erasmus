"""
Configurable to set up the new PV refitting (full PV re-reco excluding the signal candidate)
"""
__author__ = "Pieter David <pieter.david@cern.ch>"

from LHCbKernel.Configuration import LHCbConfigurableUser
from Configurables import RemoveBdaughterTracks, PatPVOffline, TrackV0Finder, PVOfflineTool
from PatPV import PVConf

class NewPVRefit(LHCbConfigurableUser):
    """
    Configurable to set up the new PV refitting (full PV re-reco excluding the signal candidate)
    """
    __slots__ = {
      # for RemoveBdaughterTracks
        "InputParticleLocation" : ""
      , "RootInTES"             : ""
      , "InputTrackLocation"    : "Rec/Track/Best"
      , "FilteredTrackLocation" : "Rec/Track/MyBest"
      # for the PV reconstruction
      , "OutputVertices"        : ""
      }
    _propertyDocDct = {
      # for RemoveBdaughterTracks
        "InputParticleLocation" : "Property passed on to RemoveBdaughterTracks"
      , "RootInTES"             : "Property passed on to RemoveBdaughterTracks"
      , "InputTrackLocation"    : "Property passed on to RemoveBdaughterTracks"
      , "FilteredTrackLocation" : "Location for filtered track container (PV re-reco input)"
      # for the PV reconstruction
      , "OutputVertices"        : "Output PV location"
      }

    __used_configurables__ = [ ( RemoveBdaughterTracks, None )
                             , ( PatPVOffline         , None ) ]

    def getUsedConf(self, type):
        return type(self._instanceName(type))

    def initialize(self):
        super( NewPVRefit, self ).initialize()

        self += self.getUsedConf( RemoveBdaughterTracks )
        self += self.getUsedConf( PatPVOffline )

    def __apply_configuration__(self):

        trackRemover = self.getUsedConf( RemoveBdaughterTracks )

        self.setOtherProps( trackRemover, [ "InputParticleLocation", "InputTrackLocation", "FilteredTrackLocation" ] )

        pvAlg = self.getUsedConf( PatPVOffline )

        PVConf.StandardPV().configureAlg( pvAlg )
        pvAlg.addTool( PVOfflineTool )
        pvAlg.PVOfflineTool.setProp( "InputTracks", [ self.getProp("FilteredTrackLocation") ] )
        self.setOtherProp( pvAlg, "OutputVertices" )
