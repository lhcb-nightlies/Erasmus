"""
Absolute trigger and stripping efficiencies for MC
"""
__author__ = "Pieter David <pieter.david@nikhef.nl>"

from LHCbKernel.Configuration import LHCbConfigurableUser, DEBUG, VERBOSE
from Configurables import GaudiSequencer, MCDecayTreeTuple
from DecayTreeTuple.Configuration import *
import re

class MCTruthTupleConf(LHCbConfigurableUser):
    """
    Absolute trigger and stripping efficiencies for MC
    """
    __slots__ = {
          "MicroDST"  : False
        , "RootInTES" : ""
        , "NTupleDir" : "DefaultName"
        , "L0Selections"        : [ "L0MuonDecision"
                                  , "L0DiMuonDecision"
                                  ]
        , "HltSelections"       : [ # Hlt1
                                    "Hlt1SingleMuonHighPTDecision"
                                  , "Hlt1TrackMuonDecision"
                                  , "Hlt1TrackAllL0Decision"
                                  , "Hlt1VertexDisplVertexDecision"
                                  , "Hlt1DiMuonHighMassDecision"
                                  # Hlt2
                                  , "Hlt2SingleMuonHighPTDecision"
                                  , "Hlt2TopoMu.*SimpleDecision"
                                  , "Hlt2TopoMu.*BBDTDecision"
                                  , "Hlt2DiMuonBDecision"
                                  , "Hlt2DiMuon.*Decision"
                                  ]
        , "StrippingSelections" : [ "StrippingFullDSTDiMuonDiMuonHighMassLineDecision"
                                  , "StrippingA1MuMuA1MuMuLineDecision"
                                  ]
        }

    __used_configurables__ = [ (GaudiSequencer  , None)
                             , (MCDecayTreeTuple, None) ]

    def getUsedConf(self, type):
        return type(self._instanceName(type))

    def initialize(self):
        super( MCTruthTupleConf, self ).initialize()

        self += self.getUsedConf( GaudiSequencer   )

    def addTupleToolMCKinematics(self, branch):
        tool = branch.addTupleTool("LoKi::Hybrid::MCTupleTool", "Kinematic")
        tool.Variables = {
                  "P"   : "MCP"
                , "PX"  : "MCPX"
                , "PY"  : "MCPY"
                , "PZ"  : "MCPZ"
                , "PE"  : "MCE"
                , "ETA" : "MCETA"
                , "PHI" : "MCPHI"
                , "M"   : "MCMASS"
                }
    def addTupleToolMCDecay(self, branch):
        tool = branch.addTupleTool("LoKi::Hybrid::MCTupleTool", "DecayVertex")
        tool.Variables = dict( (name, "MCVFUN({0}, MCISDECAY)".format(fun)) for name, fun in {
                  "VX"  : "MCVX"
                , "VY"  : "MCVY"
                , "VZ"  : "MCVZ"
                }.iteritems() )
        tool.Variables.update({
                  "CTAU": "MCCTAU"
                , "FD"  : "MCCTAU*MCMASS/MCP"
                } )
        tool.Variables.update({
                  "NDAUGS": "MCNINTREE(MCALL, True)-1" # subtract the particle itself
                } )
    def addTupleToolMCPrimaryVertex(self, branch):
        tool = branch.addTupleTool("LoKi::Hybrid::MCTupleTool", "PrimaryVertex")
        tool.Variables = dict( (name, "MCVPXFUN({0})".format(fun)) for name, fun in {
                  "VX"  : "MCVX"
                , "VY"  : "MCVY"
                , "VZ"  : "MCVZ"
                }.iteritems() )

    def addTupleToolsMuon(self, branch):
        self.addTupleToolMCKinematics(branch)

    def addTupleToolsVPion(self, branch):
        self.addTupleToolMCKinematics(branch)
        self.addTupleToolMCDecay(branch)

    def addTupleToolsHiggs(self, branch):
        self.addTupleToolMCKinematics(branch)
        self.addTupleToolMCPrimaryVertex(branch)

    def applyConf(self):

        seq = self.getUsedConf( GaudiSequencer   )
        seq.IgnoreFilterPassed = True
        tup = self.getUsedConf( MCDecayTreeTuple )
        seq.Members = [ tup ]

        if self.getProp("MicroDST"):
            self.setOtherProp(tup, "RootInTES")
        self.setOtherProp(tup, "NTupleDir")

        tup.ToolList = [ "TupleToolEventInfo", "TupleToolRecoStats" ]
        tup.Decay      = "X -> ^( X => ^mu+ ^mu- ) ^( X => ^mu+ ^mu- )"
        tup.addBranches({
              "H"      : "X ->  ( X =>  mu+  mu- )  ( X =>  mu+  mu- )"
            , "V1"     : "X -> ^( X =>  mu+  mu- )  ( X =>  mu+  mu- )"
            , "V1_MuP" : "X ->  ( X => ^mu+  mu- )  ( X =>  mu+  mu- )"
            , "V1_MuM" : "X ->  ( X =>  mu+ ^mu- )  ( X =>  mu+  mu- )"
            , "V2"     : "X ->  ( X =>  mu+  mu- ) ^( X =>  mu+  mu- )"
            , "V2_MuP" : "X ->  ( X =>  mu+  mu- )  ( X => ^mu+  mu- )"
            , "V2_MuM" : "X ->  ( X =>  mu+  mu- )  ( X =>  mu+ ^mu- )"
            })
        for bName in ("V1_MuP", "V1_MuM", "V2_MuP", "V2_MuM"):
            branch = getattr(tup, bName)
            self.addTupleToolsMuon(branch)
        for bName in ("V1", "V2"):
            branch = getattr(tup, bName)
            self.addTupleToolsVPion(branch)
        self.addTupleToolsHiggs(tup.H)

        for selStage, hdrLoc in ( ("L0", "/Event/HltLikeL0/DecReports"), ("Hlt", "/Event/Hlt/DecReports"), ("Stripping", "/Event/Strip/Phys/DecReports") ):
            selDecTool = tup.addTupleTool("LoKi__Hybrid__EvtTupleTool/TupleTool{0}".format(selStage))
            selDecTool.Preambulo += [ "from LoKiCore.functions import *" ]
            selDecTool.HLT_Location = hdrLoc
            selDecTool.HLT_Variables = dict( (re.sub("[^a-zA-Z0-9_]", "_", sel), "switch( {0} , 1., 0. )".format( "{fun}('{sel}')".format(fun=("HLT_PASS" if sel.isalnum() else "HLT_PASS_RE"), sel=sel))) for sel in self.getProp("{0}Selections".format(selStage)) )
