#! /usr/bin/env python
from ROOT import *
import sys, os
from Gaudi.Configuration import NTupleSvc
from Configurables import GaudiSequencer, DaVinci, L0Conf, SelDSTWriter, FilterDesktop

bsmumuCounterKeys = ["MuMuCouplesAnalyzed","EVT", "weird"]
sys.path.append(os.environ["HOME"] + "/vol5/URANIA/URANIA_HEAD/Phys/Bs2MuMu/python/Bs2MuMu")
#from Urania import *
#AccessPackage("Bs2MuMu")
from RTuple import *
#from smartpyROOT import *
################################### Some General Options. Remember to check it

INTERACTIVE = 0#10000
MC_INFO = 1
DST = 0
TUP = 1
TRIGGER = 1
REDOTRIGGER = 0
DebugAlgos = 0

N = -1

Njobmax = 1.e10  ### info only, not real limit. Used to set an offset in the evt no. label

SIMULATION =  1
#VERSION = "mc10d"#"stp13"
#VERSION = "stp13"
#DataDic = {"stp13":"2011","stp13_May13":"2011","stp12":"2010","mc10d":"2010","mc10u":"2010"}
DataType = "2012"#DataDic[VERSION]  ##### To pass to DV

print DataType, SIMULATION*"MONTE CARLO"


####################################
### Names for OUTPUT NTuple, Dst ###
####################################


TUPLE_FILE = os.environ["HOME"] + "/vol5/KspizeroTruth" 

#################
### DATACARDS ###
#################


from BenderAlgo.BenderV0 import *
from BenderAlgo import climbing
import GaudiPython
import Gaudi
from Gaudi.Configuration import *
tup = RTuple(TUPLE_FILE , ["evt/F","k1p1/F","k1p2/F","k1p3/F","k1ID/F","pi1p1/F","pi1p2/F","pi1p3/F","pi1ID/F","mu1p1/F","mu1p2/F","mu1p3/F","mu1ID/F","mu2p1/F","mu2p2/F","mu2p3/F","mu2ID/F","helcosthetaK/F","helcosthetaL/F","helphi/F", "Mmumu/F"])



def configure():
    
    ################### IMPORTING OPTIONS

    DaVinci().EvtMax = 0    
    DaVinci().DataType = DataType 
    DaVinci().Simulation = SIMULATION
    #DaVinci().L0 = bool(TRIGGER)

    DaVinci().UserAlgorithms = []
    #DaVinci().Hlt = bool(TRIGGER)
    DaVinci().HistogramFile = "rubish.root"
    DaVinci().Lumi = True
    #DaVinci().InputType = "MDST"


    
    importOptions("$STDOPTS/PreloadUnits.opts")
    
    from GaudiConf import IOHelper
    IOHelper('ROOT').inputFiles(['PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00032144/0000/00032144_00000002_1.allstreams.dst'], clear=True)
    

    #importOptions(datacards)  ### Be sure you DO NOT import any other options file after this one. You can overwrite eventselector input.
    ################## END IMPORTING OPTIONS
    DaVinci().applyConf()   
    gaudi = appMgr()
        
  
    

    gaudi.initialize()

configure()

gaudi = appMgr()

Mmu = 105.658
 
for i in range(10000):
    gaudi.run(1)
    TES = gaudi.evtsvc()
    
    mcB = climbing.climbing_kspizeromm(TES["MC/Particles"],MCID)
    if not mcB:
        print "MC signal not found"
        continue
    ev = mcB.endVertices().at(0)
    dotas = ev.products()
    pizero, mu1, mu2 = 0, 0, 0
    for i in range(dotas.size()):
        par = dotas.at(i)
        if MCABSID(par) == 111: pizero = par
        elif MCID(par) == 13 : mu1 = par
        elif MCID(par) == -13 : mu2 = par
        
    if not pizero: continue
    if not mu1: continue
    if not mu2: continue
    dotas = pizero.endVertices().at(0).products()
    g1 = dotas.at(0)
    g2 = dotas.at(1)
    for i in range(dotas.size()):
        par = dotas.at(i)
        if MCID(par) == -13: mu1 = par
        elif MCID(par) == 13 : mu2 = par
    
    kaon = g1 ## just renaming them as in the JpsiKpi decay, to recycle code
    pion = g2 
    Kplus_P = TLorentzVector(MCPX(kaon),MCPY(kaon),MCPZ(kaon), MCE(kaon))
    Kminus_P = TLorentzVector(MCPX(pion),MCPY(pion),MCPZ(pion), MCE(pion))
    muplus_P = TLorentzVector(MCPX(mu1),MCPY(mu1),MCPZ(mu1), MCE(mu1))
    muminus_P = TLorentzVector(MCPX(mu2),MCPY(mu2),MCPZ(mu2), MCE(mu2))
    
    angles = HelicityAngles(Kplus_P,Kminus_P,muplus_P,muminus_P)
    p1 = vector(MCPX(mu1),MCPY(mu1),MCPZ(mu1))
    p2 = vector(MCPX(mu2),MCPY(mu2),MCPZ(mu2))
    
    tup.fillItem("evt",i*1.)
    tup.fillItem("Mmumu", psqrt(IM2(p1,p2, Mmu,Mmu)))
    tup.fillItem("k1p1",MCPX(kaon))
    tup.fillItem("k1p2",MCPY(kaon))
    tup.fillItem("k1p3",MCPZ(kaon))
    tup.fillItem("k1ID",MCID(kaon))
    tup.fillItem("pi1p1",MCPX(pion))
    tup.fillItem("pi1p2",MCPY(pion))
    tup.fillItem("pi1p3",MCPZ(pion))
    tup.fillItem("pi1ID",MCID(pion))
    tup.fillItem("mu1p1",MCPX(mu1))
    tup.fillItem("mu1p2",MCPY(mu1))
    tup.fillItem("mu1p3",MCPZ(mu1))
    tup.fillItem("mu1ID",MCID(mu1))
    tup.fillItem("mu2p1",MCPX(mu2))
    tup.fillItem("mu2p2",MCPY(mu2))
    tup.fillItem("mu2p3",MCPZ(mu2))
    tup.fillItem("mu2ID",MCID(mu2))
    tup.fillItem("helcosthetaK",angles[0])
    tup.fillItem("helcosthetaL",angles[1])
    tup.fillItem("helphi",angles[2])
    tup.fill()
tup.close()
    
#t,f = getTuple(TUPLE_FILE)
