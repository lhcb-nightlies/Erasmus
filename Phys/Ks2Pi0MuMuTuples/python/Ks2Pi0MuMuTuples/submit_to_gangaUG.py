import Ganga.GPI
Job = Ganga.GPI.Job
Dirac = Ganga.GPI.Dirac
Local = Ganga.GPI.Local
Executable = Ganga.GPI.Executable
GaudiPython = Ganga.GPI.GaudiPython
LHCbDataset = Ganga.GPI.LHCbDataset
SplitByFiles = Ganga.GPI.SplitByFiles
DiracFile = Ganga.GPI.DiracFile

def get_files_from_datacard(path):
  f = open(path)
  a = f.readlines()
  files_path = ""
  
  for line in a:
    if line.find("'LFN") == -1: continue
    files_path += line +"\n"
  if files_path.find("[") ==-1: files_path = "[" + files_path
  if files_path.find("]") ==-1: files_path += "]"
  out = 'inputFiles = '+files_path

  return out

def dataPath(SIMULATION, DataType, MagnetType):
  dataDict = {0:{"2011":{}, "2012":{}}, 1:{"2011":{}, "2012":{}, "MBUG":{}}}

  ### Real data dictionaries:
  dataDict[1]["MBUG"]['md'] = "MC_Upgrade_Beam7000GeVUpgradeMagDownNu7.625nsPythia8_Sim08hNoRichSpill_Reco15U3_30000000_LDST.py"
  dataDict[1]["MBUG"]['mu'] = "MC_Upgrade_Beam7000GeVUpgradeMagUpNu7.625nsPythia8_Sim08hNoRichSpill_Reco15U3_30000000_LDST.py"
  
  return dataDict[SIMULATION][DataType][MagnetType]

def send_job(SIMULATION, DataType, MagnetType):
  dPath = dataPath(SIMULATION, DataType, MagnetType)
  inputData = "../datacards/"+dPath #"/afs/cern.ch/user/v/vchobano/cmtuser/Erasmus_HEAD/Phys/Ks2Pi0MuMuTuples/python/datacards/" + dPath
  print "inputData = %s" % inputData
  app = GaudiPython(project='Erasmus', version='v12r0')
  if SIMULATION==0:
    app.script = "./kspi0mumu_analyserUG.py" #"/afs/cern.ch/user/v/vchobano/cmtuser/Erasmus_HEAD/Phys/Ks2Pi0MuMuTuples/python/Ks2Pi0MuMuTuples/kspi0mumu_analyser.py"
  else:
    #TO BE change accordingly
    app.script = "./kspi0mumu_analyserUG.py"#"/afs/cern.ch/user/v/vchobano/cmtuser/Erasmus_HEAD/Phys/Ks2Pi0MuMuTuples/python/Ks2Pi0MuMuTuples/kspi0mumu_analyser.py"
  app.user_release_area = "/afs/cern.ch/user/v/vchobano/cmtuser"
  app.platform = "x86_64-slc6-gcc48-opt"
  j = Job(application = app, backend = Dirac())
  j.inputsandbox += ["./triggerDecisionLists.py", "./extrafunctions.py", "./climbing.py", "./ParticleTranslator.py", "./muidDebug.py"] #[ "/afs/cern.ch/user/v/vchobano/cmtuser/Erasmus_HEAD/Phys/Ks2Pi0MuMuTuples/python/Ks2Pi0MuMuTuples/triggerDecisionLists.py" ]
  exec get_files_from_datacard(inputData)
  j.inputdata = LHCbDataset(files=inputFiles)
  j.outputfiles = [LocalFile("*.root")]
#  j.application.user_release_area = "/afs/cern.ch/user/v/vchobano/cmtuser/"
  j.name = "Ks2Pi0MuMu_job_grid"+DataType+"_"+MagnetType+"_"+str(SIMULATION)
  j.splitter = SplitByFiles(filesPerJob = 5)
  j.submit()

DataTypeList = ["2011", "2012", "MBUG"]
MagnetTypeList = ["md", "mu"]

# Real data:
SIMULATION = 1
#for DataType in DataTypeList:
  #for MagnetType in MagnetTypeList:
    #send_job(SIMULATION, DataType, MagnetType)

#send_job(SIMULATION,"MBUG","mu")
send_job(SIMULATION,"MBUG","md") 
#send_job(SIMULATION,"2011","mu")
#send_job(SIMULATION,"2011","md")
#send_job(SIMULATION,"2012","mu")
#send_job(SIMULATION,"2012", "md")
## MC:
#SIMULATION = 1 #etc
