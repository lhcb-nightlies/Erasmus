from LHCbConfig import *

## Example taken from http://docplayer.net/50267-Using-a-dst-as-a-big-ntuple.html


appConf = ApplicationMgr()
appConf.TopAlg += ["UnpackMCParticle","UnpackMCVertex"]

import GaudiPython
appMgr = GaudiPython.AppMgr()
sel = appMgr.evtsel()
#LFN:/lhcb/MC/2012/ALLSTREAMS.DST/00037696/0000/00037696_00000001_1.allstreams.dst
sel.open(['PFN:./Filtered.KspizmmPARTIAL.Strip.dst'])

evt = appMgr.evtsvc()
appMgr.run(1)
evt.dump()

##a,b = seekForData('/Event/KspizmmFULL.Strip/pPhys/Particles')
##x = a.get_data()
