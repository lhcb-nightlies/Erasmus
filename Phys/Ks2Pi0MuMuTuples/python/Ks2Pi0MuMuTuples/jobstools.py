import os

DATAPATH = os.environ["KS2PI0MUMUTUPLESROOT"] +"/python/datacards/"#+subfolder



def partir(datacards,piecesize=10):
    f=open(DATAPATH+datacards+".py","r")
    lines=f.readlines()
    startfile = "from Gaudi.Configuration import *\n"
    startfile += "from GaudiConf import IOHelper\n"
    startfile += "IOHelper('ROOT').inputFiles([ \n"
    endfile = "])\n"
    i = 0
    for line in lines:
        # w = line[:5]
        if ("LFN" in line) or ("PFN" in line ): break
        i+=1

    fn,j = 0,0
    aux = []
    for line in lines[i:]:
        aux.append(line)
        j+=1
        if j == piecesize:
            out = open(DATAPATH + datacards +"_"+ str(fn) +".py","w")
            out.write(startfile)
            for i in range(len(aux) - 1 ): out.write(aux[i])
                #line = aux[i]
                #print line[len(line)-5:] #  \n is just one caracter
                #out.write(line)
            line_ = aux[len(aux)-1]
            ddd=line_.replace(",","")
            out.write(ddd)
                
            j,aux = 0,[]
            out.write(endfile)
            out.close()
            fn+=1
    f.close()
