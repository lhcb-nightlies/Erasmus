import Ganga.GPI
Job = Ganga.GPI.Job
Dirac = Ganga.GPI.Dirac
Local = Ganga.GPI.Local
Executable = Ganga.GPI.Executable
GaudiPython = Ganga.GPI.GaudiPython
LHCbDataset = Ganga.GPI.LHCbDataset
SplitByFiles = Ganga.GPI.SplitByFiles
DiracFile = Ganga.GPI.DiracFile

def get_files_from_datacard(path):
  f = open(path)
  a = f.readlines()
  files_path = ""
  
  for line in a:
    if line.find("'LFN") == -1: continue
    files_path += line +"\n"
  if files_path.find("[") ==-1: files_path = "[" + files_path
  if files_path.find("]") ==-1: files_path += "]"
  out = 'inputFiles = '+files_path

  return out

def dataPath(SIMULATION, DataType, MagnetType):
  dataDict = {0:{"2011":{}, "2012":{},"2016":{}}, 1:{"2011":{}, "2012":{}, "MBUG":{}}}

  ### Real data dictionaries:
  dataDict[0]["2011"]['md'] = "LHCb_Collision11_Beam3500GeVVeloClosedMagDown_Real_Data_Reco14_Stripping21r1_90000000_LEPTONIC.MDST.py"
  dataDict[0]["2011"]['mu'] = "LHCb_Collision11_Beam3500GeVVeloClosedMagUp_Real_Data_Reco14_Stripping21r1_90000000_LEPTONIC.MDST.py"
  dataDict[0]["2012"]['md'] = "LHCb_Collision12_Beam4000GeVVeloClosedMagDown_Real_Data_Reco14_Stripping21_90000000_LEPTONIC.MDST.py"
  dataDict[0]["2012"]['mu'] = "LHCb_Collision12_Beam4000GeVVeloClosedMagUp_Real_Data_Reco14_Stripping21_90000000_LEPTONIC.MDST.py"#"Stp21_Leptonic_MagUp2012_43.py"#
  dataDict[0]["2016"]['mu'] = "LHCb_Collision16_Beam6500GeVVeloClosedMagUp_RealData_Reco16_Stripping26_90000000_DIMUON.DST.py"
  
  dataDict[1]["MBUG"]['md'] = "MC_Upgrade_Beam7000GeVUpgradeMagDownNu7.625nsPythia8_Sim08hNoRichSpill_Reco15U3_30000000_LDST.py"
  dataDict[1]["MBUG"]['mu'] = "MC_Upgrade_Beam7000GeVUpgradeMagUpNu7.625nsPythia8_Sim08hNoRichSpill_Reco15U3_30000000_LDST.py"
  
  return dataDict[SIMULATION][DataType][MagnetType]

def send_job(SIMULATION, DataType, MagnetType):
  dPath = dataPath(SIMULATION, DataType, MagnetType)
  inputData = "../datacards/"+dPath #"/afs/cern.ch/user/v/vchobano/cmtuser/Erasmus_HEAD/Phys/Ks2Pi0MuMuTuples/python/datacards/" + dPath
  print "inputData = %s" % inputData
  app = GaudiPython(project='Erasmus', version='v13r4')
  if SIMULATION==0:
    app.script = "./kspi0mumu_analyser_local.py" #"/afs/cern.ch/user/v/vchobano/cmtuser/Erasmus_HEAD/Phys/Ks2Pi0MuMuTuples/python/Ks2Pi0MuMuTuples/kspi0mumu_analyser.py"
  else:
    #TO BE change accordingly
    app.script = "./kspi0mumu_analyser_local.py"#"/afs/cern.ch/user/v/vchobano/cmtuser/Erasmus_HEAD/Phys/Ks2Pi0MuMuTuples/python/Ks2Pi0MuMuTuples/kspi0mumu_analyser.py"
  j = Job(application = app, backend = Dirac())
  j.inputsandbox += ["./triggerDecisionLists.py"] #[ "/afs/cern.ch/user/v/vchobano/cmtuser/Erasmus_HEAD/Phys/Ks2Pi0MuMuTuples/python/Ks2Pi0MuMuTuples/triggerDecisionLists.py" ]
  exec get_files_from_datacard(inputData)
  j.inputdata = LHCbDataset(files=inputFiles)
  j.outputfiles = [LocalFile("*.root")]
  j.name = "Ks2Pi0MuMu_job_grid"+DataType+"_"+MagnetType+"_"+str(SIMULATION)
  j.splitter = SplitByFiles(filesPerJob = 5)
  j.submit()

DataTypeList = ["2011", "2012", "MBUG"]
MagnetTypeList = ["md", "mu"]

# Real data:
SIMULATION = 0
#for DataType in DataTypeList:
  #for MagnetType in MagnetTypeList:
    #send_job(SIMULATION, DataType, MagnetType)

send_job(SIMULATION,"2016","mu")
#send_job(SIMULATION,"2011","mu")
#send_job(SIMULATION,"2011","md")
#send_job(SIMULATION,"2012","mu")
#send_job(SIMULATION,"2012", "md")
## MC:
#SIMULATION = 1 #etc
