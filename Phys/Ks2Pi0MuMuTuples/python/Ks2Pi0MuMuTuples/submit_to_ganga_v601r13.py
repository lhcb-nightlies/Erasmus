import Ganga.GPI
Job = Ganga.GPI.Job
Dirac = Ganga.GPI.Dirac
Local = Ganga.GPI.Local
Executable = Ganga.GPI.Executable
GaudiPython = Ganga.GPI.GaudiPython
LHCbDataset = Ganga.GPI.LHCbDataset
SplitByFiles = Ganga.GPI.SplitByFiles
DiracFile = Ganga.GPI.DiracFile

def get_files_from_datacard(path):
    f = open(path)
    a = f.readlines()
    files_path = ""
    
    for line in a:
        if line.find("'LFN") == -1: continue
        files_path += line +"\n"
    if files_path.find("[") ==-1: files_path = "[" + files_path
    if files_path.find("]") ==-1: files_path += "]"
    out = 'inputFiles = '+files_path

    return out

def prepare_diracfile_dataset(inputList): # Only for Ganga 6.1
    outputList = []
    for lfnPath in inputList:
        outputList.append(DiracFile(lfnPath))
    return outputList

def dataPath(SIMULATION, DataType, MagnetType):
    dataDict = {0:{"2011":{}, "2012":{}, "2016":{}}, 1:{"2012":{}, "MC12_Strip_Sim09":{}, "MC15_Sim09":{}, "MC12_Strip_Sim09_Ks3pi":{}, "MBUG":{}}}
    
    ### Real data dictionaries:
    dataDict[0]["2011"]['md'] = "LHCb_Collision11_Beam3500GeVVeloClosedMagDown_Real_Data_Reco14_Stripping21r1_90000000_LEPTONIC.MDST.py"
    dataDict[0]["2011"]['mu'] = "LHCb_Collision11_Beam3500GeVVeloClosedMagUp_Real_Data_Reco14_Stripping21r1_90000000_LEPTONIC.MDST.py"
    dataDict[0]["2012"]['md'] = "LHCb_Collision12_Beam4000GeVVeloClosedMagDown_Real_Data_Reco14_Stripping21_90000000_LEPTONIC.MDST.py"
    dataDict[0]["2012"]['mu'] = "LHCb_Collision12_Beam4000GeVVeloClosedMagUp_Real_Data_Reco14_Stripping21_90000000_LEPTONIC.MDST.py"
    dataDict[0]["2016"]['md'] = "LHCb_Collision16_Beam6500GeVVeloClosedMagDown_RealData_Reco16_Stripping26_90000000_DIMUON.DST.py"
    dataDict[0]["2016"]['mu'] = "LHCb_Collision16_Beam6500GeVVeloClosedMagUp_RealData_Reco16_Stripping26_90000000_DIMUON.DST.py"
    dataDict[1]["MC12_Strip_Sim09"]['mu'] = "MC_2012_Beam4000GeV2012MagUpNu2.5Pythia8_Sim09a_Trig0x409f0045_Reco14c_Stripping21Filtered_34112407_KSPI0MUMU.STRIP.DST.py"
    dataDict[1]["MC12_Strip_Sim09"]['md'] = "MC_2012_Beam4000GeV2012MagDownNu2.5Pythia8_Sim09a_Trig0x409f0045_Reco14c_Stripping21Filtered_34112407_KSPI0MUMU.STRIP.DST.py"
    dataDict[1]["MC15_Sim09"]['mu'] = "MC_2015_Beam6500GeV2015MagUpNu1.625nsPythia8_Sim09a_Trig0x411400a2_Reco15a_Turbo02_Stripping24NoPrescalingFlagged_34112407_ALLSTREAMS.MDST.py"
    dataDict[1]["MC15_Sim09"]['md'] = "MC_2015_Beam6500GeV2015MagDownNu1.625nsPythia8_Sim09a_Trig0x411400a2_Reco15a_Turbo02_Stripping24NoPrescalingFlagged_34112407_ALLSTREAMS.MDST.py"
    dataDict[1]["MC12_Strip_Sim09_Ks3pi"]['mu'] = "MC_2012_Beam4000GeV2012MagUpNu2.5Pythia8_Sim09a_Trig0x409f0045_Reco14c_Stripping21Filtered_34102408_KSPI0MUMU.STRIP.DST.py"
    dataDict[1]["MC12_Strip_Sim09_Ks3pi"]['md'] = "MC_2012_Beam4000GeV2012MagDownNu2.5Pythia8_Sim09a_Trig0x409f0045_Reco14c_Stripping21Filtered_34102408_KSPI0MUMU.STRIP.DST.py"

    dataDict[1]["MBUG"]['md'] = "MC_Upgrade_Beam7000GeVUpgradeMagDownNu7.625nsPythia8_Sim08hNoRichSpill_Reco15U3_30000000_LDST.py"
    dataDict[1]["MBUG"]['mu'] = "MC_Upgrade_Beam7000GeVUpgradeMagUpNu7.625nsPythia8_Sim08hNoRichSpill_Reco15U3_30000000_LDST.py"
        
    return dataDict[SIMULATION][DataType][MagnetType]

def send_job(SIMULATION, DataType, MagnetType):
    #from datacardsDict import dataPath
    dPath = dataPath(SIMULATION, DataType, MagnetType) 
    #print "inputData = %s" % inputData
    inputData = "../datacards/"+dPath #"/afs/cern.ch/user/v/vchobano/cmtuser/Erasmus_HEAD/Phys/Ks2Pi0MuMuTuples/python/datacards/" + dPath
    app = GaudiPython(project='Erasmus', version='v13r4')
    app.script = "./kspi0mumu_analyser16.py"
    j = Job(application = app, backend = Dirac())
    #j.inputsandbox += ["./triggerDecisionLists.py"] # Ganga 6.0
    j.inputfiles += [LocalFile('./triggerDecisionLists.py')] # Ganga 6.1
    exec get_files_from_datacard(inputData)
    inputDiracFiles = prepare_diracfile_dataset(inputFiles) # Ganga 6.1
    j.inputdata = LHCbDataset(inputDiracFiles) # Ganga 6.1
    #j.inputdata = LHCbDataset(files=inputFiles) # Ganga 6.0
    j.outputfiles = [LocalFile("*.root")]
    j.name = "Ks2Pi0MuMu_job_grid"+DataType+"_"+MagnetType+"_"+str(SIMULATION)
    j.splitter = SplitByFiles(filesPerJob = 10, ignoremissing = True)   
    j.parallel_submit = True
    print "++++++++++++++++++"
    print "Submitting jobs..."
    print "++++++++++++++++++"
    j.submit()

DataTypeList = ["2011", "2012", "2016", "MC12_Strip_Sim09", "MC15_Sim09", "MC12_Strip_Sim09_Ks3pi", "MBUG"]
MagnetTypeList = ["md", "mu"]

#send_job(0,"2011","mu")
#send_job(0,"2011","md")
send_job(0,"2016","md")
#send_job(0,"2012","md")
