#! /usr/bin/env python
from ROOT import *
from Configurables import DaVinci
import GaudiPython


from BenderAlgo.extrafunctions import *
from Gaudi.Configuration import NTupleSvc,GaudiSequencer, EventSelector
from triggerDecisionLists import *
from BenderAlgo.BenderK0SPi0MuMu import *
#from StrippingSelections.StrippingK0s2Pi0MuMuLines import *
## linker between particles and tracks
import LinkerInstances.eventassoc as MCLinker
import os,sys
MCParticle = GaudiPython.gbl.LHCb.MCParticle
Track = GaudiPython.gbl.LHCb.Track

SAMPLE = "MC12_Strip_Down"
ISGRID = 0
RERUN_STP = 0
SIMULATION = "MC" in SAMPLE
INTERACTIVE = 0*(not ISGRID)
_DEBUG = 0
TUPLE_PATH = os.environ["HOME"] + "/vol5/"
TUPLE_FILE = TUPLE_PATH + "kspi0mm_DTF" + SAMPLE +".root"
DataDic = {}
DataDic["MC12_Strip_Up"] = "MC_2012_34112402_Beam4000GeV2012MagUpNu2.5Pythia8_Sim08f_Digi13_Trig0x409f0045_Reco14a_Stripping20r0p1Filtered_KSPI0MUMU.STRIP.DST.py"
DataDic["MC12_Strip_Down"] = "MC_2012_34112402_Beam4000GeV2012MagDownNu2.5Pythia8_Sim08f_Digi13_Trig0x409f0045_Reco14a_Stripping20r0p1Filtered_KSPI0MUMU.STRIP.DST.py"

####Examples of Stp  LINE PATHS:   ## Otherwise use dst-dump -f <the eos file> to see the content including unpacking
#/Event/Phys/Lambda02PiMuLine/Particles
#/Event/Kspi0mumu.Strip/pPhys/Particles ##PACKED!!


from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
                                "CloneDistCut" : [5000, 9e+99 ] }

#
#Raw event juggler to split Other/RawEvent into Velo/RawEvent and Tracker/RawEvent
#
from Configurables import RawEventJuggler
juggler = RawEventJuggler( DataOnDemand=True, Input=2.0, Output=4.0 )



if RERUN_STP:
    confname='RnS'
    from StrippingConf.Configuration import StrippingConf

    from StrippingSelections import buildersConf
    confs = buildersConf()
    from StrippingSelections.Utils import lineBuilder, buildStreamsFromBuilder
    streams = buildStreamsFromBuilder(confs,confname)
    leptonicMicroDSTname   = 'Leptonic'
    charmMicroDSTname      = 'Charm'
    pidMicroDSTname        = 'PID'
    bhadronMicroDSTname    = 'Bhadron'
    mdstStreams = [ leptonicMicroDSTname,charmMicroDSTname,pidMicroDSTname,bhadronMicroDSTname ]
    dstStreams  = [ "BhadronCompleteEvent", "CharmCompleteEvent", "CharmToBeSwum", "Dimuon",
                    "EW", "Semileptonic", "Calibration", "MiniBias", "Radiative" ]

    stripTESPrefix = 'Strip'

    from Configurables import ProcStatusCheck
    
    sc = StrippingConf( Streams = streams,
                        MaxCandidates = 2000,
                        AcceptBadEvents = False,
                        BadEventSelection = ProcStatusCheck(),
                        TESPrefix = stripTESPrefix,
                        ActiveMDSTStream = True,
                        Verbose = True,
                        DSTStreams = dstStreams,
                        MicroDSTStreams = mdstStreams )
    DaVinci().appendToMainSequence( [ sc.sequence() ] )

DaVinci().EvtMax = 0    
DaVinci().DataType = "2012"
# is this data?
DaVinci().Simulation = SIMULATION

# if MC, add the SeqKsResolved
DaVinci().UserAlgorithms = []

def Eostize(thingie):
    x = []#.replace("LFN:", "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod")
    for thing in thingie.Input:
        x.append(thing.replace("LFN:", "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod"))
    thingie.Input = x



NTupleSvc( Output =["T DATAFILE='"+ TUPLE_FILE + "' TYP='ROOT' OPT='NEW'"] )

## RUN THE TRIGGER
L0SelRepSeq = GaudiSequencer("L0SelRepSeq")
L0SelRepSeq.MeasureTime = True
from Configurables import L0SelReportsMaker, L0DecReportsMaker
L0SelRepSeq.Members += [ L0DecReportsMaker(), L0SelReportsMaker() ]
DaVinci().UserAlgorithms += [ L0SelRepSeq ]
importOptions("$KS2PI0MUMUTUPLESROOT/python/datacards/" + DataDic[SAMPLE])

if not ISGRID: Eostize(EventSelector())

gaudi = GaudiPython.AppMgr()

algos = []

#######################
## for stripping signal (for MC, we do not need this two categories)
#kspi0mumu_StrippingSignal = StrippingK0SPi0MuMu("Kspi0mumuStrippingSignal")
kspi0mumu_StrippingSignal = StrippingK0SPi0MuMu("BenderKspi0mumuSignal") ### this is for MCTruth matching, for data see the version below
kspi0mumu_StrippingSignal.DST = False
kspi0mumu_StrippingSignal.decayname = "K0S --> pi0mumu"

kspi0mumu_StrippingSignal.COUNTER = {}
kspi0mumu_StrippingSignal.DEBUG = _DEBUG

kspi0mumu_StrippingSignal.COUNTER["Bender(evts) " + kspi0mumu_StrippingSignal.decayname] = 0
## this corresponds to the TES[" "] location of the pars (without the "/Particles" )
kspi0mumu_StrippingSignal.LookIn = "Kspi0mumu.Strip/Phys/K0s2Pi0MuMuSignalLine"#/Particles"#"Phys/Ks2Pi0MuMuSignalLine"
                                   
algos.append(kspi0mumu_StrippingSignal)

#######################
## for stripping SB 

kspi0mumu_StrippingSideband = StrippingK0SPi0MuMu("BenderKspi0mumuSideband")
kspi0mumu_StrippingSideband.DST = False
kspi0mumu_StrippingSideband.decayname = "K0S --> pi0mumu"

kspi0mumu_StrippingSideband.COUNTER = {}
kspi0mumu_StrippingSideband.DEBUG = _DEBUG

kspi0mumu_StrippingSideband.COUNTER["Bender(evts) " + kspi0mumu_StrippingSideband.decayname] = 0
## this corresponds to the TES[" "] location of the pars (without the "/Particles" )
kspi0mumu_StrippingSideband.LookIn = "Kspi0mumu.Strip/Phys/K0s2Pi0MuMuSidebandLine"
algos.append(kspi0mumu_StrippingSideband)



#######################
TES = gaudi.evtsvc()
for algo in algos: gaudi.addAlgorithm(algo)

gaudi.initialize()

##############

## add your custom functions
for algo in algos:
    algo.extraFunctions = [trackHits, globalEvtVars, triggerBlock,  muIDDetailedInfo, more_muon_things]#triggerBlockDaughters]
    # if data, set this to false
    algo.MC_INFO = SIMULATION
    ###
    algo.TRIGGER = 1
    ## custom code, just ignore
    algo.NTupleLUN = "T"
    algo.addedKeys = []
    algo.TUP = 1
    algo.COUNTER['negSq'] = 0
    algo.COUNTER["weird"] = 0
    algo.COUNTER["EVT"] = 0
    algo.COUNTER["Sel"] = 0
    algo.COUNTER["MuMuCouplesAnalyzed"] = 0

    algo.evt_of = 1e6

    algo.l0BankDecoder=algo.tool(cpp.IL0DUFromRawTool,'L0DUFromRawTool')
    algo.rawBankDecoder=algo.tool(cpp.IOTRawBankDecoder,'OTRawBankDecoder')
    algo.PVRefitter = algo.tool(cpp.IPVReFitter,"AdaptivePVReFitter")
    algo.LifeFitter = algo.tool(cpp.ILifetimeFitter,"PropertimeFitter")
    algo.Geom =  algo.tool(cpp.IDistanceCalculator,"LoKi::DistanceCalculator")
   # algo.tistostool = algo.tool( cpp.ITriggerTisTos , 'TriggerTisTos')
    algo.tisTosToolL0 = algo.tool(cpp.ITriggerTisTos , 'L0TriggerTisTos')
    algo.tistostool1 = algo.tool( cpp.ITriggerTisTos , 'Hlt1TriggerTisTos', parent = algo)
    algo.tistostool2 = algo.tool( cpp.ITriggerTisTos , 'Hlt2TriggerTisTos', parent = algo)
    
   
    algo.l0List = l0List_2011
    algo.hlt1List = hlt1List_2011
    algo.hlt2List = hlt1List_2011
    if algo.MC_INFO :
        algo.matcher = gaudi.toolsvc().create("MCMatchObjP2MCRelator",interface="IP2MCP")
        algo.extraFunctions += [KsPi0MuMuMCtruth]




if INTERACTIVE:
    gaudi.run(INTERACTIVE)
    TES = gaudi.evtsvc()
    DET = gaudi.detsvc()
    

else:
    gaudi.run(-1)
    gaudi.stop()
    gaudi.finalize()
