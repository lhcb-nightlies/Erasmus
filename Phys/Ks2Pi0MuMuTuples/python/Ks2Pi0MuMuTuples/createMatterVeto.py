#! /usr/bin/env python
from ROOT import *

from Bender.MainMC import *
import sys
from Gaudi.Configuration import NTupleSvc, EventSelector
from Configurables import DaVinci

from GaudiPython import *

SAMPLE = "MCUpgrade_MD"
#SAMPLE = "MC12_Up"
#SAMPLE = "Data15_Down"

## class Crap(Algo):
##     def initialize ( self ) :
##         sc = Algo.initialize ( self )# used to be AlgoMC ## initialize the base class
##         if sc.isFailure() : return sc
        

##     def analyse(self):
##         TES = appMgr().evtsvc()
##         det = appMgr().detsvc()
##         return SUCCESS
##     def finalize(self): return Algo.finalize(self)
                   

SIMULATION = int("MC" in SAMPLE)
INTERACTIVE = 1
TUPLE_PATH = os.environ["HOME"] + "/vol5/"
job = "" + (not INTERACTIVE )*("_" + sys.argv[-1])
if ".py" in job: job = ""


DataDic = {}
DataDic["MC12_Strip_Up"] = "MC_2012_34112402_Beam4000GeV2012MagUpNu2.5Pythia8_Sim08f_Digi13_Trig0x409f0045_Reco14a_Stripping20r0p1Filtered_KSPI0MUMU.STRIP.DST" + job + ".py"
DataDic["MC12_Strip_Down"] = "MC_2012_34112402_Beam4000GeV2012MagDownNu2.5Pythia8_Sim08f_Digi13_Trig0x409f0045_Reco14a_Stripping20r0p1Filtered_KSPI0MUMU.STRIP.DST" + job +".py"
DataDic["Data12_Down"] = "Stp21_Leptonic_MagDown2012" + job+ ".py"
DataDic["Data12_Up"] = "Stp21_Leptonic_MagUp2012" + job + ".py"
DataDic["MC12_Down"] = "MC_2012_34112401_Beam4000GeV2012MagDownNu2.5Pythia8_Sim08e_Digi13_Trig0x409f0045_Reco14a_Stripping20NoPrescalingFlagged_ALLSTREAMS.DST.py"
DataDic["MC12_Up"]= "MC12_Mu_Pythia8.py"
DataDic["MC12_MB_Down"] = "MC_2012_30000000_Beam4000GeV2012MagDownNu2.5Pythia8_Sim08f_Digi13_Trig0x409f0045_Reco14a_Stripping20NoPrescalingFlagged_ALLSTREAMS.DST.py"
DataDic["MCUpgrade_MD"] = "Upgrade_MD.py"
DataDic["Data15_Down"] = "S24_MD" + job + ".py"
DataDic["Data15_Up"] = "S24_MU"+job+".py"

DataTypeDic = {}
for key in DataDic.keys():
    if "12" in key : DataTypeDic[key] = "2012"
    elif "11" in key: DataTypeDic[key] = "2011"
    elif "15" in key: DataTypeDic[key] = "2015"
    elif "Upgrade" in key: DataTypeDic[key] = "Upgrade"
    
dataType = DataTypeDic[SAMPLE]

dataFormats = {}

for key in DataDic.keys():
    if "_Strip_" in key:
        dataFormats[key] = "MDST"
    elif "MCUpgrade" in key:
        dataFormats[key] = "LDST"
    elif "MC12" in key:
        dataFormats[key] = "DST"
    elif "Data15" in key :
        dataFormats[key] = "DST"
    elif "Data" in key :
        dataFormats[key] = "MDST"


dataFormat = dataFormats[SAMPLE]
####Examples of Stp  LINE PATHS:   ## Otherwise use dst-dump -f <the eos file> to see the content including unpacking
#/Event/Phys/Lambda02PiMuLine/Particles
#/Event/Kspi0mumu.Strip/pPhys/Particles ##PACKED!!

RootsInTES = {}
RootsInTES["MDST"] = "/Event/Leptonic/"
RootsInTES["DST"] = False
RootsInTES["LDST"] = False
RootInTES = RootsInTES[dataFormat]


#
#Raw event juggler to split Other/RawEvent into Velo/RawEvent and Tracker/RawEvent
#
#from Configurables import RawEventJuggler
#juggler = RawEventJuggler( DataOnDemand=True, Input=2.0, Output=4.0 )


DaVinci().EvtMax = 0
if RootInTES:
    DaVinci().InputType = "DST"#dataFormat
    DaVinci().RootInTES = RootInTES
DaVinci().DataType = dataType
from Configurables import CondDB
CondDB().Upgrade    = ("Upgrade" in SAMPLE)

# is this data?
DaVinci().Simulation = SIMULATION
DaVinci().Lumi = True
# if MC, add the SeqKsResolved
DaVinci().UserAlgorithms = []

#TisTos = GaudiSequencer("TisTos")
#DaVinci().UserAlgorithms += TisTos

def Eostize(thingie):
    x = []#.replace("LFN:", "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod")
    for thing in thingie.Input:
        x.append(thing.replace("LFN:", "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod"))
    thingie.Input = x


#TUPLE_FILE = TUPLE_PATH + "kspi0mumu_ntuple" + SAMPLE + job + ".root"
#NTupleSvc( Output =["T DATAFILE='"+ TUPLE_FILE + "' TYP='ROOT' OPT='NEW'"] )


importOptions("$KS2PI0MUMUTUPLESROOT/python/datacards/" + DataDic[SAMPLE])
Eostize(EventSelector())

gaudi = AppMgr()
#gaudi.addAlgorithm(Crap("doNothing"))
gaudi.initialize()
gaudi.run(INTERACTIVE)
TES = gaudi.evtsvc()
DET = gaudi.detsvc()
sensor = DET["/dd/Structure/LHCb/BeforeMagnetRegion/VP/VPRight/Module47WithSupport/Module47/Ladder3/Sensor3"]
h = {}
for side in ["Left", "Right"]:
    h[side] = []#TH1F(side, side, 2000, -400, 900) 
for module in range(52):
    if module%2 : side = "Right"
    else: side = "Left"
    mod = "0"*(module<10) + str(module)
    lad = "0"
    name = "/dd/Structure/LHCb/BeforeMagnetRegion/VP/VP" + side +"/Module"+mod + "WithSupport/Module"+mod+"/Ladder"+lad+"/Sensor" + lad
    sensor0 =  DET[name]
    if sensor0.z() <0 : continue
    if sensor0.z()> 700: continue
    for ladder in range(4):
        lad = str(ladder)
        name = "/dd/Structure/LHCb/BeforeMagnetRegion/VP/VP" + side +"/Module"+mod + "WithSupport/Module"+mod+"/Ladder"+lad+"/Sensor" + lad
        sensor =  DET[name]
        h[side].append(sensor.z())

#h["Left"].SetLineColor(kRed)
#h["Right"].SetLineColor(kBlue)
#h["Left"].Draw()
#h["Right"].Draw("same")
        
#Blue = Right = Down??
# Ref = Left = Up??
def createBox(z, side):
    if side == "Left": region = " x < 5"# && y ?"
    else : region = " x > -5 "#&& y ?"
    z0 = str(z)
    return "(abs(Z - " +z0 +") < TOL && " + region + ")"

MatterVeto = "!("
for side in h:
    for z in h[side]:
        MatterVeto += createBox(z,side) + "||"

MatterVeto += ")"
MatterVeto = MatterVeto.replace("||)",")").replace("- -","+")
import cPickle
cPickle.dump(MatterVeto, file(os.environ["HOME"] + "/MatterVeto","w"))
cPickle.dump(h, file(os.environ["HOME"] + "/SensorZs","w"))

