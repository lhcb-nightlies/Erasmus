#-- GAUDI jobOptions generated on Wed Jul 22 17:54:57 2015
#-- Contains event types : 
#--   34112402 - 8 files - 15482 events - 3.46 GBytes


#--  Extra information about the data processing phases:


#--  Processing Pass Step-127022 

#--  StepId : 127022 
#--  StepName : Stripping20r0p1Filtered for RD WG (Xabier) 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v36r2 
#--  OptionFiles : $RDCONFIGOPTS/FilterKsPi0MuMu-Stripping20.py;$APPCONFIGOPTS/DaVinci/DataType-2012.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r203;RDConfig.v1r12 
#--  Visible : Y 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(['LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00041726/0000/00041726_00000001_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00041726/0000/00041726_00000002_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00041726/0000/00041726_00000003_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00041726/0000/00041726_00000004_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00041726/0000/00041726_00000005_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00041726/0000/00041726_00000006_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00041726/0000/00041726_00000007_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00041726/0000/00041726_00000008_1.kspi0mumu.strip.dst'
], clear=True)
