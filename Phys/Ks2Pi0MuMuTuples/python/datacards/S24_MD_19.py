from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([ 
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038496_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038547_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038548_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038549_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038550_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038646_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038647_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038648_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038702_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038703_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038778_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038799_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038800_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038838_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038845_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038859_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038874_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038884_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038898_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038915_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038916_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038982_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038983_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038984_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00038985_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039080_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039081_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039082_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039083_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039084_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039085_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039213_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039220_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039239_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039240_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039241_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039301_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039357_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039358_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039359_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039422_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039441_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039455_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039467_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039496_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039497_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039533_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039541_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039542_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039584_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039585_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039639_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039689_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039709_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039710_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039774_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039775_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039776_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039777_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039778_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039882_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039905_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039906_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039971_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039972_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039973_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0003/00049592_00039974_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040057_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040073_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040114_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040123_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040150_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040151_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040152_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040153_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040257_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040258_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040259_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040260_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040261_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040262_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040395_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040422_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040423_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040482_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040483_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040550_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040580_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040581_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040624_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040642_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040653_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040671_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040672_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040673_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040674_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040774_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040790_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040791_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0004/00049592_00040830_1.dimuon.dst'
])
