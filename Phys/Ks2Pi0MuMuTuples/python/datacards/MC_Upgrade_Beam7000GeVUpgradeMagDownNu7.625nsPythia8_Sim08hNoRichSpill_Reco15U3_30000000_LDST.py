#-- GAUDI jobOptions generated on Thu Mar  3 13:42:47 2016
#-- Contains event types : 
#--   30000000 - 339 files - 2999972 events - 1173.80 GBytes


#--  Extra information about the data processing phases:


#--  Processing Pass Step-128250 

#--  StepId : 128250 
#--  StepName : Sim08h for Upgrade with nu = 7.6 (L=2x10^33), with spillover - MD - 2015 Baseline Geometry - Pythia8 
#--  ApplicationName : Gauss 
#--  ApplicationVersion : v47r2 
#--  OptionFiles : $APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu7.6-HorExtAngle.py;$APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py;$APPCONFIGOPTS/Conditions/IgnoreCaliboffDB_LHCBv38r7.py;$DECFILESROOT/options/@{eventType}.py;$LBPYTHIA8ROOT/options/Pythia8.py;$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py;$APPCONFIGOPTS/Gauss/Gauss-Upgrade-Baseline-20150522.py;$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py 
#--  DDDB : dddb-20150702 
#--  CONDDB : sim-20150716-vc-md100 
#--  ExtraPackages : AppConfig.v3r231;DecFiles.v27r50 
#--  Visible : Y 


#--  Processing Pass Step-127608 

#--  StepId : 127608 
#--  StepName : Merge15 for Sim LDST 
#--  ApplicationName : LHCb 
#--  ApplicationVersion : v38r5 
#--  OptionFiles : $APPCONFIGOPTS/Merging/CopyDST.py 
#--  DDDB : None 
#--  CONDDB : None 
#--  ExtraPackages : AppConfig.v3r207 
#--  Visible : N 


#--  Processing Pass Step-128230 

#--  StepId : 128230 
#--  StepName : Reco15U3 for Upgrade studies - 2015 Baseline Geometry - LDST 
#--  ApplicationName : Brunel 
#--  ApplicationVersion : v48r0 
#--  OptionFiles : $APPCONFIGOPTS/Brunel/MC-WithTruth.py;$APPCONFIGOPTS/Brunel/Brunel-Upgrade-Baseline-20150522.py;$APPCONFIGOPTS/Brunel/Upgrade-RichPmt.py;$APPCONFIGOPTS/Brunel/patchUpgrade1.py;$APPCONFIGOPTS/Brunel/ldst.py;$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r231 
#--  Visible : Y 


#--  Processing Pass Step-128288 

#--  StepId : 128288 
#--  StepName : Digi14U4 for Upgrade studies with spillover - 2015 Baseline NoRichSpillover NoNoise 
#--  ApplicationName : Boole 
#--  ApplicationVersion : v29r8 
#--  OptionFiles : $APPCONFIGOPTS/Boole/Default.py;$APPCONFIGOPTS/Boole/Boole-Upgrade-Baseline-20150522.py;$APPCONFIGOPTS/Boole/EnableSpillover.py;$APPCONFIGOPTS/Boole/Upgrade-RichMaPMT-NoSpilloverDigi.py;$APPCONFIGOPTS/Boole/xdigi.py;$APPCONFIGOPTS/Boole/Boole-Upgrade-NoNoise.py;$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r232 
#--  Visible : N 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000001_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000002_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000003_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000004_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000005_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000006_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000007_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000008_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000009_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000010_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000011_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000012_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000013_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000014_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000015_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000016_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000017_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000018_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000019_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000020_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000021_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000022_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000023_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000024_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000025_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000026_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000027_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000028_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000029_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000030_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000031_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000032_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000033_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000034_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000035_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000036_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000037_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000038_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000039_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000040_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000041_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000042_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000043_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000044_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000045_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000046_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000047_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000048_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000050_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000051_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000053_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000054_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000056_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000057_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000060_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000061_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000062_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000063_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000064_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000065_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000066_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000067_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000068_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000069_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000070_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000071_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000072_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000073_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000074_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000075_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000076_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000077_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000078_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000079_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000080_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000081_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000082_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000083_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000084_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000085_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000086_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000087_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000088_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000089_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000090_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000091_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000092_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000093_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000094_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000095_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000096_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000097_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000098_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000099_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000100_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000101_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000102_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000103_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000104_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000105_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000106_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000107_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000108_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000109_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000110_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000111_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000112_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000113_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000114_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000115_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000116_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000117_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000118_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000119_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000120_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000121_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000122_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000123_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000124_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000125_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000126_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000127_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000128_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000129_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000130_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000131_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000132_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000133_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000134_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000135_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000136_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000137_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000138_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000139_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000140_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000141_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000142_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000143_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000144_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000145_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000146_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000147_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000148_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000149_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000150_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000151_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000152_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000153_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000154_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000155_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000158_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000159_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000160_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000161_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000162_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000163_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000164_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000165_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000166_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000167_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000168_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000169_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000170_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000171_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000172_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000173_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000174_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000175_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000176_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000177_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000178_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000179_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000180_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000181_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000183_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000184_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000185_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000186_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000187_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000188_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000189_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000190_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000191_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000192_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000193_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000194_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000195_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000196_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000197_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000198_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000199_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000200_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000201_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000202_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000203_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000204_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000205_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000206_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000207_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000208_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000209_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000210_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000211_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000212_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000213_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000214_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000215_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000216_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000217_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000218_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000219_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000220_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000221_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000222_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000223_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000224_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000226_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000227_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000228_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000229_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000230_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000231_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000232_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000233_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000234_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000235_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000236_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000237_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000238_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000239_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000240_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000241_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000242_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000243_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000244_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000245_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000246_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000247_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000248_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000249_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000250_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000251_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000252_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000253_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000254_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000255_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000256_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000257_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000258_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000259_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000260_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000261_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000262_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000263_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000264_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000265_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000266_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000267_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000268_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000269_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000270_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000271_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000272_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000273_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000274_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000275_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000276_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000277_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000278_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000279_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000280_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000281_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000282_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000283_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000284_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000285_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000286_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000287_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000288_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000289_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000290_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000291_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000292_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000293_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000294_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000295_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000296_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000297_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000298_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000299_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000300_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000301_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000302_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000303_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000304_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000305_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000306_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000307_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000308_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000309_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000310_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000311_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000312_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000313_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000314_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000315_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000316_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000317_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000318_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000319_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000320_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000321_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000322_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000323_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000324_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000325_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000326_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000327_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000328_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000329_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000330_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000331_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000332_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000333_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000334_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000335_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000336_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000337_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000338_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000339_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000340_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000341_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000342_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000343_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000344_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000345_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000346_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000347_1.ldst',
'LFN:/lhcb/MC/Upgrade/LDST/00047190/0000/00047190_00000348_1.ldst'
], clear=True)
