#-- GAUDI jobOptions generated on Thu Feb 18 15:31:47 2016
#-- Contains event types : 
#--   34112401 - 9 files - 51899 events - 30.57 GBytes


#--  Extra information about the data processing phases:


#--  Processing Pass Step-128496 

#--  StepId : 128496 
#--  StepName : KS2Pi0MuMu MCUpgradeFilter-fixed 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v37r0 
#--  OptionFiles : $APPCONFIGOPTS/Conditions/Upgrade.py;$APPCONFIGOPTS/DaVinci/DataType-Upgrade.py;$APPCONFIGOPTS/DaVinci/InputType-LDST.py;$HLTCONFIGOPTS/KS2Pi0MuMu.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r231;HLTConfig.v1r11 
#--  Visible : Y 


#--  Processing Pass Step-127829 

#--  StepId : 127829 
#--  StepName : Merge15 for Sim MCFILTER.LDST (copy of 127813 but with MCFILTER.LDST) 
#--  ApplicationName : LHCb 
#--  ApplicationVersion : v38r6 
#--  OptionFiles : $APPCONFIGOPTS/Merging/CopyDST.py 
#--  DDDB : None 
#--  CONDDB : None 
#--  ExtraPackages : AppConfig.v3r210 
#--  Visible : N 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mlucioma/00047611_00000001_1.mcfilter.ldst',
'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mlucioma/00047611_00000002_1.mcfilter.ldst',
'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mlucioma/00047611_00000003_1.mcfilter.ldst',
'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mlucioma/00047611_00000004_1.mcfilter.ldst',
'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mlucioma/00047611_00000005_1.mcfilter.ldst',
'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mlucioma/00047611_00000006_1.mcfilter.ldst',
'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mlucioma/00047611_00000007_1.mcfilter.ldst',
'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mlucioma/00047611_00000008_1.mcfilter.ldst',
'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mlucioma/00047611_00000009_1.mcfilter.ldst'
], clear=True)
