from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([ 
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053093_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053103_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053144_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053164_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053165_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053185_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053197_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053211_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053225_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053239_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053279_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053280_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053281_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053282_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053321_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053335_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053349_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053392_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053393_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053394_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053395_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053452_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053453_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053454_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053488_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053503_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053517_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053533_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053570_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053571_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053572_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053573_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053609_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053627_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053646_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053666_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053687_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053707_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053708_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053728_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053742_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053756_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053770_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053784_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053800_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053824_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053825_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053871_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053872_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053873_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053952_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053953_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053954_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053955_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053956_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00053957_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054012_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054058_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054059_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054060_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054061_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054109_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054110_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054132_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054146_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054162_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054177_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054191_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054197_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054206_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054221_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054230_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054268_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054269_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054270_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054271_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054314_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054315_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054352_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054353_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054354_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054401_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054402_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054403_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054481_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054482_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054483_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054484_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054485_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054486_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054551_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054552_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054621_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054622_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054623_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054624_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054625_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054626_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054727_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054728_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054729_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054730_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054731_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054732_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054787_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054803_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054817_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054831_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054845_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054859_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054875_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054889_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054905_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054923_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054937_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054952_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054966_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054980_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00054994_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00055009_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00055023_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00055037_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00055049_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00055107_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00055108_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00055110_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00055111_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00055112_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00055205_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0005/00041834_00055206_1.leptonic.mdst'
])
