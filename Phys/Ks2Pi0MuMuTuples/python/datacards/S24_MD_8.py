from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([ 
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00012959_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00012977_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00012995_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013012_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013024_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013032_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013079_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013087_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013096_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013142_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013212_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013217_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013222_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013243_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013285_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013292_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013296_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013298_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013316_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013411_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013453_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013454_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013461_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013476_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013497_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013506_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013528_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013554_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013584_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013595_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013610_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013615_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013642_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013645_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013650_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013678_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013718_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013745_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013809_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013816_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013833_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013852_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013867_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013916_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013933_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013951_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013970_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00013991_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014005_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014020_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014054_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014062_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014072_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014084_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014123_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014132_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014145_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014179_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014189_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014208_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014224_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014231_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014234_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014275_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014295_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014329_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014344_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014348_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014382_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014409_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014430_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014437_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014490_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014522_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014543_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014554_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014604_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014616_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014622_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014645_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014712_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014723_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014766_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014784_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014802_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014812_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014821_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014850_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014857_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014907_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014912_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014964_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014978_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00014981_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00015010_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00015021_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00015040_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00015056_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00015068_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0001/00049592_00015088_1.dimuon.dst'
])
