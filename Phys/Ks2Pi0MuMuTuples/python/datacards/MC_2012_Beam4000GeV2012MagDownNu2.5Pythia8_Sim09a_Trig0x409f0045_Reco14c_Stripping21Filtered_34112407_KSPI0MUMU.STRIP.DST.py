#-- GAUDI jobOptions generated on Wed Aug 10 11:58:06 2016
#-- Contains event types : 
#--   34112407 - 15 files - 30410 events - 6.76 GBytes


#--  Extra information about the data processing phases:


#--  Processing Pass Step-129823 

#--  StepId : 129823 
#--  StepName : Stripping21Filtered for RDWG (Miriam) - to use in Sim09 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v36r1p3 
#--  OptionFiles : $RDCONFIGOPTS/FilterKsPi0MuMu-Stripping21.py;$APPCONFIGOPTS/DaVinci/DV-RedoCaloPID-Stripping21.py;$APPCONFIGOPTS/DaVinci/DataType-2012.py;$APPCONFIGOPTS/DaVinci/InputType-DST.py;$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r284;RDConfig.v1r20 
#--  Visible : Y 


#--  Processing Pass Step-129824 

#--  StepId : 129824 
#--  StepName : Merge14 for Stripping21 RDWG Filtered Productions (Miriam) 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v40r1p3 
#--  OptionFiles : $APPCONFIGOPTS/Merging/DVMergeDST.py;$APPCONFIGOPTS/DaVinci/DataType-2012.py;$APPCONFIGOPTS/Merging/WriteFSR.py;$APPCONFIGOPTS/Merging/MergeFSR.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r284 
#--  Visible : N 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00052953/0000/00052953_00000001_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00052953/0000/00052953_00000002_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00052953/0000/00052953_00000003_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00052953/0000/00052953_00000004_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00052953/0000/00052953_00000005_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00052953/0000/00052953_00000007_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00052953/0000/00052953_00000008_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00052953/0000/00052953_00000009_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00052953/0000/00052953_00000010_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00052953/0000/00052953_00000011_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00052953/0000/00052953_00000012_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00052953/0000/00052953_00000013_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00052953/0000/00052953_00000014_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00052953/0000/00052953_00000015_1.kspi0mumu.strip.dst',
'LFN:/lhcb/MC/2012/KSPI0MUMU.STRIP.DST/00052953/0000/00052953_00000016_1.kspi0mumu.strip.dst'
], clear=True)
