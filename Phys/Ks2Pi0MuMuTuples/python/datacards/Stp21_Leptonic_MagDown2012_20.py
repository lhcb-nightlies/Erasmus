from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([ 
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016539_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016540_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016541_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016589_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016606_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016633_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016658_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016659_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016681_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016700_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016717_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016733_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016767_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016768_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016769_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016824_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016825_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016826_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016827_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016865_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016887_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016888_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016908_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016922_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016955_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016956_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016957_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00016990_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017026_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017027_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017028_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017089_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017090_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017091_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017092_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017133_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017149_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017198_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017199_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017200_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017201_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017202_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017295_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017296_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017297_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017298_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017299_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017300_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017357_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0001/00041836_00017410_1.leptonic.mdst'
])
