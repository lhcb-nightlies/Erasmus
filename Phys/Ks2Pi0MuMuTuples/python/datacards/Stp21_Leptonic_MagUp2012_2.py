from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([ 
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004391_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004394_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004405_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004433_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004440_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004463_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004472_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004524_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004545_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004599_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004600_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004620_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004645_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004661_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004665_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004683_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004684_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004697_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004721_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004722_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004734_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004739_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004756_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004762_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004778_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004790_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004801_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004809_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004822_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004853_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004854_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004895_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004903_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004916_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004925_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004952_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00004988_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005000_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005013_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005019_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005030_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005045_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005070_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005084_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005108_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005136_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005150_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005161_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005173_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005198_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005218_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005231_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005240_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005265_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005307_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005315_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005322_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005336_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005359_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005369_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005376_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005388_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005408_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005417_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005439_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005478_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005503_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005515_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005523_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005537_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005560_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005565_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005610_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005619_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005631_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005635_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005654_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005669_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005682_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005692_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005701_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005717_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005732_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005742_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005779_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005781_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005790_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005793_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005812_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005829_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005830_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005843_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005886_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005895_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005963_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005967_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00005980_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006021_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006033_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006054_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006061_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006078_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006081_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006100_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006117_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006131_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006146_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006153_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006171_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006204_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006209_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006221_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006236_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006245_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006283_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006293_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006306_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006322_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006358_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006381_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006402_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006410_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006434_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006445_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006461_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006469_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006491_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006505_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006539_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041834/0000/00041834_00006543_1.leptonic.mdst'
])
