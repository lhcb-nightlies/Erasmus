from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([ 
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00040802_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00040832_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00040851_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00040878_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00040879_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00040941_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00040942_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00040943_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00040944_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00040945_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041036_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041037_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041038_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041039_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041040_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041107_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041108_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041109_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041164_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041165_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041166_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041167_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041252_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041253_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041254_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041255_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041256_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041257_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041354_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041355_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041356_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041357_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041358_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041451_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041452_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041453_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041454_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041455_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041456_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041550_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041551_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041552_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041553_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041554_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041627_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041628_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041629_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041662_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041720_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0004/00041836_00041721_1.leptonic.mdst'
])
