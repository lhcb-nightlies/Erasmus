from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([ 
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049575/0000/00049575_00004412_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049575/0000/00049575_00004445_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049575/0000/00049575_00004458_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049575/0000/00049575_00004477_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049575/0000/00049575_00004481_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049575/0000/00049575_00004497_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049575/0000/00049575_00004523_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049575/0000/00049575_00004534_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049575/0000/00049575_00004546_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049575/0000/00049575_00004559_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049575/0000/00049575_00004571_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049575/0000/00049575_00004583_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049575/0000/00049575_00004596_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049575/0000/00049575_00004609_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049575/0000/00049575_00004621_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049575/0000/00049575_00004633_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049575/0000/00049575_00004646_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049575/0000/00049575_00004692_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000142_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000177_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000236_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000283_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000317_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000329_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000434_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000482_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000499_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000517_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000531_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000544_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000557_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000563_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000600_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000652_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000673_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000691_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000700_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000708_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000723_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000785_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000823_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000850_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000885_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000899_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00000947_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001000_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001045_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001090_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001125_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001167_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001169_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001182_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001194_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001195_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001230_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001282_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001291_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001306_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001330_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001382_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001469_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001480_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001535_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001571_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001612_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001651_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001655_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001702_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001707_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001722_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001749_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001755_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001783_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001813_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001838_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001889_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001906_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001942_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001967_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00001980_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002018_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002038_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002041_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002057_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002067_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002101_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002156_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002178_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002191_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002204_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002217_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002230_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002243_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002258_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002280_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002310_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002325_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002338_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002372_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision15/DIMUON.DST/00049592/0000/00049592_00002426_1.dimuon.dst'
])
