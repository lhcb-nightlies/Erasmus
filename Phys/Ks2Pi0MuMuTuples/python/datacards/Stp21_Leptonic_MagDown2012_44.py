from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([ 
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038412_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038426_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038442_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038456_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038471_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038486_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038501_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038515_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038530_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038560_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038561_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038589_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038604_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038618_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038632_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038648_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038665_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038682_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038696_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038702_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038718_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038734_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038779_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038780_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038781_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038782_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038819_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038829_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038889_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038890_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038891_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038892_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038893_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038894_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038966_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00038991_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00039005_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00039027_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00039028_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00039052_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00039072_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00039081_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00039086_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00039096_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00039113_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00039137_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00039138_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00039187_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00039198_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision12/LEPTONIC.MDST/00041836/0003/00041836_00039199_1.leptonic.mdst'
])
