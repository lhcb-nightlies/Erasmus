#-- GAUDI jobOptions generated on Mon Sep 19 19:26:22 2016
#-- Contains event types : 
#--   34112106 - 9 files - 85118 events - 34.39 GBytes


#--  Extra information about the data processing phases:


#--  Processing Pass Step-128495 

#--  StepId : 128495 
#--  StepName : KSMuMu MCUpgradeFilter-fixed 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v37r0 
#--  OptionFiles : $APPCONFIGOPTS/Conditions/Upgrade.py;$APPCONFIGOPTS/DaVinci/DataType-Upgrade.py;$APPCONFIGOPTS/DaVinci/InputType-LDST.py;$HLTCONFIGOPTS/KS2MuMu.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r231;HLTConfig.v1r11 
#--  Visible : Y 


#--  Processing Pass Step-127829 

#--  StepId : 127829 
#--  StepName : Merge15 for Sim MCFILTER.LDST (copy of 127813 but with MCFILTER.LDST) 
#--  ApplicationName : LHCb 
#--  ApplicationVersion : v38r6 
#--  OptionFiles : $APPCONFIGOPTS/Merging/CopyDST.py 
#--  DDDB : None 
#--  CONDDB : None 
#--  ExtraPackages : AppConfig.v3r210 
#--  Visible : N 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/MC/Upgrade/MCFILTER.LDST/00047605/0000/00047605_00000001_1.mcfilter.ldst',
'LFN:/lhcb/MC/Upgrade/MCFILTER.LDST/00047605/0000/00047605_00000002_1.mcfilter.ldst',
'LFN:/lhcb/MC/Upgrade/MCFILTER.LDST/00047605/0000/00047605_00000003_1.mcfilter.ldst',
'LFN:/lhcb/MC/Upgrade/MCFILTER.LDST/00047605/0000/00047605_00000004_1.mcfilter.ldst',
'LFN:/lhcb/MC/Upgrade/MCFILTER.LDST/00047605/0000/00047605_00000005_1.mcfilter.ldst',
'LFN:/lhcb/MC/Upgrade/MCFILTER.LDST/00047605/0000/00047605_00000006_1.mcfilter.ldst',
'LFN:/lhcb/MC/Upgrade/MCFILTER.LDST/00047605/0000/00047605_00000007_1.mcfilter.ldst',
'LFN:/lhcb/MC/Upgrade/MCFILTER.LDST/00047605/0000/00047605_00000008_1.mcfilter.ldst',
'LFN:/lhcb/MC/Upgrade/MCFILTER.LDST/00047605/0000/00047605_00000009_1.mcfilter.ldst'
], clear=True)
