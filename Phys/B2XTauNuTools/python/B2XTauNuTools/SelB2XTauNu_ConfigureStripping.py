#Get the original stripping line candidates
from Configurables import ProcStatusCheck, DaVinci, EventNodeKiller
from StrippingConf.Configuration import StrippingConf
from StrippingConf.StrippingStream import StrippingStream
from StrippingSelections.StrippingSL import StrippingB2XTauNu

#Kill old stripping
eventNodeKiller = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = ['/Event/AllStreams',
                         '/Event/Strip']

DaVinci().appendToMainSequence([eventNodeKiller])   # Kill old stripping banks first

# Now build the MCStrippingStreamB2XTauNu
MCStrippingStreamB2XTauNu = StrippingStream("MCStrippingStreamB2XTauNu")
ConfB2XTauNu = StrippingB2XTauNu.B2XTauNuAllLinesConf(
    name="B2XTauNu",
    #config=StrippingB2XTauNu.confdict
    config=StrippingB2XTauNu.default_config['B2XTauNuAllLines']['CONFIG']
)
MCStrippingStreamB2XTauNu.appendLines( ConfB2XTauNu.lines() )

# Standard configuration of Stripping
MCStrippingConfB2XTauNu = StrippingConf(
    Streams = [MCStrippingStreamB2XTauNu],
    MaxCandidates = 2000,
    AcceptBadEvents = False,
    BadEventSelection = ProcStatusCheck(),
    TESPrefix = 'Strip'
)

DaVinci().appendToMainSequence([MCStrippingConfB2XTauNu.sequence()])

