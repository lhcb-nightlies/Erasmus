import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *

import os, sys, inspect
# realpath() with make your script run, even if you symlink it :)
cmd_folder = os.path.realpath(
    os.path.abspath(
        os.path.split(
            inspect.getfile(inspect.currentframe()))[0]))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

from B2XTauNuTools.TupleHelper import *
outputLevel = INFO

SeqTupleLb2LctaunuNonPhys = GaudiSequencer("SeqTupleLb2LctaunuNonPhys")
TupleLb2LctaunuNonPhys = DecayTreeTuple("Lb2LctaunuNonPhysTuple")
TupleLb2LctaunuNonPhys.OutputLevel = outputLevel
TupleLb2LctaunuNonPhys.Inputs = ["Phys/Lb2LcTauNuNonPhysTauForB2XTauNu/Particles"]

TupleLb2LctaunuNonPhys.Decay = "[(Lambda_b0 -> ^(Lambda_c+ -> ^p+ ^K- ^pi+) ^(tau- -> ^pi- ^pi- ^pi- ))]CC"
TupleLb2LctaunuNonPhys.Branches = {
    "Lambda_b0" : "[(Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (tau- -> pi- pi- pi-))]CC",
    "tau_pion" : "[(Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (tau- -> ^pi- ^pi- ^pi-))]CC",
    "lc_p" : "[(Lambda_b0 -> (Lambda_c+ -> ^p+ K- pi+) (tau- -> pi- pi- pi-))]CC",
    "lc_pion" : "[(Lambda_b0 -> (Lambda_c+ -> p+ K- ^pi+) (tau- -> pi- pi- pi-))]CC",
    "lc_kaon" : "[(Lambda_b0 -> (Lambda_c+ -> p+ ^K- pi+) (tau- -> pi- pi- pi-))]CC",
    "tau" : "[(Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) ^(tau- -> pi- pi- pi-))]CC",
    "Lambda_c" : "[(Lambda_b0 -> ^(Lambda_c+ -> p+ K- pi+) (tau- -> pi- pi- pi-))]CC"}

TupleLb2LctaunuNonPhys.addTool(TupleToolDecay, name='Lambda_b0')
TupleLb2LctaunuNonPhys.addTool(TupleToolDecay, name='Lambda_c')
TupleLb2LctaunuNonPhys.addTool(TupleToolDecay, name='lc_p')
TupleLb2LctaunuNonPhys.addTool(TupleToolDecay, name='lc_kaon')
TupleLb2LctaunuNonPhys.addTool(TupleToolDecay, name='lc_pion')
TupleLb2LctaunuNonPhys.addTool(TupleToolDecay, name='tau')
TupleLb2LctaunuNonPhys.addTool(TupleToolDecay, name='tau_pion')

FinalStateConfig(TupleLb2LctaunuNonPhys.lc_p)
FinalStateConfig(TupleLb2LctaunuNonPhys.tau_pion)
FinalStateConfig(TupleLb2LctaunuNonPhys.lc_pion)
FinalStateConfig(TupleLb2LctaunuNonPhys.lc_kaon)

DalitzConfig(TupleLb2LctaunuNonPhys.tau)
DalitzConfig(TupleLb2LctaunuNonPhys.Lambda_c)

IsoConfig(TupleLb2LctaunuNonPhys.tau)
IsoConfig(TupleLb2LctaunuNonPhys.Lambda_b0)
#LifetimeConfig(TupleLb2LctaunuNonPhys.tau)

ParentConfig(TupleLb2LctaunuNonPhys.tau)
ParentConfig(TupleLb2LctaunuNonPhys.Lambda_c)
ParentConfig(TupleLb2LctaunuNonPhys.Lambda_b0)

CommonConfig(TupleLb2LctaunuNonPhys.Lambda_b0)
CommonConfig(TupleLb2LctaunuNonPhys.Lambda_c)
CommonConfig(TupleLb2LctaunuNonPhys.lc_p)
CommonConfig(TupleLb2LctaunuNonPhys.lc_kaon)
CommonConfig(TupleLb2LctaunuNonPhys.lc_pion)
CommonConfig(TupleLb2LctaunuNonPhys.tau)
CommonConfig(TupleLb2LctaunuNonPhys.tau_pion)
# Kinematic fit is using TupleToolB2XTauNuFit
# which is not used in the analysis
# which causes bugs with Lb2Lctaunu events
#KinematicFitConfig(TupleLb2LctaunuWS.Lambda_b0,isMC)

DTFConfig(TupleLb2LctaunuNonPhys, 'Lambda_c+')

TISTOSToolConfig(TupleLb2LctaunuNonPhys.Lambda_b0)
TupleToolConfig(TupleLb2LctaunuNonPhys)

TupleToolIsoGenericConfig(TupleLb2LctaunuNonPhys)

GaudiSequencer("SeqTupleLb2LctaunuNonPhys").Members.append(TupleLb2LctaunuNonPhys)
SeqTupleLb2LctaunuNonPhys.IgnoreFilterPassed = True
GaudiSequencer("SeqSelLb2LctaunuNonPhys").Members.append(SeqTupleLb2LctaunuNonPhys)

