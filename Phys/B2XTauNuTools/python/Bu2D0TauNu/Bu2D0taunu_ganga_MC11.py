dv = DaVinci()
dv.version = 'v33r2'
dv.platform = 'x86_64-slc5-gcc46-opt'
base=dv.user_release_area+'/DaVinci_'+dv.version+'/Phys/B2XTauNuTools/python/Bu2D0TauNu_MC/'
dv.optsfile = [base+'Bu2D0taunu_strp_MC11.py']

myJobs = [
	 ['Bu2D0taunuMC11a','MCMC11a12565001Beam3500GeV-2011-MagDown-Nu2-EmNoCutsSim05bTrig0x40760037FlaggedReco12aStripping17NoPrescalingFlaggedSTREAMSDST.py']
	#,['Bu2D0DsMC11a'   ,'MCMC11a11836001Beam3500GeV-2011-MagUp-Nu2-EmNoCutsSim05bTrig0x40760037FlaggedReco12aStripping17NoPrescalingFlaggedSTREAMSDST.py']
	,['Bu2D03piMC11a'  ,'MCMC11a12265021Beam3500GeV-2011-MagDown-Nu2-EmNoCutsSim05Trig0x40760037FlaggedReco12aStripping17FlaggedSTREAMSDST.py']
]

for i in myJobs:	

	j = Job (name=i[0],application=dv)

	j.backend          = Dirac()
	j.inputdata        = dv.readInputData(base+i[1])
	j.splitter         = SplitByFiles(filesPerJob=10)
	j.outputfiles      = ['DVNtuple.root','DVHistos.root']
	j.postprocessors   = RootMerger(overwrite=True,ignorefailed=True,files=['DVNtuple.root'])
	j.do_auto_resubmit = True
	j.submit()
