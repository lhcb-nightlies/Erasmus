// Include files

// from Gaudi
#include "GaudiKernel/ToolFactory.h"

// local
#include "TupleToolVtxIsolnVelo.h"
#include "LoKi/IHybridFactory.h"
#include <Kernel/GetIDVAlgorithm.h>
#include <Kernel/IDVAlgorithm.h>
#include <Kernel/IDistanceCalculator.h>
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "Event/Particle.h"
#include "Event/State.h"
#include "Event/Track.h"

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolVtxIsolnVelo
//
// @author Mitesh Patel, Patrick Koppenburg
// @date   2008-04-15
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
// actually acts as a using namespace TupleTool
DECLARE_TOOL_FACTORY( TupleToolVtxIsolnVelo )

	//=============================================================================
	// Standard constructor, initializes variables
	//=============================================================================
TupleToolVtxIsolnVelo::TupleToolVtxIsolnVelo( const std::string& type,
		const std::string& name,
		const IInterface* parent )
: TupleToolBase ( type, name , parent ),m_inputTracks(0)
{
	declareInterface<IParticleTupleTool>(this);
	m_inputTracks.push_back("Rec/Track/Best");
	declareProperty("InputTracks", m_inputTracks );
}

//=============================================================================

StatusCode TupleToolVtxIsolnVelo::initialize()
{
	StatusCode sc = TupleToolBase::initialize();
	return sc;
}

//=============================================================================

StatusCode TupleToolVtxIsolnVelo::fill( const Particle* mother
		, const Particle* P
		, const std::string& head
		, Tuples::Tuple& tuple )
{

	const std::string prefix = fullName(head);
	Assert( P && mother
			, "This should not happen, you are inside TupleToolVtxIsolnVelo.cpp :(" );

	bool test = true;
	if(P->isBasicParticle() || isPureNeutralCalo(P) ){
		Error("Not a composite particle! Can't vertex it.");
		return StatusCode::FAILURE;
	}

	// --------------------------------------------------

	const LHCb::Vertex* vtx = P->endVertex();
	if ( msgLevel(MSG::DEBUG) )
	{
		debug() << "vertex for P, ID " << P->particleID().pid() 
			<< " = " << vtx << " at " << vtx->position() << endmsg;
	}
	if ( !vtx )
	{
		Error("Can't retrieve the  vertex for " + prefix );
		return StatusCode::FAILURE;
	}

	//--------------------------------------------------
	// Get all the particle's final states
	LHCb::Particle::ConstVector source;
	LHCb::Particle::ConstVector target;
	LHCb::Particle::ConstVector finalStates;

	//   const LHCb::Particle* prefix = P;
	source.push_back(P);

	// The first iteration is for the particle to filter, which has an endVertex
	do 
	{
		target.clear();
		for(LHCb::Particle::ConstVector::const_iterator isource = source.begin();
				isource != source.end(); isource++){

			if(!((*isource)->daughters().empty()))
			{

				const LHCb::Particle::ConstVector& tmp = (*isource)->daughtersVector();

				for ( LHCb::Particle::ConstVector::const_iterator itmp = tmp.begin();
						itmp != tmp.end(); ++itmp )
				{
					target.push_back(*itmp);

					// Add the final states, i.e. particles with proto and ignoring gammas
					if((*itmp)->proto() && !isPureNeutralCalo(*itmp) ) finalStates.push_back(*itmp);
				}
			} // if endVertex
		} // isource
		source = target;
	} while(target.size() > 0);

	if (msgLevel(MSG::DEBUG)) debug() << "Final states size= " <<  finalStates.size()  << endreq;


	//--------------------------------------------------
	// Build vector of particles, excluding signal


	LHCb::Track::ConstVector theTracks;


	for(std::vector<std::string>::iterator i = m_inputTracks.begin();
			i !=m_inputTracks.end(); ++i){

		LHCb::Track::Range intracks=get<LHCb::Track::Range>(*i);

		LHCb::Track::ConstVector tracks(intracks.begin(),intracks.end());

		if (msgLevel(MSG::DEBUG)) debug() << "Getting tracks from " << *i
			<< " with " << (tracks).size() << " tracks" << endreq;


		for(LHCb::Track::ConstVector::const_iterator itr = tracks.begin();
				itr != tracks.end(); ++itr) {
			bool isSignal = false;
			for(LHCb::Particle::ConstVector::const_iterator signal = finalStates.begin();
					signal != finalStates.end(); ++signal){
				const LHCb::ProtoParticle* orig = (*signal)->proto();
				if(!orig) continue;
				const LHCb::Track* strk = orig->track();
				if(strk != (*itr)) continue;
				isSignal = true;
				break;
			}

			// Ignore if it is a signal particle
			if(isSignal) continue;

			// Check that the same particle does not appear twice

			bool isIn = false;
			for(LHCb::Track::ConstVector::const_iterator result = theTracks.begin();
					result != theTracks.end(); ++result){
				if((*result) != (*itr)) continue;
				isIn = true;
				break;
			}

			// Already in ?
			if(isIn) continue;
			if(!isIn) theTracks.push_back(*itr);

		} // itr
	} // m_inputTracks

	if (msgLevel(MSG::DEBUG)) 
		debug() << "Number of tracks to check excluding signal, repeated tracks= "
			<< theTracks.size() << endreq;
	//--------------------------------------------------

	if (msgLevel(MSG::DEBUG))
		debug() <<"Now final states should include only your particles direct desendents. finalStates.size()= "
			<<  finalStates.size()  <<endreq;


	unsigned int ntracks = theTracks.size();
	float weight = 0;
	// Now weight the candidate by the sum of 1/IPChi2 of the track on its vertex
	for ( LHCb::Track::ConstVector::const_iterator itrk = theTracks.begin();
			itrk != theTracks.end(); ++itrk)
	{
		weight += 1.0/ipchi2(*(*itrk),(*vtx));
	} // itrk

	test &= tuple->column( prefix+"_isoVelo_ntracks",  ntracks);
	test &= tuple->column( prefix+"_isoVelo_weight",  weight);


	return StatusCode(test);
}

// Shamelessly stolen from Wouter's TrackVertexer, modified to use vertices instead of recvertices
double TupleToolVtxIsolnVelo::ipchi2( const LHCb::Track& track, const LHCb::Vertex& pv) const
{
		return ipchi2( track.closestState(pv.position().z()),pv) ;
}

double TupleToolVtxIsolnVelo::ipchi2( const LHCb::State& state, const LHCb::Vertex& pv) const
{
	double tx = state.tx() ;
	double ty = state.ty() ;
	double dz = pv.position().z() - state.z() ;
	double dx = state.x() + dz * tx - pv.position().x() ;
	double dy = state.y() + dz * ty - pv.position().y() ;

	const Gaudi::SymMatrix3x3& pvcov = pv.covMatrix() ;
	const Gaudi::SymMatrix5x5& trkcov = state.covariance() ;

	// compute the covariance matrix. first only the trivial parts:
	double cov00 = pvcov(0,0) + trkcov(0,0) ;
	double cov10 = pvcov(1,0) + trkcov(1,0) ;
	double cov11 = pvcov(1,1) + trkcov(1,1) ;

	// add the contribution from the extrapolation
	cov00 += dz*dz*trkcov(2,2) + 2*dz*trkcov(2,0) ;
	cov10 += dz*dz*trkcov(3,2) + dz*(trkcov(3,0)+trkcov(2,1)) ;
	cov11 += dz*dz*trkcov(3,3) + 2*dz*trkcov(3,1) ;

	// add the contribution from pv Z
	cov00 += tx*tx*pvcov(2,2)  -  2*tx*pvcov(2,0) ;
	cov10 += tx*ty*pvcov(2,2)  -  ty*pvcov(2,0) - tx*pvcov(2,1) ;
	cov11 += ty*ty*pvcov(2,2)  -  2*ty*pvcov(2,1) ;

	// invert the covariance matrix
	double D = cov00*cov11 - cov10*cov10 ;
	double invcov00 = cov11 / D ;
	double invcov10 = -cov10 / D ;
	double invcov11 = cov00 / D ;

	return dx*dx * invcov00 + 2*dx*dy * invcov10 + dy*dy * invcov11 ;
}

