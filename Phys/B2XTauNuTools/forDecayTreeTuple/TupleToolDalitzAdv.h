// $Id: TupleToolDalitzAdv.h,v 1.1 2012-04-03 09:50:34 elsasser Exp $
#ifndef _TUPLETOOLDALITZADV_H
#define _TUPLETOOLDALITZADV_H 1

// Include files
// from Gaudi
#include "TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h"            // Interface

/** @class TupleToolDalitzAdv TupleToolDalitzAdv.h
 *
 * \brief Fill Dalitz Plot information 
 *
 * - head_Mxy : Invariant mass of the x-th and y-th daughter (x!=y and x/y start at 1). The order of the particles correspond to the ordering in the decay descriptor.
 * 
 * \sa DecayTreeTuple
 *
 *  @author Christian Elsasser
 *  @date   2012-04-03
 */
class IParticleTransporter;
class TupleToolDalitzAdv : public TupleToolBase, virtual public IParticleTupleTool {
public:
  /// Standard constructor
  TupleToolDalitzAdv( const std::string& type,
		    const std::string& name,
		    const IInterface* parent);

  virtual ~TupleToolDalitzAdv(){}; ///< Destructor

  virtual StatusCode fill( const LHCb::Particle*
			   , const LHCb::Particle*
			   , const std::string&
			   , Tuples::Tuple& );

  StatusCode initialize();

private:

  IParticleTransporter* m_transporter;
  std::string m_transporterName;
    bool m_suppressSameSign;
    bool m_suppressNeutrals;

};

#endif // _TupleToolDalitzAdv_H
