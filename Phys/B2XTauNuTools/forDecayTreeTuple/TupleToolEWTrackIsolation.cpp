 // Include files
#include "GaudiKernel/ToolFactory.h"
 #include "Event/Particle.h"
 // kernel
 #include "GaudiAlg/Tuple.h"
 #include "GaudiAlg/TupleObj.h"
 #include "GaudiKernel/PhysicalConstants.h"
 // local
 #include "TupleToolEWTrackIsolation.h"
 //-----------------------------------------------------------------------------
 // Implementation file for class : TupleToolEWTrackIsolation
 //
 // 2014-12-02 : Simone Bifani
 //-----------------------------------------------------------------------------
 // Declaration of the Algorithm Factory
 DECLARE_TOOL_FACTORY( TupleToolEWTrackIsolation )
   //=============================================================================
   // Standard constructor, initializes variables
   //=============================================================================
   TupleToolEWTrackIsolation::TupleToolEWTrackIsolation( const std::string &type,
 const std::string &name,
                                                           const IInterface *parent)
   : TupleToolBase ( type, name, parent )
   
{
  
 
    declareInterface<IParticleTupleTool>( this );
    declareProperty( "MinConeRadius", m_minConeRadius = 0.,
                      "Set the minimal deltaR of the cone around the seed" );
   declareProperty( "MaxConeRadius", m_maxConeRadius = .5,
                        "Set the maximum deltaR of the cone around the seed" );
   declareProperty( "ConeStepSize", m_coneStepSize = .5,
                        "Set the step of deltaR between two iterations" );
    declareProperty( "TrackType", m_trackType = 3,
                         "Set the type of tracks which are considered inside the cone" );
declareProperty( "ExtreParticlesLocation", m_extraParticlesLocation = "StdAllNoPIDsMuons",
                         "Set the type of particles which are considered in the charged cone" );
   declareProperty( "ExtrePhotonsLocation", m_extraPhotonsLocation = "StdLooseAllPhotons",
                        "Set the type of photons which are considered in the neutral cone" );
    declareProperty( "FillComponents", m_fillComponents = true,
                         "Flag to fill all the 3-momentum components" );
  }
 //=============================================================================
 // Destructor
 //=============================================================================
 TupleToolEWTrackIsolation::~TupleToolEWTrackIsolation() 
{
}
 //=============================================================================
 // Initialization
 //=============================================================================
 StatusCode TupleToolEWTrackIsolation::initialize() 
{
    StatusCode sc = TupleToolBase::initialize();
   if ( sc.isFailure() )
     return sc;
    if ( m_minConeRadius > m_maxConeRadius) 
 {
     if ( msgLevel(MSG::FATAL) )
       fatal() << "Max conesize smaller than min conesize." << endmsg; 
     return StatusCode::FAILURE;
     }
    if ( msgLevel(MSG::DEBUG) )
    debug() << "==> Initialize" << endmsg;
   return StatusCode::SUCCESS;
  }

 //=============================================================================
 // Fill the tuple
 //=============================================================================
 StatusCode TupleToolEWTrackIsolation::fill( const LHCb::Particle *top,
                                               const LHCb::Particle *seed,
                                                const std::string &head,
  Tuples::Tuple &tuple ) 
{ 
    const std::string prefix = fullName( head );
    if ( msgLevel(MSG::DEBUG) )
    debug() << "==> Fill" << endmsg;
    // -- The vector m_decayParticles contains all the particles that belong to the given decay
    // -- according to the decay descriptor.
    // -- Clear the vector with the particles in the specific decay
    m_decayParticles.clear();
 
    // -- Add the mother (prefix of the decay chain) to the vector
    if ( msgLevel(MSG::DEBUG) )
    debug() << "Filling particle with ID " << top->particleID().pid() << endmsg;
   m_decayParticles.push_back( top );
    // -- Save all particles that belo
  saveDecayParticles( top );
    // -- Get all particles in the event
    LHCb::Particles *parts = get<LHCb::Particles>( "Phys/" + m_extraParticlesLocation + "/Particles" );
   if ( parts->size() == 0 ) 
 {
     if ( msgLevel(MSG::WARNING) )
       warning() << "Could not retrieve extra-particles. Skipping" << endmsg;
     return StatusCode::FAILURE;   
     }
 
 
    bool test = true;
 
 
    if ( seed ) 
 {
    if ( msgLevel(MSG::VERBOSE) )
    verbose() << "Start looping through different conesizes" << endmsg;
    // -- Loop over the different conesizes
    double coneSize = m_minConeRadius;
   while ( coneSize <= m_maxConeRadius ) 
      {
    if ( msgLevel(MSG::VERBOSE) )
    verbose() << "Filling variables with conesize " << coneSize << endmsg;
    // -- Convert the conesize to a string
    char coneNumber[4];
   sprintf(coneNumber, "%.2f", coneSize);
   std::string conesize(coneNumber);
    // -- Retrieve information in the charged cone
    int multiplicity = 0;
   std::vector < double > vectorP;
   double scalarPt = 0.;
   int maximumPt_Q = 0.;
   double maximumPt_PX = 0.;
   double maximumPt_PY = 0.;
   double maximumPt_PZ = 0.;
   double maximumPt_PE = 0.;
    StatusCode sc = ChargedCone( seed, parts, coneSize, multiplicity, vectorP, scalarPt, maximumPt_Q, maximumPt_PX, maximumPt_PY, maximumPt_PZ, maximumPt_PE );
 
   if ( sc.isFailure() )
     multiplicity = -1;
 
 
    // -- Create a vector with the summed momentum of all tracks in the cone
    Gaudi::XYZVector coneMomentum;
 
   coneMomentum.SetX( vectorP[0] );
   coneMomentum.SetY( vectorP[1] );
   coneMomentum.SetZ( vectorP[2] );
    // -- Fill the tuple with the variables
   test &= tuple->column( prefix + "_" + conesize + "_cc_mult", multiplicity);  if (m_fillComponents) 
 {
     test &= tuple->column( prefix + "_" + conesize + "_cc_PX", coneMomentum.X() ); 
     test &= tuple->column( prefix + "_" + conesize + "_cc_PY", coneMomentum.Y() ) ;
     test &= tuple->column( prefix + "_" + conesize + "_cc_PZ", coneMomentum.Z() );
     
     }
   test &= tuple->column( prefix + "_" + conesize + "_cc_vPT", sqrt( coneMomentum.Perp2() ) );
   test &= tuple->column( prefix + "_" + conesize + "_cc_sPT", scalarPt );
   if ( coneSize == 0. ) 
 {
     test &= tuple->column( prefix + "_" + conesize + "_cc_maxPt_Q", maximumPt_Q ); 
     test &= tuple->column( prefix + "_" + conesize + "_cc_maxPt_PT", sqrt( pow(maximumPt_PX, 2) + pow(maximumPt_PY, 2) ) );
     if (m_fillComponents) 
   {
       test &= tuple->column( prefix + "_" + conesize + "_cc_maxPt_PX", maximumPt_PX ); 
       test &= tuple->column( prefix + "_" + conesize + "_cc_maxPt_PY", maximumPt_PY );
       test &= tuple->column( prefix + "_" + conesize + "_cc_maxPt_PZ", maximumPt_PZ );
       test &= tuple->column( prefix + "_" + conesize + "_cc_maxPt_PE", maximumPt_PE );
       }
     
     }
    // -- Retrieve information in the neutral cone
    int nmultiplicity = 0;
   std::vector < double > nvectorP;
   double nscalarPt = 0.;
 
   double nmaximumPt_PX = 0.;
 
   double nmaximumPt_PY = 0.;
 
   double nmaximumPt_PZ = 0.;
    // -- Get all photons in the event
    LHCb::Particles *photons = get<LHCb::Particles>( "Phys/" + m_extraPhotonsLocation + "/Particles" );
   if ( photons->size() != 0 ) 
 {
   
     StatusCode nsc = NeutralCone( seed, photons, coneSize, nmultiplicity, nvectorP, nscalarPt, nmaximumPt_PX, nmaximumPt_PY, nmaximumPt_PZ );
   
     if ( nsc.isFailure() )
       nmultiplicity = -1;
     }
 
   else 
 {
   
     if ( msgLevel(MSG::WARNING) )
       Warning( "Could not retrieve photons" );
     nmultiplicity = -1;
     nvectorP.push_back( 0 );
     nvectorP.push_back( 0 );
     nvectorP.push_back( 0 );
     }
    // -- Create a vector with the summed momentum of all tracks in the cone
    Gaudi::XYZVector neutralConeMomentum;
   neutralConeMomentum.SetX( nvectorP[0] );
   neutralConeMomentum.SetY( nvectorP[1] );
   neutralConeMomentum.SetZ( nvectorP[2] );
 
 
    // -- Fill the tuple with the variables
  
                              test &= tuple->column( prefix + "_" + conesize + "_nc_mult", nmultiplicity);
                                if (m_fillComponents) 
                              {
                                  test &= tuple->column( prefix + "_" + conesize + "_nc_PX", neutralConeMomentum.X() );
  test &= tuple->column( prefix + "_" + conesize + "_nc_PY", neutralConeMomentum.Y() ) ;
                                
                                  test &= tuple->column( prefix + "_" + conesize + "_nc_PZ", neutralConeMomentum.Z() );   
                                  }
  test &= tuple->column( prefix + "_" + conesize + "_nc_vPT", sqrt( neutralConeMomentum.Perp2() ) );
                               test &= tuple->column( prefix + "_" + conesize + "_nc_sPT", nscalarPt );
  if ( coneSize == 0. ) 
                              {
                                
                                  test &= tuple->column( prefix + "_" + conesize + "_nc_maxPt_PT", sqrt( pow(nmaximumPt_PX, 2) + pow(nmaximumPt_PY, 2) ) );
                                
                                  if (m_fillComponents) 
                                {
                         test &= tuple->column( prefix + "_" + conesize + "_nc_maxPt_PX", nmaximumPt_PX );
    
                                    test &= tuple->column( prefix + "_" + conesize + "_nc_maxPt_PY", nmaximumPt_PY );
                                  
                                    test &= tuple->column( prefix + "_" + conesize + "_nc_maxPt_PZ", nmaximumPt_PZ );
                                  
                                    
                                    }
                                
                                  
                                  }
                                // -- Increase the counter with the stepsize
                                coneSize += m_coneStepSize;
                                }
  
  }
   else 
      {
        
 
    if ( msgLevel(MSG::WARNING) )
    warning() << "The seed particle is not valid. Skipping" << endmsg;
 
   return StatusCode::FAILURE; 
  }
    if ( msgLevel(MSG::VERBOSE) )
    verbose() << "Stop looping through different conesizes" << endmsg;
 
 
    return StatusCode( test );
 
 
   
  }
 
  //=============================================================================
    // Save the particles in the decay chain (recursive function)
    //=============================================================================
    void TupleToolEWTrackIsolation::saveDecayParticles( const LHCb::Particle *top ) 
 {
   
 
    // -- Get the daughters of the top particle
    const SmartRefVector< LHCb::Particle > daughters = top->daughters();
 

    // -- Fill all the daugthers in m_decayParticles
  for ( SmartRefVector< LHCb::Particle >::const_iterator ip = daughters.begin(); ip != daughters.end(); ++ip ) 
 {
   
 
    // -- If the particle is stable, save it in the vector, or...
    if ( (*ip)->isBasicParticle() ) 
 {
   
     if ( msgLevel(MSG::DEBUG) )
       debug() << "Filling particle with ID " << (*ip)->particleID().pid() << endmsg;
   
     m_decayParticles.push_back( (*ip) );
   
     
     }
 
   else 
 {
   
     // -- if it is not stable, call the function recursively
       m_decayParticles.push_back( (*ip) );
   
     if ( msgLevel(MSG::DEBUG) )
       debug() << "Filling particle with ID " << (*ip)->particleID().pid() << endmsg;
   
     saveDecayParticles( (*ip) );
   
     
     }
 
 
    
  }
 
 
    return;
 
 
   
  }
 
  //=============================================================================
    // Loop over all the tracks in the cone which do not belong to the desired decay
    //=============================================================================
    StatusCode TupleToolEWTrackIsolation::ChargedCone( const LHCb::Particle *seed,
                                                            const LHCb::Particles *parts,
                                                            const double rcut,
                                                            int &mult,
                                                            std::vector < double > &vP,
                                                            double &sPt,
                                                            int &maxPt_Q, double &maxPt_PX, double &maxPt_PY, double &maxPt_PZ, double &maxPt_PE ) 
 {
   

    // -- Initialize values
    mult = 0;
 
   double sPx = 0.;
 
  double sPy = 0.;
 
   double sPz = 0.;
 
   sPt = 0.;
 
   maxPt_Q = 0;
 
   maxPt_PX = 0.;
 
   maxPt_PY = 0.;
 
   maxPt_PZ = 0.;
 
   maxPt_PE = 0.;
 
 
    // -- Get the 4-momentum of the seed particle
   Gaudi::LorentzVector seedMomentum = seed->momentum();
 
 
    for ( LHCb::Particles::const_iterator ip = parts->begin(); ip != parts->end(); ++ip ) 
 {
   
     const LHCb::Particle *particle = (*ip);
   
 
    const LHCb::ProtoParticle *proto = particle->proto();
 
   if ( proto ) 
 {
   
 
    const LHCb::Track *track = proto->track();
 
   if ( track ) 
 {
   
 
    // -- Check if the track belongs to the decay itself
    bool isInDecay = isTrackInDecay( track );
 
   if ( isInDecay )
  continue;
 
 
    // -- Get the 3-momentum of the track
    Gaudi::XYZVector trackMomentum = track->momentum();
 
    //-- Calculate the difference in Eta and Phi between the seed particle and a track
 double deltaPhi = fabs( seedMomentum.Phi() - trackMomentum.Phi() );
    if ( deltaPhi > M_PI ) deltaPhi = 2 * M_PI - deltaPhi;
 double deltaEta = seedMomentum.Eta() - trackMomentum.Eta();
 double deltaR = sqrt( deltaPhi * deltaPhi + deltaEta * deltaEta );
 
 
    if ( track->type() == m_trackType ) 
 {
   
     if ( ( rcut == 0. ) || ( deltaR < rcut ) ) 
   {
     
       // -- Calculate vector information
  sPx += trackMomentum.X();
     
       sPy += trackMomentum.Y();
     
       sPz += trackMomentum.Z();
     

    // -- Calculate scalar information
    sPt += sqrt( trackMomentum.Perp2() );
 
 
    mult++;
 
   
   }
 if ( ( rcut == 0. ) || ( deltaR < rcut ) ) 
 {
   
  /*
  // Extra Electron
  double prsE = 50.;
  double eCalEoP = .10;
  double hCalEoP = .05;
  if ( proto->info( LHCb::ProtoParticle::CaloPrsE, -1. ) > prsE ) {
  if ( proto->info( LHCb::ProtoParticle::CaloEcalE, -1. ) / track->p() > eCalEoP ) {
  if ( ( proto->info( LHCb::ProtoParticle::CaloHcalE, -1. ) > 0 ) && ( proto->info( LHCb::ProtoParticle::CaloHcalE, -1. ) / track->p() < hCalEoP ) ) {
  if ( track->pt() > sqrt(pow(maxPt_PX, 2.) + pow(maxPt_PY, 2.)) ) {
  maxPt_Q = track->charge();
  maxPt_PX = track->momentum().X();
  maxPt_PY = track->momentum().Y();
  maxPt_PZ = track->momentum().Z();
  maxPt_PE = particle->momentum().E();
  }
  }
  }
  }
  */
    // Extra Muon
    double minP = 10.e3;
  if ( track->p() > minP ) 
 {
   
     const LHCb::MuonPID *muonPID = proto->muonPID();
   
     if ( muonPID ) 
   {
     
       if ( muonPID->IsMuon() ) 
     {
       
         if ( track->pt() > sqrt(pow(maxPt_PX, 2.) + pow(maxPt_PY, 2.)) ) 
       {
         
           maxPt_Q = track->charge();
         
           maxPt_PX = track->momentum().X();
         
           maxPt_PY = track->momentum().Y();
         
           maxPt_PZ = track->momentum().Z();
         
           maxPt_PE = particle->momentum().E();
         
           
           }
       
         
         }
     
       
       }
   
     
     }
 
 
    
  }
 
   
   }
 
   
   }
 
   
   }
 
   
   }
 
 
    vP.push_back( sPx );
 
   vP.push_back( sPy );
 
  vP.push_back( sPz );
 
 
    return StatusCode::SUCCESS;
 

   
  }
 
  //=============================================================================
    // Loop over all the photons in the cone
    //=============================================================================
    StatusCode TupleToolEWTrackIsolation::NeutralCone( const LHCb::Particle *seed,
                                                            const LHCb::Particles *photons,
                                                            const double rcut,
                                                            int &mult,
                                                            std::vector < double > &vP,
                                                            double &sPt,
                                                            double &maxPt_PX, double &maxPt_PY, double &maxPt_PZ ) 
 {
   

    // -- Initialize values
    mult = 0;
 
   double sPx = 0.;
 
   double sPy = 0.;
 
   double sPz = 0.;
 
   sPt = 0.;
 
   maxPt_PX = 0.;
 
   maxPt_PY = 0.;
 
   maxPt_PZ = 0.;
 
 
    // -- Get the 4-momentum of the seed particle
    Gaudi::LorentzVector seedMomentum = seed->momentum();
 
 
    for ( LHCb::Particles::const_iterator ip = photons->begin(); ip != photons->end(); ++ip ) 
 {
   
     const LHCb::Particle *photon = (*ip);
   
 
    // -- Get the 3-momentum of the photon
    Gaudi::XYZVector photonMomentum = photon->momentum().Vect();
 
 
    // -- Calculate the difference in Eta and Phi between the seed particle and a photons
    double deltaPhi = fabs( seedMomentum.Phi() - photonMomentum.Phi() );
 
   if ( deltaPhi > M_PI )
     deltaPhi = 2 * M_PI - deltaPhi;
 
   double deltaEta = seedMomentum.Eta() - photonMomentum.Eta();
 
   double deltaR = sqrt( deltaPhi * deltaPhi + deltaEta * deltaEta );
 
 
    if ( ( rcut == 0. ) || ( deltaR < rcut ) ) 
 {
   
     // -- Calculate vector information
       sPx += photonMomentum.X();
   
    sPy += photonMomentum.Y();
   
     sPz += photonMomentum.Z();
   
 
    // -- Calculate scalar information
    sPt += sqrt( photonMomentum.Perp2() );
 
 
    mult++;
 
   
   }
  if ( ( rcut == 0. ) || ( deltaR < rcut ) ) 
 {
   
 
    // Extra Photon
    if ( sqrt( photonMomentum.Perp2() ) > sqrt(pow(maxPt_PX, 2.) + pow(maxPt_PY, 2.)) ) 
 {
   
     maxPt_PX = photonMomentum.X();
   
     maxPt_PY = photonMomentum.Y();
   
     maxPt_PZ = photonMomentum.Z();
   
     
     }
 
   
   }
 
 
    
  }
 
 
    vP.push_back( sPx );
   vP.push_back( sPy );
  vP.push_back( sPz );
 
 
    return StatusCode::SUCCESS;
 
 
   
  }
 
  //=============================================================================
    // Check if the track is already in the decay
    //=============================================================================
    bool TupleToolEWTrackIsolation::isTrackInDecay( const LHCb::Track *track ) 
 {
   
 
    bool isInDecay = false;
 
 
    for ( std::vector<const LHCb::Particle*>::iterator ip = m_decayParticles.begin(); ip != m_decayParticles.end(); ++ip ) 
 {
   
     const LHCb::ProtoParticle *proto = (*ip)->proto();
   
     if ( proto ) 
   {  const LHCb::Track *myTrack = proto->track();
 
   if ( myTrack ) 
 {
   
 
    if ( myTrack == track ) 
 {
   
     if ( msgLevel( MSG::DEBUG ) )
       debug() << "Track is in decay, skipping it" << endmsg;
   
     isInDecay = true;
   
     
     }
 
 
    
  }
 
   
   }
   
     
     }
 
 
    return isInDecay;
 
 
   
  }
 
