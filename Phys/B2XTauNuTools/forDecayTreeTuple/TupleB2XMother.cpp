// Include files 

// from Gaudi
#include "GaudiKernel/ToolFactory.h" 
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

// from Generators                                                                                                                                                                  
#include "GenEvent/HepMCUtils.h"
// from Phys
#include "Kernel/IParticle2MCAssociator.h"
// from LHCb                                                                                                                                                                        
#include "Event/Particle.h"
#include "Event/MCParticle.h"
// local
#include "TupleB2XMother.h"
//-----------------------------------------------------------------------------
// Implementation file for class : TupleB2XMother
//
// 2016-01-06 : Benedetto Gianluca Siddi
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( TupleB2XMother )

using namespace LHCb;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleB2XMother::TupleB2XMother( const std::string& type,
                                const std::string& name,
                                const IInterface* parent )
: TupleToolBase ( type, name , parent )
, m_p2mcAssocType("DaVinciSmartAssociator")
  
{
  //declareInterface<TupleB2XMother>(this);
  declareInterface<IParticleTupleTool>(this);
  
  declareProperty( "InputParticles", 
                   m_InputParticles = {"Phys/StdAllNoPIDsPions/Particles"}, 
                   "List of input Particles");
  // declareProperty( "Target",         
  //                  m_TargetParticle = "tau+", 
  //                  "Particle to add Particles to");
  declareProperty( "isMC", m_isMC = false);
  

  declareProperty( "IP2MCPAssociatorType", m_p2mcAssocType);
  declareProperty( "ChargedThetaMin"   , m_chargedThetaMin   = 10 * Gaudi::Units::mrad ) ;
  declareProperty( "ChargedThetaMax"   , m_chargedThetaMax   = 400 * Gaudi::Units::mrad ) ;
  declareProperty( "NeutralThetaMin"   , m_neutralThetaMin   = 5 * Gaudi::Units::mrad ) ;
  declareProperty( "NeutralThetaMax"   , m_neutralThetaMax   = 400 * Gaudi::Units::mrad ) ;
  

  
}
//=============================================================================
// Destructor
//=============================================================================
TupleB2XMother::~TupleB2XMother() {} 

//=============================================================================
StatusCode TupleB2XMother::initialize() 
{
  //if( ! TupleToolBase::initialize() ) return StatusCode::FAILURE;
  
  
  const StatusCode sc = TupleToolBase::initialize();
  if( sc.isFailure() )
    return sc;

  m_p2mcAssoc = tool<IParticle2MCAssociator>( m_p2mcAssocType, this );  

  //m_assoc = tool<IParticle2MCWeightedAssociator>("DaVinciSmartAssociator");
  //get some tools
 
  // m_particleDescendants = tool<IParticleDescendants> ( "ParticleDescendants");
  
  // if( !m_particleDescendants ) 
  //   return Error("Unable to retrieve the ParticleDescendants tool", StatusCode::FAILURE);
  
  // iteration = 0;
  
  return StatusCode::SUCCESS;
  

}


//===========================================================================================
StatusCode TupleB2XMother::fill( const Particle* mother
                                 , const Particle* Mother
                                 , const std::string& head
                                 , Tuples::Tuple& tuple )
{
  debug()<<"Filling the stuff"<<endmsg;
  
  Assert( Mother && mother, "This should not happen, you are inside TupleB2XMother.cpp :(" );
  
  
  // set test true
  bool test=true;
  bool fromB = false;                                                                                                                                                         
  bool fromD = false;  

  MCParticleVector stables;

  int nK=0;
  int npi=0;
  int np=0;
  int ngam=0;
  int nneu=0;
  int nele=0;
  int nmu=0;
  int npi0 = 0;
  int neta = 0;
  int netaprime = 0;
  int nrho = 0;
  int nrhop = 0;
  int nomega = 0;
  int nphi = 0;
  int nKst = 0;
  int nKstp = 0;
  int nKz = 0;
  int nKl = 0;
  int nKs = 0;
  
  
  int nK_acc=0;
  int npi_acc=0;
  int np_acc=0;
  int ngam_acc=0;
  int nneu_acc=0;
  int nele_acc=0;
  int nmu_acc=0;
  int npi0_acc = 0;
  int neta_acc = 0;
  int netaprime_acc = 0;
  int nrho_acc = 0;
  int nrhop_acc = 0;
  int nomega_acc = 0;
  int nphi_acc = 0;
  int nKst_acc = 0;
  int nKstp_acc = 0;
  int nKz_acc = 0;
  int nKl_acc = 0;
  int nKs_acc = 0;
  
  Double_t z_vert_moth  = -9999.;  
  Int_t motherKey = -1;
  Bool_t isAssociated = true;
  Int_t motherID = 0;
  
  const MCParticle *mclink = (const MCParticle*)m_p2mcAssoc->relatedMCP(Mother);
  if(!mclink) 
  {
    isAssociated = false;
    debug()<<"No MClink"<<endmsg;
  }
  
  else
  {
    Int_t IDmclink = mclink->particleID().pid();
    Int_t IDpart = Mother->particleID().pid();
    debug()<<"HEAD: "<<fullName(head)<<"\t PartID: "<<IDpart<<"\t MClink ID: "<<IDmclink<<endmsg;
    const MCParticle *PMother = (const MCParticle *)ancestor(mclink);
    fromB = PMother->particleID().hasBottom();
    if (fromB) stables =  create_finalstatedaughterarray_for_mcmother(PMother);
    
    //if (fromB) test = products(PMother,head,tuple);
    if (PMother && fromB)
    { 
      z_vert_moth = PMother->endVertices()[0]->position().z();
      motherKey = PMother->key(); 
      motherID = PMother->particleID().pid();
      
    }
    
    
  }
  const std::string prefix = fullName(head);
  //stables =  create_finalstatedaughterarray_for_mcmother(mc_mom);
  
  if ( stables.empty() )
    //Exception( "Signal has no stable daughters !" ) ;
    if ( msgLevel(MSG::DEBUG) ) debug()<<"Signal has no stable daughters !"<<endmsg;
  //counting pions kaons etc ...
  // we count total number and number in accpetance

  for ( MCParticleVector::const_iterator it = stables.begin() ; it != stables.end() ; ++it ) {
    if(abs((*it)->originVertex()->position().Z())>500) continue;
    
    Double_t angle = (*it) -> momentum().theta() ;
    
    if ( msgLevel(MSG::DEBUG) ) debug() << "Check particle " << (*it) -> particleID().pid() << " with angle "
            << (*it) -> momentum().theta() / Gaudi::Units::mrad
            << " mrad." << endmsg ;
    
    // count number of various categories for phtons ask more than 20 MeV
    if( abs((*it) -> particleID().pid()) == 211) npi++;
    if( abs((*it) -> particleID().pid()) == 321) nK++;
    if( abs((*it) -> particleID().pid()) == 2212) np++;
    if( abs((*it) -> particleID().pid()) == 22 &&  (*it) -> momentum().P()>20.) ngam++;
    if( abs((*it) -> particleID().pid()) == 2112) nneu++;
    if( abs((*it) -> particleID().pid()) == 11) nele++;
    if( abs((*it) -> particleID().pid()) == 15) nmu++;

    if( abs((*it) -> particleID().pid()) == 111) npi0++;
    if( abs((*it) -> particleID().pid()) == 221) neta++;
    if( abs((*it) -> particleID().pid()) == 331) netaprime++;
    if( abs((*it) -> particleID().pid()) == 113) nrho++;
    if( abs((*it) -> particleID().pid()) == 213) nrhop++;
    if( abs((*it) -> particleID().pid()) == 223) nomega++;
    if( abs((*it) -> particleID().pid()) == 333) nphi++;
    if( abs((*it) -> particleID().pid()) == 313) nKst++;
    if( abs((*it) -> particleID().pid()) == 323) nKstp++;
    if( abs((*it) -> particleID().pid()) == 311) nKz++;
    if( abs((*it) -> particleID().pid()) == 130) nKl++;
    if( abs((*it) -> particleID().pid()) == 310) nKs++;
    
    angle = (*it) -> momentum().theta() ;
    if ( (fabs( sin( angle ) ) <fabs( sin( m_chargedThetaMax))) && (fabs( sin( angle ) ) > fabs( sin( m_chargedThetaMin ) ) ))
    {
      // count number of various categories for phtons ask more than 20 MeV
      if( abs((*it) -> particleID().pid()) == 211) npi_acc++;
      if( abs((*it) -> particleID().pid()) == 321) nK_acc++;
      if( abs((*it) -> particleID().pid()) == 2212) np_acc++;
      if( abs((*it) -> particleID().pid()) == 11) nele_acc++;
      if( abs((*it) -> particleID().pid()) == 15) nmu_acc++;
      if( abs((*it) -> particleID().pid()) == 111) npi0_acc++;
      if( abs((*it) -> particleID().pid()) == 221) neta_acc++;
      if( abs((*it) -> particleID().pid()) == 331) netaprime_acc++;
      if( abs((*it) -> particleID().pid()) == 113) nrho_acc++;
      if( abs((*it) -> particleID().pid()) == 213) nrhop_acc++;
      if( abs((*it) -> particleID().pid()) == 223) nomega_acc++;
      if( abs((*it) -> particleID().pid()) == 333) nphi_acc++;
      if( abs((*it) -> particleID().pid()) == 313) nKst_acc++;
      if( abs((*it) -> particleID().pid()) == 323) nKstp_acc++;
      if( abs((*it) -> particleID().pid()) == 311) nKz_acc++;
      if( abs((*it) -> particleID().pid()) == 130) nKl_acc++;
      if( abs((*it) -> particleID().pid()) == 310) nKs_acc++;

    }
    if ( (fabs( sin( angle ) ) < fabs( sin( m_neutralThetaMax))) && (fabs( sin( angle ) ) > fabs( sin( m_neutralThetaMin ) )) )
    {
      if( abs((*it) -> particleID().pid()) == 2112) nneu_acc++;
      if( abs((*it) -> particleID().pid()) == 22 &&  (*it) -> momentum().P()>20.) ngam_acc++;
    }            
  }
  test &= tuple -> column( prefix + "_Added_IsAssociated", isAssociated);
  
  test &= tuple -> column( prefix + "_Added_z_endvertex_mother", z_vert_moth);
  test &= tuple -> column( prefix + "_Added_mother_key", motherKey);
  test &= tuple -> column( prefix + "_Added_mother_id", motherID);

  test &= tuple -> column( prefix + "_Added_npi", npi);
  test &= tuple -> column( prefix + "_Added_nK", nK);
  test &= tuple -> column( prefix + "_Added_np", np);
  test &= tuple -> column( prefix + "_Added_ngamma", ngam);
  test &= tuple -> column( prefix + "_Added_nneu", nneu);
  test &= tuple -> column( prefix + "_Added_nele", nele);
  test &= tuple -> column( prefix + "_Added_nmu", nmu);
  test &= tuple -> column( prefix + "_Added_npi0", npi0);
  test &= tuple -> column( prefix + "_Added_neta", neta);
  test &= tuple -> column( prefix + "_Added_netaprime", netaprime);
  test &= tuple -> column( prefix + "_Added_nrho", nrho);
  test &= tuple -> column( prefix + "_Added_nrhop", nrhop);
  test &= tuple -> column( prefix + "_Added_nomega", nomega);
  test &= tuple -> column( prefix + "_Added_nphi", nphi);
  test &= tuple -> column( prefix + "_Added_nKst", nKst);
  test &= tuple -> column( prefix + "_Added_nKstp", nKstp);
  test &= tuple -> column( prefix + "_Added_nKz", nKz);
  test &= tuple -> column( prefix + "_Added_nKl", nKl);
  test &= tuple -> column( prefix + "_Added_nKs", nKs);


  test &= tuple -> column( prefix + "_Added_npi_acc", npi_acc);
  test &= tuple -> column( prefix + "_Added_nK_acc", nK_acc);
  test &= tuple -> column( prefix + "_Added_np_acc", np_acc);
  test &= tuple -> column( prefix + "_Added_ngamma_acc", ngam_acc);
  test &= tuple -> column( prefix + "_Added_nneu_acc", nneu_acc);
  test &= tuple -> column( prefix + "_Added_nele_acc", nele_acc);
  test &= tuple -> column( prefix + "_Added_nmu_acc", nmu_acc); 
  test &= tuple -> column( prefix + "_Added_npi0_acc", npi0_acc);
  test &= tuple -> column( prefix + "_Added_neta_acc", neta_acc);
  test &= tuple -> column( prefix + "_Added_netaprime_acc", netaprime_acc);
  test &= tuple -> column( prefix + "_Added_nrho_acc", nrho_acc);
  test &= tuple -> column( prefix + "_Added_nrhop_acc", nrhop_acc);
  test &= tuple -> column( prefix + "_Added_nomega_acc", nomega_acc);
  test &= tuple -> column( prefix + "_Added_nphi_acc", nphi_acc);
  test &= tuple -> column( prefix + "_Added_nKst_acc", nKst_acc);
  test &= tuple -> column( prefix + "_Added_nKstp_acc", nKstp_acc);
  test &= tuple -> column( prefix + "_Added_nKz_acc", nKz_acc);
  test &= tuple -> column( prefix + "_Added_nKl_acc", nKl_acc);
  test &= tuple -> column( prefix + "_Added_nKs_acc", nKs_acc);

  return StatusCode(test);  
}


//=============================================================================                                                                                                     
// Get ancestor                                                                                                                                                                     
//=============================================================================                                                                                                     
const LHCb::MCParticle* TupleB2XMother::ancestor(const LHCb::MCParticle*imc) const 
{

  if ( msgLevel(MSG::DEBUG) )
  { 
    debug()<<"Part PID "<< imc->particleID() << endmsg;
    debug()<<"Part Key "<<imc->key()<<endmsg;
  }
  
  if (imc->mother()) 
  {
    
    const LHCb::MCParticle*mc_mom = imc->mother();
    
    if(!mc_mom->particleID().hasBottom())
    {
      
      if ( msgLevel(MSG::DEBUG) )
      {
        
      //debug()<<"Mom particle "<< mc_mom << endmsg;
        debug()<<"Mom PID "<< mc_mom->particleID() << endmsg;
        debug()<<"Mom Key "<<mc_mom->key()<<endmsg;
        debug()<<"Mom mother "<< mc_mom->mother() << endmsg;
        //debug()<<"mother PID "<< mc_mom->mother()->particleID() << endmsg; 
      }
      
      return ancestor(mc_mom);
    }
    if(msgLevel(MSG::DEBUG))
    {
      
      debug()<<"Mom PID "<< mc_mom->particleID() << endmsg;
      debug()<<"Mom Key "<<mc_mom->key()<<endmsg;
      debug()<<"Mom mother "<< mc_mom->mother() << endmsg;
    }
    
    return mc_mom;
    
  }
  else 
  {
    
    return imc;
    
  }
  
}





TupleB2XMother::MCParticleVector
TupleB2XMother::create_finalstatedaughterarray_for_mcmother(const LHCb::MCParticle* topmother)
//Uses Patrick's tool (ParticleDescendants) to create an array of the final state products of the MCParticle                                                                        
//determined to be the joint mother (if indeeed there is one) of the MCParticles associated to the final                                                                            
//state particle daughters of the candidate Particle. For obvious reasons, this function is only invoked for                                                                        
//background catgegories 0->50.                                                                                                                                              
{
  if (msgLevel(MSG::VERBOSE)) verbose() << "Creating an array of final state daughters for the mc mother"
                                        << endmsg;
  bool isitstable = false;
  MCParticleVector finalstateproducts;
  MCParticleVector tempfinalstateproducts;
  //Check for an MCMother with a null endvertex -- really shouldn't happen                                                                                                          
  //here though, but the Cat is often betrayed by other pieces of code...                                                                                                           
  const SmartRefVector<LHCb::MCVertex>& motherEndVertices = topmother->endVertices();
  if (motherEndVertices.empty() )
    Exception("The Cat found a common MC mother but it has no daughters, please report this as a bug.");
  //Assuming all went well...                                                                                                                                                       
  SmartRefVector<LHCb::MCVertex>::const_iterator iV = motherEndVertices.begin();
  if (msgLevel(MSG::VERBOSE)) verbose() << "Mother "
                                        << topmother->particleID().pid()
                                        << " has "
                                        <<  (*iV)->products().size()
                                        << " daughters"
                                        << endmsg;
  for ( SmartRefVector<LHCb::MCParticle>::const_iterator iP = (*iV)->products().begin();
        iP != (*iV)->products().end(); ++iP )
  {
    //Need to protect against MCParticles with null endVertices,                                                                                                                    
    //which are assumed to be stable                                                                                                                                                
    const SmartRefVector<LHCb::MCVertex>& VV = (*iP)->endVertices();  
    if ( !VV.empty() ) 
    {
      isitstable = (*(VV.begin()))->products().empty();
    }
    else
    {
      isitstable = true;
    }
    if (msgLevel(MSG::VERBOSE))
    { 
      verbose() << (*iP)->particleID().abspid()
                << " stable: "
                << isStable( (*iP)->particleID().abspid() );
      if (isitstable) verbose() << endmsg;
      else verbose() << " products: "
                     << (*(VV.begin()))->products().size()
                     << endmsg ; 
    }
    if ( isitstable ||
         isStable( (*iP)->particleID().abspid() )
         )
    {
      //If it is a stable, add it to the final state array                                                                                                                          
      finalstateproducts.push_back(*iP);
    }
    else
    {
      if (isMeson((*iP)->particleID().abspid())) finalstateproducts.push_back(*iP);
      //Otherwise recurse                                                                                                                                                           
      tempfinalstateproducts = create_finalstatedaughterarray_for_mcmother(*iP);
      finalstateproducts.insert(finalstateproducts.end(),
                                tempfinalstateproducts.begin(),
                                tempfinalstateproducts.end()) ; 
    }
  }
  if (msgLevel(MSG::VERBOSE)) verbose() << "returning "
                                        << finalstateproducts.size()
                                        << " final state products"
                                        << endmsg ;
  return finalstateproducts; 
}

bool TupleB2XMother::isStable(int pid)
//A cheap and cheerful list of particles considered stable for the purposes of                                                                                                      
//this tool. Not the cleverest way to do this, but works OK.                                                                                                                        
{
  
  if ( pid < 0 ) pid *= -1;
  if ( pid == 11 || //electron                                                                                                                                                      
       pid == 22 || //photon  - photon conversion is treated elsewhere                                                                                                              
       pid == 13 || //muon                                                                                                                                                          
       pid == 12 || //neutrinos                                                                                                                                                     
       pid == 14 ||
       pid == 16 ||
       pid == 211 || //pion (charged)                                                                                                                                               
       pid == 321 || //kaon (charged)                                                                                                                                               
       pid == 2212 //|| //proton                                                                                                                                                    
       ) return true;
  else return false;
  

}

bool TupleB2XMother::isMeson(int pid)
//A cheap and cheerful list of particles considered stable for the purposes of                                                                                                      
//this tool. Not the cleverest way to do this, but works OK.                                                                                                                        
{
  

  if ( pid < 0 ) pid *= -1;
  
  if ( pid == 111 ||
       pid == 221 ||
       pid == 331 ||
       pid == 113 ||
       pid == 213 ||
       pid == 223 ||
       pid == 333 ||
       pid == 313 ||
       pid == 323 ||
       pid == 311 ||
       pid == 130 ||
       pid == 310
       ) return true;
  
  else return false;
  


}


