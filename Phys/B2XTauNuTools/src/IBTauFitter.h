// $Id: $
#ifndef IBTAUFITTER_H 
#define IBTAUFITTER_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/ToolFactory.h" 

#include "GaudiKernel/Vector4DTypes.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/GenericMatrixTypes.h"

#include "Event/Particle.h"

static const InterfaceID IID_IBTauFitter ( "IBTauFitter", 1, 0 );

/** @class IBTauFitter IBTauFitter.h
 *  
 *
 *  @author Anne Keune
 *  @date   2010-04-30
 */

typedef ROOT::Math::SMatrix<double, 17, 17,
                            ROOT::Math::MatRepSym<double,17> > SymMatrix17x17;
typedef ROOT::Math::SMatrix<double, 4, 4,
                            ROOT::Math::MatRepSym<double,4> > SymMatrix4x4;
typedef ROOT::Math::SMatrix<double,17,17> Matrix17x17;
typedef ROOT::Math::SVector<double,17> Vector17;
typedef ROOT::Math::SVector<double,4> Vector4;

//using namespace Gaudi;
class IBTauFitter : virtual public IAlgTool {
public: 

  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_IBTauFitter; }

  virtual void setMeasuredData(Vector17 m) = 0 ;
  virtual void setWeights(SymMatrix17x17 W) = 0 ;
  //virtual void setWeights(Matrix17x17 W) = 0 ;
  

  virtual void setMeasuredData(const LHCb::VertexBase* pVertex, const LHCb::VertexBase* bVertex, 
                               const LHCb::Particle* mother, const LHCb::Particle* rest) = 0 ;
  virtual StatusCode setWeights(const LHCb::VertexBase* pVertex, const LHCb::VertexBase* bVertex , 
                                const LHCb::Particle* mother, const LHCb::Particle* rest) = 0 ;
  virtual void setInitialGuess(int i, int j) = 0 ;
  virtual void setInitialGuess(Vector17 x0) = 0 ;

  virtual StatusCode performFit() = 0 ;
  virtual void performMinuit() = 0 ;

  virtual Vector17 getMeasuredData() = 0;
  virtual SymMatrix17x17 getWeights() = 0;
  //virtual Matrix17x17 getWeights() = 0;
  virtual Vector17 getResult() = 0;

  virtual Vector17 calculateModel(Vector17 x) = 0 ;
  virtual Matrix17x17 calculateDerivative(Vector17 x) = 0 ;

  virtual Gaudi::LorentzVector tauMomentum() = 0 ;
  virtual Gaudi::LorentzVector nuMomentum() = 0 ;
  virtual Gaudi::LorentzVector pipipiMomentum() = 0 ;
  virtual Gaudi::LorentzVector BMomentum() = 0 ;
  virtual Gaudi::LorentzVector nu2Momentum() = 0 ;
  virtual Gaudi::LorentzVector DstMomentum() = 0 ;

  virtual Gaudi::LorentzVector initial_tauMomentum() = 0 ;
  virtual Gaudi::LorentzVector initial_BMomentum() = 0 ;

  virtual Gaudi::XYZPoint bVertex() = 0 ;
  virtual Gaudi::XYZPoint eVertex() = 0 ;
  virtual Gaudi::XYZPoint PVertex() = 0 ;

  //Added by Donal 9/4/13 to give measured error on the vertices
  virtual Gaudi::XYZPoint bVertexErr() = 0 ;
  virtual Gaudi::XYZPoint eVertexErr() = 0 ;
  virtual Gaudi::XYZPoint PVertexErr() = 0 ;

  //Added by Donal 9/4/13 to give mesured error on 4-momentum components (px,py,pz,m2 as defined)
  virtual Gaudi::LorentzVector pipipiMomentumErr() = 0 ;
  virtual Gaudi::LorentzVector DstMomentumErr() = 0 ;

  
  virtual double chi2() = 0 ;
  virtual unsigned int iterations() = 0 ;

  virtual double chi2_at_10() = 0 ;
  virtual double chi2_at_50() = 0 ;
  virtual double chi2_at_100() = 0 ;
  virtual double chi2_at_500() = 0 ;
  virtual double chi2_at_1000() = 0 ;


protected:

private:

};
#endif // IBTAUFITTER_H
