LOCOUT=/afs/cern.ch/user/c/cofitzpa/work/public/DstTauNu/bdt_results_s20r1
DIR1=Bd2DsttaunuWSTuple/DecayTree
DIR2=Bd2DsttaunuNonPhysTuple/DecayTree
DIR3=Bd2DsttaunuTuple/DecayTree

for path in "$@"
do
	fname=$(basename $path .root)
	passname=$fname"_comboBDT.root"

	root -b "AddComboKillerBDTToNtuple.C+(\"$path\",\"$LOCOUT/$passname\",\"$DIR1\")"
	root -b "AddComboKillerBDTToNtuple.C+(\"$path\",\"$LOCOUT/$passname\",\"$DIR2\")"
	root -b "AddComboKillerBDTToNtuple.C+(\"$path\",\"$LOCOUT/$passname\",\"$DIR3\")"
done
