#!/usr/bin/env python

"""
Bender module to run the following sequence over B2KShh stripped data samples:
- run an algorithm to store the reco info for all Stripped candidates
"""

from Bender.Main import *
from Gaudi.Configuration import *

def configure( datafiles, catalogues = [], params = {}, castor = False ) :

    #======= B->KShh Configuration and Setup =========#
    magtype        = params.get( 'magtype',   'MagDown'     )
    whichStripping = params.get( 'stripping', 'Stripping21' )
    extended_hypos = params.get( 'extended_hypos', False )
    #=================================================#

    knownMagTypes = [ 'MagDown', 'MagUp' ]

    if magtype not in knownMagTypes :
        e = Exception('Unsupported magnet setting: ' + magtype)
        raise e

    known2011StrippingVersions = [ 'Stripping21r1' ]
    known2012StrippingVersions = [ 'Stripping21' ]

    whichData = '' 
    if whichStripping in known2011StrippingVersions:
        whichData = '2011'
    elif whichStripping in known2012StrippingVersions:
        whichData = '2012'
    else :
        e = Exception('Unsupported Stripping version: ' + whichStripping)
        raise e
    
    h1type = 'K'
    h2type = 'K'

    from Configurables import DaVinci
    
    daVinci = DaVinci()
    daVinci.DataType        = whichData
    daVinci.Simulation      = False
    daVinci.Lumi            = True
    daVinci.InputType       = "MDST"
    daVinci.TupleFile       = 'Xib'+h1type+h2type+'-Collision'+whichData[2:]+'-'+magtype+'-'+whichStripping+'.root'
    daVinci.EvtMax          = -1
    daVinci.PrintFreq       = 10000

    from Configurables import CondDB
    CondDB().LatestGlobalTagByDataType = whichData

    setData( datafiles, catalogues )

    stream = {}
    stream['Stripping21']      = 'Bhadron'
    stream['Stripping21r1']    = 'Bhadron'
    
    teslocation = '/Event/'+stream[whichStripping]+'/'
    daVinci.RootInTES = teslocation

    if h1type == 'pi' and h2type == 'pi' :
        filterStrip = "HLT_PASS_RE('StrippingX2LcPiPiSSLc2PKPiBeauty2CharmLineDecision')"
        inputLocationDD = 'Phys/X2LcPiPiSSLc2PKPiBeauty2CharmLine/Particles'
    elif h1type == 'K' and h2type == 'pi' :    
        filterStrip = "HLT_PASS_RE('StrippingX2LcKPiSSLc2PKPiBeauty2CharmLineDecision')"
        inputLocationDD = 'Phys/X2LcKPiSSLc2PKPiBeauty2CharmLine/Particles'
    else :
        filterStrip = "HLT_PASS_RE('StrippingX2LcKKSSLc2PKPiBeauty2CharmLineDecision')"
        inputLocationDD = 'Phys/X2LcKKSSLc2PKPiBeauty2CharmLine/Particles'

    from PhysConf.Filters import LoKi_Filters

    filterVoid  = "EXISTS('/Event/Strip/Phys/DecReports')"
    filterAlg = LoKi_Filters( VOID_Code = filterVoid, STRIP_Code = filterStrip  )
    filters = filterAlg.filters('Filters')
    filters.reverse()
    daVinci.EventPreFilters = filters

    reco_daughters = [ h1type, h2type, 'Lc' ]

    from B2KShh.RecoAlgo_Xibhh import XibReco

    gaudi = appMgr()

    algXib2LcKpi = XibReco(
        'Xib2LcKpi',
        reco_daughters,
        extended_hypos, False, False, False, 
        RootInTES = teslocation,
        Inputs = [ inputLocationDD ]
        )

    userSeq = gaudi.algorithm('GaudiSequencer/DaVinciUserSequence' , True )

    userSeq.Members += [ algXib2LcKpi.name() ]

    return SUCCESS

#############

if '__main__' == __name__ :

    datafiles = [
            #'PFN:/data/lhcb/phsdba/B2KShh/DST/Data/2012-Stripping20-MagDown/00020198_00000224_1.bhadron.mdst'
            '/data/lhcb/phreba/Xib2Lchh/data/00041836_00000190_1.bhadron.mdst'
    ]

    pars = {}
    pars[ 'stripping' ] = 'Stripping21'
    pars[ 'magtype' ]   = 'MagDown'

    configure( datafiles, params = pars, castor=False )

    run(-1)

#############

