#!/usr/bin/env python

"""
Bender module to run the following sequence over B2KShh stripped data samples:
- run an algorithm to store the reco info for all Stripped candidates
"""

from Bender.Main import *
from Gaudi.Configuration import *

def configure( datafiles, catalogues = [], params = {}, castor = False ) :

    #======= B->KShh Configuration and Setup =========#
    magtype        = params.get( 'magtype',   'MagUp'     )
    whichStripping = params.get( 'stripping', 'Stripping21' )
    extended_hypos = params.get( 'extended_hypos', False )
    dddbtag = params.get( 'dddbtag', '' )
    conddbtag = params.get( 'conddbtag', '' )
    mode = params.get( 'mode', '' )
    btype = params.get( 'btype', '' )
    lctype = params.get( 'lctype', '' )
    isXGen = params.get( 'isXGen', False )
    #=================================================#

    knownMagTypes = [ 'MagDown', 'MagUp' ]

    if magtype not in knownMagTypes :
        e = Exception('Unsupported magnet setting: ' + magtype)
        raise e

    known2012StrippingVersions = [ 'Stripping21' ]

    whichData = '' 
    if whichStripping in known2012StrippingVersions:
        whichData = '2012'
    else :
        e = Exception('Unsupported Stripping version: ' + whichStripping)
        raise e
    
    h1type = 'K'
    h2type = 'K'
    from Configurables import DaVinci
    
    daVinci = DaVinci()
    daVinci.DataType        = whichData
    daVinci.Simulation      = True
    daVinci.Lumi            = False
    daVinci.InputType       = "DST"
    daVinci.TupleFile       = 'Xib'+h1type+h2type+'-MC'+whichData[2:]+'-'+magtype+'-'+whichStripping+'.root'
    daVinci.EvtMax          = -1
    daVinci.PrintFreq       = 10000

    daVinci.DDDBtag = dddbtag
    daVinci.CondDBtag = conddbtag

    from Configurables import CondDB
    CondDB().LatestGlobalTagByDataType = whichData

    setData( datafiles, catalogues )

    stream = {}
    stream['Stripping21']      = 'Bhadron'
    stream['Stripping21r1']    = 'Bhadron'
    
    #teslocation = '/Event/'+stream[whichStripping]+'/'
    #daVinci.RootInTES = teslocation

    if h1type == 'pi' and h2type == 'pi' :
        inputLocationDD = '/Event/AllStreams21/Phys/X2LcPiPiSSLc2PKPiBeauty2CharmLine/Particles'
    elif h1type == 'K' and h2type == 'pi' :    
        inputLocationDD = '/Event/AllStreams21/Phys/X2LcKPiSSLc2PKPiBeauty2CharmLine/Particles'
    else:     
        inputLocationDD = '/Event/AllStreams21/Phys/X2LcKKSSLc2PKPiBeauty2CharmLine/Particles'

    reco_daughters = [ h1type, h2type, 'Lc' ]
    mc_daughters = [ h1type, h2type, 'Lc' ]

    from Xib2Lchh.RecoAlgo import XibReco
    from Xib2Lchh.MCTruthAlgo import XibMCTruth

    #algGenMCTruth = XibMCTruth( mode, btype, h1type, h2type, lctype, isXGen )

    gaudi = appMgr()
    mc_decay_descriptors = []

    algXib2lchhpi = XibReco(
        'Xib2lchhpi',
        reco_daughters,
        #Signal MC
        #extended_hypos, False, True, True, 
        #Background MC
        extended_hypos, False, True, False, 
        mc_daughters,
        mc_decay_descriptors,
#        RootInTES = teslocation,
        PP2MCs = [ 'Relations/Rec/ProtoP/Charged'], 
        Inputs = [ inputLocationDD ]
        )

    userSeq = gaudi.algorithm('GaudiSequencer/DaVinciUserSequence' , True )

    #userSeq.Members += [ algGenMCTruth.name() ]
    userSeq.Members += [ algXib2lchhpi.name() ]

    return SUCCESS

#############

if '__main__' == __name__ :

    datafiles = [
            #'/data/lhcb/phreba/Xib2Lchh/mcdata/lchh/lckk_mu_0.dst',
            #'/data/lhcb/phreba/Xib2Lchh/mcdata/lchh/lckk_mu_1.dst',
            #'/data/lhcb/phreba/Xib2Lchh/mcdata/lchh/lckk_mu_2.dst',
            #'/data/lhcb/phreba/Xib2Lchh/mcdata/lchh/lckk_mu_3.dst',
            #'/data/lhcb/phreba/Xib2Lchh/mcdata/lchh/lckk_mu_4.dst',
            #'/data/lhcb/phreba/Xib2Lchh/mcdata/lchh/lckk_mu_5.dst',
            #'/data/lhcb/phreba/Xib2Lchh/mcdata/lchh/lckk_mu_6.dst',
            #'/data/lhcb/phreba/Xib2Lchh/mcdata/lchh/lckk_mu_7.dst',
            #'/data/lhcb/phreba/Xib2Lchh/mcdata/lchh/lckk_mu_8.dst',
            #'/data/lhcb/phreba/Xib2Lchh/mcdata/lchh/lckk_mu_9.dst',
            #'/data/lhcb/phreba/Xib2Lchh/mcdata/lchh/lckk_mu_10.dst',
            #'/data/lhcb/phreba/Xib2Lchh/mcdata/lchh/lckk_mu_11.dst',
            #'/data/lhcb/phreba/Xib2Lchh/mcdata/lchh/lckk_mu_12.dst',
            #'/data/lhcb/phreba/Xib2Lchh/mcdata/lchh/lckk_mu_13.dst',
            #'/data/lhcb/phreba/Xib2Lchh/mcdata/lchh/lckk_mu_14.dst',
            '/data/lhcb/phreba/Xib2Lchh/mcdata/lchh/lckk_mu_15.dst'
    ]

    pars = {}
    pars[ 'dddbtag' ]   = 'Sim08-20130503-1'
    pars[ 'conddbtag' ] = 'Sim08-20130503-1-vc-mu100'
    pars[ 'mode' ] = 'Xib2Lchh'
    pars[ 'btype' ] = 'Xib'
    pars[ 'lctype' ] = 'Lc'

    configure( datafiles, params = pars, castor=False )

    run(-1)

#############

