#!/usr/bin/env python

from Bender.Main import * 
from Bender.MainMC import *
import math
from ROOT import Double, TLorentzVector, TVector3, TRotation, TLorentzRotation, TMath
from Xib2Lchh.ThreeBodyKinematics import ThreeBodyKinematics

# Begin XibReco Class Definition

class XibReco(AlgoMC) :

    """
    Algorithm to perform ntupling (and potentially offline selection) for
    Xib2Lchh analyses.
    Reads stripped candidates from the DST and stores useful info in an ntuple.
    Each instance of this algorithm creates a single ntuple, which contains
    information on all (considered) final states, but only for KS's composed of
    either DD, LD or LL tracks.  So one would need 3 instances of this
    algorithm in a job to create ntuples for all possible KS categories.
    """

    def __init__( self, name, reco_daughters, use_extended_hypothesis_set = False, wrongsign = False, simulation = False, signalmc = False, mc_daughters = [], mc_decay_descriptors = [], **kwargs ) :
        super(XibReco,self).__init__( name, **kwargs )

        self.simplenames = {}
        self.simplenames[5132]  = 'Xib'
        self.simplenames[-5132] = 'Xib'
        self.simplenames[5332]  = 'Omb'
        self.simplenames[-5332] = 'Omb'
        self.simplenames[321]   = 'K'
        self.simplenames[-321]  = 'K'
        self.simplenames[211]   = 'pi'
        self.simplenames[-211]  = 'pi'
        self.simplenames[2212]  = 'p'
        self.simplenames[-2212] = 'p'
        self.simplenames[4122]  = 'Lc'
        self.simplenames[-4122] = 'Lc'
        self.simplenames[521]   = 'Bp'
        self.simplenames[-521]  = 'Bp'
        self.simplenames[411]   = 'Dp'
        self.simplenames[-411]  = 'Dp'

        self.simulation = simulation
        self.signalmc = False
        if self.simulation :
            self.signalmc = signalmc

        self.wrongsign = wrongsign
        
        self.input_mc_dds = mc_decay_descriptors

        self.set_daug_types( reco_daughters, use_extended_hypothesis_set )

        self.set_mc_daug_types( mc_daughters )


    def set_mc_daug_types( self, mc_daughters ) :
        """
        Define the MC-truth decay mode
        """

        self.mcgdaugnames = {}

        self.mcdaug1ID = None
        self.mcdaug2ID = None
        self.mcdaug3ID = None
        self.mcdaug3ConjID = None

        if not self.signalmc :
            return

        if len(mc_daughters) != 3 :
            self.Warning( 'MC daughters not supplied, will assume they are the same as the reco daughters!', SUCCESS )
            self.mcgdaugnames = dict( self.gdaugnames )
            self.mcdaug1ID = LHCb.ParticleID(self.daug1ID)
            self.mcdaug2ID = LHCb.ParticleID(self.daug2ID)
            self.mcdaug3ID = LHCb.ParticleID(self.daug3ID)
            self.mcdaug3ConjID = LHCb.ParticleID(self.daug3ConjID)
            return

        # Lc stuff
        if mc_daughters[2] == 'Lc' or mc_daughters[2] == 'Lambda_c+' :
            self.mcdaug3ID     = LHCb.ParticleID(4122)
            self.mcdaug3ConjID = LHCb.ParticleID(-4122)
            self.mcgdaugnames[ 211] = 'Lcpi'
            self.mcgdaugnames[-211] = 'Lcpi'
            self.mcgdaugnames[ 2212] = 'Lcp'
            self.mcgdaugnames[-2212] = 'Lcp'
            self.mcgdaugnames[ 321] = 'LcK'
            self.mcgdaugnames[-321] = 'LcK'
            self.lcname = 'Lc'

        # Dp stuff
        if mc_daughters[2] == 'Dp' or mc_daughters[2] == 'D+' :
            self.mcdaug3ID     = LHCb.ParticleID(411)
            self.mcdaug3ConjID = LHCb.ParticleID(-411)
            self.mcgdaugnames[ 211] = 'Dpi'
            self.mcgdaugnames[-211] = 'Dpi'
            self.mcgdaugnames[ 321] = 'DK'
            self.mcgdaugnames[-321] = 'DK'
            self.lcname = 'Dp'

        # determine the charged daughter types
        if mc_daughters[0] == 'pi' and mc_daughters[1] == 'pi':
            self.mcdaug1ID = LHCb.ParticleID(-211)
            self.mcdaug2ID = LHCb.ParticleID(-211)

        elif mc_daughters[0] == 'K' and mc_daughters[1] == 'K':
            self.mcdaug1ID = LHCb.ParticleID(-321)
            self.mcdaug2ID = LHCb.ParticleID(-321)

        elif mc_daughters[0] == 'K' and mc_daughters[1] == 'pi':
            self.mcdaug1ID = LHCb.ParticleID(-321)
            self.mcdaug2ID = LHCb.ParticleID(-211)

        elif mc_daughters[0] == 'pi' and mc_daughters[1] == 'K':
            self.mcdaug1ID = LHCb.ParticleID(-211)
            self.mcdaug2ID = LHCb.ParticleID(-321)

        else :
            self.Warning( 'MC hh types ('+mc_daughters[0]+' and '+mc_daughters[1]+') not recognised, setting to pions.' , SUCCESS )
            self.mcdaug1ID = LHCb.ParticleID(-211)
            self.mcdaug2ID = LHCb.ParticleID(-211)


    def set_daug_types( self, daughters, use_extended_hypothesis_set ) :
        """
        Define the decay mode to be selected
        """

        self.gdaugnames = {}

        self.daug1ID = None
        self.daug2ID = None
        self.daug3ID = None
        self.daug3ConjID = None

        self.hypotheses = []

        if len(daughters) != 3 :
            self.Error( 'Expected 3 reco daughters but have %d.  Cannot continue.' % len(daughters), FAILURE )

        if self.wrongsign :
            if daughters[0] == 'pi' and daughters[1] == 'pi' :
                self.hypotheses = [ (211,-211) ]
                self.Info( 'Set wrong-sign hypo (211,-211)' )

            elif daughters[0] == 'K' and daughters[1] == 'K' :    
                self.hypotheses = [ (321,-321) ]
                self.Info( 'Set wrong-sign hypo (321,-321)' )

            elif daughters[0] == 'K' and daughters[1] == 'pi' :    
                self.hypotheses = [ (321,-211), (211,-321) ]
                self.Info( 'Set wrong-sign hypo (321,-211), (211,-321)' )

            elif daughters[0] == 'pi' and daughters[1] == 'K' :    
                self.hypotheses = [ (211,-321), (321,-211) ]
                self.Info( 'Set wrong-sign hypo (211,-321), (321,-211)' )

            else :    
                self.hypotheses = [ (211,-211) ]
                self.Info( 'Default - Set wrong-sign hypo (211,-211)' )
        else :        
            if daughters[0] == 'pi' and daughters[1] == 'pi' :
                self.hypotheses = [ (-211,-211) ]
                self.Info( 'Set same-sign hypo (-211,-211)' )

            elif daughters[0] == 'K' and daughters[1] == 'K' :    
                self.hypotheses = [ (-321,-321) ]
                self.Info( 'Set same-sign hypo (-321,-321)' )

            elif daughters[0] == 'K' and daughters[1] == 'pi' :    
                self.hypotheses = [ (-321,-211) ]
                self.Info( 'Set same-sign hypo (-321,-211)' )

            elif daughters[0] == 'pi' and daughters[1] == 'K' :    
                self.hypotheses = [ (-211,-321) ]
                self.Info( 'Set same-sign hypo (-321,-211)' )

            else :    
                self.hypotheses = [ (-211,-211) ]
                self.Info( 'Default - Set same-sign hypo (-211,-211)' )

        # Lc stuff
        if daughters[2] == 'Lc' or daughters[2] == 'Lambda_c+' :
            self.daug3ID     = LHCb.ParticleID(4122)
            self.daug3ConjID = LHCb.ParticleID(-4122)
            self.gdaugnames[ 211] = 'Lcpi'
            self.gdaugnames[-211] = 'Lcpi'
            self.gdaugnames[ 2212] = 'Lcp'
            self.gdaugnames[-2212] = 'Lcp'
            self.gdaugnames[ 321] = 'LcK'
            self.gdaugnames[-321] = 'LcK'
            self.lcname = 'Lc'

        # Dp stuff - Fix
        if daughters[2] == 'Dp' or daughters[2] == 'D+' :
            self.daug3ID     = LHCb.ParticleID(411)
            self.daug3ConjID = LHCb.ParticleID(-411)
            self.gdaugnames[ 211] = 'Dpi'
            self.gdaugnames[-211] = 'Dpi'
            self.gdaugnames[ 321] = 'DK'
            self.gdaugnames[-321] = 'DK'
            self.lcname = 'Dp'

        # determine the charged daughter types
        if  self.wrongsign :
            if daughters[0] == 'pi' and daughters[1] == 'pi':
                self.daug1ID = LHCb.ParticleID( 211 )
                self.daug2ID = LHCb.ParticleID(-211 )
                
            elif daughters[0] == 'K' and daughters[1] == 'K':
                self.daug1ID = LHCb.ParticleID( 321 )
                self.daug2ID = LHCb.ParticleID(-321 )
                
            elif daughters[0] == 'K' and daughters[1] == 'pi':
                self.daug1ID = LHCb.ParticleID( 321)
                self.daug2ID = LHCb.ParticleID(-211)
                
            elif daughters[0] == 'pi' and daughters[1] == 'K':
                self.daug1ID = LHCb.ParticleID( 211)
                self.daug2ID = LHCb.ParticleID(-321)
                
            else :
                self.Warning( 'hh types ('+daughters[0]+' and '+daughters[1]+') not recognised, setting to pions.' , SUCCESS )
                self.daug1ID = LHCb.ParticleID(211)
                self.daug2ID = LHCb.ParticleID(-211)
                
        else :
            if daughters[0] == 'pi' and daughters[1] == 'pi':
                self.daug1ID = LHCb.ParticleID(-211)
                self.daug2ID = LHCb.ParticleID(-211)

            elif daughters[0] == 'K' and daughters[1] == 'K':
                self.daug1ID = LHCb.ParticleID(-321)
                self.daug2ID = LHCb.ParticleID(-321)

            elif daughters[0] == 'K' and daughters[1] == 'pi':
                self.daug1ID = LHCb.ParticleID(-321)
                self.daug2ID = LHCb.ParticleID(-211)

            elif daughters[0] == 'pi' and daughters[1] == 'K':
                self.daug1ID = LHCb.ParticleID(-211)
                self.daug2ID = LHCb.ParticleID(-321)

            else :
                self.Warning( 'hh types ('+daughters[0]+' and '+daughters[1]+') not recognised, setting to pions.' , SUCCESS )
                self.daug1ID = LHCb.ParticleID(-211)
                self.daug2ID = LHCb.ParticleID(-211)


    def check_types( self ) :

        okB = [ -1, 0, 1 ]
        finalB = 0
        for id in ( self.daug1ID, self.daug2ID, self.daug3ID ) :
            if id.isBaryon() :
                if id.pid() > 0 :
                    finalB += 1
                else :
                    finalB -= 1

        if finalB not in okB :
            self.Error( 'Final state baryon number is unexpected value: '+str(finalB) )
            return FAILURE

        # TODO any other checks we can do here?

        return SUCCESS


    def form_mc_decay_descriptors( self ) :
        """
        Create the decay descriptors for reading the MC particles from the MC truth location.
        This is created based on the MC daughter types or uses the ones provided (if available).
        """

        if not self.signalmc :
            return

        if len(self.input_mc_dds) == 4 :
            # use the descriptors provided by the user
            self.mc_decay_descriptor_Xb = self.input_mc_dds[0]
            self.mc_decay_descriptor_h1 = self.input_mc_dds[1]
            self.mc_decay_descriptor_h2 = self.input_mc_dds[2]
            self.mc_decay_descriptor_lc = self.input_mc_dds[3]

        else :
            daug1_name = self.partpropsvc.find( self.mcdaug1ID ).name()
            daug2_name = self.partpropsvc.find( self.mcdaug2ID ).name()
            daug3_name = self.partpropsvc.find( self.mcdaug3ID ).name()

            daug1_conj_name = self.partpropsvc.find( self.mcdaug1ID ).anti().name()
            daug2_conj_name = self.partpropsvc.find( self.mcdaug2ID ).anti().name()
            daug3_conj_name = self.partpropsvc.find( self.mcdaug3ID ).anti().name()

            if self.mcdaug3ID == self.lcID :

                ddpart1 = '( ( Xb ) =>  %s  %s  %s )' % (daug1_name, daug2_name, daug3_name)
                ddpart2 = '( ( Xb ) =>  %s  %s  %s )' % (daug1_conj_name, daug2_conj_name, daug3_conj_name)
                self.mc_decay_descriptor_Xb = '[ %s, %s ]' % (ddpart1, ddpart2)

                ddpart1 = '( ( Xb ) =>  ^%s  %s  %s )' % (daug1_name, daug2_name, daug3_name)
                ddpart2 = '( ( Xb ) =>  ^%s  %s  %s )' % (daug1_conj_name, daug2_conj_name, daug3_conj_name)
                self.mc_decay_descriptor_h1 = '[ %s, %s ]' % (ddpart1, ddpart2)

                ddpart1 = '( ( Xb ) =>  %s  ^%s  %s )' % (daug1_name, daug2_name, daug3_name)
                ddpart2 = '( ( Xb ) =>  %s  ^%s  %s )' % (daug1_conj_name, daug2_conj_name, daug3_conj_name)
                self.mc_decay_descriptor_h2 = '[ %s, %s ]' % (ddpart1, ddpart2)

                ddpart1 = '( ( Xb ) =>  %s  %s  ^%s )' % (daug1_name, daug2_name, daug3_name)
                ddpart2 = '( ( Xb ) =>  %s  %s  ^%s )' % (daug1_conj_name, daug2_conj_name, daug3_conj_name)
                self.mc_decay_descriptor_lc = '[ %s, %s ]' % (ddpart1, ddpart2)

            else :
                # Dp part
                ddpart1 = '( ( Xb ) =>  %s  %s  %s )' % (daug1_name, daug2_name, daug3_name)
                ddpart2 = '( ( Xb ) =>  %s  %s  %s )' % (daug1_conj_name, daug2_conj_name, daug3_conj_name)
                self.mc_decay_descriptor_Xb = '[ %s, %s ]' % (ddpart1, ddpart2)

                ddpart1 = '( ( Xb ) =>  ^%s  %s  %s )' % (daug1_name, daug2_name, daug3_name)
                ddpart2 = '( ( Xb ) =>  ^%s  %s  %s )' % (daug1_conj_name, daug2_conj_name, daug3_conj_name)
                self.mc_decay_descriptor_h1 = '[ %s, %s ]' % (ddpart1, ddpart2)

                ddpart1 = '( ( Xb ) =>  %s ^%s  %s )' % (daug1_name, daug2_name, daug3_name)
                ddpart2 = '( ( Xb ) =>  %s ^%s  %s )' % (daug1_conj_name, daug2_conj_name, daug3_conj_name)
                self.mc_decay_descriptor_h2 = '[ %s, %s ]' % (ddpart1, ddpart2)

                ddpart1 = '( ( Xb ) =>  %s  %s ^%s )' % (daug1_name, daug2_name, daug3_name)
                ddpart2 = '( ( Xb ) =>  %s  %s ^%s )' % (daug1_conj_name, daug2_conj_name, daug3_conj_name)
                self.mc_decay_descriptor_lc = '[ %s, %s ]' % (ddpart1, ddpart2)

        self.Info( 'Will use the MC decay descriptors:' )
        self.Info( self.mc_decay_descriptor_Xb )
        self.Info( self.mc_decay_descriptor_h1 )
        self.Info( self.mc_decay_descriptor_h2 )
        self.Info( self.mc_decay_descriptor_lc )


    def form_reco_decay_descriptor( self ) :
        """
        Create the decay descriptor for reading the reconstructed particles from the stripping location.
        This is created based on the daughter types.
        """

        daug1_name = self.partpropsvc.find( self.daug1ID ).name()
        daug2_name = self.partpropsvc.find( self.daug2ID ).name()
        daug3_name = self.partpropsvc.find( self.daug3ID ).name()
        rho_name = self.partpropsvc.find( LHCb.ParticleID(-213) ).name()

        if self.daug3ID == self.dpID :
            # Given the generic stripping line, tighten the decay descriptor
            #self.decay_descriptor = '[ ( Xb ) -> %s %s ]CC' % (rho_name, daug3_name)
            self.decay_descriptor = '[ ( Xb ) -> (%s -> pi- pi-) (%s -> K- pi+ pi+) ]CC' % (rho_name, daug3_name)
        else :
            self.decay_descriptor = '[ ( Xb ) -> %s %s %s ]CC' % (daug1_name, daug2_name, daug3_name)

        self.Info( 'Will use the decay descriptor '+self.decay_descriptor )


    def initialize( self ) :

        sc = AlgoMC.initialize( self )
        if sc.isFailure() :
            return sc

        sc = self.check_tes_locations()
        if sc.isFailure() :
            return sc

        self.tistostool   = self.tool( cpp.ITriggerTisTos,   'TriggerTisTos' )
        self.l0tistostool = self.tool( cpp.ITriggerTisTos, 'L0TriggerTisTos' )

        self.bkgtools = []
        if self.simulation :
            # the first of these can't be used in Bender <= v18r3
            self.bkgtools.append( self.tool( cpp.IBackgroundCategory, 'BackgroundCategoryViaRelations' ) )
            self.bkgtools.append( self.tool( cpp.IBackgroundCategory, 'BackgroundCategory' )             )

        self.lifetimetool = self.tool( cpp.ILifetimeFitter, 'PropertimeFitter' )

        self.stateprovider = self.tool( cpp.ITrackStateProvider, 'TrackStateProvider' )

        self.partpropsvc = self.ppSvc()

        self.part2calo = self.tool( cpp.IPart2Calo, 'Part2Calo' )

        # check the validity of the decay
        sc = self.check_types()
        if sc.isFailure() :
            return sc

        # set up the DP kinematics objects
        self.piID = LHCb.ParticleID(211)
        self.kID  = LHCb.ParticleID(321)
        self.pID  = LHCb.ParticleID(2212)
        self.lcID = LHCb.ParticleID(4122)
        self.dpID = LHCb.ParticleID(411)
        self.xbID = LHCb.ParticleID(5132)
        self.obID = LHCb.ParticleID(5332)
        self.bpID = LHCb.ParticleID(521)

        piMass = self.partpropsvc.find( self.piID ).mass()
        kMass  = self.partpropsvc.find( self.kID  ).mass()
        pMass  = self.partpropsvc.find( self.pID  ).mass()
        lcMass  = self.partpropsvc.find( self.lcID  ).mass()
        dpMass  = self.partpropsvc.find( self.dpID  ).mass()
        xbMass = self.partpropsvc.find( self.xbID ).mass()
        obMass = self.partpropsvc.find( self.obID ).mass()
        bpMass = self.partpropsvc.find( self.bpID ).mass()

        self.kinematics = {}

        self.kinematics[5132] = {}
        self.kinematics[5132][4122] = {}
        self.kinematics[5132][4122][211] = {}
        self.kinematics[5132][4122][321] = {}
        self.kinematics[5132][4122][211][211]   = ThreeBodyKinematics( piMass, piMass, lcMass, xbMass )
        self.kinematics[5132][4122][211][321]   = ThreeBodyKinematics( piMass,  kMass, lcMass, xbMass )
        self.kinematics[5132][4122][321][211]   = ThreeBodyKinematics(  kMass, piMass, lcMass, xbMass )
        self.kinematics[5132][4122][321][321]   = ThreeBodyKinematics(  kMass,  kMass, lcMass, xbMass )
        self.kinematics[5132][411] = {}
        self.kinematics[5132][411][211] = {}
        self.kinematics[5132][411][321] = {}
        self.kinematics[5132][411][211][211]   = ThreeBodyKinematics( piMass, piMass, dpMass, xbMass )
        self.kinematics[5132][411][211][321]   = ThreeBodyKinematics( piMass,  kMass, dpMass, xbMass )
        self.kinematics[5132][411][321][211]   = ThreeBodyKinematics(  kMass, piMass, dpMass, xbMass )
        self.kinematics[5132][411][321][321]   = ThreeBodyKinematics(  kMass,  kMass, dpMass, xbMass )

        self.kinematics[5332] = {}
        self.kinematics[5332][4122] = {}
        self.kinematics[5332][4122][211] = {}
        self.kinematics[5332][4122][321] = {}
        self.kinematics[5332][4122][211][211]   = ThreeBodyKinematics( piMass, piMass, lcMass, obMass )
        self.kinematics[5332][4122][211][321]   = ThreeBodyKinematics( piMass,  kMass, lcMass, obMass )
        self.kinematics[5332][4122][321][211]   = ThreeBodyKinematics(  kMass, piMass, lcMass, obMass )
        self.kinematics[5332][4122][321][321]   = ThreeBodyKinematics(  kMass,  kMass, lcMass, obMass )
        self.kinematics[5332][411] = {}
        self.kinematics[5332][411][211] = {}
        self.kinematics[5332][411][321] = {}
        self.kinematics[5332][411][211][211]   = ThreeBodyKinematics( piMass, piMass, dpMass, obMass )
        self.kinematics[5332][411][211][321]   = ThreeBodyKinematics( piMass,  kMass, dpMass, obMass )
        self.kinematics[5332][411][321][211]   = ThreeBodyKinematics(  kMass, piMass, dpMass, obMass )
        self.kinematics[5332][411][321][321]   = ThreeBodyKinematics(  kMass,  kMass, dpMass, obMass )

        self.kinematics[521] = {}
        self.kinematics[521][4122] = {}
        self.kinematics[521][4122][211] = {}
        self.kinematics[521][4122][321] = {}
        self.kinematics[521][4122][211][211]   = ThreeBodyKinematics( piMass, piMass, lcMass, bpMass )
        self.kinematics[521][4122][211][321]   = ThreeBodyKinematics( piMass,  kMass, lcMass, bpMass )
        self.kinematics[521][4122][321][211]   = ThreeBodyKinematics(  kMass, piMass, lcMass, bpMass )
        self.kinematics[521][4122][321][321]   = ThreeBodyKinematics(  kMass,  kMass, lcMass, bpMass )
        self.kinematics[521][411] = {}
        self.kinematics[521][411][211] = {}
        self.kinematics[521][411][321] = {}
        self.kinematics[521][411][211][211]   = ThreeBodyKinematics( piMass, piMass, dpMass, bpMass )
        self.kinematics[521][411][211][321]   = ThreeBodyKinematics( piMass,  kMass, dpMass, bpMass )
        self.kinematics[521][411][321][211]   = ThreeBodyKinematics(  kMass, piMass, dpMass, bpMass )
        self.kinematics[521][411][321][321]   = ThreeBodyKinematics(  kMass,  kMass, dpMass, bpMass )

        # form the decay descriptors
        self.form_reco_decay_descriptor()
        self.form_mc_decay_descriptors()

        return SUCCESS


    def find_kinematics( self, parentID, h1ID, h2ID, lcID ) :

        try :
            kine = self.kinematics[abs(parentID)][abs(lcID)][abs(h1ID)][abs(h2ID)]
        except Exception, e :
            self.Error( 'Problem retrieving kinematics object for %d -> %d %d %d' % ( parentID, h1ID, h2ID, lcID ) )
            raise e

        return kine


    def check_tes_locations( self ) :
        """
        Called by initialize to check that the TES locations specified are as expected.
        """

        if 1 != len( self.Inputs ) :
            return self.Error( 'Expected 1 TES location in Inputs but found %d' % len( self.Inputs ) )

        #loc = self.Inputs[0]
        #if loc.find('DD') > -1 :
        #    if self.name().find('DD') < 0 :
        #        return self.Error( 'TES location contains "DD" but my name does not' )
        #elif loc.find('LL') > -1 :
        #    if self.name().find('LL') < 0 :
        #        return self.Error( 'TES location contains "LL" but my name does not' )
        #elif loc.find('LD') > -1 :
        #    if self.name().find('LD') < 0 :
        #        return self.Error( 'TES location contains "LD" but my name does not' )
        #else :
        #    return self.Error( 'TES location does not contain "LL", "LD" or "DD"' )

        return SUCCESS


    def p4_tuple( self, tuple, particle, name, dtf_params = None ) :
        """
        Store the id, charge, 4-momentum, mass, p_t and eta of a particle.
        """

        p4 = particle.momentum()
        pid = particle.particleID()

        tuple.column_int(    name + '_KEY',   particle.key()             )
        tuple.column_int(    name + '_ID',    pid.pid()                  )
        tuple.column_int(    name + '_Q',     particle.charge()          )
        tuple.column_double( name + '_P',     p4.P()                     )
        tuple.column_double( name + '_PE',    p4.E()                     )
        tuple.column_double( name + '_PX',    p4.Px()                    )
        tuple.column_double( name + '_PY',    p4.Py()                    )
        tuple.column_double( name + '_PZ',    p4.Pz()                    )
        tuple.column_double( name + '_PT',    p4.Pt()                    )
        tuple.column_double( name + '_ETA',   p4.Eta()                   )
        tuple.column_double( name + '_PHI',   p4.Phi()                   )
        tuple.column_double( name + '_THETA', p4.Theta()                 )
        tuple.column_double( name + '_M',     p4.M()                     )
        tuple.column_double( name + '_MM',    particle.measuredMass()    )
        tuple.column_double( name + '_MMERR', particle.measuredMassErr() )
        if dtf_params :
            p4 = dtf_params.momentum()
            tuple.column_double( name + '_DTFM',     p4.m().value() )
            tuple.column_double( name + '_DTFMERR',  p4.m().error() )


    def mc_p4_tuple( self, tuple, mcparticles, mcmatched, name, signal_matched ) :
        """
        Store the id, charge, 4-momentum, mass, p_t and eta of an MC particle
        """

        extra_name = ''
        if signal_matched :
            extra_name = '_SIG'

        if mcparticles.empty() or mcmatched==0 :
            tuple.column_int(    name + extra_name + '_TRUEID',            -1   )
            tuple.column_int(    name + extra_name + '_MOTHER_TRUEID',     -1   )
            tuple.column_int(    name + extra_name + '_GDMOTHER_TRUEID',   -1   )
            tuple.column_int(    name + extra_name + '_GTGDMOTHER_TRUEID', -1   )
            tuple.column_int(    name + extra_name + '_MOTHER_KEY',        -1   )
            tuple.column_int(    name + extra_name + '_GDMOTHER_KEY',      -1   )
            tuple.column_int(    name + extra_name + '_GTGDMOTHER_KEY',    -1   )
            tuple.column_int(    name + extra_name + '_TRUEQ',             -1   )
            tuple.column_double( name + extra_name + '_TRUEP',             -1.1 )
            tuple.column_double( name + extra_name + '_TRUEPE',            -1.1 )
            tuple.column_double( name + extra_name + '_TRUEPX',            -1.1 )
            tuple.column_double( name + extra_name + '_TRUEPY',            -1.1 )
            tuple.column_double( name + extra_name + '_TRUEPZ',            -1.1 )
            tuple.column_double( name + extra_name + '_TRUEPT',            -1.1 )
            tuple.column_double( name + extra_name + '_TRUEETA',           -1.1 )
            tuple.column_double( name + extra_name + '_TRUEPHI',           -1.1 )
            tuple.column_double( name + extra_name + '_TRUETHETA',         -1.1 )
            tuple.column_double( name + extra_name + '_TRUEM',             -1.1 )
            tuple.column_int(    name + extra_name + '_OSCIL',             -1   )
        else :
            mcparticle = mcparticles[0]
            motherid = -1
            gdmotherid = -1
            gtgdmotherid = -1
            motherkey = -1  
            gdmotherkey = -1  
            gtgdmotherkey = -1  
            mcmother = mcparticle.mother()
            if mcmother :
                motherid = int(MCID(mcmother))
                motherkey = mcmother.key()
                mcgrandmother = mcmother.mother()
                if mcgrandmother :
                    gdmotherid = int(MCID(mcgrandmother))
                    gdmotherkey = mcgrandmother.key()
                    mcgreatgrandmother = mcgrandmother.mother()
                    if mcgreatgrandmother :
                        gtgdmotherid = int(MCID(mcgreatgrandmother))
                        gtgdmotherkey = mcgreatgrandmother.key()
            tuple.column_int(    name + extra_name + '_TRUEID',            int(MCID(mcparticle))         )
            tuple.column_int(    name + extra_name + '_MOTHER_TRUEID',     motherid                      )
            tuple.column_int(    name + extra_name + '_GDMOTHER_TRUEID',   gdmotherid                    )
            tuple.column_int(    name + extra_name + '_GTGDMOTHER_TRUEID', gtgdmotherid                  )
            tuple.column_int(    name + extra_name + '_MOTHER_KEY',        motherkey                     )
            tuple.column_int(    name + extra_name + '_GDMOTHER_KEY',      gdmotherkey                   )
            tuple.column_int(    name + extra_name + '_GTGDMOTHER_KEY',    gtgdmotherkey                 )
            tuple.column_int(    name + extra_name + '_TRUEQ',             int(MC3Q(mcparticle)/3)       )
            tuple.column_double( name + extra_name + '_TRUEP',             MCP(mcparticle)               )
            tuple.column_double( name + extra_name + '_TRUEPE',            MCE(mcparticle)               )
            tuple.column_double( name + extra_name + '_TRUEPX',            MCPX(mcparticle)              )
            tuple.column_double( name + extra_name + '_TRUEPY',            MCPY(mcparticle)              )
            tuple.column_double( name + extra_name + '_TRUEPZ',            MCPZ(mcparticle)              )
            tuple.column_double( name + extra_name + '_TRUEPT',            MCPT(mcparticle)              )
            tuple.column_double( name + extra_name + '_TRUEETA',           MCETA(mcparticle)             )
            tuple.column_double( name + extra_name + '_TRUEPHI',           MCPHI(mcparticle)             )
            tuple.column_double( name + extra_name + '_TRUETHETA',         MCTHETA(mcparticle)           )
            tuple.column_double( name + extra_name + '_TRUEM',             MCM(mcparticle)               )
            tuple.column_int(    name + extra_name + '_OSCIL',             int(MCOSCILLATED(mcparticle)) )


    def ip_tuple( self, tuple, particle, name ) :
        """
        Store the impact parameter info for the particle
        """

        primaries = self.vselect('PV', PRIMARY )
        bestPV = self.bestVertex( particle )

        minipfun     = MINIP( primaries, self.geo() )
        minipchi2fun = MINIPCHI2( primaries, self.geo() )
        ipbpvfun     = IP( bestPV, self.geo() )
        ipchi2bpvfun = IPCHI2( bestPV, self.geo() )

        tuple.column_double( name + '_MINIP',         minipfun(particle)     )
        tuple.column_double( name + '_MINIPCHI2',     minipchi2fun(particle) )
        tuple.column_double( name + '_IP_OWNPV',      ipbpvfun(particle)     )
        tuple.column_double( name + '_IPCHI2_OWNPV',  ipchi2bpvfun(particle) )

        bpvpos = bestPV.position()
        tuple.column_double( name + '_OWNPV_X', bpvpos.x() )
        tuple.column_double( name + '_OWNPV_Y', bpvpos.y() )
        tuple.column_double( name + '_OWNPV_Z', bpvpos.z() )

        covMatrix = bestPV.covMatrix()
        tuple.column_double( name + '_OWNPV_XERR', TMath.Sqrt( covMatrix(0,0) ) )
        tuple.column_double( name + '_OWNPV_YERR', TMath.Sqrt( covMatrix(1,1) ) )
        tuple.column_double( name + '_OWNPV_ZERR', TMath.Sqrt( covMatrix(2,2) ) )

        chi2 = bestPV.chi2()
        ndof = bestPV.nDoF()
        chi2ndof = bestPV.chi2PerDoF()
        tuple.column_int(    name + '_OWNPV_NDOF',     ndof                     )
        tuple.column_double( name + '_OWNPV_CHI2',     chi2                     )
        tuple.column_double( name + '_OWNPV_CHI2NDOF', chi2ndof                 )
        tuple.column_double( name + '_OWNPV_PROB',     TMath.Prob( chi2, ndof ) )

        ntrk = bestPV.tracks().size()
        tuple.column_int( name + '_OWNPV_NTRACKS', ntrk )


    def vtx_tuple( self, tuple, bcand, lccand ) :
        """
        Store vertex info for the B and Lc
        """

        bcand_name = 'B'
        lccand_name = self.lcname

        for particle in [bcand, lccand] :
            if particle is bcand :
                name = bcand_name
            else :
                name = lccand_name

            vertex = particle.endVertex()

            vtxpos = vertex.position()
            tuple.column_double( name + '_ENDVERTEX_X',    vtxpos.x() )
            tuple.column_double( name + '_ENDVERTEX_Y',    vtxpos.y() )
            tuple.column_double( name + '_ENDVERTEX_Z',    vtxpos.z() )

            covMatrix = vertex.covMatrix()
            tuple.column_double( name + '_ENDVERTEX_XERR', TMath.Sqrt( covMatrix(0,0) ) )
            tuple.column_double( name + '_ENDVERTEX_YERR', TMath.Sqrt( covMatrix(1,1) ) )
            tuple.column_double( name + '_ENDVERTEX_ZERR', TMath.Sqrt( covMatrix(2,2) ) )

            chi2 = vertex.chi2()
            ndof = vertex.nDoF()
            chi2ndof = vertex.chi2PerDoF()
            tuple.column_int(    name + '_ENDVERTEX_NDOF',     ndof                     )
            tuple.column_double( name + '_ENDVERTEX_CHI2',     chi2                     )
            tuple.column_double( name + '_ENDVERTEX_CHI2NDOF', chi2ndof                 )
            tuple.column_double( name + '_ENDVERTEX_PROB',     TMath.Prob( chi2, ndof ) )

            primaries = self.vselect('PV', PRIMARY )
            minvdfun     = MINVVD( primaries )
            minvdchi2fun = MINVVDCHI2( primaries )

            tuple.column_double( name + '_MINVD',     minvdfun(particle)     )
            tuple.column_double( name + '_MINVDCHI2', minvdchi2fun(particle) )

            bestPV = self.bestVertex( particle )
            vdfun     = VD( bestPV )
            vdchi2fun = VDCHI2( bestPV )
            dirafun   = DIRA( bestPV )

            tuple.column_double( name + '_VD_OWNPV',     vdfun(particle)     )
            tuple.column_double( name + '_VDCHI2_OWNPV', vdchi2fun(particle) )
            tuple.column_double( name + '_DIRA_OWNPV',   dirafun(particle)   )

        b_vtx = bcand.endVertex()
        vdfun     = VD(b_vtx)
        vdchi2fun = VDCHI2(b_vtx)
        tuple.column_double( lccand_name+'_VTX_SEP',     vdfun(lccand)     )
        tuple.column_double( lccand_name+'_VTX_SEPCHI2', vdchi2fun(lccand) )


    def mc_vtx_tuple( self, tuple, mcparticles, mcmatched, name, signal_matched ) :
        """
        Store vertex and lifetime info for the MC particle
        """

        extra_name = ''
        if signal_matched :
            extra_name = '_SIG'

        if mcparticles.empty() or mcmatched==0 :
            tuple.column_double( name + extra_name + '_TRUEORIGINVERTEX_X', -1.1 )
            tuple.column_double( name + extra_name + '_TRUEORIGINVERTEX_Y', -1.1 )
            tuple.column_double( name + extra_name + '_TRUEORIGINVERTEX_Z', -1.1 )
            tuple.column_double( name + extra_name + '_TRUECTAU'          , -1.1 )
        else :
            mcparticle = mcparticles[0]
            tuple.column_double( name + extra_name + '_TRUEORIGINVERTEX_X', MCVFASPF(MCVX)(mcparticle) )
            tuple.column_double( name + extra_name + '_TRUEORIGINVERTEX_Y', MCVFASPF(MCVY)(mcparticle) )
            tuple.column_double( name + extra_name + '_TRUEORIGINVERTEX_Z', MCVFASPF(MCVZ)(mcparticle) )
            tuple.column_double( name + extra_name + '_TRUECTAU'          , MCCTAU(mcparticle)         )


    def trk_tuple( self, tuple, particle, name ) :
        """
        Store track-related quantities:
        - track chi2 per dof
        - PID quantities: PIDk, PIDp, PIDmu and ISMUON
        """

        # track quantities
        tuple.column_double( name + '_TRACK_CHI2NDOF',  TRCHI2DOF(particle)   )
        tuple.column_double( name + '_TRACK_PCHI2',     TRPCHI2(particle)     )
        tuple.column_double( name + '_TRACK_GHOSTPROB', TRGHOSTPROB(particle) )

        # PID quantities
        tuple.column_double( name + '_PIDe',        PIDe(particle)         )
        tuple.column_double( name + '_PIDmu',       PIDmu(particle)        )
        tuple.column_double( name + '_PIDpi',       PIDpi(particle)        )
        tuple.column_double( name + '_PIDK',        PIDK(particle)         )
        tuple.column_double( name + '_PIDp',        PIDp(particle)         )
        tuple.column_double( name + '_PROBNNe',     PROBNNe(particle)      )
        tuple.column_double( name + '_PROBNNmu',    PROBNNmu(particle)     )
        tuple.column_double( name + '_PROBNNpi',    PROBNNpi(particle)     )
        tuple.column_double( name + '_PROBNNK',     PROBNNK(particle)      )
        tuple.column_double( name + '_PROBNNp',     PROBNNp(particle)      )
        tuple.column_double( name + '_PROBNNghost', PROBNNghost(particle)  )

        hasmuon = -1
        ismuon = -1
        hasproto = HASPROTO(particle)
        if hasproto :
            hasmuon = HASMUON(particle)
            if hasmuon :
                ismuon = ISMUON(particle)
        tuple.column_int( name + '_hasProto', int(hasproto) )
        tuple.column_int( name + '_hasMuon',  int(hasmuon)  )
        tuple.column_int( name + '_isMuon',   int(ismuon)   )

        # HCAL info for L0 Hadron correction - Fix this
        self.part2calo.match( particle, '/dd/Structure/LHCb/DownstreamRegion/Hcal' )

        caloX = self.part2calo.caloState().x()
        caloY = self.part2calo.caloState().y()
        caloZ = self.part2calo.caloState().z()
        caloP = self.part2calo.caloState().p()
        caloM = particle.measuredMass()

        trackET = TMath.Sqrt( caloP*caloP + caloM*caloM ) * TMath.Sqrt( caloX*caloX + caloY*caloY ) / TMath.Sqrt( caloX*caloX + caloY*caloY + caloZ*caloZ )

        region = self.isinside_HCAL( caloX, caloY )

        tuple.column_double( name + '_L0Calo_HCAL_realET',      trackET )
        tuple.column_double( name + '_L0Calo_HCAL_xProjection', caloX   )
        tuple.column_double( name + '_L0Calo_HCAL_yProjection', caloY   )
        tuple.column_int(    name + '_L0Calo_HCAL_region',      region  )


    def isinside_HCAL( self, caloX, caloY ) :
        """
        Determine whether the particle is in the Inner, Outer part of the HCAL
        or outside it completely
        """

        inside = True
        inner = False
        outer = False

        HCAL_CellSize_Inner = 131.3
        HCAL_CellSize_Outer = 262.6
        HCAL_xMax_Inner = 2101
        HCAL_yMax_Inner = 1838
        HCAL_xMax_Outer = 4202
        HCAL_yMax_Outer = 3414

        # projection inside calo
        if TMath.Abs( caloX ) < HCAL_xMax_Outer and TMath.Abs( caloY ) < HCAL_yMax_Outer :
            # projection inside inner calo (else is outer calo)
            if TMath.Abs( caloX ) < HCAL_xMax_Inner and TMath.Abs( caloY ) < HCAL_yMax_Inner :
                # projections outside the beampipe (in x)
                if TMath.Abs( caloX ) > 2*HCAL_CellSize_Inner :
                    inner = True
                elif TMath.Abs( caloY )  > 2*HCAL_CellSize_Inner :
                    inner = True
                else :
                    inside = False
            else :
                outer = True
        else :
            inside = False

        if not inside :
            return -1
        elif inner :
            return 1
        elif outer :
            return 0
        else :
            return -999


    def pid_swap( self, bcand, bpid ) :
        """
        Change the ID of the B candidate and the IDs of its charged daughters.
        The particle charges will be unchanged, i.e. supplied IDs will be
        treated as unsigned values.
        """

        # first find the "original" IDs
        orig_b_pid = bcand.particleID().pid()

        # now swap the ID of the B
        bpid = abs(bpid)
        if orig_b_pid < 0 :
            bpid = -bpid
        bcand.setParticleID( LHCb.ParticleID(bpid) )

        return (orig_b_pid)


    def pid_swap_back( self, bcand, orig_bpid ) :
        """
        Change the IDs of the B candidate and its charged daughters back to
        their original values.
        """

        bcand.setParticleID( LHCb.ParticleID( orig_bpid ) )


    def dtf_p4_tuple( self, dpinfo, bcand ) :
        """
        Store the results of a vertex fit where the masses of various particles
        in the decay tree are constrained to their PDG values.
        Values stored are:
        - information on the success (or otherwise) of the fit
        - the vertex chi2, ndof and probability
        - the B lifetime, associated uncertainty and significance
        - the 4-momentum of the B and its 3 daughters
        - the invariant mass-squared pairs m12Sq, m13Sq, m23Sq
        - the square DP co-ordinates mPrime and thPrime
        """

        # Name of the Lc/Dp
        lcname = self.lcname

        # perform the fit adding constraints on both the parent and V0 masses
        pv = self.bestVertex( bcand )
        fitter = cpp.DecayTreeFitter.Fitter( bcand, pv, self.stateprovider )
        fitter.setMassConstraint( bcand )
        for daug in bcand.daughters():
            daug_pid = daug.particleID()
            if daug_pid == self.daug3ID or daug_pid == self.daug3ConjID :
                fitter.setMassConstraint( daug )
        fitter.fit()

        #bp4 = Gaudi.Math.LorentzVectorWithError()
        bp4 = TLorentzVector()
        bctau = Gaudi.Math.ValueWithError()

        daug_id = [ 0, 0, 0 ]
        #daug_p4 = [ Gaudi.Math.LorentzVectorWithError(), Gaudi.Math.LorentzVectorWithError(), Gaudi.Math.LorentzVectorWithError() ]
        daug_p4 = [ TLorentzVector(), TLorentzVector(), TLorentzVector() ]
        daug_names = [ 'h1', 'h2', lcname ]

        m12Sq = -1.1
        m13Sq = -1.1
        m23Sq = -1.1

        mPrime = -1.1
        thPrime = -1.1

        cThetap = -1.1
        phip = -1.1
        phihh = -1.1

        # check if the fit succeeded
        if fitter.status() == 0 :

            # store information on the B candidate
            bparams = fitter.fitParams( bcand )
            temp_p4 = bparams.momentum()
            bp4 = TLorentzVector(temp_p4.Px(),temp_p4.Py(), temp_p4.Pz(), temp_p4.E())
            bctau = bparams.ctau()

            bcandID = bcand.particleID().pid()

            # loop through 3 B daughters and store their 4-momenta
            if not self.wrongsign :
                h1cand, h2cand, lccand = self.find_daughters( bcand )
            else :
                h1cand, h2cand, lccand = self.find_daughters_wrongsign( bcand )

            daughters = [ h1cand, h2cand, lccand ]

            for i in range(3) :
                params = fitter.fitParams( daughters[i] )
                temp_p4 = params.momentum()
                daug_p4[i] = TLorentzVector(temp_p4.Px(),temp_p4.Py(), temp_p4.Pz(), temp_p4.E())
                daug_id[i] = daughters[i].particleID().abspid()

            p12 = daug_p4[0] + daug_p4[1]
            p13 = daug_p4[0] + daug_p4[2]
            p23 = daug_p4[1] + daug_p4[2]

            m12Sq = p12.M2()
            m13Sq = p13.M2()
            m23Sq = p23.M2()

            kine = self.find_kinematics( bcandID, daug_id[0], daug_id[1], daug_id[2] )
            if kine.withinDPLimits( m13Sq, m23Sq ) :
                kine.updateKinematics( m13Sq, m23Sq )
                mPrime = kine.mPrime
                thPrime = kine.thPrime

            if m12Sq<0 :     m12Sq=-1.1
            if m12Sq>100e6 : m12Sq=-1.1
            if m13Sq<0 :     m13Sq=-1.1
            if m13Sq>100e6 : m13Sq=-1.1
            if m23Sq<0 :     m23Sq=-1.1
            if m23Sq>100e6 : m23Sq=-1.1

            cThetap, phip, phihh = self.angvariables(bp4, daug_p4[0], daug_p4[1], daug_p4[2])

        # store the fit results (sucess or otherwise, lifetime, 4-momenta and DP co-ordinates)
        dpinfo.store_fit_results( fitter, bctau, bp4, daug_p4, m12Sq, m13Sq, m23Sq, mPrime, thPrime, cThetap, phip, phihh )

    def dtf_p4_tuple_all( self, dpinfo, bcand ) :
        """
        Store the results of a vertex fit where the masses of various particles
        in the decay tree are constrained to their PDG values.
        Values stored are:
        - information on the success (or otherwise) of the fit
        - the vertex chi2, ndof and probability
        - the B lifetime, associated uncertainty and significance
        - the 4-momentum of the B and its 3 daughters
        - the invariant mass-squared pairs m12Sq, m13Sq, m23Sq
        - the square DP co-ordinates mPrime and thPrime
        """

        # Name of the Lc/Dp
        lcname = self.lcname

        # perform the fit adding constraints on V0 mass
        pv = self.bestVertex( bcand )
        fitter = cpp.DecayTreeFitter.Fitter( bcand, pv, self.stateprovider )
        for daug in bcand.daughters():
            daug_pid = daug.particleID()
            if daug_pid == self.daug3ID or daug_pid == self.daug3ConjID :
                fitter.setMassConstraint( daug )
        fitter.fit()

        #bp4 = Gaudi.Math.LorentzVectorWithError()
        bp4 = TLorentzVector()
        bctau = Gaudi.Math.ValueWithError()

        daug_id = [ 0, 0, 0 ]
        #daug_p4 = [ Gaudi.Math.LorentzVectorWithError(), Gaudi.Math.LorentzVectorWithError(), Gaudi.Math.LorentzVectorWithError() ]
        daug_p4 = [ TLorentzVector(), TLorentzVector(), TLorentzVector() ]
        daug_names = [ 'h1', 'h2', lcname ]

        m12Sq = -1.1
        m13Sq = -1.1
        m23Sq = -1.1

        mPrime = -1.1
        thPrime = -1.1

        cThetap = -1.1
        phip = -1.1
        phihh = -1.1

        # check if the fit succeeded
        if fitter.status() == 0 :

            # store information on the B candidate
            bparams = fitter.fitParams( bcand )
            temp_p4 = bparams.momentum()
            bp4 = TLorentzVector(temp_p4.Px(),temp_p4.Py(), temp_p4.Pz(), temp_p4.E())
            bctau = bparams.ctau()

            bcandID = bcand.particleID().pid()

            # loop through 3 B daughters and store their 4-momenta
            if not self.wrongsign :
                h1cand, h2cand, lccand = self.find_daughters( bcand )
            else :
                h1cand, h2cand, lccand = self.find_daughters_wrongsign( bcand )

            daughters = [ h1cand, h2cand, lccand ]

            for i in range(3) :
                params = fitter.fitParams( daughters[i] )
                temp_p4 = params.momentum()
                daug_p4[i] = TLorentzVector(temp_p4.Px(),temp_p4.Py(), temp_p4.Pz(), temp_p4.E())
                daug_id[i] = daughters[i].particleID().abspid()

            p12 = daug_p4[0] + daug_p4[1]
            p13 = daug_p4[0] + daug_p4[2]
            p23 = daug_p4[1] + daug_p4[2]

            m12Sq = p12.M2()
            m13Sq = p13.M2()
            m23Sq = p23.M2()

            #kine = self.find_kinematics( bcandID, daug_id[0], daug_id[1], daug_id[2] )
            #if kine.withinDPLimits( m13Sq, m23Sq ) :
            #    kine.updateKinematics( m13Sq, m23Sq )
            #    mPrime = kine.mPrime
            #    thPrime = kine.thPrime

            if m12Sq<0 :     m12Sq=-1.1
            if m12Sq>100e6 : m12Sq=-1.1
            if m13Sq<0 :     m13Sq=-1.1
            if m13Sq>100e6 : m13Sq=-1.1
            if m23Sq<0 :     m23Sq=-1.1
            if m23Sq>100e6 : m23Sq=-1.1

            cThetap, phip, phihh = self.angvariables(bp4, daug_p4[0], daug_p4[1], daug_p4[2])

        # store the fit results (sucess or otherwise, lifetime, 4-momenta and DP co-ordinates)
        dpinfo.store_fit_results( fitter, bctau, bp4, daug_p4, m12Sq, m13Sq, m23Sq, mPrime, thPrime, cThetap, phip, phihh )


    def calc_DP_info( self, bcand ) :
        """
        Calculate the DP position and the daughter 4-momenta with the
        B-candidate mass constrained to either the nominal Bd, Bs or Lb mass
        and under various mass assumptions for the daughters
        """

        self.dpinfos = []

        if self.daug1ID == LHCb.ParticleID(-321) and self.daug2ID == LHCb.ParticleID(-211) :
            self.h1ID = LHCb.ParticleID(-321)
            self.h2ID = LHCb.ParticleID(-211)

        elif self.daug1ID == LHCb.ParticleID(-211) and self.daug2ID == LHCb.ParticleID(-211) :
             self.h1ID = LHCb.ParticleID(-211)
             self.h2ID = LHCb.ParticleID(-211)

        elif self.daug1ID == LHCb.ParticleID(-321) and self.daug2ID == LHCb.ParticleID(-321) :
             self.h1ID = LHCb.ParticleID(-321)
             self.h2ID = LHCb.ParticleID(-321)

        else :
             self.h1ID = LHCb.ParticleID(-211)
             self.h2ID = LHCb.ParticleID(-211)

        h1_id = self.h1ID.abspid()
        h2_id = self.h2ID.abspid()

        bhypos = []

        if self.lcname == 'Lc' :
            bhypos = [ 5132, 5332 ]
        else :
            bhypos = [ 521 ]

        for b_id in bhypos :

            # construct the suffix to be of the form, e.g. Bs2KpiKS
            suffix = self.simplenames[b_id]+'2'+self.simplenames[h1_id]+self.simplenames[h2_id]+self.simplenames[self.daug3ID.abspid()]

            # do the PID swap
            orig_b_pid = self.pid_swap( bcand, b_id )

            # create the object to store the information
            dpinfo = DalitzInfo( self.lcname, suffix )

            # do the fits and store the information
            self.dtf_p4_tuple( dpinfo, bcand )

            # do the swap back to the original IDs
            self.pid_swap_back( bcand, orig_b_pid )

            # append the DP info object to the list
            self.dpinfos.append( dpinfo )

            if b_id == 5132 or b_id == -5132 :
                # New part to get dtf variables without the B mass constraint
                suffix = self.simplenames[h1_id]+self.simplenames[h2_id]+self.simplenames[self.daug3ID.abspid()]
                print 'XoXoXoXoXoX'
                print suffix
                dpinfo = DalitzInfo( self.lcname, suffix )
                self.dtf_p4_tuple_all( dpinfo, bcand );
                self.dpinfos.append( dpinfo )

    def store_DP_info( self, tuple ) :
        """
        Store the pre-calculated DP information in the supplied tuple
        """
        
        for info in self.dpinfos :
            info.fill_tuple( tuple )


    def mc_dp_tuple( self, tuple, mcparents ) :
        """
        Store the MC truth DP co-ordinates
        """

        # loop through the B daughters and store their 4-momenta treating the
        # expected 3 daughters and PHOTOS photons separately

        daug_names = [ 'h1', 'h2', self.lcname ]

        # if there is no MC info
        if mcparents.empty() :
            tuple.column_int( 'nPHOTOS', -1 )
            for daug_name in daug_names :
                tuple.column_double( daug_name + '_CORRPE', -1.1 )
                tuple.column_double( daug_name + '_CORRPX', -1.1 )
                tuple.column_double( daug_name + '_CORRPY', -1.1 )
                tuple.column_double( daug_name + '_CORRPZ', -1.1 )
            tuple.column_double( 'm12Sq_MC',   -1.1 )
            tuple.column_double( 'm13Sq_MC',   -1.1 )
            tuple.column_double( 'm23Sq_MC',   -1.1 )
            tuple.column_double( 'mPrime_MC',  -1.1 )
            tuple.column_double( 'thPrime_MC', -1.1 )
            return

        # otherwise take the first entry
        # TODO - can we do something more clever here?
        mcparent = mcparents[0]

        mcparent_id = mcparent.particleID().pid()

        mcparentp4 = TLorentzVector( MCPX(mcparent), MCPY(mcparent), MCPZ(mcparent), MCE(mcparent) )

        daug_id = [ 0, 0, 0 ]
        daug_p4 = [ TLorentzVector(), TLorentzVector(), TLorentzVector() ]
        gamma_p4 = []

        # python list of children, may initially contain resonances so it is 
        # recursively searched through to find the correct final state particles.
        children = self.mc_find_daug( [ daug for daug in mcparent.children( True ) ] )

        # check that we are left with 3 non-photon daughters
        ndaug = 0
        for daug in children :
            if daug.particleID().pid() != 22 :
                ndaug += 1
        if ndaug != 3 :
            self.Error( 'Parent particle does not have 3 non-photon final-state daughters' )
            e = Exception('Unexpected number of final-state particles')
            raise e

        gotFirstCand = False

        for daug in children :

            daugID = daug.particleID()

            px = MCPX( daug )
            py = MCPY( daug )
            pz = MCPZ( daug )
            pe = MCE ( daug )

            p4 = TLorentzVector( px, py, pz, pe )

            if 22 == daugID.pid() :
                gamma_p4.append( p4 )
            elif self.mcdaug1ID.pid() == self.mcdaug2ID.pid() :
                if daugID == self.mcdaug3ID or daugID == self.mcdaug3ConjID :
                    daug_id[2] = daugID.pid()
                    daug_p4[2] = p4
                elif not gotFirstCand :
                    daug_id[0] = daugID.pid()
                    daug_p4[0] = p4
                    gotFirstCand = True
                else :
                    daug_id[1] = daugID.pid()
                    daug_p4[1] = p4
            else :
                if daugID == self.mcdaug3ID or daugID == self.mcdaug3ConjID :
                    daug_id[2] = daugID.pid()
                    daug_p4[2] = p4
                elif daugID.abspid() == self.mcdaug1ID.abspid() :
                    daug_id[0] = daugID.pid()
                    daug_p4[0] = p4
                else :
                    daug_id[1] = daugID.pid()
                    daug_p4[1] = p4

        ngamma = len(gamma_p4)
        tuple.column_int( 'nPHOTOS', ngamma )

        if 0 != ngamma :

            for gamma in gamma_p4 :
                minangle = 1000.0
                mindaug = -1
                for daug in daug_p4 :
                    gammaPHat = gamma.Vect().Unit()
                    daugPHat = daug.Vect().Unit()
                    angle = TMath.ACos( gammaPHat.Dot( daugPHat ) )
                    if abs(angle) < minangle :
                        minangle = angle
                        mindaug = daug_p4.index(daug)

                daug_p4[ mindaug ] += gamma


        p12 = daug_p4[0] + daug_p4[1]
        p13 = daug_p4[0] + daug_p4[2]
        p23 = daug_p4[1] + daug_p4[2]

        for i in range(3) :
            tuple.column_double( daug_names[i] + '_CORRPE',  daug_p4[i].E()  )
            tuple.column_double( daug_names[i] + '_CORRPX',  daug_p4[i].Px() )
            tuple.column_double( daug_names[i] + '_CORRPY',  daug_p4[i].Py() )
            tuple.column_double( daug_names[i] + '_CORRPZ',  daug_p4[i].Pz() )

        m12Sq = p12.M2()
        m13Sq = p13.M2()
        m23Sq = p23.M2()

        mPrime = -1.1
        thPrime = -1.1

        cThetap = -1.1
        phip = -1.1
        phihh = -1.1

        #get the angular variables
        cThetap, phip, phihh = self.angvariables(mcparentp4, daug_p4[0], daug_p4[1], daug_p4[2])
        tuple.column( 'cThetap_MC',  cThetap )
        tuple.column( 'phip_MC',  phip )
        tuple.column( 'phihh_MC',  phihh )

        kine = self.find_kinematics( mcparent_id, daug_id[0], daug_id[1], daug_id[2] )
        if kine.withinDPLimits( m13Sq, m23Sq ) :
            kine.updateKinematics( m13Sq, m23Sq )
            mPrime = kine.mPrime
            thPrime = kine.thPrime

        if m12Sq<0 :     m12Sq=-1.1
        if m12Sq>100e6 : m12Sq=-1.1
        if m13Sq<0 :     m13Sq=-1.1
        if m13Sq>100e6 : m13Sq=-1.1
        if m23Sq<0 :     m23Sq=-1.1
        if m23Sq>100e6 : m23Sq=-1.1

        tuple.column_double( 'm12Sq_MC', m12Sq )
        tuple.column_double( 'm13Sq_MC', m13Sq )
        tuple.column_double( 'm23Sq_MC', m23Sq )

        tuple.column_double( 'mPrime_MC',  mPrime  )
        tuple.column_double( 'thPrime_MC', thPrime )


    def mc_find_daug(self, children):
        lower_children = []
        for daug in children:
            daugID = daug.particleID()
            if 22 == daugID.pid() :
                lower_children.append(daug)
                continue
            elif daugID.abspid() in [ self.mcdaug1ID.abspid(), self.mcdaug2ID.abspid(), self.mcdaug3ID.abspid() ] :
                lower_children.append(daug)
                continue
            else :
                # get the daughters of the unknown particle
                daug_children = [ lower_daug for lower_daug in daug.children( True ) ] 
                # check that there are daughters (resonance) and not just unknown
                if len(daug_children)>0:
                    # apply same recursive function to daughter's children
                    lower_children.extend( self.mc_find_daug( daug_children ) )
                    continue
                else :
                    self.Error( 'Daughter does not match any of the expected daughter types and has no children: '+str(daugID.pid()) )
                    e = Exception('Unknown daughter type')
                    raise e
        return lower_children


    def trig_tuple( self, tuple, bcand ) :

        # Find the TCK
        location = 'Hlt/DecReports'
        if self.RootInTES != '' :
            location = self.RootInTES + location
        hdr = self.get(location)
        tck = -1
        if hdr :
            tck = hdr.configuredTCK()
        tuple.column_int("tck", tck )

        # Setup the TISTOS tools
        self.l0tistostool.setOfflineInput( bcand )
        self.tistostool.setOfflineInput( bcand )

        # Get the L0 decisions, plus TIS & TOS info

        l0triggers = self.l0tistostool.triggerSelectionNames('L0.*Decision')
        self.l0tistostool.setTriggerInput( 'L0.*Decision' )
        l0 = self.l0tistostool.tisTosTobTrigger()
        l0dec = l0.decision()
        l0tis = l0.tis()
        l0tos = l0.tos()
        tuple.column_int("L0Global_Dec", l0dec )
        tuple.column_int("L0Global_TIS", l0tis )
        tuple.column_int("L0Global_TOS", l0tos )

        l0declist = self.l0tistostool.triggerSelectionNames( self.tistostool.kTrueRequired, self.tistostool.kAnything,     self.tistostool.kAnything     )
        l0tislist = self.l0tistostool.triggerSelectionNames( self.tistostool.kTrueRequired, self.tistostool.kTrueRequired, self.tistostool.kAnything     )
        l0toslist = self.l0tistostool.triggerSelectionNames( self.tistostool.kTrueRequired, self.tistostool.kAnything,     self.tistostool.kTrueRequired )

        # set the list of L0 decisions to be stored
        l0tuplelist = ['L0DiMuonDecision',
                       'L0MuonDecision',
                       'L0ElectronDecision',
                       'L0ElectronHiDecision',
                       'L0PhotonDecision',
                       'L0HadronDecision']

        for line in l0triggers :
            if line in l0tuplelist :
                l0dec = 0
                l0tis = 0
                l0tos = 0
                if line in l0declist : l0dec = 1
                if line in l0tislist : l0tis = 1
                if line in l0toslist : l0tos = 1
                tuple.column_int(line+"_Dec", l0dec )
                tuple.column_int(line+"_TIS", l0tis )
                tuple.column_int(line+"_TOS", l0tos )

        # Get the HLT decisions, plus TIS & TOS info

        hlt1triggers = self.tistostool.triggerSelectionNames('Hlt1.*Decision')
        self.tistostool.setTriggerInput( 'Hlt1.*Decision' )
        hlt1 = self.tistostool.tisTosTobTrigger()
        hlt1dec = hlt1.decision()
        hlt1tis = hlt1.tis()
        hlt1tos = hlt1.tos()
        tuple.column_int("Hlt1Global_Dec", hlt1dec )
        tuple.column_int("Hlt1Global_TIS", hlt1tis )
        tuple.column_int("Hlt1Global_TOS", hlt1tos )

        hlt1declist = self.tistostool.triggerSelectionNames( self.tistostool.kTrueRequired, self.tistostool.kAnything,     self.tistostool.kAnything     )
        hlt1tislist = self.tistostool.triggerSelectionNames( self.tistostool.kTrueRequired, self.tistostool.kTrueRequired, self.tistostool.kAnything     )
        hlt1toslist = self.tistostool.triggerSelectionNames( self.tistostool.kTrueRequired, self.tistostool.kAnything,     self.tistostool.kTrueRequired )

        #set the list of hlt1 decision to be stored
        hlt1tuplelist = ['Hlt1TrackAllL0Decision',
                         'Hlt1TrackMuonDecision',
                         'Hlt1TrackPhotonDecision']

        for line in hlt1triggers :
            if line in hlt1tuplelist :
                hlt1dec = 0
                hlt1tis = 0
                hlt1tos = 0
                if line in hlt1declist : hlt1dec = 1
                if line in hlt1tislist : hlt1tis = 1
                if line in hlt1toslist : hlt1tos = 1
                tuple.column_int(line+"_Dec", hlt1dec )
                tuple.column_int(line+"_TIS", hlt1tis )
                tuple.column_int(line+"_TOS", hlt1tos )

        hlt2triggers = self.tistostool.triggerSelectionNames('Hlt2.*Decision')
        self.tistostool.setTriggerInput( 'Hlt2.*Decision' )
        hlt2 = self.tistostool.tisTosTobTrigger()
        hlt2dec = hlt2.decision()
        hlt2tis = hlt2.tis()
        hlt2tos = hlt2.tos()
        tuple.column_int("Hlt2Global_Dec", hlt2dec )
        tuple.column_int("Hlt2Global_TIS", hlt2tis )
        tuple.column_int("Hlt2Global_TOS", hlt2tos )

        hlt2declist = self.tistostool.triggerSelectionNames( self.tistostool.kTrueRequired, self.tistostool.kAnything,     self.tistostool.kAnything     )
        hlt2tislist = self.tistostool.triggerSelectionNames( self.tistostool.kTrueRequired, self.tistostool.kTrueRequired, self.tistostool.kAnything     )
        hlt2toslist = self.tistostool.triggerSelectionNames( self.tistostool.kTrueRequired, self.tistostool.kAnything,     self.tistostool.kTrueRequired )

        # set the list of hlt2 decision to be stored
        hlt2tuplelist = ['Hlt2Topo2BodyBBDTDecision',
                         'Hlt2Topo3BodyBBDTDecision',
                         'Hlt2Topo4BodyBBDTDecision',
                         'Hlt2Topo2BodySimpleDecision',
                         'Hlt2Topo3BodySimpleDecision',
                         'Hlt2Topo4BodySimpleDecision',
                         'Hlt2B2HHDecision',
                         'Hlt2B2HHPi0_MergedDecision']

        for line in hlt2triggers :
            if line in hlt2tuplelist :
                hlt2dec = 0
                hlt2tis = 0
                hlt2tos = 0
                if line in hlt2declist : hlt2dec = 1
                if line in hlt2tislist : hlt2tis = 1
                if line in hlt2toslist : hlt2tos = 1
                tuple.column_int(line+"_Dec", hlt2dec )
                tuple.column_int(line+"_TIS", hlt2tis )
                tuple.column_int(line+"_TOS", hlt2tos )


    def store_DTF_fit_results( self, tuple, fitter, suffix ) :
        """
        Fill the supplied tuple with results from the supplied DTF object
        """

        # store info on the success (or otherwise) of the fit
        tuple.column_int( 'B_DTF_STATUS_'+suffix,  fitter.status()  )
        tuple.column_int( 'B_DTF_ERRCODE_'+suffix, fitter.errCode() )
        tuple.column_int( 'B_DTF_NITER_'+suffix,   fitter.nIter()   )

        if fitter.status() != 0 :
            tuple.column_int(    'B_ENDVERTEX_NDOF_'+suffix, -1   )
            tuple.column_double( 'B_ENDVERTEX_CHI2_'+suffix, -1.1 )
            tuple.column_double( 'B_ENDVERTEX_PROB_'+suffix, -1.1 )
            tuple.column_double( 'B_M_'+suffix,              -1.1 )
            if 'PV' in suffix :
                tuple.column_double( 'B_CTAU_'+suffix,    -1.1 )
                tuple.column_double( 'B_CTAUERR_'+suffix, -1.1 )
                tuple.column_double( 'B_CTAUSIG_'+suffix, -1.1 )

        else :
            chi2 = fitter.chiSquare()
            ndof = fitter.nDof()
            prob = TMath.Prob( chi2, ndof )

            tuple.column_int(    'B_ENDVERTEX_NDOF_'+suffix, ndof )
            tuple.column_double( 'B_ENDVERTEX_CHI2_'+suffix, chi2 )
            tuple.column_double( 'B_ENDVERTEX_PROB_'+suffix, prob )

            bcand = fitter.particle()
            bparams = fitter.fitParams( bcand )

            bp4 = bparams.momentum()
            tuple.column_double( 'B_M_'+suffix,    bp4.m().value() )
            tuple.column_double( 'B_MERR_'+suffix, bp4.m().error() )

            if 'PV' in suffix :
                bctau = bparams.ctau()
                tuple.column_double( 'B_CTAU_'+suffix,    bctau.value() )
                tuple.column_double( 'B_CTAUERR_'+suffix, bctau.error() )
                tuple.column_double( 'B_CTAUSIG_'+suffix, bctau.value()/bctau.error() )


    def extra_B_vars_tuple( self, tuple, bcand ) :
        """
        Store the value of the B mass and lifetime (where appropriate)
        resulting from fits with different sets of constraints:
        - V0 mass constraint
        - PV constraint
        - V0 mass and PV constraints
        Also store the vertex chi2, ndof and probability of the fit.
        """

        # TupleToolPropertime method
        bestPV = self.bestVertex( bcand )
        tau = Double(0.0)
        tau_err = Double(0.0)
        tau_chisq = Double(0.0)
        self.lifetimetool.fit( bestPV, bcand, tau, tau_err, tau_chisq )
        tau *= TMath.C()/1e6
        tau_err *= TMath.C()/1e6
        tuple.column_double( 'B_TAU_TT',    tau     )
        tuple.column_double( 'B_TAUERR_TT', tau_err )

        # lc mass constraint
        fitterlc = cpp.DecayTreeFitter.Fitter( bcand, self.stateprovider )
        for daug in bcand.daughters():
            daug_pid = daug.particleID()
            if daug_pid == self.daug3ID or daug_pid == self.daug3ConjID :
                fitterlc.setMassConstraint( daug )
        fitterlc.fit()

        self.store_DTF_fit_results( tuple, fitterlc, self.lcname )

        # PV constraint
        fitterPV = cpp.DecayTreeFitter.Fitter( bcand, bestPV, self.stateprovider )
        fitterPV.fit()

        self.store_DTF_fit_results( tuple, fitterPV, 'PV' )

        # V0 mass and PV constraints
        fitterlcPV = cpp.DecayTreeFitter.Fitter( bcand, bestPV, self.stateprovider )
        for daug in bcand.daughters():
            daug_pid = daug.particleID()
            if daug_pid == self.daug3ID or daug_pid == self.daug3ConjID :
                fitterlcPV.setMassConstraint( daug )
        fitterlcPV.fit()

        self.store_DTF_fit_results( tuple, fitterlcPV, 'PV'+self.lcname )


    def bkg_category_tuple( self, tuple, cand, name ) :
        """
        Reproducce information stored by TupleToolMCBackgroundInfo
        """

        category = -1
        for tool in self.bkgtools :
            category = tool.category( cand )
            if category > -1 and category < 1001 :
                break

        if category > 1000 :
            category = -1

        tuple.column_int( name + '_BKGCAT', category )


    def vtx_isolation_tuple( self, tuple, bcand ) :
        """
        Reproduce information stored by TupleToolVtxIsoln
        """

        # 20/04/2015 - updated to access information stored by Stripping21 only
        #            - uses RELINFO functor to retrieve the stored information
        #            - have cone variables for deltaR = 1.0, 1.5 and 1.7
        #            - have vertex isolation information

        # get the TES location of our input particles and remove the trailing 'Particles'
        teslocation = self.RootInTES + self.Inputs[0] + '/'

        angles_location_names = [ 'P2ConeVar1', 'P2ConeVar2', 'P2ConeVar3' ]
        angles_variable_names = [ 'CONEANGLE', 'CONEMULT', 'CONEPTASYM' ]
        angles = []

        for name in angles_location_names :
            angle  = '%.1f' % RELINFO( teslocation+name, 'CONEANGLE',  -1.1 )(bcand)
            mult   = int(RELINFO( teslocation+name, 'CONEMULT',   -1.1 )(bcand))
            ptasym = RELINFO( teslocation+name, 'CONEPTASYM', -1.1 )(bcand)
            tuple.column_int( 'B_STRIP_CONEMULT_'+angle.replace('.','_'), mult )
            tuple.column_double( 'B_STRIP_PTASYM_'+angle.replace('.','_'), ptasym )

        #vtxiso_location_name = 'VtxIsolationVar'
        #vtxiso_variable_names = [ 'VTXISONUMVTX', 'VTXISODCHI2ONETRACK', 'VTXISODCHI2MASSONETRACK', 'VTXISODCHI2TWOTRACK', 'VTXISODCHI2MASSTWOTRACK' ]

        #for name in vtxiso_variable_names :
        #    value = RELINFO( teslocation+vtxiso_location_name, name, -1.1 )(bcand)
        #    tuple.column_double( 'B_STRIP_'+name, value )


    def mc_match_signal( self, tuple, mcB, mch1, mch2, mclc, h1cand, h2cand, lccand ) :
        """
        Do the MC matching for signal MC
        """

        h1name = 'h1'
        h2name = 'h2'
        lcname = self.lcname

        mcmatchh1 = 0
        mcmatchh2 = 0
        mcmatchlc = 0

        if not mcB.empty() :
            mcMatcher = self.mcTruth()

            h1FromMC = MCTRUTH( mcMatcher, mch1 )
            h2FromMC = MCTRUTH( mcMatcher, mch2 )
            lcFromMC = MCTRUTH( mcMatcher, mclc )

            if h1FromMC(h1cand) :
                mcmatchh1 = 1
            if h2FromMC(h2cand) :
                mcmatchh2 = 1
            if lcFromMC(lccand) :
                mcmatchlc = 1

            if (mcmatchh1 == 0 or mcmatchh2 == 0) and self.daug1ID==self.daug2ID :

                mymch1 = mch1
                mymch2 = mch2

                mymch2 = self.mcselect('mymch2', self.mc_decay_descriptor_h1)
                mymch1 = self.mcselect('mymch1', self.mc_decay_descriptor_h2)

                if not mymch1 == 0 or not mymch2 == 0 :
                    myh1FromMC = MCTRUTH( mcMatcher, mymch1 )
                    myh2FromMC = MCTRUTH( mcMatcher, mymch2 )

                    if myh1FromMC(h1cand) :
                        mcmatchh1 = 2
                    if myh2FromMC(h2cand) :
                        mcmatchh2 = 2
                
        tuple.column_int( h1name+'_SIG_mcMatch', mcmatchh1 )
        tuple.column_int( h2name+'_SIG_mcMatch', mcmatchh2 )
        tuple.column_int( lcname+'_SIG_mcMatch', mcmatchlc )

        mcmatched = 0
        if mcmatchh1 == 1 and mcmatchh2 == 1 and mcmatchlc == 1 :
            mcmatched = 1
        elif (mcmatchh1 == 2 or mcmatchh2 == 2) and mcmatchlc == 1 :
            mcmatched = 2

        tuple.column_int( 'B_SIG_mcMatch', mcmatched )

        # store the MC truth info about the B and its daughters
        self.mc_p4_tuple( tuple, mcB, mcmatched, 'B', True )
        self.mc_vtx_tuple( tuple, mcB, mcmatched, 'B', True )
        self.mc_p4_tuple( tuple, mch1, mcmatchh1, h1name, True )
        self.mc_p4_tuple( tuple, mch2, mcmatchh2, h2name, True )
        self.mc_p4_tuple( tuple, mclc, mcmatchlc, lcname, True )
        self.mc_vtx_tuple( tuple, mclc, mcmatchlc, lcname, True )


    def mc_match_general( self, tuple, h1cand, h2cand, lccand ) :
        """
        Do the generialised MC matching
        """

        h1name = 'h1'
        h2name = 'h2'
        lcname = self.lcname

        mcMatcher = self.mcTruth()
        h1fun = RCTRUTH( h1cand, mcMatcher )
        h2fun = RCTRUTH( h2cand, mcMatcher )
        lcfun = RCTRUTH( lccand, mcMatcher )

        mch1 = self.mcselect( 'h1matches', h1fun & ( ('pi+' == MCABSID) | ('K+' == MCABSID) | ('p+' == MCABSID) | ('mu+' == MCABSID) | ('e+' == MCABSID) ) )
        mch2 = self.mcselect( 'h2matches', h2fun & ( ('pi+' == MCABSID) | ('K+' == MCABSID) | ('p+' == MCABSID) | ('mu+' == MCABSID) | ('e+' == MCABSID) ) )
        mclc = self.mcselect( 'lcmatches', lcfun & ( ('Lambda_c+' == MCABSID) | ('pi+' == MCABSID) | ('K+' == MCABSID) | ('p+' == MCABSID) | ('mu+' == MCABSID) | ('e+' == MCABSID) ) )

        mcmatchh1 = 0
        mcmatchh2 = 0
        mcmatchlc = 0
        if len(mch1) > 0 :
            mcmatchh1 = 1
        if len(mch2) > 0 :
            mcmatchh2 = 1
        if len(mclc) > 0 :
            mcmatchlc = 1
        tuple.column_int( h1name+'_mcMatch', mcmatchh1 )
        tuple.column_int( h2name+'_mcMatch', mcmatchh2 )
        tuple.column_int( lcname+'_mcMatch', mcmatchlc )

        # store the MC truth info about the reconstructed daughters
        self.mc_p4_tuple( tuple, mch1, mcmatchh1, h1name, False )
        self.mc_p4_tuple( tuple, mch2, mcmatchh2, h2name, False )
        self.mc_p4_tuple( tuple, mclc, mcmatchlc, lcname, False )
        self.mc_vtx_tuple( tuple, mclc, mcmatchlc, lcname, False )


    def unique_cands( self, cands ) :
        """
        Find unique candidates in a list
        """

        unique_cands = []
        evt_keys = []
        for cand in cands :

            cand_keys = []
            for daug in cand.children() :
                daug_pid = daug.particleID()
                if daug_pid == self.daug3ID or daug_pid == self.daug3ConjID :
                    for gdaug in daug.children() :
                        cand_keys.append( -gdaug.key() )
                else :
                    cand_keys.append( daug.key() )

            cand_keys.sort()

            if cand_keys in evt_keys :
                continue
            else :
                unique_cands.append( cand )
                evt_keys.append( cand_keys )

        return unique_cands


    def get_ntracks( self ) :
        """
        Extracts the number of Best and Long tracks in the event
        """

        nbest = 0
        nlong = 0
        nspdhits = 0
        nrich1hits = 0
        nrich2hits = 0

        try :
            summary = self.get('/Event/Rec/Summary')
            nbest = summary.info(summary.nTracks,0)
            nlong = summary.info(summary.nLongTracks,0)
            nspdhits = summary.info(summary.nSPDhits,0)
            nrich1hits = summary.info(summary.nRich1Hits,0)
            nrich2hits = summary.info(summary.nRich2Hits,0)
        except :
            try :
                tracks = self.get( 'Rec/Track/Best' )
                nlong = 0
                nbest = len(tracks)
                for trk in tracks :
                    if 3 == trk.type() :
                        nlong += 1                         
            except :
                self.Error( 'Information about number of tracks not found neither in /Event/Rec/Summary nor in Rec/Track/Best', SUCCESS )

        return nbest, nlong, nspdhits, nrich1hits, nrich2hits


    def find_daughters_wrongsign( self, bcand ) :
        """
        Identify the 3 daughters of the given parent (opposite-sign hh')
        """

        h1cand = None
        h2cand = None
        lccand = None

        for daug in bcand.children() :
            daug_pid = daug.particleID()

            if daug_pid == self.daug3ID or daug_pid == self.daug3ConjID :
                lccand = daug
            elif daug.charge() > 0 :
                h1cand = daug
            else :
                h2cand = daug

        return h1cand, h2cand, lccand


    def find_daughters( self, bcand ) :
        """
        Identify the 3 daughters of the given parent (same-sign hh')
        """

        h1cand = None
        h2cand = None
        lccand = None

        gotFirstCand = False
        for daug in bcand.children() :
            daug_pid = daug.particleID()

            if self.lcname == 'Lc' :
                if daug_pid == self.daug3ID or daug_pid == self.daug3ConjID :
                    lccand = daug
                elif not gotFirstCand :
                    h1cand = daug
                    gotFirstCand = True
                else :
                    h2cand = daug

            if self.lcname == 'Dp' :
                if daug_pid == self.daug3ID or daug_pid == self.daug3ConjID :
                    lccand = daug
                else :     
                    for gdaug in daug.children() :
                        if not gotFirstCand :
                            h1cand = gdaug
                            gotFirstCand = True
                        else :
                            h2cand = gdaug

        return h1cand, h2cand, lccand

    def angvariables( self, XibP4, h1P4, h2P4, pP4 ) :
        """
        Calculate and returns the angular variables related to the decay
        """

        beam = TVector3(0, 0, 1).Unit()

        XibP3 = XibP4.Vect()
        h1P3 = h1P4.Vect()
        h2P3 = h2P4.Vect()
        pP3 = pP4.Vect()
        #for boosting into Xib frame
        XibRest = XibP4.BoostVector()

        x = XibP3.Unit()
        #z = beam.Cross(x).Unit()
        z = beam.Cross(x)
        z = z.Unit()
        y = z.Cross(x).Unit() #-beam
        #y = TVector3(0, 0, -1).Unit()

        # Boost into Xib rest frame
        pP4.Boost(-XibRest)
        h1P4.Boost(-XibRest)
        h2P4.Boost(-XibRest)

        rot = TRotation().RotateAxes(x, y, z)

        rot4 = TLorentzRotation(rot)
        rot4.Invert()

        XibP4 = rot4 * XibP4
        pP4 = rot4 * pP4
        h1P4 = rot4 * h1P4
        h2P4 = rot4 * h2P4

        # 4-vectors are now in the correct reference frame (I hope)
        cThetap = pP4.Vect().CosTheta()
        phip = pP4.Vect().Phi()

        hhNormal = h1P4.Vect().Cross(h2P4.Vect()).Unit()
        lPolNormal = pP4.Vect().Cross(z).Unit()

        # Make it signed
        if lPolNormal.Cross(hhNormal).Z() > 0:
            phihh = hhNormal.Angle(lPolNormal)
        else:
            phihh = -hhNormal.Angle(lPolNormal)

        return cThetap, phip, phihh


    def calc_randno( self, runNum, evtNum ) :
        """
        Calculate the "random" numbers used to identify events for the BDT training
        """

        myRandom1 = (( 134*evtNum + runNum ) % 531241)/531241.0
        myRandom2 = (( 134*runNum + evtNum ) % 531241)/531241.0

        return myRandom1, myRandom2


    def analyse( self ) :
        """
        The method called in the event loop
        """

        # select the candidates from the input
        cands = self.select( 'candidates', self.decay_descriptor )
        #cands = self.unique_cands( all_cands )
        nCands = len(cands)

        if 0 == nCands :
            return SUCCESS

        # select MC particles
        if self.signalmc :
            mcB  = self.mcselect('mcB',  self.mc_decay_descriptor_Xb)
            mch1 = self.mcselect('mch1', self.mc_decay_descriptor_h1)
            mch2 = self.mcselect('mch2', self.mc_decay_descriptor_h2)
            mclc = self.mcselect('mclc', self.mc_decay_descriptor_lc)

        # get the event header
        evthdr = self.get( '/Event/Rec/Header' )
        runNum = evthdr.runNumber()
        evtNum = evthdr.evtNumber()
        myRdm1, myRdm2 = self.calc_randno( runNum, evtNum )

        # get the track info
        nbest, nlong, nspdhits, nrich1hits, nrich2hits = self.get_ntracks()

        # loop through the candidates
        iCand = 0

        for bcand in cands :

            # check that the candidate has an associated PV
            bestPV = self.bestVertex( bcand )
            if not bestPV :
                self.Warning( 'Candidate %d in event %d has no associated PV!' % (iCand,evtNum), SUCCESS )
                continue

            # calculate all the DP information (common to all ntuples)
            self.calc_DP_info( bcand )

            # get the daughters of this candidate
            if not self.wrongsign :
                h1cand, h2cand, lccand = self.find_daughters( bcand )
            else :
                h1cand, h2cand, lccand = self.find_daughters_wrongsign( bcand )

            # apply the cut on the z-separation of the V0 and B vtx
            bvtx = bcand.endVertex()
            lcvtx = lccand.endVertex()
            vtx_z_sep = lcvtx.position().z() - bvtx.position().z()
            if vtx_z_sep < 0 :
                continue

            # get the right ntuple
            tuple_name = 'Xib2%s%s%s' % ( self.simplenames[self.daug1ID.pid()], self.simplenames[self.daug2ID.pid()], self.lcname )
            tuple = self.nTuple( tuple_name )

            # store the event level info
            tuple.column_int( 'runNumber',  runNum     )
            tuple.column_int( 'evtNumber',  evtNum     )
            tuple.column_int( 'nCands',     nCands     )
            tuple.column_int( 'iCand',      iCand      )
            tuple.column_int( 'BestTracks', nbest      )
            tuple.column_int( 'LongTracks', nlong      )
            tuple.column_int( 'SpdHits',    nspdhits   )
            tuple.column_int( 'Rich1Hits',  nrich1hits )
            tuple.column_int( 'Rich2Hits',  nrich2hits )

            tuple.column_double( 'myRandom1', myRdm1 )
            tuple.column_double( 'myRandom2', myRdm2 )

            # store info for the B
            self.p4_tuple( tuple, bcand, 'B' )
            self.ip_tuple( tuple, bcand, 'B' )
            self.extra_B_vars_tuple( tuple, bcand )

            # store trigger info
            self.trig_tuple( tuple, bcand )

            # store vertex info for the B and KS
            self.vtx_tuple( tuple, bcand, lccand )

            # store vertex isolation info for B
            if not self.wrongsign :
                self.vtx_isolation_tuple( tuple, bcand )

            if self.simulation :

                if self.signalmc :
                    # store the MC-truth DP info
                    self.mc_dp_tuple( tuple, mcB )

                    # do the MC matching to the signal MC decay
                    self.mc_match_signal( tuple, mcB, mch1, mch2, mclc, h1cand, h2cand, lccand )

                # do the general MC matching
                self.mc_match_general( tuple, h1cand, h2cand, lccand )

                # store the MC background category info
                self.bkg_category_tuple( tuple, bcand, 'B'  )
                self.bkg_category_tuple( tuple, lccand, self.lcname )


            # store information on the daughters
            self.p4_tuple( tuple, h1cand, 'h1' )
            self.ip_tuple( tuple, h1cand, 'h1' )
            self.trk_tuple( tuple, h1cand, 'h1' )

            self.p4_tuple( tuple, h2cand, 'h2' )
            self.ip_tuple( tuple, h2cand, 'h2' )
            self.trk_tuple( tuple, h2cand, 'h2' )

            self.p4_tuple( tuple, lccand, self.lcname )
            self.ip_tuple( tuple, lccand, self.lcname )

            # and the lc daughters
            if self.lcname == 'Lc' :
                for gdaug in lccand.children() :
                    gdaugID = gdaug.particleID().pid()
                    gdaugname = self.gdaugnames[ gdaugID ]
                    self.p4_tuple( tuple, gdaug, gdaugname )
                    self.trk_tuple( tuple, gdaug, gdaugname )

            # Dp different as we have identical pi daughters
            if self.lcname == 'Dp' :
                gotFirstCand = False
                for gdaug in lccand.children() :
                    gdaugID = gdaug.particleID().pid()
                    if gdaugID == 321 or gdaugID == -321 :
                        gdaugname = self.gdaugnames[ gdaugID ]
                        self.p4_tuple( tuple, gdaug, gdaugname )
                        self.trk_tuple( tuple, gdaug, gdaugname )

                    elif not gotFirstCand :
                        self.p4_tuple( tuple, gdaug, 'Dpi1' )
                        self.trk_tuple( tuple, gdaug, 'Dpi1' )
                        gotFirstCand = True

                    else :    
                        self.p4_tuple( tuple, gdaug, 'Dpi2' )
                        self.trk_tuple( tuple, gdaug, 'Dpi2' )


            # store all the pre-calculated DP information
            self.store_DP_info( tuple )

            # fill the ntuple
            tuple.write()

            iCand += 1

        return SUCCESS

# End of XibReco Class Definition


# Begin DalitzInfo Class Definition

class DalitzInfo(object) :

    """
    Helper class to store Dalitz-plot fit information and fill a tuple
    """

    def __init__( self, lcname, suffix ) :
        """
        Constructor
        """

        self.lcname = lcname
        self.suffix = suffix
        self.init_values()


    def init_values( self ) :
        """
        Set default values for all variables
        """

        self.status  = -1
        self.errcode = -1
        self.niter   = -1

        self.ndof = -1
        self.chi2 = -1.1
        self.prob = -1.1

        self.b_ctau    = -1.1
        self.b_ctauerr = -1.1
        self.b_ctausig = -1.1

        self.b_p4  = None
        self.daug_p4 = {}
        self.daug_p4['h1'] = None
        self.daug_p4['h2'] = None
        self.daug_p4[self.lcname] = None

        self.m12Sq = -1.1
        self.m13Sq = -1.1
        self.m23Sq = -1.1

        self.mPrime  = -1.1
        self.thPrime = -1.1

        self.cThetap = -1.1
        self.phip = -1.1
        self.phihh = -1.1

    def store_fit_results( self, fitter, bctau, bp4, daug_p4, m12Sq, m13Sq, m23Sq, mPrime, thPrime, cThetap, phip, phihh ) :
        """
        Extract the fit information from the fitter and store it
        """

        self.init_values()

        self.status  = fitter.status()
        self.errcode = fitter.errCode()
        self.niter   = fitter.nIter()

        if self.status != 0 :
            return

        self.chi2 = fitter.chiSquare()
        self.ndof = fitter.nDof()
        self.prob = TMath.Prob( self.chi2, self.ndof )

        self.b_ctau    = bctau.value()
        self.b_ctauerr = bctau.error()
        self.b_ctausig = self.b_ctau/self.b_ctauerr

        self.b_p4 = TLorentzVector(bp4)

        self.daug_p4['h1'] = TLorentzVector(daug_p4[0])
        self.daug_p4['h2'] = TLorentzVector(daug_p4[1])
        self.daug_p4[self.lcname] = TLorentzVector(daug_p4[2])

        self.m12Sq = m12Sq
        self.m13Sq = m13Sq
        self.m23Sq = m23Sq

        self.mPrime  = mPrime
        self.thPrime = thPrime

        self.cThetap = cThetap
        self.phip = phip
        self.phihh = phihh


    def fill_tuple( self, tuple ) :
        """
        Fill the supplied ntuple with the information
        """

        # store info on the success (or otherwise) of the mass constrained fit
        tuple.column_int( 'B_DTF_STATUS_'+self.suffix,  self.status  )
        tuple.column_int( 'B_DTF_ERRCODE_'+self.suffix, self.errcode )
        tuple.column_int( 'B_DTF_NITER_'+self.suffix,   self.niter   )

        # also store the fit chisq, ndof and associated probability
        tuple.column_int(    'B_ENDVERTEX_NDOF_'+self.suffix, self.ndof )
        tuple.column_double( 'B_ENDVERTEX_CHI2_'+self.suffix, self.chi2 )
        tuple.column_double( 'B_ENDVERTEX_PROB_'+self.suffix, self.prob )

        # store information on the B candidate
        tuple.column_double( 'B_CTAU_'+self.suffix,    self.b_ctau    )
        tuple.column_double( 'B_CTAUERR_'+self.suffix, self.b_ctauerr )
        tuple.column_double( 'B_CTAUSIG_'+self.suffix, self.b_ctausig )

        if self.b_p4 :
            tuple.column_double( 'B_M_'+self.suffix,     self.b_p4.M() )
            #tuple.column_double( 'B_MERR_'+self.suffix,  self.b_p4.m().error()  )
            tuple.column_double( 'B_PX_'+self.suffix,    self.b_p4.Px()         )
            tuple.column_double( 'B_PY_'+self.suffix,    self.b_p4.Py()         )
            tuple.column_double( 'B_PZ_'+self.suffix,    self.b_p4.Pz()         )
            tuple.column_double( 'B_PE_'+self.suffix,    self.b_p4.E()          )
        else :
            tuple.column_double( 'B_M_'+self.suffix,     -1.1 )
            tuple.column_double( 'B_MERR_'+self.suffix,  -1.1 )
            tuple.column_double( 'B_PX_'+self.suffix,    -1.1 )
            tuple.column_double( 'B_PY_'+self.suffix,    -1.1 )
            tuple.column_double( 'B_PZ_'+self.suffix,    -1.1 )
            tuple.column_double( 'B_PE_'+self.suffix,    -1.1 )

        # loop through 3 B daughters and store their 4-momenta
        for daugname in self.daug_p4.keys() :
            p4 = self.daug_p4[ daugname ]
            if p4 :
                tuple.column_double( daugname+'_M_'+self.suffix,  p4.M()  )
                tuple.column_double( daugname+'_PX_'+self.suffix, p4.Px() )
                tuple.column_double( daugname+'_PY_'+self.suffix, p4.Py() )
                tuple.column_double( daugname+'_PZ_'+self.suffix, p4.Pz() )
                tuple.column_double( daugname+'_PE_'+self.suffix, p4.E()  )
            else :
                tuple.column_double( daugname+'_M_'+self.suffix,  -1.1 )
                tuple.column_double( daugname+'_PX_'+self.suffix, -1.1 )
                tuple.column_double( daugname+'_PY_'+self.suffix, -1.1 )
                tuple.column_double( daugname+'_PZ_'+self.suffix, -1.1 )
                tuple.column_double( daugname+'_PE_'+self.suffix, -1.1 )

        # store the DP co-ordinates
        tuple.column_double( 'm12Sq_'+self.suffix, self.m12Sq )
        tuple.column_double( 'm13Sq_'+self.suffix, self.m13Sq )
        tuple.column_double( 'm23Sq_'+self.suffix, self.m23Sq )

        # store the square DP co-ordinates
        tuple.column_double( 'mPrime_'+self.suffix,  self.mPrime  )
        tuple.column_double( 'thPrime_'+self.suffix, self.thPrime )

        #store the angular variables
        tuple.column( 'cThetap'+self.suffix,  self.cThetap )
        tuple.column( 'phip'+self.suffix,  self.phip )
        tuple.column( 'phihh'+self.suffix,  self.phihh )

# End DalitzInfo Class Definition

