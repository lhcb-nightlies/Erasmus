#!/usr/bin/env python
# =============================================================================
# $Id: StdLooseKs.py,v 1.5 2009-05-26 13:48:13 gcowan Exp $ 
# =============================================================================
## @file  CommonParticles/StdLooseKs.py
#  configuration file for 'Standard Loose ' 
#  @author Patrick Koppenburg
#  @date 2009-02-18
# =============================================================================
"""
Configuration file for 'Standard Loose V0HM2MuMu'
"""
# =============================================================================
__all__ = (
    'StdLooseV0HM2MuMuLL' ,
    'StdLooseV0HM2Things' ,
    'locations'
    )
# =============================================================================
from Gaudi.Configuration import *
from Configurables       import CombineParticles, FilterDesktop
from CommonParticles.Utils import *

## ============================================================================
## create the algorithm 
StdLooseV0HM2MuMuLL = CombineParticles ( 'StdLooseV0HM2MuMuLL' )

StdLooseV0HM2MuMuLL.Inputs = [ "Phys/StdLooseMuons/Particles" ]
StdLooseV0HM2MuMuLL.DecayDescriptor = "KS0 -> mu+ mu-" 

StdLooseV0HM2MuMuLL.DaughtersCuts = { "mu+" : "(PT>2.*GeV)" } 
StdLooseV0HM2MuMuLL.CombinationCut = "(ADOCACHI2CUT(25, '')) "
StdLooseV0HM2MuMuLL.MotherCut = "(VFASPF(VCHI2) < 25.)"

## configure Data-On-Demand service 
locations = updateDoD ( StdLooseV0HM2MuMuLL )


## ============================================================================
#  configuration file for 'Standard Loose Downstream ' 
#  @author Greig Cowan
#  @date 2009-04-27
# ============================================================================= 
StdLooseV0HM2Things = CombineParticles ( 'StdLooseV0HM2Things' )
StdLooseV0HM2Things.Inputs = [ "Phys/StdNoPIDsPions/Particles" ]
StdLooseV0HM2Things.DecayDescriptor = "KS0 -> pi+ pi-" 

StdLooseV0HM2Things.DaughtersCuts = { "pi+" : "(PT>5.*GeV)"} 
StdLooseV0HM2Things.CombinationCut = "(ADOCACHI2CUT(25, ''))"
StdLooseV0HM2Things.MotherCut = "(VFASPF(VCHI2) < 25.)"

## configure Data-On-Demand service 
locations.update( updateDoD ( StdLooseV0HM2Things ))

