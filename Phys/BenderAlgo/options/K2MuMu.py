#!/usr/bin/env python
# =============================================================================
# $Id: StdLooseKs.py,v 1.5 2009-05-26 13:48:13 gcowan Exp $ 
# =============================================================================
## @file  CommonParticles/StdLooseKs.py
#  configuration file for 'Standard Loose ' 
#  @author Patrick Koppenburg
#  @date 2009-02-18
# =============================================================================
"""
Configuration file for 'Standard Loose Ks2MuMu'
"""
# =============================================================================
__all__ = (
    'StdLooseKs2MuMuLL' ,
    'StdLooseKs2MuMuDD' ,
    'StdLooseKs2MuMuLD' ,
    'locations'
    )
# =============================================================================
from Gaudi.Configuration import *
from Configurables       import CombineParticles, FilterDesktop
from CommonParticles.Utils import *

## ============================================================================
## create the algorithm 
StdLooseKs2MuMuLL = CombineParticles ( 'StdLooseKs2MuMuLL' )

StdLooseKs2MuMuLL.Inputs = [ "Phys/StdLooseMuons/Particles" ]
StdLooseKs2MuMuLL.DecayDescriptor = "KS0 -> mu+ mu-" 

StdLooseKs2MuMuLL.DaughtersCuts = { "mu+" : "(P > 2.*GeV) & (MIPCHI2DV(PRIMARY) > 36.)" } 
StdLooseKs2MuMuLL.CombinationCut = "(ADAMASS('KS0') < 50.*MeV) & (ADOCACHI2CUT(25, '')) "
StdLooseKs2MuMuLL.MotherCut = "(ADMASS('KS0') < 35.*MeV) & (VFASPF(VCHI2) < 25.)"

## configure Data-On-Demand service 
locations = updateDoD ( StdLooseKs2MuMuLL )


## ============================================================================
#  configuration file for 'Standard Loose Downstream ' 
#  @author Greig Cowan
#  @date 2009-04-27
# ============================================================================= 
StdLooseKs2MuMuDD = CombineParticles ( 'StdLooseKs2MuMuDD' )
DDPionCuts = "(P > 2.*GeV) & (MIPCHI2DV(PRIMARY) > 36.)"
StdLooseKs2MuMuDD.Inputs = [ "Phys/StdNoPIDsDownMuons/Particles" ]
StdLooseKs2MuMuDD.DecayDescriptor = "KS0 -> mu+ mu-" 

StdLooseKs2MuMuDD.DaughtersCuts = { "mu+" : DDPionCuts } 
StdLooseKs2MuMuDD.CombinationCut = "(ADAMASS('KS0') < 80.*MeV) & (ADOCACHI2CUT(25, ''))"
StdLooseKs2MuMuDD.MotherCut = "(ADMASS('KS0') < 64.*MeV) & (VFASPF(VCHI2) < 25.)"

## configure Data-On-Demand service 
locations.update( updateDoD ( StdLooseKs2MuMuDD ))

