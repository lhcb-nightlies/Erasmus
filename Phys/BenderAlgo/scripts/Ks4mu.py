#! /usr/bin/env python
from ROOT import *
import sys, os
from Gaudi.Configuration import NTupleSvc
from Configurables import GaudiSequencer, DaVinci, L0Conf, SelDSTWriter, FilterDesktop

bsmumuCounterKeys = ["MuMuCouplesAnalyzed","EVT", "weird"]

################################### Some General Options. Remember to check it

INTERACTIVE = 0

MC_INFO = 1
DST = 0
TUP = 1
TRIGGER = 0
REDOTRIGGER = 0
VERSION = "KW"
DOLUMI = False
DebugAlgos = 0

N = -1

Njobmax = 1.e10  ### info only, not real limit. Used to set an offset in the evt no. label

SIMULATION =  1

DataType = "2012"
print DataType, SIMULATION*"MONTE CARLO"

#########################
### ALGORITHMS TO ADD ###
#########################

__KsMuMuLL = 0
__KsMuMuDD = 0
__K24MuLL = 1
__K24MuDD = 1

### datacards
#datacards = os.environ["HOME"] + "/KW/Ksmm_TC.py"
#datacards = os.environ["HOME"] + "/KW/Ksmm_08c.py"
#datacards = os.environ["HOME"] + "/KW/Kpsipi_misid.py"
datacards = os.environ["HOME"] + "/KW/Ksmmmm.py"


###########################
### FOR BATCH MODE ONLY ###
###########################

string = ""
args = sys.argv
for arg in args[1:]:
    if arg in ["-b","-b-"]: continue
    string += "_" + arg

N0 = string.replace("_","")
N0 = int(max(N0,"0"))


####################################
### Names for OUTPUT NTuple, Dst ###
####################################
main_name = bool(__K24MuDD + __K24MuLL)*"K24Mu" + bool(__KsMuMuDD + __KsMuMuLL)*"Ksmm"
 
TUPLE_FILE = "~/vol5/KW/" + main_name + VERSION + SIMULATION*"MC" + string+".root"
HISTO_FILE = "~/vol5/KW/" + main_name + VERSION + SIMULATION*"MC" + "_H_"+ string+".root"

#HISTo_FILE = TUPLE_FILE

if TUP:
    NTupleSvc( Output =["T DATAFILE='"+ TUPLE_FILE + "' TYP='ROOT' OPT='NEW'"] )

from BenderAlgo.BenderV0 import *
from BenderAlgo.BenderB2Xprongs import *
from BenderAlgo.extrafunctions import *
from ROOT import *

import GaudiPython

import Gaudi
from Gaudi.Configuration import *
    
def configure():
   
    DaVinci().EvtMax = 0    
    DaVinci().DataType = DataType 
    DaVinci().Simulation = SIMULATION
    #DaVinci().L0 = bool(TRIGGER)

    DaVinci().UserAlgorithms = []
    #DaVinci().Hlt = bool(TRIGGER)
    DaVinci().HistogramFile = HISTO_FILE
    DaVinci().Lumi = DOLUMI
    importOptions(os.environ["HOME"] + "/KW/StdDownMuons.py")
    if (__KsMuMuLL or __KsMuMuDD) : importOptions(os.environ["HOME"] + "/KW/K2MuMu.py")
    if (__K24MuLL or __K24MuDD) : importOptions(os.environ["HOME"] + "/KW/K24Mu.py")
                                                  
    importOptions(datacards)  ### Be sure you DO NOT import any other options file after this one. You can overwrite eventselector input.

    ################## END IMPORTING OPTIONS
    DaVinci().applyConf()   
    gaudi = appMgr()
        
    algos = []
    
    if __KsMuMuLL:
        algLL = B2QQ('Ks2MuMuLL')
        #if SIMULATION: alg.LookIn = "AllStreams/Phys/Bs2MuMuWideMassLine"
        #else: alg.LookIn = "Dimuon/Phys/Bs2MuMuLinesWideMassLine" #"Dimuon/Phys/Bs2MuMuWideMassLine"
        algLL.LookIn =  "Phys/StdLooseKs2MuMuLL"
        algLL.decayname = "Ks-->mumu (LL)"
        algLL.COUNTER = {}
        algLL.COUNTER["Bender(evts) " + algLL.decayname] = 0
    
        algLL.Sel_Above_GL = 0
        algLL.extraFunctions = [DTF_M]#trackHits]#, subdetectorDLLs, geoVarOld, more_muon_things, addSomeGLs, globalEvtVars, CDF_iso_and_Consk, addTMVAs,addBDTS,addBDTS_cx]

        if SIMULATION*MC_INFO: algLL.extraFunctions += [BQQMCtruth, mc_geometry]
        for key in bsmumuCounterKeys:
            algLL.COUNTER[key] = 0
        algos.append(algLL)
        gaudi.addAlgorithm(algLL)
      
        algLL.InputLocations = ['/Event/Phys/StdLooseMuons' ]   ### Need to be LooseMuons. This is used only for J/Psi veto studies and has nothing to do with what is the Bs2mumu candidate made of 

    if __KsMuMuDD:
        algDD = B2QQ('Ks2MuMuDD')
        #if SIMULATION: alg.LookIn = "AllStreams/Phys/Bs2MuMuWideMassLine"
        #else: alg.LookIn = "Dimuon/Phys/Bs2MuMuLinesWideMassLine" #"Dimuon/Phys/Bs2MuMuWideMassLine"
        algDD.LookIn = "Phys/StdLooseKs2MuMuDD"
        algDD.decayname = "Ks-->mumu (DD)"
        algDD.COUNTER = {}
        algDD.COUNTER["Bender(evts) " + algDD.decayname] = 0
    
        algDD.Sel_Above_GL = 0
        algDD.extraFunctions = [DTF_M]#trackHits]#, subdetectorDDDs, geoVarOld, more_muon_things, addSomeGLs, globalEvtVars, CDF_iso_and_Consk, addTMVAs,addBDTS,addBDTS_cx]

        if SIMULATION*MC_INFO: algDD.extraFunctions += [BQQMCtruth, mc_geometry]
        for key in bsmumuCounterKeys:
            algDD.COUNTER[key] = 0
        algos.append(algDD)
        gaudi.addAlgorithm(algDD)
      
        algDD.InputLocations = ['/Event/Phys/StdLooseMuons' ]   ### Need to be LooseMuons. This is used only for J/Psi veto studies and has nothing to do with what is the Bs2mumu candidate made of 
    
    if __K24MuLL:
        bs1LL = B2Xprong("K24muL")
        bs1LL.Nprong = 4   ### number of bodies in the final state!
        bs1LL.COUNTER= {"EVT":0,"No Online Tracks":0, "No Linked Tracks":0, "Sel":0,"CandidatesAnalyzed":0}
        bs1LL.LookIn =    "Phys/StdLooseKs24MuLL"### Container in the TES where to find the candidates !
        bs1LL.extraFunctions = []#globalEvtVars]
        algos.append(bs1LL)
        gaudi.addAlgorithm(bs1LL)
        bs1LL.decayname = "K --> 4mu (LLLL)"

    if __K24MuDD:
        bs1DD = B2Xprong("K24muD")
        bs1DD.Nprong = 4   ### number of bodies in the final state!
        bs1DD.COUNTER= {"EVT":0,"No Online Tracks":0, "No Linked Tracks":0, "Sel":0,"CandidatesAnalyzed":0}
        bs1DD.LookIn = "Phys/StdLooseKs24MuDD"   ### Container in the TES where to find the candidates !
        bs1DD.extraFunctions = []#globalEvtVars]
        algos.append(bs1DD)
        gaudi.addAlgorithm(bs1DD)
        bs1DD.decayname = "K --> 4mu (DDDD)"

    gaudi.initialize()
    from BenderAlgo import PIDcalTools
 
    ########################
    ##COMMON ATRIBUTES ####
    ######################
   
    for algo in algos:
        algo.MC_INFO = SIMULATION*MC_INFO
        algo.TRIGGER = TRIGGER
        algo.PIDcalTools = PIDcalTools
        algo.NTupleLUN = "T"
        algo.addedKeys = []
        #algo.runinfo = runinfo
        algo.DST = DST
        algo.TUP = TUP
        algo.COUNTER['negSq'] = 0
        algo.COUNTER["weird"] = 0
        algo.DEBUG = DebugAlgos ### Do not define global DEBUG
       
        algo.evt_of = Njobmax*N0

        algo.l0BankDecoder=algo.tool(cpp.IL0DUFromRawTool,'L0DUFromRawTool')
        algo.rawBankDecoder=algo.tool(cpp.IOTRawBankDecoder,'OTRawBankDecoder')

        algo.PVRefitter = algo.tool(cpp.IPVReFitter,"AdaptivePVReFitter")
        algo.LifeFitter = algo.tool(cpp.ILifetimeFitter,"PropertimeFitter")
        algo.Geom =  algo.tool(cpp.IDistanceCalculator,"LoKi::DistanceCalculator")

        if TRIGGER:
            algo.tistostool = algo.tool( cpp.ITriggerTisTos , 'TriggerTisTos')
            algo.tisTosToolL0= algo.tool(cpp.ITriggerTisTos , 'L0TriggerTisTos')
            algo.extraFunctions.append(triggerBlock)
            
            import Bs2MuMu.triggerDecisionLists as trigList
            algo.l0List = trigList.l0List_2011
            algo.hlt1List = trigList.hlt1List_2011
            algo.hlt2List = trigList.hlt2List_2011
            
configure()

gaudi = appMgr()

if INTERACTIVE:
    for i in range(INTERACTIVE):
        gaudi.run(1)
        TES = gaudi.evtsvc()
       
else:
        
    gaudi.run(N)
    gaudi.stop()
    #if DST: gaudi.service( 'PoolRootKeyEvtCnvSvc').finalize() # This line prevents from loosing your output dst if something doesn't know how to finalize...
    gaudi.finalize()
#
#len(TES["Phys/B2HHFilterSel/Particles"])
