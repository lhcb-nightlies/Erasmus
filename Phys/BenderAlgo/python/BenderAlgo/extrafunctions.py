###
# Diego Martinez Santos Feb 25 2009
###
from ROOT import *
from ROOT import Double
import climbing 
from muidDebug import *
from copy import *
from SomeUtils.alyabar import *
from Bender.MainMC import *  
import os
#import GaudiPython
cnv = 1./(2.9979e-01)
munames=["mu1","mu2"]
routing_bit_46 = 'Hlt1(?!ODIN)(?!L0)(?!Lumi)(?!Tell1)(?!MB)(?!NZS)(?!Velo)(?!BeamGas)(?!Incident).*Decision'
routing_bit_77 = 'Hlt2(?!Forward)(?!DebugEvent)(?!Express)(?!Transparent)(?!PassThrough).*Decision'
GLs = {}

def Giacomo(muon, mid):
    out = {}
    proto = muon.proto()
    pTrack = proto.track()
    pid = mid.getMuonID(pTrack)
    mTrack = pid.muonTrack()
    chi2cag = 150.
    richPID = proto.richPID();
    if mTrack:
        if mTrack.nDoF(): chi2cag = min(chi2cag, mTrack.chi2()*1./mTrack.nDoF())
    out["chi2cag"] = chi2cag
    out["TrChi2DoF"] = pTrack.chi2()*1./pTrack.nDoF()
    out["MatchChi2"] = pTrack.info( cpp.LHCb.Track.FitMatchChi2, -1 )
    out["Tlik"]  = pTrack.likelihood();
    if pTrack.info(cpp.LHCb.Track.FitVeloNDoF, 0): out["TrVchi2DoF"] = pTrack.info(cpp.LHCb.Track.FitVeloChi2, -1.)*1./pTrack.info(cpp.LHCb.Track.FitVeloNDoF, 0)
    else : out["TrVchi2DoF"] = 150.
    #if pTrack.info(cpp.LHCb.Track.FitVPNDoF, 0):  out["TrVPchi2DoF"] = pTrack.info(cpp.LHCb.Track.FitVPChi2, -1.)*1./pTrack.info(cpp.LHCb.Track.FitVPNDoF, 0)
    #else: out["TrVPchi2DoF"] = 150.
    if pTrack.info(cpp.LHCb.Track.FitTNDoF, 0): out["TrTChi2DoF"] = pTrack.info(cpp.LHCb.Track.FitTChi2, -1.)*1./pTrack.info(cpp.LHCb.Track.FitTNDoF, 0)
    else: out["TrTChi2DoF"] = 150.
    
    if richPID:
         out["Rich1Gas"] = float(richPID.usedRich1Gas())
         out["Rich2Gas"] = float(richPID.usedRich2Gas())
         out["KThr"] = float(richPID.kaonHypoAboveThres())
    else:
         out["Rich1Gas"] = float(-9999.)#richPID.usedRich1Gas()
         out["Rich2Gas"] = float(-9999.)#richPID.usedRich2Gas()
         out["KThr"] = float(-9999.)#richPID.kaonHypoAboveThres()
         
    out["Vcharge"] = proto.info(cpp.LHCb.ProtoParticle.VeloCharge,-10000.);
    #out["VPcharge"] = proto.info(cpp.LHCb.ProtoParticle.VPCharge,-10000.);
    
    for s in range(5):
        i = str(s)
        out["match_"+i] = mid.muonIDPropertyI(pTrack, "match", s);
        if out["match_" + i] == 2: out["match_"+i] = 1 + mid.muonIDPropertyI(pTrack, "clusize", s);
        out["mSigma_" + i] = mid.muonIDPropertyD(pTrack, "matchSigma", s); 
        out["iso_" + i] = mid.muonIDPropertyD(pTrack, "iso", s);
        out["time_" + i] = mid.muonIDPropertyD(pTrack, "time", s);
    return out
    

def numberOfTrackHits(track):
    ids = track.lhcbIDs()
    v, tt, it, ot = 0,0,0,0
    for id in ids:
        if id.isVelo(): v+=1
        if id.isTT(): tt+=1
        if id.isIT(): it+=1
        if id.isOT(): ot+=1
    return v, tt, it, ot

def numberOfTrackHitsUG(track):
    ids = track.lhcbIDs()
    v, ft, ut, hc = 0,0,0,0
    for id in ids:
        if id.isVP(): v+=1
        if id.isUT(): ut+=1
        if id.isFT(): ft+=1
        
    return v, ut, ft

def Eostize(thingie):
    x = []#.replace("LFN:", "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod")
    for thing in thingie.Input:
        x.append(thing.replace("LFN:", "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod"))
    thingie.Input = x


#GLs["GLKsm_MC10"] = cPickle.load(file(os.environ["BS2MUMUROOT"] + "/operators/GLKsm_MC10") )

def massHypo(algo, CandidateInfo, Done):
     """Uses WrongMass LoKi functor to calculate different mass 
hypothesis"""
     b = Done["Candidate"]
     CandidateInfo["MKK"] = WM('K+','K-') (b)
     CandidateInfo["MKpi"] = WM('K+','pi-') (b)
     CandidateInfo["MpiK"] = WM('pi+','K-') (b)
     CandidateInfo["Mpipi"] = WM('pi+','pi-') (b)
     CandidateInfo["Mmumu"] = WM('mu+','mu-') (b)
     return 1

def DTF_M(algo, CandidateInfo, Done):
    
     b = Done["Candidate"]
     CandidateInfo["DTF_MASS"] = DTF_FUN(M, True)(b)
     return 1

def GiacomoStuff(algo, CandidateInfo, Done):
    out1 = Giacomo(Done["mu1"], algo.mid)
    out2 = Giacomo(Done["mu2"], algo.mid)
    for key in out1.keys():
        CandidateInfo["mu1_GG_" + key] = out1[key]
        CandidateInfo["mu2_GG_" + key] = out2[key]

    
def strippingDec(algo, candInfo, Done):
     """
     16.12.2011 Johannes Albrecht
     add info on stp dec.
     """
     TES = Done["TES"]  
     
     dec=0
     p=TES['/Event/Phys/Bs2MuMuLinesNoMuIDLine/Particles']
     if p:
          if p.size():
               dec=1
     candInfo['Stp17_Bs2MuMuLinesNoMuIDLine']=dec
     
     dec=0
     p=TES['/Event/Phys/Bs2MuMuLinesWideMassLine/Particles']
     if p:
          if p.size():
               dec=1
     candInfo['Stp17_Bs2MuMuLinesWideMassLine']=dec   

     dec=0
     p=TES['/Event/Phys/Bs2MuMuLinesBu2JPsiKLine/Particles']
     if p:
          if p.size():
               dec=1
     candInfo['Stp17_Bs2MuMuLinesBu2JPsiKLine']=dec   

     dec=0
     p=TES['/Event/Phys/Bs2MuMuLinesBd2JPsiKstLine/Particles']
     if p:
          if p.size():
               dec=1
     candInfo['Stp17_Bs2MuMuLinesBd2JPsiKstLine']=dec 

     dec=0
     p=TES['/Event/Phys/Bs2MuMuLinesBs2JPsiPhiLine/Particles']
     if p:
          if p.size():
               dec=1
     candInfo['Stp17_Bs2MuMuLinesBs2JPsiPhiLine']=dec 

     return 1

     
def pileupInfo(algo, candInfo, Done):
     """
     13.09.2011 Johannes Albrecht
     add infor abour prev and next crossings
     """
     TES = Done["TES"]  
     l0 = TES["Trig/L0/L0DUReport"]
     if (not l0) and algo.DEBUG:
          print "L0DUReport not there, check options"
          print "trigger block will not be filled"
          return
     
     candInfo["sumET_m2"] = l0.sumEt(-2)
     candInfo["sumET_m1"] = l0.sumEt(-1)
     candInfo["sumET_0"] = l0.sumEt(0)
     candInfo["sumET_p1"] = l0.sumEt(1)
     candInfo["sumET_p2"] = l0.sumEt(2)

     return 1

def globalEvtVars(algo, candInfo, Done):
    """
    03.11.2010 Johannes Albrecht
    add GEC variables, taken from
    $Id: TupleToolRecoStats.cpp,v 1.5
    """
    algo.l0BankDecoder.fillDataMap()
    l0BankDecoderOK = algo.l0BankDecoder.decodeBank()
    if not l0BankDecoderOK :
        print "Readout error : unable to monitor the L0DU rawBank"
        
    nSpd = algo.l0BankDecoder.data("Spd(Mult)")
    candInfo['nSPD']=nSpd
  
    nHitsInOT = algo.rawBankDecoder.totalNumberOfHits()
    candInfo['nOT']=nHitsInOT

    TES = Done["TES"]  
    nIt=0
    IT=TES["Raw/IT/Clusters"]
    if IT:
        nIt=IT.size()
    candInfo['nIT']=nIt

    nVelo=0
    VELO=TES["Raw/Velo/LiteClusters"]
    if VELO:
        nVelo=VELO.size()
    candInfo['nVelo']=nVelo

    nTr=0
    tracks=TES["Rec/Track/Best"]
    if tracks:
         nTr=tracks.size()
    candInfo['nTr']=nTr
    
    return 1



def triggerBlock(algo,candInfo, Done):
    """
    21.09.2015 Diego Martinez Santos
    
    """
    ###
    # first, save the used TCK
    #
    TES = Done["TES"]    
    odin = TES['/Event/DAQ/ODIN']
    
    if (not odin) and algo.DEBUG :
        print "ODIN is not there, check options"
        print "trigger block will not be filled"
        return
    candInfo['TCK']=odin.triggerConfigurationKey()
    
    tistostool1 = algo.tistostool1
    tistostool1.setOfflineInput(Done["Candidate"])
    tistostool2 = algo.tistostool2
    tistostool2.setOfflineInput(Done["Candidate"])
    
    l0Tistostool = algo.tisTosToolL0
    l0Tistostool.setOfflineInput(Done["Candidate"])
    
##     ####
##     # get L0 decisions
    
    l0 = TES["Trig/L0/L0DUReport"]
    if (not l0) and algo.DEBUG:
        print "L0DUReport not there, check options"
        print "trigger block will not be filled"
        return
    
    candInfo["L0Decision"] = l0.decision()
    t=l0Tistostool.triggerTisTos("L0.*Decision")
    candInfo["L0Tis"] = t.tis()
    candInfo["L0Tos"] = t.tos()
    
    for l0Name in algo.l0List:
        t=l0Tistostool.triggerTisTos("L0"+l0Name+"Decision")
        candInfo["L0"+l0Name+"Decision"] = t.decision()
        candInfo["L0"+l0Name+"Tis"] = t.tis()
        candInfo["L0"+l0Name+"Tos"] = t.tos()
        
        
    if algo.DEBUG:
        print " ***            L0 Decisions:         ***"
        print " avaialable: "
        for i in range(25):
            print l0.channelName(i)
        print ""
        print " and selected: ",algo.l0List

    
    #hltList = algo.hlt1List + algo.hlt2List

    #select Hlt1 corresponding to routing bit 46 (Hlt1Physics)
    #Hlt1.*Decision does not work on Mc as rate limited lines always fire
    t = tistostool1.triggerTisTos(routing_bit_46)
    candInfo["Hlt1Decision"]=t.decision()
    candInfo["Hlt1Tis"]=t.tis()
    candInfo["Hlt1Tos"]=t.tos()
    t = tistostool1.tosSelection(routing_bit_46)
    print "Here ", t
    candInfo["Hlt1TosB"]=t
    t = tistostool1.tisSelection(routing_bit_46)
    print "There ", t
    candInfo["Hlt1TisB"]=t

    #select Hlt2 corresponding to routing bit 77 (Hlt2Physics)
    t = tistostool2.triggerTisTos(routing_bit_77)
    candInfo["Hlt2Decision"]=t.decision()
    candInfo["Hlt2Tis"]=t.tis()
    candInfo["Hlt2Tos"]=t.tos()

    # check validity tistostool.triggerSelectionNames('Hlt.*Decision'):
    for hltName in algo.hlt1List:
        hltNameClean=hltName.replace(".*","__st__") 
        t = tistostool1.triggerTisTos(hltName+'Decision')
        candInfo[hltNameClean+"Decision"]=t.decision()
        candInfo[hltNameClean+"Tis"]=t.tis()
        candInfo[hltNameClean+"Tos"]=t.tos()
    for hltName in algo.hlt2List:
        hltNameClean=hltName.replace(".*","__st__") 
        t = tistostool2.triggerTisTos(hltName+'Decision')
        candInfo[hltNameClean+"Decision"]=t.decision()
        candInfo[hltNameClean+"Tis"]=t.tis()
        candInfo[hltNameClean+"Tos"]=t.tos()
         
    if algo.DEBUG:
        print " ***            HLT Decisions:         ***"
        print " avaialable: "
        for hltname in tistostool.triggerSelectionNames('Hlt.*Decision'):
            print hltname
        print ""
        print " and selected HLT1: ",algo.hlt1List
        print " and selected HLT2: ",algo.hlt2List
             
    return 1


def triggerBlockDaughters(algo,candInfo, Done):
    """
    27.07.2011 Xabier Cid Vidal
    
    add trigger tis/tos infos of candidate daughters.
    Based on triggerBlock
    """

    TES = Done["TES"]    
    odin = TES['/Event/DAQ/ODIN']
    
    if (not odin) and algo.DEBUG :
         print "ODIN is not there, check options"
         print "trigger block will not be filled"
         return

    for mu in munames:
     
         tistostool = algo.tistostool
         if "target" in dir(Done[mu]): tistostool.setOfflineInput(Done[mu].target())
         else: tistostool.setOfflineInput(Done[mu])
    
         l0Tistostool = algo.tisTosToolL0
         if "target" in dir(Done[mu]): l0Tistostool.setOfflineInput(Done[mu].target())
         else: l0Tistostool.setOfflineInput(Done[mu])


         t=l0Tistostool.triggerTisTos("L0.*Decision")
         candInfo[mu+"_L0Tis"] = t.tis()
         candInfo[mu+"_L0Tos"] = t.tos()

         for l0Name in algo.l0List:
             t=l0Tistostool.triggerTisTos("L0"+l0Name+"Decision")
             candInfo[mu+"_L0"+l0Name+"Tis"] = t.tis()
             candInfo[mu+"_L0"+l0Name+"Tos"] = t.tos()


         hltList = algo.hlt1List + algo.hlt2List

         #select Hlt1 corresponding to routing bit 46 (Hlt1Physics)
         #Hlt1.*Decision does not work on Mc as rate limited lines always fire
         t = tistostool.triggerTisTos('Hlt1(?!ODIN)(?!L0)(?!Lumi)(?!Tell1)(?!MB)(?!NZS)(?!Velo)(?!BeamGas)(?!Incident).*Decision')
         candInfo[mu+"_Hlt1Tis"]=t.tis()
         candInfo[mu+"_Hlt1Tos"]=t.tos()

         #select Hlt2 corresponding to routing bit 77 (Hlt2Physics)
         t = tistostool.triggerTisTos('Hlt2(?!Forward)(?!DebugEvent)(?!Express)(?!Transparent)(?!PassThrough).*Decision')
         candInfo[mu+"_Hlt2Tis"]=t.tis()
         candInfo[mu+"_Hlt2Tos"]=t.tos()

         # check validity tistostool.triggerSelectionNames('Hlt.*Decision'):
         for hltName in hltList:
             hltNameClean=hltName.replace(".*","__st__") 
             t = tistostool.triggerTisTos(hltName+'Decision')
             candInfo[mu+"_"+hltNameClean+"Tis"]=t.tis()
             candInfo[mu+"_"+hltNameClean+"Tos"]=t.tos()
             
    return 1


         
def addTrigInfo(algo,D, Done):
    """
    trigger decisions and TisTosTobbing 
    """
    TES = Done["TES"]
    tistostool = algo.tistostool
    tistostool.setOfflineInput(Done["Candidate"])
    l0 = TES["Trig/L0/L0DUReport"]
    ### L0 decisions
    D["L0_d"] = l0.decision()
    for i in range(100):
        name = l0.channelName(i)
        if name.find(",")!= -1: continue
        if name.find("defined")!= -1: break
        D["L0"+name+"_d"] = l0.channelDecisionByName(name)
        
    l0 = tistostool.selectionTisTos(tistostool.triggerSelectionNames('Hlt1L0.*Decision'))

    D["L0_tis"] = l0.tis()
    D["L0_tos"] = l0.tos()
    D["L0_dxch"] = l0.decision()
   
    for l0name in tistostool.triggerSelectionNames('Hlt1L0.*Decision'):
        l0 = tistostool.selectionTisTos(tistostool.triggerSelectionNames(l0name))
        l0n = l0name[l0name.find("L0"):l0name.find("Decision")]
        D[l0n + "_tis"] = l0.tis()
        D[l0n + "_tos"] = l0.tos()
        D[l0n + "_dxch"] = l0.decision()
    
    hlt_ = tistostool.triggerTisTos('Hlt1.*Decision')
    D["HLT1_d"] = hlt_.decision()
    D["HLT1_tis"] = hlt_.tis()
    D["HLT1_tos"] = hlt_.tos()

    for hltname in tistostool.triggerSelectionNames('Hlt1.*Decision'):
        hlt = tistostool.triggerTisTos(hltname)
        hltn = hltname[:hltname.find("Decision")]
        D[hltn + "_d"] = hlt.decision()
        D[hltn + "_tis"] = hlt.tis()
        D[hltn + "_tos"] = hlt.tos()
        if hlt.tos() and not hlt.decision():
            print hltname, "TOS but not TRIG"
            print "(00)", "TOS:",hlt.tos(), "TIS:", hlt.tis(), "D:", hlt.decision()
            print TES["Rec/Header"].evtNumber(), TES["Rec/Header"].runNumber()
   
    ## Selections
    tistostool.setTriggerInput()
    
    tistostool.addToTriggerInput('Hlt2.*Decision')
    selections = tistostool.triggerTisTos()
    D["HLT2_d"] = selections.decision()
    D["HLT2_tis"] = selections.tis()
    D["HLT2_tos"] = selections.tos()
    
    for hltname in tistostool.triggerSelectionNames('Hlt2.*Decision'):
       hlt = tistostool.triggerTisTos(hltname)
       hltn = hltname[:hltname.find("Decision")]
       D[hltn + "_d"] = hlt.decision()
       D[hltn + "_tis"] = hlt.tis()
       D[hltn + "_tos"] = hlt.tos()
    
    tistostool.setTriggerInput()
  
    return 1


def addTrigRedux(algo,D, Done):
    """ Celestino Rodriguez
    """
    tistostool = algo.tistostool
    tistostool.setOfflineInput(Done["Candidate"])
    TES = Done["TES"]
    l0 = TES["Trig/L0/L0DUReport"]
    ### L0 decisions
    D["L0_d"] = l0.decision()
    l0 = tistostool.selectionTisTos(tistostool.triggerSelectionNames('Hlt1L0.*Decision'))
    D["L0_tis"] = l0.tis()
    D["L0_tos"] = l0.tos()
    D["L0_dxch"] = l0.decision()
    hlt_ = tistostool.triggerTisTos('Hlt1Global')
    D["HLT1_d"] = hlt_.decision()
    D["HLT1_tis"] = hlt_.tis()
    D["HLT1_tos"] = hlt_.tos()
    
    selections = tistostool.triggerTisTos('Hlt2Global')
    D["HLT2_d"] = selections.decision()
    D["HLT2_tis"] = selections.tis()
    D["HLT2_tos"] = selections.tos()
    tistostool.setTriggerInput()
    
    return 1

## def onlineIPandPt(algo,CandidateInfo,Done):
##     """
##     online IP and pt using Hlt2 tracks for mu1 and mu2
##     """
##     TES, mu1, mu2  = Done["TES"], Done["mu1"], Done["mu2"]
##     IPid = algo.IPid
##     onl1 = dv22Tools.getOnlineTrack(TES,mu1.proto().track())
##     onl2 = dv22Tools.getOnlineTrack(TES,mu2.proto().track())
##     if onl1 == None: algo.COUNTER["No Online Tracks"] += 1
##     if onl1 == 0 or onl2 ==0 : algo.COUNTER["No Linked Tracks"] += 1
##     if onl1:
##         CandidateInfo["mu1ipOnl"] = onl1.info(IPid,-999.)
##         CandidateInfo["mu1ptOnl"] = onl1.pt()
        
##     else:
##         CandidateInfo["mu1ipOnl"] = 0.
##         CandidateInfo["mu1ptOnl"] = 0.
        
##     if onl2:
##         CandidateInfo["mu2ipOnl"] = onl2.info(IPid,-999.)
##         CandidateInfo["mu2ptOnl"] = onl2.pt()
##     else:
##         CandidateInfo["mu2ipOnl"]=  0.
##         CandidateInfo["mu2ptOnl"]=  0.


def BQQMCtruth(algo,CandidateInfo,Done):
    """
    MC info about mu1, mu2 (or h1, h2) and the B mother
    """
    self, mu1, mu2, link = algo, Done["mu1"], Done["mu2"], Done["link"]
    mc1, mo1, rmo1, key1 = climbing.usualInfo(mu1, link, MCID)
    mc2, mo2, rmo2, key2 = climbing.usualInfo(mu2, link, MCID)
    Done["MC_mo1"] = 0
    Done["MC_mo2"] = 0
    CandidateInfo["mc1"] = float(mc1)
    CandidateInfo["mo1"] = float(mo1)
    CandidateInfo["rmo1"] = float(rmo1)
    CandidateInfo["rmo1key"] = float(key1)
    CandidateInfo["mc2"] =  float(mc2)
    CandidateInfo["mo2"] = float(mo2)
    CandidateInfo["rmo2"] = float(rmo2)
    CandidateInfo["rmo2key"] = float(key2)
    if mc1:
        mc1_ = link.first(mu1.proto().track())
        CandidateInfo["mc_mu1_px"] = MCPX(mc1_)
        CandidateInfo["mc_mu1_py"] = MCPY(mc1_)
        CandidateInfo["mc_mu1_pz"] = MCPZ(mc1_)
        ov_ = mc1_.originVertex()
        CandidateInfo["mc_mu1_vx"] = MCVX(ov_)
        CandidateInfo["mc_mu1_vy"] = MCVY(ov_)
        CandidateInfo["mc_mu1_vz"] = MCVZ(ov_)

        ov_ = climbing.rootMother(mc1_).originVertex()
        CandidateInfo["mc_mu1_moth_vx"] = MCVX(ov_)
        CandidateInfo["mc_mu1_moth_vy"] = MCVY(ov_)
        CandidateInfo["mc_mu1_moth_vz"] = MCVZ(ov_)
        Done["MC_mo1"] = mc1_.mother()
        
    else:
        CandidateInfo["mc_mu1_px"] = 0.
        CandidateInfo["mc_mu1_py"] = 0.
        CandidateInfo["mc_mu1_pz"] = 0.

        CandidateInfo["mc_mu1_vx"] = 0.#MCVX(ov_)
        CandidateInfo["mc_mu1_vy"] = 0.#MCVY(ov_)
        CandidateInfo["mc_mu1_vz"] = 0.#MCVZ(ov_)

        CandidateInfo["mc_mu1_moth_vx"] = 0.#MCVX(ov_)
        CandidateInfo["mc_mu1_moth_vy"] = 0.#MCVY(ov_)
        CandidateInfo["mc_mu1_moth_vz"] = 0.
    if mc2:
        mc2_ = link.first(mu2.proto().track())
        CandidateInfo["mc_mu2_px"] = MCPX(mc2_)
        CandidateInfo["mc_mu2_py"] = MCPY(mc2_)
        CandidateInfo["mc_mu2_pz"] = MCPZ(mc2_)

        ov_ = mc2_.originVertex()
        CandidateInfo["mc_mu2_vx"] = MCVX(ov_)
        CandidateInfo["mc_mu2_vy"] = MCVY(ov_)
        CandidateInfo["mc_mu2_vz"] = MCVZ(ov_)

        ov_ = climbing.rootMother(mc2_).originVertex()
        CandidateInfo["mc_mu2_moth_vx"] = MCVX(ov_)
        CandidateInfo["mc_mu2_moth_vy"] = MCVY(ov_)
        CandidateInfo["mc_mu2_moth_vz"] = MCVZ(ov_)
        Done["MC_mo2"] = mc2_.mother()
        
    else:
        CandidateInfo["mc_mu2_px"] = 0.
        CandidateInfo["mc_mu2_py"] = 0.
        CandidateInfo["mc_mu2_pz"] = 0.

        CandidateInfo["mc_mu2_vx"] = 0.#MCVX(ov_)
        CandidateInfo["mc_mu2_vy"] = 0.#MCVY(ov_)
        CandidateInfo["mc_mu2_vz"] = 0.#MCVZ(ov_)

        CandidateInfo["mc_mu2_moth_vx"] = 0.#MCVX(ov_)
        CandidateInfo["mc_mu2_moth_vy"] = 0.#MCVY(ov_)
        CandidateInfo["mc_mu2_moth_vz"] = 0.
        
    x1 = link.first(mu1.proto().track())
    if x1 and x1.mother():
        pdts = x1.mother().endVertices().at(0).products()
        sz = pdts.size()
        nph = 0
        isBhh = 1
        for dg in range(sz):
            if "target" in dir(pdts.at(dg)): dota = pdts.at(dg).target()
            else: dota = pdts.at(dg)
            aidi = MCID(dota)
            if abs(aidi) == 22: nph += 1
            if abs(aidi) not in [321,211,22,13]:
                isBhh = 0
                break
        if (sz - nph) > 2: isBhh = 0
    else: isBhh, nph = 0, 0
    CandidateInfo["rmo1isB2qq"] =  isBhh
    CandidateInfo["mc_Nphotons"] = nph
   
def KsPi0MuMuMCtruth(algo, CandidateInfo, Done):
     BQQMCtruth(algo, CandidateInfo, Done)
     CandidateInfo["mc_pi0_px"] = 0.
     CandidateInfo["mc_pi0_py"] = 0.
     CandidateInfo["mc_pi0_pz"] = 0.
     CandidateInfo["mc_gamma_px"] = 0.
     CandidateInfo["mc_gamma_py"] = 0.
     CandidateInfo["mc_gamma_pz"] = 0.
     CandidateInfo["mc_Gamma_px"] = 0.
     CandidateInfo["mc_Gamma_py"] = 0.
     CandidateInfo["mc_Gamma_pz"] = 0.
     CandidateInfo["mc_gamma_ex"] = 0.#gamma.endVertex().position().x()
     CandidateInfo["mc_gamma_ey"] = 0.#gamma.endVertex().position().y()
     CandidateInfo["mc_gamma_ez"] = -1000.#gamma.endVertex().position().z()
     CandidateInfo["mc_Gamma_ex"] = 0.#gamma.endVertex().position().x()
     CandidateInfo["mc_Gamma_ey"] = 0.#gamma.endVertex().position().y()
     CandidateInfo["mc_Gamma_ez"] = -1000.#gamma.endVertex().position().z()
     
     CandidateInfo["mc_g1_px"] = 0.
     CandidateInfo["mc_g1_py"] = 0.
     CandidateInfo["mc_g1_pz"] = 0.
     CandidateInfo["mc_g2_px"] = 0.
     CandidateInfo["mc_g2_py"] = 0.
     CandidateInfo["mc_g2_pz"] = 0.
     CandidateInfo["mc_g1_ex"] = 0.#gamma.endVertex().position().x()
     CandidateInfo["mc_g1_ey"] = 0.#gamma.endVertex().position().y()
     CandidateInfo["mc_g1_ez"] = -1000.#gamma.endVertex().position().z()
     CandidateInfo["mc_g2_ex"] = 0.#gamma.endVertex().position().x()
     CandidateInfo["mc_g2_ey"] = 0.#gamma.endVertex().position().y()
     CandidateInfo["mc_g2_ez"] = -1000.#gamma.endVertex().position().z()

     CandidateInfo["mc_g1_ID"] = 0.5
     CandidateInfo["mc_g2_ID"] = 0.4
     
     
     rpi0 = Done["pi0"]

     rph1,rph2 = rpi0.daughters()
     
     mcph1 = algo.matcher.relatedMCP(rph1)
     mcph2 = algo.matcher.relatedMCP(rph2)

     if mcph1:
     
          CandidateInfo["mc_g1_ID"] = MCID(mcph1)
          CandidateInfo["mc_g1_px"] = MCPX(mcph1)
          CandidateInfo["mc_g1_py"] = MCPY(mcph1)
          CandidateInfo["mc_g1_pz"] = MCPZ(mcph1)
          if mcph1.endVertices().size():
               CandidateInfo["mc_g1_ex"] = mcph1.endVertices().at(0).target().position().x()
               CandidateInfo["mc_g1_ey"] = mcph1.endVertices().at(0).target().position().y()
               CandidateInfo["mc_g1_ez"] = mcph1.endVertices().at(0).target().position().z()
        
     if mcph2:
          CandidateInfo["mc_g2_ID"] = MCID(mcph2)
          CandidateInfo["mc_g2_px"] = MCPX(mcph2)
          CandidateInfo["mc_g2_py"] = MCPY(mcph2)
          CandidateInfo["mc_g2_pz"] = MCPZ(mcph2)
          if mcph2.endVertices().size():
               CandidateInfo["mc_g2_ex"] = mcph2.endVertices().at(0).target().position().x()
               CandidateInfo["mc_g2_ey"] = mcph2.endVertices().at(0).target().position().y()
               CandidateInfo["mc_g2_ez"] = mcph2.endVertices().at(0).target().position().z()
        
     
     if not Done["MC_mo1"] or not Done["MC_mo2"]: return
         
     for i in range( Done["MC_mo1"].daughters().size()):
          dota = Done["MC_mo1"].daughters().at(i)
          if MCABSID(dota) != 111: continue
          if dota.daughters().size() != 2 : continue
          CandidateInfo["mc_pi0_px"] = MCPX(dota)
          CandidateInfo["mc_pi0_py"] = MCPY(dota)
          CandidateInfo["mc_pi0_pz"] = MCPZ(dota)
          gamma = dota.daughters().at(0)
          CandidateInfo["mc_gamma_px"] = MCPX(gamma)
          CandidateInfo["mc_gamma_py"] = MCPY(gamma)
          CandidateInfo["mc_gamma_pz"] = MCPZ(gamma)

          if gamma.endVertices().size():
               CandidateInfo["mc_gamma_ex"] = gamma.endVertices().at(0).target().position().x()
               CandidateInfo["mc_gamma_ey"] = gamma.endVertices().at(0).target().position().y()
               CandidateInfo["mc_gamma_ez"] = gamma.endVertices().at(0).target().position().z()
          

          Gamma = dota.daughters().at(1)
          CandidateInfo["mc_Gamma_px"] = MCPX(Gamma)
          CandidateInfo["mc_Gamma_py"] = MCPY(Gamma)
          CandidateInfo["mc_Gamma_pz"] = MCPZ(Gamma)
          if Gamma.endVertices().size():
               CandidateInfo["mc_Gamma_ex"] = Gamma.endVertices().at(0).target().position().x()
               CandidateInfo["mc_Gamma_ey"] = Gamma.endVertices().at(0).target().position().y()
               CandidateInfo["mc_Gamma_ez"] = Gamma.endVertices().at(0).target().position().z()
         
     return
          

def geoVarOld(algo,CandidateInfo,Done):
    """
    adds the DOCA, ip, lifetime etc computed as they were in
    DC04 - DC06 results
    Diego Martinez Santos
    """
    mu1o = vector(CandidateInfo["mu1o1"],CandidateInfo["mu1o2"],CandidateInfo["mu1o3"])
    mu2o = vector(CandidateInfo["mu2o1"],CandidateInfo["mu2o2"],CandidateInfo["mu2o3"])
    mu1p = vector(CandidateInfo["mu1p1"],CandidateInfo["mu1p2"],CandidateInfo["mu1p3"])
    mu2p = vector(CandidateInfo["mu2p1"],CandidateInfo["mu2p2"],CandidateInfo["mu2p3"])
    SV = vector(CandidateInfo["SV1"],CandidateInfo["SV2"],CandidateInfo["SV3"])
    PV = vector(CandidateInfo["PV1"],CandidateInfo["PV2"],CandidateInfo["PV3"])
    Bp = mu1p+mu2p
    hhh = closest_point(mu1o,mu1p,mu2o,mu2p)
    rSV = SV - PV
    life_ps = vmod(rSV)*cnv*CandidateInfo["Bmass"]/vmod(Bp) ## in ps!!
    Bip = dpr(PV,SV,Bp)
    DOCA = 2.*vmod(hhh[0] - hhh[1])
    lessIPSmu = CandidateInfo["lessIPSmu"]
    life = life_ps/1.493
    CandidateInfo["life_ps_old"] = life_ps
    CandidateInfo["life_old"] = life
    CandidateInfo["Bip_old"] = Bip
    CandidateInfo["DOCA_old"] = DOCA
    CandidateInfo["angle_old"] = ACO(rSV,Bp)
    CandidateInfo["mu1ip_old"] = dpr(PV,mu1o,mu1p)
    CandidateInfo["mu2ip_old"] = dpr(PV,mu2o,mu2p)

## def addGL(algo,CandidateInfo ,Done):
##     """
##     adds the output variables of the GL
##     """
##     out = GL(CandidateInfo)
##     for key in out.keys(): CandidateInfo["GL"+key +"."] = out[key]


## def addSomeGLs(algo,CandidateInfo ,Done):
##     """
##     adds the output variables of the GL
##     """
##     out ={}
##     for name in GLs.keys():
##          out[name] = GLs[name](CandidateInfo)
##          for key in out[name].keys(): CandidateInfo[name+key +"."] = out[name][key]
    
## def addGL_a_la_old(algo,CandidateInfo ,Done):
##     """
##     adds the output variables of the GL, using DOCA, life and IP computed with the old functions
##     """
##     if not "life_old" in CandidateInfo.keys(): geoVarOld(algo,CandidateInfo ,Done)
##     D = CandidateInfo
##     rdr = {}
##     rdr["DOCA"] = D["DOCA_old"]
##     rdr["Bip"] = D["Bip_old"]
##     rdr["mu1iso"] = D["mu1iso"]
##     rdr["mu2iso"] = D["mu2iso"]
##     rdr["life"] = D["life_old"]
##     rdr["lessIPSmu"] = D["lessIPSmu"]
    
##     out = GL(rdr)
##     for key in out.keys(): CandidateInfo["GLoDef"+key +"."] = out[key]
    
def testParalelNtuple(algo,CandidateInfo ,Done, name = "A_simple_test"):
    """
    a stupid check to see if we can create an alternative ntuple inside the main loop
    """
    tup = algo.nTuple( name )
    TES = Done["TES"]
    tup.column("evtNum", TES["Rec/Header"].evtNumber())
    tup.write()

def PromptKsCounter(algo,Done, name = "PromptKsCounter"):
        """
        Counter of Ks originated at the interaction region, to get efficiencies in MB
        """
        tup = algo.nTuple( name )
        TES = Done["TES"]
        mcpars = TES["MC/Particles"]
        for par in mcpars:
            if MCID(par) != 310: continue
            if MCABSID(par.mother()) != 311: continue
            ov = par.mother().originVertex().position()
            if sqrt(ov.x()**2 + ov.y()**2) > 2 : continue
            if ov.z() > 650 : continue
            if not "endVertices" in dir(par): continue
            if not par.endVertices().size(): continue
            if par.endVertices().at(0).position().z()> 1000: continue
            
            tup.column("PX", MCPX(par))
            tup.column("PY", MCPY(par))
            tup.column("PZ", MCPZ(par))
            tup.column("MOM", MCID(par.mother()))
            tup.write()

def PromptKs2Pi0MuMuCounter(algo,Done, name = "PromptKsCounter"):
        """
        Counter of Ks originated at the interaction region, to get efficiencies in MB
        """
        tup = algo.nTuple( name )
        TES = Done["TES"]
        mcpars = TES["MC/Particles"]
        for par in mcpars:
            if MCID(par) != 310: continue
            if MCABSID(par.mother()) != 311: continue
            ov = par.mother().originVertex().position()
            if sqrt(ov.x()**2 + ov.y()**2) > 2 : continue
            if ov.z() > 650 : continue
            if not "endVertices" in dir(par): continue
            if not par.endVertices().size(): continue
            if par.endVertices().at(0).position().z()> 1000: continue
            dotas = par.endVertices().at(0).products()
            isMu , isPi0 =0,0
            isWrong = 0
            for dota in dotas:
                if MCABSID(dota) == 13 : isMu +=1
                if MCABSID(dota) == 110 : isPi0 +=1
                if MCABSID(dota) == 22 : continue
                else:
                    isWrong = 1
                    break
            if isWrong: continue
            if not (isMu==2 and isPi0 == 1): continue
            tup.column("PX", MCPX(par))
            tup.column("PY", MCPY(par))
            tup.column("PZ", MCPZ(par))
            tup.column("MCSVX", par.endVertices().at(0).position().x())
            tup.column("MCSVY",par.endVertices().at(0).position().y() )
            tup.column("MCSVZ", par.endVertices().at(0).position().z())
            tup.column("MOM", MCID(par.mother()))
            tup.write()
                        
def MuCoordsNtuple(algo,CandidateInfo ,Done, name = "MuCords"):
    """
    NTuple with muon coordinates
    """
    algo.PIDcalTools.SaveMuonCoords(Done["mu1"], algo, name)
    algo.PIDcalTools.SaveMuonCoords(Done["mu2"], algo, name)
    
   

## def BuJPsiKMCtruth(algo,CandidateInfo,Done):
##     """
##     MC info about B+ -> JPsi K+
##     """

## def onlineMuIDcal(algo, CandidateInfo, Done):
##     """
##     look if mu1 and/or mu2 have a friend L0 muon or muon segment 
##     """
##     mu1, mu2 = Done["mu1"], Done["mu2"]
##     self, TES = algo, Done["TES"]
##     if "target" in dir(mu1): mdv22Tools.muonTisTos(mu1.target(),mu2.target(),CandidateInfo,self.tistostool)
##     else: mdv22Tools.muonTisTos(mu1,mu2,CandidateInfo,algo.tistostool)

##     if "target" in dir(mu1): CandidateInfo["mu1L0friend"] = algo.PIDcalTools.OnlStatus(mu1.target(), TES)
##     else: CandidateInfo["mu1L0friend"] = algo.PIDcalTools.OnlStatus(mu1, TES)
##     if "target" in dir(mu2): CandidateInfo["mu2L0friend"] = algo.PIDcalTools.OnlStatus(mu2.target(), TES)
##     else: CandidateInfo["mu2L0friend"] = algo.PIDcalTools.OnlStatus(mu2, TES)
                
##     if "target" in dir(mu1): CandidateInfo["mu1Segment"] = algo.PIDcalTools.OnlStatus(mu1.target(), TES, path = "Hlt/Track/MuonSegmentForL0Single")
##     else: CandidateInfo["mu1Segment"] = algo.PIDcalTools.OnlStatus(mu1, TES, path = "Hlt/Track/MuonSegmentForL0Single")
##     if "target" in dir(mu2): CandidateInfo["mu2Segment"] = algo.PIDcalTools.OnlStatus(mu2.target(), TES, path = "Hlt/Track/MuonSegmentForL0Single")
##     else: CandidateInfo["mu2Segment"] = algo.PIDcalTools.OnlStatus(mu2, TES, path = "Hlt/Track/MuonSegmentForL0Single")


def muIDDetailedInfo(algo, CandidateInfo, Done):
    """
    27.07.2011 Xabier Cid Vidal
    
    add muonID more detailed info
    Includes details on muon hits multiplicities, hits in FoI, and muon regions of each track
    """

    TES = Done["TES"]

    for mu in munames:
         mhf=muonhitsFOI(Done[mu],TES)
         
         for st in range(1,6):
              gr = GetRegion(mu,st)
              reg = gr.reg(CandidateInfo)
              if reg==-1: CandidateInfo[mu+"_RegMuSt"+str(st)] = reg
              else: CandidateInfo[mu+"_RegMuSt"+str(st)] = reg+1
              CandidateInfo[mu+"_HinFMuSt"+str(st)] = mhf[st]
              
    myd=muonhits(TES)
    for st in range(1,6):
        for reg in range(1,5): CandidateInfo["MultMuSt"+str(st)+"Reg"+str(reg)]=myd[st][reg]


def subdetectorDLLs(algo, CandidateInfo, Done):
    mu1 = Done["mu1"].proto()
    mu2 = Done["mu2"].proto()
    #rid1 = mu1.richPID()
    #rid2 = mu2.richPID()
    CandidateInfo["mu1_submuDLL"] = mu1.extraInfo()(mu1.MuonMuLL) - mu1.extraInfo()(mu1.MuonBkgLL)
    CandidateInfo["mu2_submuDLL"] = mu2.extraInfo()(mu2.MuonMuLL) - mu2.extraInfo()(mu2.MuonBkgLL)
   
    CandidateInfo["mu1_subrDLLe"] = mu1.extraInfo()(mu1.RichDLLe)
    CandidateInfo["mu2_subrDLLe"] = mu2.extraInfo()(mu2.RichDLLe)
    CandidateInfo["mu1_subrDLLmu"] = mu1.extraInfo()(mu1.RichDLLmu)
    CandidateInfo["mu2_subrDLLmu"] = mu2.extraInfo()(mu2.RichDLLmu)
    CandidateInfo["mu1_subrDLLk"] = mu1.extraInfo()(mu1.RichDLLk)
    CandidateInfo["mu2_subrDLLk"] = mu2.extraInfo()(mu2.RichDLLk)
    CandidateInfo["mu1_subrDLLp"] = mu1.extraInfo()(mu1.RichDLLp)
    CandidateInfo["mu2_subrDLLp"] = mu2.extraInfo()(mu2.RichDLLp)
   
    CandidateInfo["mu1_subHDLLe"] = mu1.extraInfo()(mu1.HcalPIDe)
    CandidateInfo["mu2_subHDLLe"] = mu2.extraInfo()(mu2.HcalPIDe)
    CandidateInfo["mu1_subHDLLmu"] = mu1.extraInfo()(mu1.HcalPIDmu)
    CandidateInfo["mu2_subHDLLmu"] = mu2.extraInfo()(mu2.HcalPIDmu)
    
## CandidateInfo["mu1_subHDLLpi"] = mu1.extraInfo()(mu1.HcalPIDpi)
## CandidateInfo["mu2_subHDLLpi"] = mu2.extraInfo()(mu2.HcalPIDpi)
    
##     CandidateInfo["mu1_subHDLLk"] = mu1.extraInfo()(mu1.HcalPIDk)
##     CandidateInfo["mu2_subHDLLk"] = mu2.extraInfo()(mu2.HcalPIDk)
    CandidateInfo["mu1_subeDLLe"] = mu1.extraInfo()(mu1.EcalPIDe)
    CandidateInfo["mu2_subeDLLe"] = mu2.extraInfo()(mu2.EcalPIDe)
    CandidateInfo["mu1_subeDLLmu"] = mu1.extraInfo()(mu1.EcalPIDmu)
    CandidateInfo["mu2_subeDLLmu"] = mu2.extraInfo()(mu2.EcalPIDmu)
    
    
    ## CandidateInfo["mu1_subHDLLpi"] = mu1.extraInfo()(mu1.HcalPIDpi)
##     CandidateInfo["mu2_subHDLLpi"] = mu2.extraInfo()(mu2.HcalPIDpi)
    
##     CandidateInfo["mu1_subHDLLk"] = mu1.extraInfo()(mu1.HcalPIDk)
##     CandidateInfo["mu2_subHDLLk"] = mu2.extraInfo()(mu2.HcalPIDk)
    CandidateInfo["mu1_HcalE"] = mu1.extraInfo()(mu1.CaloHcalE)
    CandidateInfo["mu2_HcalE"] = mu2.extraInfo()(mu2.CaloHcalE)
    CandidateInfo["mu1_EcalE"] = mu1.extraInfo()(mu1.CaloEcalE)
    CandidateInfo["mu2_EcalE"] = mu2.extraInfo()(mu2.CaloEcalE)
    
def trackHits(algo, CandidateInfo, Done):
    mu1 = Done["mu1"].proto().track()
    mu2 = Done["mu2"].proto().track()
    v, tt, it, ot = numberOfTrackHits(mu1)
    CandidateInfo["mu1_hitsInV"] = v
    CandidateInfo["mu1_hitsInTT"] = tt
    CandidateInfo["mu1_hitsInIT"] = it
    CandidateInfo["mu1_hitsInOT"] = ot
    v, ut, ft = numberOfTrackHitsUG(mu1)
    CandidateInfo["mu1_hitsInVP"] = v
    CandidateInfo["mu1_hitsInUT"] = ut
    CandidateInfo["mu1_hitsInFT"] = ft
  
    v, tt, it, ot = numberOfTrackHits(mu2)
    CandidateInfo["mu2_hitsInV"] = v
    CandidateInfo["mu2_hitsInTT"] = tt
    CandidateInfo["mu2_hitsInIT"] = it
    CandidateInfo["mu2_hitsInOT"] = ot
    
    v, ut, ft = numberOfTrackHitsUG(mu2)
    CandidateInfo["mu2_hitsInVP"] = v
    CandidateInfo["mu2_hitsInUT"] = ut
    CandidateInfo["mu2_hitsInFT"] = ft
  
    CandidateInfo["mu1_trackType"] = mu1.type()
    CandidateInfo["mu2_trackType"] = mu2.type()
    
def mc_geometry(algo, CandidateInfo, Done):
    if "mcB" not in Done.keys(): return 0
    b = Done["mcB"]
    pv = b.originVertex()
    CandidateInfo["mc_pvx"] = MCVX(pv)
    CandidateInfo["mc_pvy"] = MCVY(pv)
    CandidateInfo["mc_pvz"] = MCVZ(pv)
    sv = b.endVertices().at(0)
    CandidateInfo["mc_svx"] = MCVX(sv)
    CandidateInfo["mc_svy"] = MCVY(sv)
    CandidateInfo["mc_svz"] = MCVZ(sv)
    for p in b.daughters():
        if MCID(p) == -13: mu1 = p
        #elif MCID(p) == 13: mu2 = p
        
   
    CandidateInfo["muon_mcmass"] = MCM(mu1)
    CandidateInfo["B_macmass"] = MCM(b)

    return 1

def muon_acc(algo, CandidateInfo, Done):
    #mu1, mu2 = Done["mu1"], Done["mu2"]
    
    mu1b, mu2b = Done["mu1"].proto().muonPID(), Done["mu2"].proto().muonPID()
    if mu1b:
        CandidateInfo["mu1_pidPreslMom"] = float(mu1b.PreSelMomentum())
        CandidateInfo["mu1_pidInAcc"] = float(mu1b.InAcceptance())
    
    else:
        CandidateInfo["mu1_pidPreslMom"] = 0.
        CandidateInfo["mu1_pidInAcc"] = 0.
        
    if mu2b:
        CandidateInfo["mu2_pidPreslMom"] = float(mu2b.PreSelMomentum())
        CandidateInfo["mu2_pidInAcc"] = float(mu2b.InAcceptance())
    
    else:
        CandidateInfo["mu2_pidPreslMom"] = 0.
        CandidateInfo["mu2_pidInAcc"] = 0.

    return 1

def more_muon_things(algo, CandidateInfo,Done):
    """Gets chi2 of the muon tracks and dinstance to track extrapolation
    also calls muon_acc
    """
    
    muon_acc(algo, CandidateInfo, Done)
    mu1, mu2 = Done["mu1"], Done["mu2"]
    TES = Done["TES"]
    
    tmuons = TES['Rec/Track/Muon']

    CandidateInfo["mu1_pidDistance"] = 1.e6
    CandidateInfo["mu1_pidChi2"] = 1.e6
    CandidateInfo["mu1_probNNmu"] = 1.e6
    CandidateInfo["mu2_pidDistance"] = 1.e6
    CandidateInfo["mu2_pidChi2"] = 1.e6
    CandidateInfo["mu2_probNNmu"] = 1.e6
    
    if CandidateInfo["mu1ismu"]:
#        CandidateInfo["mu1_pidDistance"],CandidateInfo["mu1_pidChi2"] = dv22Tools.muon_info_chi2m(mu1,tmuons)
        CandidateInfo["mu1_probNNmu"] = mu1.proto().info(701,-1000)
    if CandidateInfo["mu2ismu"]:
 #       CandidateInfo["mu2_pidDistance"],CandidateInfo["mu2_pidChi2"] = dv22Tools.muon_info_chi2m(mu2,tmuons)
        CandidateInfo["mu2_probNNmu"] = mu2.proto().info(701,-1000)

    try: CandidateInfo["mu1_nShared"] = mu1.proto().muonPID().nShared()
    except: CandidateInfo["mu1_nShared"] = -1
    
    try: CandidateInfo["mu2_nShared"] = mu2.proto().muonPID().nShared()
    except: CandidateInfo["mu2_nShared"] = -1

    return 1

def Bu2JPsiK_MC(algo, CandidateInfo, Done):

     self, mu1, mu2, kaon,  link = algo, Done["mu1"], Done["mu2"], Done['kaon'], Done["link"]
     mc1, mo1, rmo1, key1 = climbing.usualInfo(mu1, link, MCID)
     mc2, mo2, rmo2, key2 = climbing.usualInfo(mu2, link, MCID)
     mc3, mo3, rmo3, key3 = climbing.usualInfo(kaon, link, MCID)
    
     CandidateInfo["mc1"] = float(mc1)
     CandidateInfo["mo1"] = float(mo1)
     CandidateInfo["rmo1"] = float(rmo1)
     CandidateInfo["rmo1key"] = float(key1)
     CandidateInfo["mc2"] =  float(mc2)
     CandidateInfo["mo2"] = float(mo2)
     CandidateInfo["rmo2"] = float(rmo2)
     CandidateInfo["rmo2key"] = float(key2)
     CandidateInfo["mck"] = float(mc3)
     CandidateInfo["mok"] = float(mo3)
     CandidateInfo["rmok"] = float(rmo3)
     CandidateInfo["rmokkey"] = float(key3)
    
     if mc1:
          mc1_ = link.first(mu1.proto().track())
          CandidateInfo["mc_mu1_px"] = MCPX(mc1_)
          CandidateInfo["mc_mu1_py"] = MCPY(mc1_)
          CandidateInfo["mc_mu1_pz"] = MCPZ(mc1_)
     else:
          CandidateInfo["mc_mu1_px"] = 0.
          CandidateInfo["mc_mu1_py"] = 0.
          CandidateInfo["mc_mu1_pz"] = 0.
     if mc2:
          mc2_ = link.first(mu2.proto().track())
          CandidateInfo["mc_mu2_px"] = MCPX(mc2_)
          CandidateInfo["mc_mu2_py"] = MCPY(mc2_)
          CandidateInfo["mc_mu2_pz"] = MCPZ(mc2_)
     else:
          CandidateInfo["mc_mu2_px"] = 0.
          CandidateInfo["mc_mu2_py"] = 0.
          CandidateInfo["mc_mu2_pz"] = 0.
        
     x1 = link.first(kaon.proto().track())
     if x1 and x1.mother():
          pdts = x1.mother().endVertices().at(0).products()
          sz = pdts.size()
          nph = 0
          isMC = 1
          for dg in range(sz):
               if "target" in dir(pdts.at(dg)): dota = pdts.at(dg).target()
               else: dota = pdts.at(dg)
               aidi = MCID(dota)
               if abs(aidi) == 22: nph += 1
               if abs(aidi) not in [321,22,443]:
                    isMC = 0
                    break
          if (sz - nph) > 2: isMC = 0
     else: isMC, nph = 0, 0
     CandidateInfo["isMCT"] =  isMC
     CandidateInfo["mc_Nphotons"] = nph


def B2JpsiKst_MC(algo, CandidateInfo, Done):

     self, mu1, mu2, kaon, pion,  link = algo, Done["mu1"], Done["mu2"], Done['kaon'], Done['pion'], Done["link"]
     mc1, mo1, rmo1, key1 = climbing.usualInfo(mu1, link, MCID)
     mc2, mo2, rmo2, key2 = climbing.usualInfo(mu2, link, MCID)
     mc3, mo3, rmo3, key3 = climbing.usualInfo(kaon, link, MCID)
     mc4, mo4, rmo4, key4 = climbing.usualInfo(pion, link, MCID)
    
     CandidateInfo["mc1"] = float(mc1)
     CandidateInfo["mo1"] = float(mo1)
     CandidateInfo["rmo1"] = float(rmo1)
     CandidateInfo["rmo1key"] = float(key1)
     CandidateInfo["mc2"] =  float(mc2)
     CandidateInfo["mo2"] = float(mo2)
     CandidateInfo["rmo2"] = float(rmo2)
     CandidateInfo["rmo2key"] = float(key2)
     CandidateInfo["mck"] = float(mc3)
     CandidateInfo["mok"] = float(mo3)
     CandidateInfo["rmok"] = float(rmo3)
     CandidateInfo["rmokkey"] = float(key3)
     CandidateInfo["mcPI"] = float(mc4)
     CandidateInfo["mopi"] = float(mo4)
     CandidateInfo["rmopi"] = float(rmo4)
     CandidateInfo["rmopikey"] = float(key4)
     CandidateInfo["mc_Nphotons"] = 0
     CandidateInfo["mc_NphotonsB"] = 0
     CandidateInfo["mc_NphotonsJpsi"] = 0
     CandidateInfo["mc_NphotonsKst0"] = 0
     CandidateInfo["isMCT"] = 0
     CandidateInfo["mc_JpsiM"] = 0.
     CandidateInfo["mc_KstM"] = 0.
     CandidateInfo["mc_mmKpiM"] = 0.
     CandidateInfo["mc_JpsiKpiM"] = 0.

     if mc1:
          mc1_ = link.first(mu1.proto().track())
          CandidateInfo["mc_mu1_px"] = MCPX(mc1_)
          CandidateInfo["mc_mu1_py"] = MCPY(mc1_)
          CandidateInfo["mc_mu1_pz"] = MCPZ(mc1_)
          pmu1 = vector(CandidateInfo["mc_mu1_px"],CandidateInfo["mc_mu1_py"],CandidateInfo["mc_mu1_pz"])
     else:
          CandidateInfo["mc_mu1_px"] = 0.
          CandidateInfo["mc_mu1_py"] = 0.
          CandidateInfo["mc_mu1_pz"] = 0.
     if mc2:
          mc2_ = link.first(mu2.proto().track())
          CandidateInfo["mc_mu2_px"] = MCPX(mc2_)
          CandidateInfo["mc_mu2_py"] = MCPY(mc2_)
          CandidateInfo["mc_mu2_pz"] = MCPZ(mc2_)
          pmu2 = vector(CandidateInfo["mc_mu2_px"],CandidateInfo["mc_mu2_py"],CandidateInfo["mc_mu2_pz"])
     else:
          CandidateInfo["mc_mu2_px"] = 0.
          CandidateInfo["mc_mu2_py"] = 0.
          CandidateInfo["mc_mu2_pz"] = 0.
      
     if mc3:
          mc3_ = link.first(kaon.proto().track())
          CandidateInfo["mc_kaon_px"] = MCPX(mc3_)
          CandidateInfo["mc_kaon_py"] = MCPY(mc3_)
          CandidateInfo["mc_kaon_pz"] = MCPZ(mc3_)
          kaonev = mc3_.endVertices().at(0).position()
          CandidateInfo["mc_kaon_edvx"] = kaonev.x()
          CandidateInfo["mc_kaon_edvy"] = kaonev.y()
          CandidateInfo["mc_kaon_edvz"] = kaonev.z()
          pk = vector(CandidateInfo["mc_kaon_px"],CandidateInfo["mc_kaon_py"],CandidateInfo["mc_kaon_pz"])
     else:
          CandidateInfo["mc_kaon_px"] = 0.
          CandidateInfo["mc_kaon_py"] = 0.#MCPY(kaon)
          CandidateInfo["mc_kaon_pz"] = 0.#MCPZ(kaon)
          #kaonev = kaon.endVertices().at(0).position()
          CandidateInfo["mc_kaon_edvx"] = 0.#kaonev.x()
          CandidateInfo["mc_kaon_edvy"] = 0.#kaonev.y()
          CandidateInfo["mc_kaon_edvz"] = 0.#kaonev.z()
     
     if mc4:
          mc4_ = link.first(pion.proto().track())
          CandidateInfo["mc_pion_px"] = MCPX(mc4_)
          CandidateInfo["mc_pion_py"] = MCPY(mc4_)
          CandidateInfo["mc_pion_pz"] = MCPZ(mc4_)
          pionev = mc4_.endVertices().at(0).position()
          CandidateInfo["mc_pion_edvx"] = pionev.x()
          CandidateInfo["mc_pion_edvy"] = pionev.y()
          CandidateInfo["mc_pion_edvz"] = pionev.z()
          ppi = vector(CandidateInfo["mc_pion_px"],CandidateInfo["mc_pion_py"],CandidateInfo["mc_pion_pz"])
     else:
          CandidateInfo["mc_pion_px"] = 0.
          CandidateInfo["mc_pion_py"] = 0.#MCPY(kaon)
          CandidateInfo["mc_pion_pz"] = 0.#MCPZ(kaon)
          #kaonev = kaon.endVertices().at(0).position()
          CandidateInfo["mc_pion_edvx"] = 0.#kaonev.x()
          CandidateInfo["mc_pion_edvy"] = 0.#kaonev.y()
          CandidateInfo["mc_pion_edvz"] = 0.#kaonev.z()
     
     if not mc3 or not mc4 or not mc1 or not mc2: return 1
     if not mo1 or not mo2 or not mo3 or not mo4: return 1
     if not mo1 == mo2 : return 1
     if not mo3 == mo4: return 1
     if mo1 != 443: return 1
     if abs(mo3) != 313: return 1
     Jpsi = mc1_.mother()
     if Jpsi.key() != mc2_.mother().key(): return 1
     B = Jpsi.mother()
     if not B : return 1
     Kst = mc3_.mother()
     if Kst.key() != mc4_.mother().key(): return 1


     pdts = B.endVertices().at(0).products()
     sz = pdts.size()
     isMC = 1
     for dg in range(sz):
          if "target" in dir(pdts.at(dg)): dota = pdts.at(dg).target()
          else: dota = pdts.at(dg)
          aidi = MCID(dota)
          if abs(aidi) == 22: CandidateInfo["mc_NphotonsB"] += 1
          if abs(aidi) not in [313,22,443]:
               isMC = 0
               break
     if (sz - CandidateInfo["mc_NphotonsB"]) > 2: isMC = 0

     pdts = Jpsi.endVertices().at(0).products()
     sz = pdts.size()
     for dg in range(sz):
          if "target" in dir(pdts.at(dg)): dota = pdts.at(dg).target()
          else: dota = pdts.at(dg)
          aidi = MCID(dota)
          if abs(aidi) == 22: CandidateInfo["mc_NphotonsJpsi"] += 1
          if abs(aidi) not in [13,22]:
               isMC = 0
               break
     if (sz - CandidateInfo["mc_NphotonsJpsi"]) > 2: isMC =0 
     
     pdts = Kst.endVertices().at(0).products()
     sz = pdts.size()
     for dg in range(sz):
          if "target" in dir(pdts.at(dg)): dota = pdts.at(dg).target()
          else: dota = pdts.at(dg)
          aidi = MCID(dota)
          if abs(aidi) == 22: CandidateInfo["mc_NphotonsKst0"] += 1
          if abs(aidi) not in [22,321,211]:
               isMC = 0
               break
     if (sz - CandidateInfo["mc_NphotonsKst0"]) > 2: isMC =0 
     CandidateInfo["mc_JpsiM"] = sqrt(IM2(pmu1,pmu2,MCM(mc1_),MCM(mc2_)))
     CandidateInfo["mc_KstM"] = sqrt(IM2(pk,ppi,MCM(mc3_),MCM(mc4_)))
     pJ = pmu1 + pmu2
     pKs = pk + ppi
     CandidateInfo["mc_mmKpiM"] = sqrt(IM2( pJ, pKs,CandidateInfo["mc_JpsiM"], CandidateInfo["mc_KstM"]))
     CandidateInfo["mc_JpsiKpiM"] = sqrt(IM2(pJ, pKs, MCM(Jpsi), CandidateInfo["mc_KstM"]))

     
     CandidateInfo["mc_Nphotons"] += CandidateInfo["mc_NphotonsKst0"] + CandidateInfo["mc_NphotonsJpsi"] + CandidateInfo["mc_NphotonsB"]
     CandidateInfo["isMCT"] =  isMC
     


def BabarVars(algo, CandidateInfo, Done):
     otherB_boost_mag=0.
     otherB_boost_angle=0.
     b = Done["Candidate"]
     mu1 = Done["mu1"]
     if Q(mu1) < 0:
          print "warning: positive muon is negative. Check unnecessary cc in decay descriptor"
          mu1 = Done["mu2"]
          
     pB = b.momentum()
     boostToB = pB.BoostToCM()
     boostToB = Math.Boost( boostToB.X(), boostToB.Y(), boostToB.Z())
     #p1 = mu1.momentum()
     p1 = boostToB(mu1.momentum())

     p1b = vector(p1.x(),p1.y(), p1.z())
     ptproj = vector(0.,0.,0.)
     
     for pion in Done["TES"]["Phys/StdNoPIDsPions/Particles"] :
          ip = Double(0.)
          ips2 = Double(-1.)
          algo.Geom.distance(pion, Done["refittedPV"], ip, ips2)
       
          if ips2 < 16: continue
          if ips2 > 1600: continue
          pt = PT(pion)
         
          if pt <200:continue
          if pt > 2000: continue
          if P(pion) > 30000.: continue
          if ratio001(pt, CandidateInfo["mu1pt"]) < 0.0001: continue
          if ratio001(pt, CandidateInfo["mu2pt"]) < 0.0001: continue
          
          
          ptrack = boostToB( pion.momentum())
          ptrack = vector(ptrack.x(),ptrack.y(),ptrack.z())         
          
          ptproj = ptproj + ptrack  ## ptproj += ptrack does not work. Makes the sum as lists rather tham vectors
        
          
     CandidateInfo["otherB_boost_angle"] = ACO(p1b, ptproj)
     CandidateInfo["otherB_boost_mag"] = vmod(ptproj)


def CDF_iso_and_Consk(algo, CandidateInfo, Done):
     from math import sqrt as s_q_r_t
     mu1, mu2 = Done["mu1"], Done["mu2"]
     b = Done["Candidate"]
     PV = Done["PV"]
     pt_mu1, pt_mu2 = CandidateInfo["mu1pt"], CandidateInfo["mu2pt"]

     px_bs = PX(b)
     py_bs = PY(b)
     pz_bs = PZ(b)

     pt_bs = PT(b)
     p_bs = P(b)
     pl_bs = s_q_r_t(p_bs**2-pt_bs**2)

     etha_bs = 0.5*log((p_bs+pl_bs)/(p_bs-pl_bs ))
     #if py_bs<0: etha_bs = -0.5*log((p_bs+pl_bs)/(p_bs-pl_bs ))

     phi_bs = acos(px_bs/pt_bs)
     if py_bs>0 and px_bs>0: phi_bs =  phi_bs
     if py_bs<0 and px_bs>0: phi_bs = -phi_bs
     if py_bs<0 and px_bs<0: phi_bs = -phi_bs
     if py_bs>0 and px_bs<0: phi_bs =  phi_bs

     iso_bs_1_200  = pt_bs
     iso_bs_4_100  = pt_bs
     iso_giampi = pt_bs
     iso_2 = pt_bs

     
     for par in Done["TES"]["Phys/StdAllNoPIDsPions/Particles"]:
          track = par.proto().track()
          if track.chi2PerDoF() > 5: continue
          phi_track = track.phi()
          delta_phi = abs(phi_bs - phi_track)
          if delta_phi>pi:  delta_phi = 2*pi-delta_phi
               
          pl_track = s_q_r_t(track.p()**2-track.pt()**2)
          etha_track = 0.5*log((track.p()+pl_track)/(track.p()-pl_track) )
          #if phi_track < 0: etha_track = -0.5*log((track.p()+pl_track)/(track.p()-pl_track) )
          delta_etha = abs(etha_bs - etha_track)


          rad_cone = s_q_r_t(delta_phi**2+delta_etha**2)
          
          ptr = track.momentum()
          ptr_x = ptr.x()
          ptr_y = ptr.y()
          ptr_z = ptr.z()
          ptr_  = sqrt(ptr_x**2+ptr_y**2+ptr_z**2)
          
          ltr = ptr_x/ptr_
          mtr = ptr_y/ptr_
          ntr = ptr_z/ptr_
          
          otest = track.position()
          xtrack = otest.x()
          ytrack = otest.y()
          ztrack = otest.z()
          
          dxtr = VX(PV)-xtrack
          dytr = VY(PV)-ytrack
          dztr = VZ(PV)-ztrack
          

          tr_pt = track.pt()

          
          up_part = s_q_r_t( (dytr*ntr-dztr*mtr)**2 +(dxtr*ntr-dztr*ltr)**2 +(dxtr*mtr-dytr*ltr)**2)
          low_part = s_q_r_t(ltr**2+mtr**2+ntr**2)
          if low_part !=0: trIP = up_part/low_part


          flag =( min (abs(pt_mu1-track.pt()), abs(pt_mu2-track.pt()))>0.01 and rad_cone<1. )
          flag2 =( min (ratio001(pt_mu1,track.pt()), ratio001(pt_mu2,track.pt()))>0.0001 and rad_cone<1. )
          ################################# flag2 =( min (ratio001(pt_mu1,track.pt()), ratio001(pt_mu2,track.pt())) and rad_cone<1. )
          if flag and trIP<0.1  and track.pt() > 200.: iso_bs_1_200 += tr_pt
          if flag and trIP<0.4  and track.pt() > 100.: iso_bs_4_100 += tr_pt
          if flag: iso_giampi += tr_pt
          if flag2:  iso_2 += tr_pt

     iso_bs_1_200 = pt_bs/iso_bs_1_200
     iso_bs_4_100 = pt_bs/iso_bs_4_100
     iso_2 = pt_bs/iso_2
     iso_giampi = pt_bs/iso_giampi

     mu_mass = 105.7
     Bs_mass = CandidateInfo["Bmass"] #5366.3
     cosnk = ( -PX(mu1)*PY(b)+PY(mu1)*PX(b) )/( sqrt(PX(b)**2+PY(b)**2)*0.5*sqrt(Bs_mass**2-4*mu_mass**2) )

     CandidateInfo["CDF_iso"] = iso_bs_1_200
     CandidateInfo["CDF_iso_4"] = iso_bs_4_100
     CandidateInfo["CDF_iso_CDF"] = iso_giampi
     CandidateInfo["Cosnk"] = cosnk
     CandidateInfo["yet_another_CDF_iso"] = iso_2
     
     
def CDF_iso(algo, CandidateInfo, Done):
     from math import sqrt as s_q_r_t
     mu1, mu2 = Done["mu1"], Done["mu2"]
     b = Done["Candidate"]
     PV = Done["PV"]
     
     pt_mu1, pt_mu2, pt_k = CandidateInfo["mu1pt"], CandidateInfo["mu2pt"], PT(Done["kaon"])
     px_bs = PX(b)
     py_bs = PY(b)
     pz_bs = PZ(b)

     pt_bs = PT(b)
     p_bs = P(b)
     pl_bs = s_q_r_t(p_bs**2-pt_bs**2)

     etha_bs = 0.5*log((p_bs+pl_bs)/(p_bs-pl_bs ))
     #if py_bs<0: etha_bs = -0.5*log((p_bs+pl_bs)/(p_bs-pl_bs ))

     phi_bs = acos(px_bs/pt_bs)
     if py_bs>0 and px_bs>0: phi_bs =  phi_bs
     if py_bs<0 and px_bs>0: phi_bs = -phi_bs
     if py_bs<0 and px_bs<0: phi_bs = -phi_bs
     if py_bs>0 and px_bs<0: phi_bs =  phi_bs

     iso_bs_1_200  = pt_bs
     iso_bs_4_100  = pt_bs
     iso_giampi = pt_bs
     iso_2 = pt_bs
     for par in Done["TES"]["Phys/StdAllNoPIDsPions/Particles"]:
          track = par.proto().track()
          if track.chi2PerDoF() > 5: continue
          phi_track = track.phi()
          delta_phi = abs(phi_bs - phi_track)
          if delta_phi>pi:  delta_phi = 2*pi-delta_phi
               
          pl_track = s_q_r_t(track.p()**2-track.pt()**2)
          etha_track = 0.5*log((track.p()+pl_track)/(track.p()-pl_track) )
          #if phi_track < 0: etha_track = -0.5*log((track.p()+pl_track)/(track.p()-pl_track) )
          delta_etha = abs(etha_bs - etha_track)


          rad_cone = s_q_r_t(delta_phi**2+delta_etha**2)
          
          ptr = track.momentum()
          ptr_x = ptr.x()
          ptr_y = ptr.y()
          ptr_z = ptr.z()
          ptr_  = sqrt(ptr_x**2+ptr_y**2+ptr_z**2)
          
          ltr = ptr_x/ptr_
          mtr = ptr_y/ptr_
          ntr = ptr_z/ptr_
          
          otest = track.position()
          xtrack = otest.x()
          ytrack = otest.y()
          ztrack = otest.z()
          
          dxtr = VX(PV)-xtrack
          dytr = VY(PV)-ytrack
          dztr = VZ(PV)-ztrack
          

          tr_pt = track.pt()

          
          up_part = s_q_r_t( (dytr*ntr-dztr*mtr)**2 +(dxtr*ntr-dztr*ltr)**2 +(dxtr*mtr-dytr*ltr)**2)
          low_part = s_q_r_t(ltr**2+mtr**2+ntr**2)
          if low_part !=0: trIP = up_part/low_part
          #ratio001(pt, CandidateInfo["mu1pt"]) < 0.0001

          flag =( min (abs(pt_mu1-track.pt()), abs(pt_mu2-track.pt()), abs(pt_k-track.pt()) )>0.01 and rad_cone<1. )
          flag2 =( min (ratio001(pt_mu1,track.pt()), ratio001(pt_mu2,track.pt()), ratio001(pt_k,track.pt()) )>0.0001 and rad_cone<1. )
          if flag and trIP<0.1  and track.pt() > 200.: iso_bs_1_200 += tr_pt
          if flag and trIP<0.4  and track.pt() > 100.: iso_bs_4_100 += tr_pt
          if flag:  iso_giampi += tr_pt
          if flag2:  iso_2 += tr_pt

     iso_bs_1_200 = pt_bs/iso_bs_1_200
     iso_bs_4_100 = pt_bs/iso_bs_4_100
     iso_giampi = pt_bs/iso_giampi
     iso_2 = pt_bs/iso_2

     CandidateInfo["CDF_iso"] = iso_bs_1_200
     CandidateInfo["CDF_iso_4"] = iso_bs_4_100
     CandidateInfo["CDF_iso_CDF"] = iso_giampi
     CandidateInfo["yet_another_CDF_iso"] = iso_2
    


     
def maxEventPT(algo, CandidateInfo, Done):

     TES=Done["TES"]
     ltracks=filter(lambda x: x.type()==3,TES["Rec/Track/Best"])
     
     CandidateInfo["maxEventPT"] = 0
     for ltr in ltracks:
          if ltr.pt()>CandidateInfo["maxEventPT"]:
               CandidateInfo["maxEventPT"] = ltr.pt()


def maxEventPTMuon(algo, CandidateInfo, Done):

     TES=Done["TES"]
     
     def isMuon(tr):
          for mpid in TES["Rec/Muon/MuonPID"]:
               if mpid.idTrack().key()==tr.key(): return mpid.IsMuon()
          return False

     ltracks=filter(lambda x: x.type()==3,TES["Rec/Track/Best"])
     
     CandidateInfo["maxEventPTMuon"] = 0
     for ltr in ltracks:
          if isMuon(ltr) and ltr.pt()>CandidateInfo["maxEventPTMuon"]:
               CandidateInfo["maxEventPTMuon"] = ltr.pt()


def bsjpsi_phi_mc_info(algo, candInfo, Done):
    mcpars = Done["TES"]["MC/Particles"]
    Bs = climbing.climbing_bsjpsiphi(mcpars, MCID)
    if not Bs : print "cancer"
    #ev = Bs.endVertices().at(0)
    ev = Bs.endVertices().at(0).products()
   
    for i in range(ev.size()):
         q = ev.at(i)
         
         mcid = abs(MCID(q))
         
         if mcid == 443: jpsi = q
         if mcid == 333: phi = q
    if abs(MCID(jpsi)) != 443 : cancer
    if abs(MCID(phi)) != 333 : cancer2
   
         
    ev = jpsi.endVertices().at(0).products()
         
    for i in range(ev.size()):
         q = ev.at(i)
       
         mcid = MCID(q)
       
         if mcid == 13: mu2 = q
         if mcid == -13: mu1 = q

    ev = phi.endVertices().at(0).products()
   
    for i in range(ev.size()):
         q = ev.at(i)
         
         mcid = MCID(q)
         
         if mcid == 321: k1 = q
         if mcid == -321: k2 = q
    ev = Bs.endVertices().at(0).position()
    candInfo["mc_ctau"] = MCCTAU(Bs)
    candInfo["mc_bs_x"] = ev.x()
    candInfo["mc_bs_y"] = ev.y()
    candInfo["mc_bs_z"] = ev.z()
    ev = Bs.originVertex().position()
    candInfo["mc_bs_ox"] = ev.x()
    candInfo["mc_bs_oy"] = ev.y()
    candInfo["mc_bs_oz"] = ev.z()

    candInfo["mc_mu1_px"] = MCPX(mu1)
    candInfo["mc_mu1_py"] = MCPY(mu1)
    candInfo["mc_mu1_pz"] = MCPZ(mu1)

    candInfo["mc_mu2_px"] = MCPX(mu2)
    candInfo["mc_mu2_py"] = MCPY(mu2)
    candInfo["mc_mu2_pz"] = MCPZ(mu2)

    candInfo["mc_k1_px"] = MCPX(k1)
    candInfo["mc_k1_py"] = MCPY(k1)
    candInfo["mc_k1_pz"] = MCPZ(k1)

    candInfo["mc_k2_px"] = MCPX(k2)
    candInfo["mc_k2_py"] = MCPY(k2)
    candInfo["mc_k2_pz"] = MCPZ(k2)

    pk1 = vector(MCPX(k1) , MCPY(k1) , MCPZ(k1))
    pk2 = vector(MCPX(k2) , MCPY(k2) , MCPZ(k2))
    pmu2 = vector(MCPX(mu2) , MCPY(mu2) , MCPZ(mu2))
    pmu1 = vector(MCPX(mu1) , MCPY(mu1) , MCPZ(mu1))
    
    SV = vector(candInfo["mc_bs_x"],candInfo["mc_bs_y"],candInfo["mc_bs_z"])
    PV = vector(candInfo["mc_bs_ox"],candInfo["mc_bs_oy"],candInfo["mc_bs_oz"])

    candInfo["mc_k1_ip"] = dpr(PV, SV, pk1)
    candInfo["mc_k2_ip"] = dpr(PV, SV, pk2)
    candInfo["mc_mu2_ip"] = dpr(PV, SV, pmu2)
    candInfo["mc_mu1_ip"] = dpr(PV, SV, pmu1)
    #candInfo["mc_mu1_ip"] = dpr(PV, SV, pmu1)
    
# Fake 4 momentums (only slopes fine)
    pk1b = vunit(pk1)*candInfo["k1ptot"]
    pk2b = vunit(pk2)*candInfo["k2ptot"]
    pmu1b = vunit(pmu1)*candInfo["mu1ptot"]
    pmu2b = vunit(pmu2)*candInfo["mu2ptot"]
    
    Bp = pk1b + pk2b + pmu1b + pmu2b

    candInfo["mc_B_IPdata_ms"] = dpr(PV,SV,Bp)
    rSV = SV - PV

    mJ = sqrt(IM2(pmu1b,pmu2b,MCM(mu1),MCM(mu2)))
    mphi = sqrt(IM2(pk1b,pk2b,MCM(k1),MCM(k2)))

    mB = sqrt(IM2(pmu1b+pmu2b , pk1b+pk2b, mJ,mphi ))
    candInfo["mc_Bmass_data_ms"] = mB           
              
    candInfo["mc_life_ps_data_ms"]  = vmod(rSV)*cnv*candInfo["mc_Bmass_data_ms"]/vmod(Bp)
    
       
