#! /usr/bin/env python
import GaudiPython                

class ForceExecution(GaudiPython.PyAlgorithm):            
    def execute(self):
        #self.COUNTER["EVT"] += 1
        gaudi = GaudiPython.AppMgr()
        if 'force' not in dir(self):
            print "No mehod force"
            return GaudiPython.SUCCESS
        if not self.force:
            print "No algorithms in 'force'"
            return GaudiPython.SUCCESS
        for algo in self.force:
            if not algo in gaudi.algorithms():
                print algo + " not in Gaudi"
                continue
            gaudi.algorithm(algo).execute()
        
        return GaudiPython.SUCCESS
       
    def finalize(self):
        print "ForceExecution counter. \n -----------"
        #print self.COUNTER
        return GaudiPython.SUCCESS
