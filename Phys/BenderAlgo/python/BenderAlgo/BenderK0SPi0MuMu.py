#! /usr/bin/env python
from Bender.MainMC import *
from Configurables import DaVinci
import climbing
from SomeUtils.alyabar import *   
from LinkerInstances.eventassoc import * 
import GaudiPython
from select import selectVertexMin
import BenderTools.TisTos
from ROOT import Double, DecayTreeFitter
#import BenderTools.TisTosMC
#selectVertexMin = LoKi.SelectVertex.selectMin
from extrafunctions import PromptKs2Pi0MuMuCounter
###Defining some variables
GBL = GaudiPython.gbl.LHCb
KMASS = 493.677
MJPsi = 3096.920
c_light = 299.792458
light_cte = 1000./c_light
print c_light

class StrippingK0SPi0MuMu(AlgoMC):   #AlgoMC
    def initialize ( self ) :

        sc = AlgoMC.initialize ( self ) ## initialize the base class
#        if not self.TRIGGER: return SUCCESS
        if sc.isFailure() : return sc

        #
        ## container to collect trigger information, e.g. list of fired lines 
        #
        triggers = {}
        triggers ['KS0'] = {} ## slot to keep information for KS
        
        #
        ## the lines to be investigated in details
        #
        lines    = {}
        lines    [ "KS0" ] = {} ## trigger lines for KS

        #
        ## six mandatory keys:
        #
        lines    [ "KS0" ][   'L0TOS' ] = 'L0.*Decision'
        lines    [ "KS0" ][   'L0TIS' ] = 'L0.*Decision'
        lines    [ "KS0" ][ 'Hlt1TOS' ] = 'Hlt1(?!ODIN)(?!L0)(?!Lumi)(?!Tell1)(?!MB)(?!NZS)(?!Velo)(?!BeamGas)(?!Incident).*Decision'
        lines    [ "KS0" ][ 'Hlt1TIS' ] = 'Hlt1(?!ODIN)(?!L0)(?!Lumi)(?!Tell1)(?!MB)(?!NZS)(?!Velo)(?!BeamGas)(?!Incident).*Decision'
        lines    [ "KS0" ][ 'Hlt2TOS' ] = 'Hlt2(?!Forward)(?!DebugEvent)(?!Express)(?!Transparent)(?!PassThrough).*Decision'
        lines    [ "KS0" ][ 'Hlt2TIS' ] = 'Hlt2(?!Forward)(?!DebugEvent)(?!Express)(?!Transparent)(?!PassThrough).*Decision'

        sc = self.tisTos_initialize ( triggers , lines )
        if sc.isFailure() : return sc
     
        return SUCCESS   
      
    def analyse(self):
        self.COUNTER["EVT"] += 1
        TES = appMgr().evtsvc()
        Done = {}   ### This is the dictionary with some objects that might be passed to the functions in the final loop. NOTE that it is defined BEFORE the kssloop
        Done["TES"] = TES
        if self.PROMPT_KS_COUNTER: PromptKs2Pi0MuMuCounter(self,Done)
        ksloop = TES["/Event/"+self.LookIn+"/Particles"]
        if not ksloop :
            if self.DEBUG: print "Nothing to do", self.name()
            return SUCCESS
        ksloop = ksloop.containedObjects()
        if not ksloop.size(): return SUCCESS
        
        pvs_ = self.vselect("pvs_", ISPRIMARY)
        if not pvs_.size(): return SUCCESS
       
        ips2cuter = MIPCHI2(pvs_,self.geo())
       
        odin = TES["DAQ/ODIN"]
        runNum,evtNum = odin.runNumber(), odin.eventNumber()

        ### Define DTF functors:
        mfit_dtf  = DTF_FUN  ( M , True , strings('pi0') ) ## use PV and J/psi-mass constraint
        c2_dtf = DTF_CHI2NDOF ( True , strings('pi0') ) ## ditto
        ctau_dtf  = DTF_CTAU ( 0 , True , strings('pi0') )
        
        
        if self.MC_INFO or not ("bunchCrossingType") in dir(odin):
            ###print "oooooo xtype = -1 "
            xtype = -1.
            ptype = TES["Gen/Header"].collisions()[0].processType()
            mcpv = TES["Gen/Header"].numOfCollisions()
            bid = -1
            Done["link"]= linkedTo(GBL.MCParticle, GBL.Track, "Rec/Track/Best")
            
            
        else:
            bid = float(odin.bunchId())
            xtype = float(odin.bunchCrossingType())
            ptype = -1.
            mcpv = -1.
            
        
        for ks in ksloop:

            if abs(ks.particleID().pid())!=310: continue

            if "target" in dir(ks.daughters().at(0)): par1= ks.daughters().at(0).target()
            else: par1=ks.daughters().at(0)
            if "target" in dir(ks.daughters().at(1)): par2 = ks.daughters().at(1).target() 
            else: par2 = ks.daughters().at(1)
                                           
            if ABSID(par1) == 111: pi0, jpsi = par1,par2
            else: pi0, jpsi = par2, par1

            if "target" in dir(jpsi.daughters().at(0)): parn1= jpsi.daughters().at(0).target()
            else: parn1=jpsi.daughters().at(0)
            if "target" in dir(jpsi.daughters().at(1)): parn2 = jpsi.daughters().at(1).target() 
            else: parn2 = jpsi.daughters().at(1)

            if ID(parn1) == -13: mu1, mu2 = parn1, parn2
            else: mu1, mu2 = parn2, parn1

            if ("mctruthcut" in dir(self)) and self.mctruthcut:
                if not self.cand_match(mu1,mu2): continue
            
            PVips2 = VIPCHI2( ks, self.geo())
            PV = selectVertexMin(pvs_, PVips2, (PVips2 >= 0. ))
            
            if "target" in dir(mu1): mu1DOCA = CLAPP (mu1.target(), self.geo())
            else: mu1DOCA = CLAPP (mu1, self.geo())
            if "target" in dir(mu2): mu2DOCA = CLAPP (mu2.target(), self.geo())
            else: mu2DOCA = CLAPP (mu2, self.geo())
           
            if not PV:
                if self.DEBUG: print "no PV there!!!!! exit"
                self.COUNTER["weird"] +=1
                continue
            
            Done["refittedPV"] = PV.clone()
            self.PVRefitter.remove(ks, Done["refittedPV"])

            ctau = CTAU (PV)
            ctau_r = CTAU (Done["refittedPV"])
            KSlife_ = ctau(ks)
            KSlife_ps = KSlife_*light_cte  #in picosecond
            Dis2 = VDCHI2( PV ) ###` To PV which minimizes KS IPS
            Dis2_r = VDCHI2(Done["refittedPV"])
        
            PVvec = vector( VX(PV), VY(PV), VZ(PV) )
            SVvec = vector( VX(ks.endVertex()), VY(ks.endVertex()), VZ(ks.endVertex()) )
            rSV = SVvec - PVvec
            
            KSp = vector(PX(ks), PY(ks), PZ(ks))
       
            KSpt = vtmod(KSp)
            KSips2 = PVips2(PV)
            KSip = dpr(PVvec, SVvec, KSp)
            
            mu1ips2, mu2ips2 = ips2cuter(mu1), ips2cuter(mu2)
            sigDOFS = Dis2(ks)

            ipscheck = min(sigDOFS, mu1ips2, mu2ips2, KSips2)
            if ipscheck < 0:
                if self.DEBUG: print "error on IPS calculations"
                self.COUNTER["negSq"] +=1
                ### continue, Let's decide what to do afterwards..

            sigDOFS =psqrt(sigDOFS)
            KSips = psqrt(KSips2)
            
            track1 = mu1.proto().track()
            track2 = mu2.proto().track()
            o1, o2 = track1.position(), track2.position()
            mippvs = MIP( pvs_, self.geo() )
            iptoPV = IP (PV, self.geo())
            
            mu1ippvs_ = mippvs (mu1) # IP
            mu2ippvs_ = mippvs (mu2)
            mu1ip_ = iptoPV (mu1)
            mu2ip_ = iptoPV (mu2)
           
            Done["Candidate"], Done["mu1"], Done["mu2"],Done["pi0"] = ks, mu1, mu2,pi0
            gamma1 = pi0.daughters().at(0)
            gamma2 = pi0.daughters().at(1)
            CandidateInfo = {}
            CandidateInfo["evt"] = self.COUNTER["EVT"] + self.evt_of
            CandidateInfo["ipscheck"] = ipscheck
            CandidateInfo["Vchi2"] = VCHI2(ks.endVertex())
            CandidateInfo["KSip_r"], CandidateInfo["KSips_r"] = Double(0.), Double(0.)
            self.Geom.distance(ks, Done["refittedPV"],CandidateInfo["KSip_r"], CandidateInfo["KSips_r"])
            CandidateInfo["KSips_r"] = float(psqrt(CandidateInfo["KSips_r"]))
            CandidateInfo["KSip_r"] = float(CandidateInfo["KSip_r"])
            CandidateInfo["mu1ips"] = psqrt(mu1ips2)
            CandidateInfo["mu2ips"] = psqrt(mu2ips2)
            CandidateInfo["mu1mip"] = mu1ippvs_
            CandidateInfo["mu2mip"] = mu2ippvs_
            CandidateInfo["mu1ip"] = mu1ip_
            CandidateInfo["mu2ip"] = mu2ip_
            CandidateInfo["KS_ctau"] = KSlife_ ## in milimeters !!!!!
            CandidateInfo["KSlife_ps"] = KSlife_ps  ### in ps !!
            CandidateInfo["pi0mass"] = M(pi0)
            CandidateInfo["KSmass"] = M(ks)
            CandidateInfo["KSpt"] = KSpt
            CandidateInfo["xtype"] = xtype
            CandidateInfo["bid"] = bid
            CandidateInfo["mu1ismu"] = ISMUON(mu1)
            CandidateInfo["mu2ismu"] = ISMUON(mu2)

            if "target" in dir(mu1): muDOCA = CLAPP (mu1.target(), self.geo())
            else: muDOCA = CLAPP (mu1, self.geo())

            CandidateInfo["DOCA"] = muDOCA(mu2)
            CandidateInfo["longTracks"] = len(TES[self.RootInTES + 'Rec/Track/Best'])
            CandidateInfo["KSlife_ps_r"] = ctau_r(ks)*light_cte
            CandidateInfo["KSdissig_r"] = psqrt(Dis2_r(ks))
            
            CandidateInfo["mu1p1"] = PX(mu1)
            CandidateInfo["mu1p2"] = PY(mu1)
            CandidateInfo["mu1p3"] = PZ(mu1)
            CandidateInfo["mu2p1"] = PX(mu2)
            CandidateInfo["mu2p2"] = PY(mu2)       
            CandidateInfo["mu2p3"] = PZ(mu2)

            CandidateInfo["pi0p1"] = PX(pi0)
            CandidateInfo["pi0p2"] = PY(pi0)       
            CandidateInfo["pi0p3"] = PZ(pi0)
            
            CandidateInfo["g1p1"] = PX(gamma1)
            CandidateInfo["g1p2"] = PY(gamma1)       
            CandidateInfo["g1p3"] = PZ(gamma1)
            CandidateInfo["g2p1"] = PX(gamma2)
            CandidateInfo["g2p2"] = PY(gamma2)       
            CandidateInfo["g2p3"] = PZ(gamma2)
            
            CandidateInfo["mu1pt"] = PT(mu1)
            CandidateInfo["mu1ptot"] = P(mu1)
            CandidateInfo["mu2pt"] = PT(mu2)
            CandidateInfo["mu2ptot"] = P(mu2)
            CandidateInfo["pi0pt"] = PT(pi0)
            CandidateInfo["pi0ptot"] = P(pi0)

            CandidateInfo["mu1o1"] = o1.x()
            CandidateInfo["mu1o2"] = o1.y()
            CandidateInfo["mu1o3"] = o1.z()
            CandidateInfo["mu2o1"] = o2.x()
            CandidateInfo["mu2o2"] = o2.y()
            CandidateInfo["mu2o3"] = o2.z()
            CandidateInfo["g1csize"] = gamma1.proto().calo().size()
            CandidateInfo["g2csize"] = gamma2.proto().calo().size()
                                                
            CandidateInfo["g1e1"] = gamma1.proto().calo().at(0).target().position().x()
            CandidateInfo["g1e2"] = gamma1.proto().calo().at(0).target().position().y()
            CandidateInfo["g1e3"] = gamma1.proto().calo().at(0).target().position().z()
            
            CandidateInfo["g2e1"] = gamma2.proto().calo().at(0).target().position().x()
            CandidateInfo["g2e2"] = gamma2.proto().calo().at(0).target().position().y()
            CandidateInfo["g2e3"] = gamma2.proto().calo().at(0).target().position().z()
            
            CandidateInfo["PIDmu_mu1"] = PIDmu(mu1)   
            CandidateInfo["PIDmu_mu2"] = PIDmu(mu2)   
            CandidateInfo["PIDe_mu1"] = PIDe(mu1)   
            CandidateInfo["PIDe_mu2"] = PIDe(mu2)   
            CandidateInfo["PIDk_mu1"] = PIDK(mu1)   
            CandidateInfo["PIDk_mu2"] = PIDK(mu2)   
            
            # ClusterMass = 322, // CaloID estimator : MergedPi0Alg cluster Mass (neutral)
            # IsPhoton = 381, // Gamma/Pi0 separation variable (neutral)

            #proto1 = pi0.proto()
            #caloEn = {"EcalE":proto1.CaloEcalE,"HcalE":proto1.CaloHcalE,
            #          "PrsE":proto1.CaloPrsE,"SpdE":proto1.CaloSpdE,
            #          "ClusterMass":proto1.ClusterMass,"IsPhoton":proto1.IsPhoton}

            
            #for par in ["mu1","mu2","pi0"]:
            #    for calo in caloEn:
            #        CandidateInfo[calo+"_"+par]=eval(par).proto().extraInfo().get(caloEn[calo])


            CandidateInfo["SV1"] = VX(ks.endVertex())           
            CandidateInfo["SV2"] = VY(ks.endVertex())           
            CandidateInfo["SV3"] = VZ(ks.endVertex())          
            CandidateInfo["evtNum"] = TES["Rec/Header"].evtNumber()
            CandidateInfo["runNum"] = TES["Rec/Header"].runNumber()          
            CandidateInfo["PV1"] = VX(PV)
            CandidateInfo["PV2"] = VY(PV)
            CandidateInfo["PV3"] = VZ(PV)
            CandidateInfo["KSips"] = KSips
            CandidateInfo["KS_IP" ] =  KSip
            CandidateInfo["KSdissig"]= sigDOFS
            CandidateInfo["KS_pt"] = CandidateInfo["KSpt"]
            CandidateInfo["lessIPS"] = min(CandidateInfo["mu1ips"], CandidateInfo["mu2ips"] )

            CandidateInfo["mu1_track_Chi2"] = TRCHI2 (mu1)
            CandidateInfo["mu2_track_Chi2"] = TRCHI2 (mu2)

            CandidateInfo["mu1_track_Chi2DoF"] = TRCHI2DOF (mu1)
            CandidateInfo["mu2_track_Chi2DoF"] = TRCHI2DOF (mu2)


            KULLBACK = TINFO ( LHCb.Track.CloneDist , 1.e+9 )
            CandidateInfo["mu1_track_CloneDist"] = KULLBACK(mu1)
            CandidateInfo["mu2_track_CloneDist"] = KULLBACK(mu2)
            CandidateInfo["LF_time"], CandidateInfo["LF_terr"], CandidateInfo["LF_tchi2"] = Double(0.), Double(0.), Double(0.)
            sc = self.LifeFitter.fit ( Done["refittedPV"], ks , CandidateInfo["LF_time"],  CandidateInfo["LF_terr"], CandidateInfo["LF_tchi2"] )

            CandidateInfo["LF_time"] = CandidateInfo["LF_time"]*1000
            CandidateInfo["LF_terr"] = CandidateInfo["LF_terr"]*1000
            CandidateInfo["mu1GP"] = TrGHOSTPROB(mu1.proto().track())
            CandidateInfo["mu2GP"] = TrGHOSTPROB(mu2.proto().track())
            CandidateInfo["ProbNNmu1"] = PROBNNmu(mu1)
            CandidateInfo["ProbNNmu2"] = PROBNNmu(mu2)
            CandidateInfo["ProbNNghost1"] = PROBNNghost(mu1)
            CandidateInfo["ProbNNghost2"] = PROBNNghost(mu2)
            theDira = DIRA(Done["refittedPV"])
            CandidateInfo["DIRA"] = theDira(ks)

            ### DTF
            CandidateInfo["Ksmass_DTF"] =  mfit_dtf(ks)
            CandidateInfo["chi2_DTF"] = c2_dtf(ks)
            CandidateInfo["cTau_DTF"] = ctau_dtf(ks)
            ### Clasic DTF
            dtf = DecayTreeFitter.Fitter(ks, PV)
            dtf.setMassConstraint(LHCb.ParticleID(111), True)
            dtf.fit()
            CandidateInfo["Ksmass_DTF_classic"] = dtf.fitParams().momentum().m().value()
            CandidateInfo["chi2_DTF_classic"] = dtf.chiSquare()
            CandidateInfo["cTau_DTF"] = dtf.fitParams().ctau().value()
            
            for function in self.extraFunctions:
                function(self, CandidateInfo, Done)
                          
            if self.TUP:
                tup = self.nTuple( self.name() )
                for key in CandidateInfo.keys():
                    tup.column(key,CandidateInfo[key]) 
                if self.TRIGGER:
                    self.decisions   ( ks , self.triggers['KS0'] )
                    self.tisTos      (
                        ks               , ## particle 
                        tup               , ## n-tuple
                        'KS_'            , ## prefix for variable name in n-tuple
                        self.lines['KS0'] , ## trigger lines to be inspected
                        verbose = True      ## good ooption for those who hates bit-wise operations 
                        )
                tup.write()
                
            if self.DST:
                self.addedKeys = CandidateInfo.keys()
                self.addedKeys.sort()
                for i in range(len(self.addedKeys)):
                    j = 500 + i
                    ks.addInfo(j,CandidateInfo[self.addedKeys[i]])
           
                    
        self.setFilterPassed( True )
        if self.DEBUG: print "Bender has found a candidate"
        self.COUNTER["Sel"] += 1
        return SUCCESS
    
    def finalize(self):
        print "Bender counter. \n -----------"
        print self.COUNTER
        if self.DST:
            print "[||] Keys for added Info", self.name()
            for i in range(len(self.addedKeys)):
                print 500+i, self.addedKeys[i]
        print "../../../../../"
        return AlgoMC.finalize(self)
