from ParticleTranslator import PTranslate

def climbing_bsmumu(particles,MCID, DC04 = 0):
    for par in particles:
        #rmo = rootMother(par)
        if abs(MCID(par)) != 531: continue
        ev = par.endVertices().at(0)
        if DC04: ev = ev.ptr()
        ev = ev.products()
        if ev.size() < 2: continue # A lot of photons are possible
        prdts = [22, 13, -13]
        ok = 1
        for i in range(ev.size()):
            q = ev.at(i)
            if DC04: q = q.ptr()
            if MCID(q) not in prdts:
                ok = 0
                break
        if not ok: continue
        return par

def climbing_b2qq(particles,MCID,decay):
    for par in particles:
        #rmo = rootMother(par)
        mom = decay[0]
        dotas = [decay[1],decay[2]]
        if abs(MCID(par)) != mom: continue
        ev = par.endVertices().at(0)
        ev = ev.products()
        if ev.size() < 2: continue # A lot of photons are possible
        prdts = [22]
        prdts += dotas
        ok = 1
        for i in range(ev.size()):
            q = ev.at(i)
            if abs(MCID(q)) not in prdts:
                ok = 0
                break
        if not ok: continue
        return par
    
def climbing_bujpsik(particles, MCID):
    for par in particles:
        #rmo = rootMother(par)
        if abs(MCID(par)) != 521: continue
        ev = par.endVertices()
        ev = ev.at(ev.size()-1)
        ev = ev.products()
        if ev.size() < 2: continue # A lot of photons are possible?
        prdts = [443, 321, 22]
        NJpsi = 0
        Nk = 0
        ok = 1
        for i in range(ev.size()):
            q = ev.at(i)
            mcid = abs(MCID(q))
            if mcid not in prdts:
                ok = 0
                break
            if mcid == 443: NJpsi += 1
            if mcid == 321: Nk+=1
            
        if NJpsi != 1: ok = 0
        if Nk != 1: ok = 0

        if not ok: continue
        return par


def climbing_bdjpsikst(particles, MCID):
    for par in particles:
        #rmo = rootMother(par)
        if abs(MCID(par)) != 511: continue
        ev = par.endVertices()
        ev = ev.at(ev.size()-1)
        ev = ev.products()
        if ev.size() < 2: continue # A lot of photons are possible?
        prdts = [443, 313, 22]
        NJpsi = 0
        Nk = 0
        ok = 1
        for i in range(ev.size()):
            q = ev.at(i)
            mcid = abs(MCID(q))
            if mcid not in prdts:
                ok = 0
                break
            if mcid == 443: NJpsi += 1
            if mcid == 313: Nk+=1
            
        if NJpsi != 1: ok = 0
        if Nk != 1: ok = 0

        if not ok: continue
        return par

def climbing_kspizeromm(particles, MCID):
    for par in particles:
        #rmo = rootMother(par)
        if abs(MCID(par)) != 310: continue
        ev = par.endVertices()
        if not ev.size(): continue
        ev = ev.at(ev.size()-1)
        ev = ev.products()
        if ev.size() < 3: continue # A lot of photons are possible?
        prdts = [111, 13, 22]
        Npizero = 0
        Nmu = 0
        ok = 1
        for i in range(ev.size()):
            q = ev.at(i)
            mcid = abs(MCID(q))
            if mcid not in prdts:
                ok = 0
                break
            if mcid == 111: Npizero += 1
            if mcid == 13: Nmu+=1
            
        if Npizero != 1: ok = 0
        if Nmu != 2: ok = 0

        if not ok: continue
        return par
       
def climbing_bsjpsiphi(particles,MCID, DC04 = 0):
    for par in particles:
        #rmo = rootMother(par)
        if abs(MCID(par)) != 531: continue
        ev = par.endVertices().at(0)
        if DC04: ev = ev.ptr()
        ev = ev.products()
        if ev.size() != 2: continue 
        prdts = [443, 333]
        NJpsi = 0
        Nphi = 0
        ok = 1
        for i in range(ev.size()):
            q = ev.at(i)
            if DC04: q = q.ptr()
            mcid = abs(MCID(q))
            if mcid not in prdts:
                ok = 0
                break
            if mcid == 443: NJpsi += 1
            if mcid == 333: Nphi+=1
        if NJpsi != 1: ok = 0
        if Nphi != 1: ok = 0
        if not ok: continue
        return par


def climbing_bsjpsiphi_to_be_deleted(particles,MCID, DC04 = 0):
    for par in particles:
        #rmo = rootMother(par)
        if abs(MCID(par)) != 531: continue
        ev = par.endVertices().at(0)
        if DC04: ev = ev.ptr()
        ev = ev.products()
        if ev.size() != 2: continue 
        prdts = [443, 333]
        ok = 1
        for i in range(ev.size()):
            q = ev.at(i)
            if DC04: q = q.ptr()
            if abs(MCID(q)) not in prdts:
                ok = 0
                break
        if not ok: continue
        return par


def endProducts(mc,zcut = 1000, rhocut = 5.):
    out = []
    if mc.endVertices().size()==0:return [mc]
    for i in range(mc.endVertices().size()):
        ev =mc.endVertices().at(i).ptr()
        endV = ev.position()
        #endV = vector(endV.x(),endV.y(),endV.z())
        rho2 = endV.x()*endV.x()+endV.y()*endV.y()
        rhocut =rhocut*rhocut
        if endV.z()-mc.originVertex().position().z() > zcut or rho2 > rhocut: break
        prods = ev.products()
        for j in range(prods.size()):
            cand = prods.at(j).ptr()
            epc = endProducts(cand)
            for par in epc:
                out.append(par)
    if out == [] :return [mc]
    return out
def getFirstBs(pars):
    k,l = [], []
    for par in pars:
        mo = rootMother(par)
        ki = mo.key()
        if (not ki in k) and mo.particleID().hasBottom():
            k.append(ki)
            l.append(mo)
    return l
def printProducts(par,fMo = 0):
    Mpid = PTranslate(par.particleID().pid())
    sts = fMo*"\t"
    if not fMo: print "Starting in particle ",Mpid
    else: print sts + "Daughter ",Mpid," has products :"
    evs = par.endVertices()
    for i in range(evs.size()):
        ev = evs.at(i).ptr().products()
        hhh = []
        for j in range(ev.size()):
            dgt = ev.at(j).ptr()
            print sts + "\t"+PTranslate(dgt.particleID().pid())
            if dgt.endVertices().size() and dgt.endVertices().at(0).ptr().products().size(): hhh.append(dgt)
        for ddd in hhh:
            printProducts(ddd,fMo+1)

    

        
def rootB(mcpars,pid=531):
    """Returns a list with Bs's or (main mothers with pid if other)
    """
    broot = []
    for i in range(mcpars.size()):
        mcpar=mcpars.containedObjects()[i]
        if ((mcpar.particleID().abspid() == pid) and
            (mcpar.mother() == None)):
            broot.append(mcpar)
    return broot

def quark(q,mo1):
    """finds a quark in a number
    """
    mo1=1.*mo1
    mo=str(mo1)
    m1=mo[len(mo)-min(len(mo),6) : len(mo)-3]# 0, point and spin
    q=str(q)
    if m1.find(q)!=-1: return 1
    return 0

def rootMother(mcpar):
    """Returns the first mother of the MC particle mcpar.
    """

    mom = mcpar.mother()
    if (mom == None): return mcpar
    return rootMother(mom)
    
def signalParticles(mcpars):
    """Returns a list with all the Bs's keys with all the products per key
    """
    roots = rootB(mcpars)
    keys = map(lambda x: x.key(),roots)
    bsignal = {}
    for key in keys: bsignal[key] = [] 
    for mcpar in mcpars:
        mom = rootMother(mcpar)
        if (mom.key() in keys):
            bsignal[key].append(mcpar)
    return bsignal

def signalParticles2(mcpars):
    """Returns a list with all the Bs's keys with all the products per key
    """
    roots = rootB(mcpars)
    keys = map(lambda x: x.key(),roots)
    bsignal = {}
    for key in keys: bsignal[key] = [] 
    for i in range(mcpars.size()):
        mcpar=mcpars.containedObjects()[i]
        mom = rootMother(mcpar)
        if (mom.key() in keys):
            bsignal[key].append(mcpar)
    return bsignal


def makeList(cobjs):
    lobjs = cobjs.size()*[0]
    vobjs = cobjs.containedObjects()
    for i in range(cobjs.size()): lobjs[i] = vobjs[i]
    return lobjs

def byKey(key,lobjs):
    for obj in lobjs:
        if (obj.key() == key): return obj
    return None

def hasKey(objs,obj):
    for o in objs:
        if (o.key() == obj.key()): return True
    return False


def signalTracks(mcpars,tracks,link):
    """Returns the list with tracks in 'tracks' which came from a MC B decay
    """
    roots = rootB(mcpars)
    keys = map(lambda x: x.key(),roots)
    bsignal = {}
    for key in keys: bsignal[key] = [] 
    for track in tracks:
       
        mcpar=link.first(track.key())
        
        if mcpar and mcpar.originVertex().position().z()<750.: #last VELO station
            mom = rootMother(mcpar)
            if (mom.key() in keys):
                bsignal[key].append(track)
    return bsignal,keys

def usualInfo(particle, link, MCID = None):
    if 'origin' in dir(particle):
        mc = link.first(particle.origin().track())
    else:
        mc = link.first(particle.proto().track())
    if not mc: return 0, 0, 0, 0
    mo = mc.mother()
    if not MCID:
        mcID = mc.particleID().pid()
    else:
        mcID = MCID(mc)

    if not mo: return mcID, mcID, mcID, mc.key()

    rmo = rootMother(mc)
    if not MCID:
        return mcID, mo.particleID().pid(), rmo.particleID().pid(), rmo.key()
    else:
        return mcID, MCID(mo), MCID(rmo), rmo.key()
