#!/usr/bin/env python

"""
Bender module to run the following sequence over B2KShh stripped data samples:
- run an algorithm to store the reco info for all Stripped candidates
"""

from Bender.Main import *
from Gaudi.Configuration import *

def configure( datafiles, catalogues = [], params = {}, castor = False ) :

    #======= B->KShh Configuration and Setup =========#
    magtype        = params.get( 'magtype', 'MagDown' )
    whichStripping = params.get( 'stripping', 'Stripping20' )
    extended_hypos = params.get( 'extended_hypos', False )
    #=================================================#

    # no need to configure this anymore, it should be taken from the Stripping revision number
    whichData = '' 

    known2011StrippingVersions = [ 'Stripping20r1', 'Stripping20r1p1' ]
    known2012StrippingVersions = [ 'Stripping20',   'Stripping20r0p1' ]

    if whichStripping in known2011StrippingVersions:
        whichData = '2011'
    elif whichStripping in known2012StrippingVersions:
        whichData = '2012'
    else :
        e = Exception('Unknown Stripping version ' + whichStripping)
        raise e
    
    from Configurables import DaVinci

    daVinci = DaVinci()
    daVinci.DataType        = whichData
    daVinci.Simulation      = False
    daVinci.Lumi            = True
    daVinci.InputType       = "MDST"
    daVinci.TupleFile       = 'B2KShh-Collision'+whichData[2:]+'-'+magtype+'-'+whichStripping+'_WS.root'
    daVinci.EvtMax          = -1
    daVinci.PrintFreq       = 100000

    from Configurables import CondDB
    CondDB().LatestGlobalTagByDataType = whichData

    setData( datafiles, catalogues )

    stream = {}
    stream['Stripping20']      = 'Bhadron'
    stream['Stripping20r1']    = 'Bhadron'
    stream['Stripping20r0p1']  = 'Bhadron'
    stream['Stripping20r1p1']  = 'Bhadron'
    
    teslocation = '/Event/'+stream[whichStripping]
    daVinci.RootInTES = teslocation

    filterStrip = "HLT_PASS_RE('StrippingB2KShh[DL]{2}SameSignLineDecision')"

    inputLocationDD = 'Phys/B2KShhDDSameSignLine/Particles'
    inputLocationLL = 'Phys/B2KShhLLSameSignLine/Particles'

    h1type = 'pi'
    h2type = 'pi'

    if whichStripping == 'Stripping20r1p1' or whichStripping == 'Stripping20r0p1' :
        h1type = 'K'
        filterStrip = "HLT_PASS_RE('StrippingB2KSKpi[DL]{2}SameSignLineDecision')"
        inputLocationDD = 'Phys/B2KSKpiDDSameSignLine/Particles'
        inputLocationLL = 'Phys/B2KSKpiLLSameSignLine/Particles'

    from PhysConf.Filters import LoKi_Filters

    filterVoid  = "EXISTS('/Event/Strip/Phys/DecReports')"
    filterAlg = LoKi_Filters( VOID_Code = filterVoid, STRIP_Code = filterStrip  )
    filters = filterAlg.filters('Filters')
    filters.reverse()

    daVinci.EventPreFilters = filters

    reco_daughters = [ h1type, h2type, 'KS0' ]

    from PhysConf.MicroDST import uDstConf
    uDstConf ( teslocation )

    from B2KShh.RecoAlgo import B2KShhReco
    
    gaudi = appMgr()

    algB2KShhDD = B2KShhReco(
        'B2KShhDD',
        reco_daughters,
        extended_hypos, True, False, False, 
        Inputs = [ inputLocationDD ],
        RootInTES = teslocation
        )
    algB2KShhLL = B2KShhReco(
        'B2KShhLL',
        reco_daughters,
        extended_hypos, True, False, False, 
        Inputs = [ inputLocationLL ],
        RootInTES = teslocation
        )

    userSeq = gaudi.algorithm('GaudiSequencer/DaVinciUserSequence' , True )

    userSeq.Members += [ algB2KShhDD.name() ]
    userSeq.Members += [ algB2KShhLL.name() ]

    return SUCCESS

#############

if '__main__' == __name__ :

    datafiles = [
            'PFN:/data/lhcb/phsdba/B2KShh/DST/Data/2012-Stripping20-MagDown/00020198_00000067_1.bhadron.mdst',
            'PFN:/data/lhcb/phsdba/B2KShh/DST/Data/2012-Stripping20-MagDown/00020198_00000224_1.bhadron.mdst'

            #'PFN:/data/lhcb/phsdba/B2KShh/DST/Data/2012-Stripping20r0p1-MagDown/00024183_00000046_1.bhadron.mdst',
            #'PFN:/data/lhcb/phsdba/B2KShh/DST/Data/2012-Stripping20r0p1-MagDown/00024183_00000062_1.bhadron.mdst'
    ]

    pars = {}
    pars[ 'stripping' ] = 'Stripping20'
    #pars[ 'stripping' ] = 'Stripping20r0p1'
    pars[ 'magtype' ]   = 'MagDown'

    configure( datafiles, params = pars, castor=False )

    run(-1)

#############

