#!/usr/bin/env python

"""
Bender module to run the following sequence over B2KShh stripped data samples:
- run an algorithm to store the reco info for all Stripped candidates
"""

from Bender.Main import *
from Gaudi.Configuration import *

def configure( datafiles, catalogues = [], params = {}, castor = False ) :

    #======= B->KShh Configuration and Setup =========#
    magtype        = params.get( 'magtype',   'MagDown'     )
    whichStripping = params.get( 'stripping', 'Stripping20' )
    extended_hypos = params.get( 'extended_hypos', False )
    #=================================================#

    knownMagTypes = [ 'MagDown', 'MagUp' ]

    if magtype not in knownMagTypes :
        e = Exception('Unsupported magnet setting: ' + magtype)
        raise e

    known2011StrippingVersions = [ 'Stripping20r1', 'Stripping20r1p2' ]
    known2012StrippingVersions = [ 'Stripping20',   'Stripping20r0p2' ]

    whichData = '' 
    if whichStripping in known2011StrippingVersions:
        whichData = '2011'
    elif whichStripping in known2012StrippingVersions:
        whichData = '2012'
    else :
        e = Exception('Unsupported Stripping version: ' + whichStripping)
        raise e
    
    from Configurables import DaVinci
    
    daVinci = DaVinci()
    daVinci.DataType        = whichData
    daVinci.Simulation      = False
    daVinci.Lumi            = True
    daVinci.InputType       = "MDST"
    daVinci.TupleFile       = 'Lb2Lhh-Collision'+whichData[2:]+'-'+magtype+'-'+whichStripping+'.root'
    daVinci.EvtMax          = -1
    daVinci.PrintFreq       = 10000

    from Configurables import CondDB
    CondDB().LatestGlobalTagByDataType = whichData

    setData( datafiles, catalogues )

    stream = {}
    stream['Stripping20']      = 'Bhadron'
    stream['Stripping20r1']    = 'Bhadron'
    stream['Stripping20r0p2']  = 'Bhadron'
    stream['Stripping20r1p2']  = 'Bhadron'
    
    teslocation = '/Event/'+stream[whichStripping]+'/'
    daVinci.RootInTES = teslocation

    filterStrip = "HLT_PASS_RE('StrippingLb2V0hh[DL]{2}LineDecision')"

    inputLocationDD = 'Phys/Lb2V0hhDDLine/Particles'
    inputLocationLL = 'Phys/Lb2V0hhLLLine/Particles'

    h1type = 'pi'
    h2type = 'pi'

    from PhysConf.Filters import LoKi_Filters

    filterVoid  = "EXISTS('/Event/Strip/Phys/DecReports')"
    filterAlg = LoKi_Filters( VOID_Code = filterVoid, STRIP_Code = filterStrip  )
    filters = filterAlg.filters('Filters')
    filters.reverse()
    daVinci.EventPreFilters = filters

    reco_daughters = [ h1type, h2type, 'Lambda0' ]

    from B2KShh.RecoAlgo import B2KShhReco

    gaudi = appMgr()

    algLb2LhhDD = B2KShhReco(
        'Lb2LhhDD',
        reco_daughters,
        extended_hypos, False, False, False, 
        RootInTES = teslocation,
        Inputs = [ inputLocationDD ]
        )
    algLb2LhhLL = B2KShhReco(
        'Lb2LhhLL',
        reco_daughters,
        extended_hypos, False, False, False, 
        RootInTES = teslocation,
        Inputs = [ inputLocationLL ]
        )

    userSeq = gaudi.algorithm('GaudiSequencer/DaVinciUserSequence' , True )

    userSeq.Members += [ algLb2LhhDD.name() ]
    userSeq.Members += [ algLb2LhhLL.name() ]

    return SUCCESS

#############

if '__main__' == __name__ :

    datafiles = [
            #'PFN:/data/lhcb/phsdba/B2KShh/DST/Data/2012-Stripping20-MagDown/00020198_00000224_1.bhadron.mdst'
            'PFN:/data/lhcb/phsdba/B2KShh/DST/Data/2012-Stripping20r0p2-MagDown/00030264_00004002_1.bhadron.mdst'
    ]

    pars = {}
    #pars[ 'stripping' ] = 'Stripping20'
    pars[ 'stripping' ] = 'Stripping20r0p2'
    pars[ 'magtype' ]   = 'MagDown'

    configure( datafiles, params = pars, castor=False )

    run(-1)

#############

