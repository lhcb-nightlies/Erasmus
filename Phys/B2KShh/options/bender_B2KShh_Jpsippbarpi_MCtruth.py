#!/usr/bin/env python

"""
Bender module to run the following sequence over B2KShh signal MC samples:
- run an algorithm to store the MC truth DP position (and other info) for all generated events
"""

from Bender.Main import *

def configure( datafiles, catalogues = [] ) :

    #======= B->KShh Configuration and Setup =========#
    mode = 'Bu2Jpsippbarpi'
    whichMC = 'MC11'
    magtype = 'MagDown'
    isXGen = False
    #=================================================#

    knownMCTypes = [ 'MC10', 'MC11' ]

    if whichMC not in knownMCTypes :
        e = Exception('Unknown MC version')
        raise e

    dddbTag = {}
    dddbTag['MC10'] = 'head-20101206'
    dddbTag['MC11'] = 'head-20111102'

    conddbTag = {}
    conddbTag['MC10'] = {}
    conddbTag['MC10']['MagUp'] = 'sim-20101210-vc-mu100'
    conddbTag['MC10']['MagDown'] = 'sim-20101210-vc-md100'
    conddbTag['MC11'] = {}
    conddbTag['MC11']['MagUp'] = 'sim-20111111-vc-mu100'
    conddbTag['MC11']['MagDown'] = 'sim-20111111-vc-md100'

    from Configurables import DaVinci

    daVinci = DaVinci()
    daVinci.DataType        = '20'+whichMC[2:]
    daVinci.Simulation      = True
    daVinci.Lumi            = False
    daVinci.DDDBtag         = dddbTag[whichMC]
    daVinci.CondDBtag       = conddbTag[whichMC][magtype]
    daVinci.InputType       = "DST"
    daVinci.TupleFile       = mode+'-'+whichMC+'-'+magtype+'-MCTruth.root'
    daVinci.EvtMax          = -1

    setData( datafiles, catalogues )

    gaudi = appMgr()

    from B2KShh.DecayParticle import DecayParticle
    muplus = DecayParticle( -13, 0, False, 'mup' )
    muminus = DecayParticle( 13, 0, False, 'mum'  )
    proton = DecayParticle( 2212, 0, False, 'p' )
    antiproton = DecayParticle( -2212, 0, False, 'pbar' )
    pion = DecayParticle( 211, 0, True, 'pi' )
    jpsi = DecayParticle( 443, 2, False, 'Jpsi' )
    jpsi.addDaughter( muplus )
    jpsi.addDaughter( muminus )
    bdecay = DecayParticle( 521, 4, False, 'B' )
    bdecay.addDaughter( jpsi )
    bdecay.addDaughter( proton )
    bdecay.addDaughter( antiproton )
    bdecay.addDaughter( pion )

    from B2KShh.GenericMCTruthAlgo import GenericMCTruth
    
    algGenMCTrue = GenericMCTruth( mode, bdecay, isXGen )

    userSeq = gaudi.algorithm('GaudiSequencer/DaVinciUserSequence' , True )
    userSeq.Members += [ algGenMCTrue.name() ]

    return SUCCESS 

#############

if '__main__' == __name__ :

    datafiles = [
#    "DATAFILE='PFN:/data/lhcb/phsdba/B2KShh/DST/MC11/Bd2KSpipi-MagUp/00015443_00000019_1.allstreams.dst' TYP='POOL_ROOTTREE' OPT='READ'"
#    "DATAFILE='PFN:/afs/cern.ch/user/a/amartens/workspace/public/00020550_00000054_1.xgen' TYP='POOL_ROOTTREE' OPT='READ'"
    "DATAFILE='PFN:/afs/cern.ch/work/t/tlatham/test-DSTs/MC11a/12145041-MagDown/00018974_00000005_1.allstreams.dst' TYP='POOL_ROOTTREE' OPT='READ'"

    ]

    configure(datafiles)
    run(-1)

#############

