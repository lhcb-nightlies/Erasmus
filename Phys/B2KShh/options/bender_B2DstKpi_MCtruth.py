#!/usr/bin/env python

"""
Bender module to run the following sequence over B+ -> Dst- K+ pi+ signal MC samples:
- run an algorithm to store the MC truth info for all generated events
"""

from Bender.Main import *

def configure( datafiles, catalogues = [], params = {}, castor = False ) :

    #======= B->Dsthh Configuration and Setup =========#
    mode      = params.get( 'mode',      'B2DstKpi' )
    dataType  = params.get( 'dataType',  '2011'     )
    magType   = params.get( 'magType',   'MagDown'  )
    isXGen    = params.get( 'isXGen',    True       )
    dddbTag   = params.get( 'dddbtag',   ''         )
    conddbTag = params.get( 'conddbtag', ''         )
    #==================================================#

    knownDataTypes = [ '2011', '2012' ]

    if dataType not in knownDataTypes :
        e = Exception('Unknown data type')
        raise e

    dddbTags = {}
    dddbTags['2011'] = 'Sim08-20130503'
    dddbTags['2012'] = 'Sim08-20130503-1'

    conddbTags = {}
    conddbTags['2011'] = {}
    conddbTags['2011']['MagUp'] = 'Sim08-20130503-vc-mu100'
    conddbTags['2011']['MagDown'] = 'Sim08-20130503-vc-md100'
    conddbTags['2012'] = {}
    conddbTags['2012']['MagUp'] = 'Sim08-20130503-1-vc-mu100'
    conddbTags['2012']['MagDown'] = 'Sim08-20130503-1-vc-md100'

    from Configurables import DaVinci

    daVinci = DaVinci()
    daVinci.DataType        = dataType
    daVinci.Simulation      = True
    daVinci.Lumi            = False
    if dddbTag != '' :
        daVinci.DDDBtag     = dddbTag
    else :
        daVinci.DDDBtag     = dddbTags[dataType]
    if conddbTag != '' :
        daVinci.CondDBtag   = conddbTag
    else :
        daVinci.CondDBtag   = conddbTags[dataType][magType]
    daVinci.InputType       = "DST"
    daVinci.TupleFile       = mode+'-'+dataType+'-'+magType+'-MCTruth.root'
    daVinci.EvtMax          = -1

    setData( datafiles, catalogues )

    gaudi = appMgr()

    from B2KShh.DecayParticle import DecayParticle
    ddaug_kaon = DecayParticle(  321, 0, False, 'Ddau_K' )
    ddaug_pion = DecayParticle( -211, 0, False, 'Ddau_pi' )
    d0bar = DecayParticle( -421, 2, False, 'D' )
    d0bar.addDaughter( ddaug_kaon )
    d0bar.addDaughter( ddaug_pion )
    slow_pion = DecayParticle( -211, 0, False, 'Dst_pi' )
    dstar = DecayParticle( -413, 2, False, 'Dst' )
    dstar.addDaughter( d0bar )
    dstar.addDaughter( slow_pion )
    kaon = DecayParticle( 321, 0, False, 'K' )
    pion = DecayParticle( 211, 0, False, 'pi' )
    bdecay = DecayParticle( 521, 3, True, 'B' )
    bdecay.addDaughter( dstar )
    bdecay.addDaughter( kaon )
    bdecay.addDaughter( pion )

    from B2KShh.GenericMCTruthAlgo import GenericMCTruth
    
    algGenMCTrue = GenericMCTruth( mode, bdecay, isXGen )

    userSeq = gaudi.algorithm('GaudiSequencer/DaVinciUserSequence' , True )
    userSeq.Members += [ algGenMCTrue.name() ]

    return SUCCESS 

#############

if '__main__' == __name__ :

    datafiles = [
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-0-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-1000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-2000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-3000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-4000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-5000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-6000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-7000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-8000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-9000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-10000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-11000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-12000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-13000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-14000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-15000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-16000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-17000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-18000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-19000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-20000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-21000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-22000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-23000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-24000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-25000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-26000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-27000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-28000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-29000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-30000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-31000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-32000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-33000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-34000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-35000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-36000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-37000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-38000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-39000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-40000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-41000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-42000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-43000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-44000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-45000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-46000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-47000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-48000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-49000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-50000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-51000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-52000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-53000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-54000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-55000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-56000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-57000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-58000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-59000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-60000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-61000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-62000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-63000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-64000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-65000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-66000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-67000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-68000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-69000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-70000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-71000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-72000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-73000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-74000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-75000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-76000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-77000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-78000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-79000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-80000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-81000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-82000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-83000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-84000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-85000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-86000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-87000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-88000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-89000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-90000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-91000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-92000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-93000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-94000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-95000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-96000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-97000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-98000-12165081-LHCbAcceptance.xgen',
            '/data/lhcb/phsdba/B2DstKpi/2011/LHCbAcceptance/GaussJob-99000-12165081-LHCbAcceptance.xgen',
    ]

    configure(datafiles)
    run(-1)

#############

