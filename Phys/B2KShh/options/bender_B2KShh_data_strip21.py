#!/usr/bin/env python

"""
Bender module to run the following sequence over B2KShh stripped data samples:
- run an algorithm to store the reco info for all Stripped candidates
"""

from Bender.Main import *
from Gaudi.Configuration import *

def configure( datafiles, catalogues = [], params = {}, castor = False ) :

    #======= B->KShh Configuration and Setup =========#
    magtype        = params.get( 'magtype',   'MagDown'     )
    whichStripping = params.get( 'stripping', 'Stripping21' )
    extended_hypos = params.get( 'extended_hypos', False )
    #=================================================#

    knownMagTypes = [ 'MagDown', 'MagUp' ]

    if magtype not in knownMagTypes :
        e = Exception('Unsupported magnet setting: ' + magtype)
        raise e

    known2011StrippingVersions = [ 'Stripping21r1' ]
    known2012StrippingVersions = [ 'Stripping21'   ]

    whichData = '' 
    if whichStripping in known2011StrippingVersions:
        whichData = '2011'
    elif whichStripping in known2012StrippingVersions:
        whichData = '2012'
    else :
        e = Exception('Unsupported Stripping version: ' + whichStripping)
        raise e
    
    from Configurables import DaVinci
    
    daVinci = DaVinci()
    daVinci.DataType        = whichData
    daVinci.Simulation      = False
    daVinci.Lumi            = True
    daVinci.InputType       = "MDST"
    daVinci.TupleFile       = 'B2KShh-Collision'+whichData[2:]+'-'+magtype+'-'+whichStripping+'.root'
    daVinci.EvtMax          = -1
    daVinci.PrintFreq       = 100000

    from Configurables import CondDB
    CondDB().LatestGlobalTagByDataType = whichData

    setData( datafiles, catalogues )

    stream = {}
    stream['Stripping21']      = 'Bhadron'
    stream['Stripping21r1']    = 'Bhadron'
    
    teslocation = '/Event/'+stream[whichStripping]+'/'
    daVinci.RootInTES = teslocation

    filterStrip = "HLT_PASS_RE('.*B2KShh.*')"

    h1type = 'pi'
    h2type = 'pi'

    algNamesAndLocations = {}
    if whichData == '2011' :
        algNamesAndLocations['B2KShh_DD_2011'] = 'Phys/B2KShh_MVAFilter_DD_2011_OS/Particles'
        algNamesAndLocations['B2KShh_LL_2011'] = 'Phys/B2KShh_MVAFilter_LL_2011_OS/Particles'
    else :
        algNamesAndLocations['B2KShh_DD_2012a'] = 'Phys/B2KShh_MVAFilter_DD_2012a_OS/Particles'
        algNamesAndLocations['B2KShh_LL_2012a'] = 'Phys/B2KShh_MVAFilter_LL_2012a_OS/Particles'
        algNamesAndLocations['B2KShh_DD_2012b'] = 'Phys/B2KShh_MVAFilter_DD_2012b_OS/Particles'
        algNamesAndLocations['B2KShh_LL_2012b'] = 'Phys/B2KShh_MVAFilter_LL_2012b_OS/Particles'

    from PhysConf.Filters import LoKi_Filters

    filterVoid  = "EXISTS('/Event/Strip/Phys/DecReports')"
    filterAlg = LoKi_Filters( VOID_Code = filterVoid, STRIP_Code = filterStrip  )
    filters = filterAlg.filters('Filters')
    filters.reverse()
    daVinci.EventPreFilters = filters

    reco_daughters = [ h1type, h2type, 'KS0' ]

    from B2KShh.RecoAlgo import B2KShhReco

    gaudi = appMgr()

    algs = []

    for algName,inputLoc in algNamesAndLocations.items() :
        alg = B2KShhReco(   algName,
                            reco_daughters,
                            extended_hypos, False, False, False,
                            RootInTES = teslocation,
                            Inputs = [ inputLoc ]
                        )
        algs.append( alg )

    userSeq = gaudi.algorithm('GaudiSequencer/DaVinciUserSequence' , True )

    for alg in algs :
        userSeq.Members += [ alg.name() ]

    return SUCCESS

#############

if '__main__' == __name__ :

    datafiles = [
            #'/data/lhcb/phsdba/B2KShh/DST/Data/2012-Stripping20-MagDown/00020198_00000002_1.bhadroncompleteevent.dst'
            #'/data/lhcb/phsdba/B2KShh/DST/Data/2012-Stripping20r0p1-MagDown/00024183_00000017_1.bhadroncompleteevent.dst'
            '/data/lhcb/phsdba/B2KShh/DST/Data/2012-Stripping21/00041836_00000642_1.bhadron.mdst'
    ]

    pars = {}
    pars[ 'stripping' ] = 'Stripping21'
    pars[ 'magtype' ]   = 'MagDown'

    configure( datafiles, params = pars, castor=False )

    run(-1)

#############

