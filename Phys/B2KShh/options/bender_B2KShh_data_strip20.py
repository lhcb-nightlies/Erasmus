#!/usr/bin/env python

"""
Bender module to run the following sequence over B2KShh stripped data samples:
- run an algorithm to store the reco info for all Stripped candidates
"""

from Bender.Main import *
from Gaudi.Configuration import *

def configure( datafiles, catalogues = [], params = {}, castor = False ) :

    #======= B->KShh Configuration and Setup =========#
    magtype        = params.get( 'magtype',   'MagDown'     )
    whichStripping = params.get( 'stripping', 'Stripping20' )
    extended_hypos = params.get( 'extended_hypos', False )
    #=================================================#

    knownMagTypes = [ 'MagDown', 'MagUp' ]

    if magtype not in knownMagTypes :
        e = Exception('Unsupported magnet setting: ' + magtype)
        raise e

    known2011StrippingVersions = [ 'Stripping20r1', 'Stripping20r1p1' ]
    known2012StrippingVersions = [ 'Stripping20',   'Stripping20r0p1' ]

    whichData = '' 
    if whichStripping in known2011StrippingVersions:
        whichData = '2011'
    elif whichStripping in known2012StrippingVersions:
        whichData = '2012'
    else :
        e = Exception('Unsupported Stripping version: ' + whichStripping)
        raise e
    
    from Configurables import DaVinci
    
    daVinci = DaVinci()
    daVinci.DataType        = whichData
    daVinci.Simulation      = False
    daVinci.Lumi            = True
    daVinci.InputType       = "DST"
    daVinci.TupleFile       = 'B2KShh-Collision'+whichData[2:]+'-'+magtype+'-'+whichStripping+'.root'
    daVinci.EvtMax          = -1
    daVinci.PrintFreq       = 100000

    from Configurables import CondDB
    CondDB().LatestGlobalTagByDataType = whichData

    setData( datafiles, catalogues )

    stream = {}
    stream['Stripping20']      = 'BhadronCompleteEvent'
    stream['Stripping20r1']    = 'BhadronCompleteEvent'
    stream['Stripping20r0p1']  = 'BhadronCompleteEvent'
    stream['Stripping20r1p1']  = 'BhadronCompleteEvent'
    
    teslocation = '/Event/'+stream[whichStripping]+'/'

    filterStrip = "HLT_PASS_RE('.*B2KShh.*')"

    inputLocationDD = 'Phys/B2KShhDDLine/Particles'
    inputLocationLL = 'Phys/B2KShhLLLine/Particles'

    h1type = 'pi'
    h2type = 'pi'

    if whichStripping == 'Stripping20r1p1' or whichStripping == 'Stripping20r0p1' :
        h1type = 'K'
        filterStrip = "HLT_PASS_RE('StrippingB2KSKpi[DL]{2}LineDecision')"
        inputLocationDD = 'Phys/B2KSKpiDDLine/Particles'
        inputLocationLL = 'Phys/B2KSKpiLLLine/Particles'

    from PhysConf.Filters import LoKi_Filters

    filterVoid  = "EXISTS('/Event/Strip/Phys/DecReports')"
    filterAlg = LoKi_Filters( VOID_Code = filterVoid, STRIP_Code = filterStrip  )
    filters = filterAlg.filters('Filters')
    filters.reverse()
    daVinci.EventPreFilters = filters

    reco_daughters = [ h1type, h2type, 'KS0' ]

    from B2KShh.RecoAlgo import B2KShhReco

    gaudi = appMgr()

    algB2KShhDD = B2KShhReco(
        'B2KShhDD',
        reco_daughters,
        extended_hypos, False, False, False,
        Inputs = [ teslocation+inputLocationDD ]
        )
    algB2KShhLL = B2KShhReco(
        'B2KShhLL',
        reco_daughters,
        extended_hypos, False, False, False,
        Inputs = [ teslocation+inputLocationLL ]
        )

    userSeq = gaudi.algorithm('GaudiSequencer/DaVinciUserSequence' , True )

    userSeq.Members += [ algB2KShhDD.name() ]
    userSeq.Members += [ algB2KShhLL.name() ]

    return SUCCESS

#############

if '__main__' == __name__ :

    datafiles = [
            '/data/lhcb/phsdba/B2KShh/DST/Data/2012-Stripping20-MagDown/00020198_00000002_1.bhadroncompleteevent.dst'
            #'/data/lhcb/phsdba/B2KShh/DST/Data/2012-Stripping20r0p1-MagDown/00024183_00000017_1.bhadroncompleteevent.dst'
    ]

    pars = {}
    pars[ 'stripping' ] = 'Stripping20'
    pars[ 'magtype' ]   = 'MagDown'

    configure( datafiles, params = pars, castor=False )

    run(-1)

#############

