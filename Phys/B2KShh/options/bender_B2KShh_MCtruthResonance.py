#!/usr/bin/env python

"""
Bender module to run the following sequence over B2KShh signal MC samples:
- run an algorithm to store the MC truth DP position (and other info) for all generated events
"""

from Bender.Main import *

def configure( datafiles, catalogues = [] ) :

    #======= B->KShh Configuration and Setup =========#
    btype  = 'Bs'
    restype = 'Kst0'
    bachtype = 'KS0'
    resdaug1 = 'K+'
    resdaug2 = 'pi-'
    mode = btype+'2'+restype+bachtype
    whichMC = 'MC11a'
    magtype = 'MagDown'
    #=================================================#

    knownMCTypes = [ 'MC10', 'MC11a' ]

    if whichMC not in knownMCTypes :
        e = Exception('Unknown MC version')
        raise e

    dddbTag = {}
    dddbTag['MC10']  = 'head-20101206'
    dddbTag['MC11a'] = 'head-20111102'

    conddbTag = {}
    conddbTag['MC10'] = {}
    conddbTag['MC10']['MagUp'] = 'sim-20101210-vc-mu100'
    conddbTag['MC10']['MagDown'] = 'sim-20101210-vc-md100'
    conddbTag['MC11a'] = {}
    conddbTag['MC11a']['MagUp'] = 'sim-20111111-vc-mu100'
    conddbTag['MC11a']['MagDown'] = 'sim-20111111-vc-md100'

    from Configurables import DaVinci

    daVinci = DaVinci()
    daVinci.DataType        = '20'+whichMC[2:4]
    daVinci.Simulation      = True
    daVinci.Lumi            = False
    daVinci.DDDBtag         = dddbTag[whichMC]
    daVinci.CondDBtag       = conddbTag[whichMC][magtype]
    daVinci.InputType       = "DST"
    daVinci.TupleFile       = mode+'-'+whichMC+'-'+magtype+'-MCTruth.root'
    daVinci.EvtMax          = -1

    setData( datafiles, catalogues )

    gaudi = appMgr()

    from B2KShh.MCTruthAlgoResonance import B2KShhMCTruthResonance
    
    algGenMCTrue = B2KShhMCTruthResonance( mode, btype, restype, bachtype, resdaug1, resdaug2 )

    userSeq = gaudi.algorithm('GaudiSequencer/DaVinciUserSequence' , True )
    userSeq.Members += [ algGenMCTrue.name() ]

    return SUCCESS 

#############

if '__main__' == __name__ :

    datafiles = [
    'PFN:/data/lhcb/phsdba/B2KShh/DST/MC11/Bs2Kst0KS-MagDown/00016206_00000001_1.allstreams.dst'
    ]

    configure(datafiles)
    run(-1)

#############

