
from Bender.Main import *
from Bender.MainMC import *

class DecayParticle(object) :

    def __init__( self, pdgCode, ndaug_expected, pluscc, branch_name ) :

        self.pdgCode = pdgCode
        self._ndaug_expected = ndaug_expected
        self.daughters = []
        self.pluscc = pluscc
        self.branch_name = branch_name
        self.special = False
        self.match = False


    def __cmp__( self, other ) :

        assert isinstance(other, DecayParticle)
        return cmp(self.pdgCode, other.pdgCode)


    def addDaughter( self, daughter ) :

        if len( self.daughters ) == self._ndaug_expected :
            print "Problem adding daughter - already have expected number"
            return

        self.daughters.append( daughter )
        self.daughters.sort()


    def specialDescriptor( self, special = True ) :

        self.special = special


    def decayDescriptor( self, partpropsvc, conj = False ) :

        decay_descriptor = ''

        partid = LHCb.ParticleID( self.pdgCode )
        partprop = partpropsvc.find( partid )
        partname = partprop.name()

        if not self.special :
            if conj :
                antipartprop = partprop.anti()
                partname = antipartprop.name()

            if len( self.daughters ) == 0 :
                decay_descriptor = partname
            else :
                decay_descriptor = '( '+partname+' => '

                for daug in self.daughters :

                    decay_descriptor += daug.decayDescriptor( partpropsvc, conj )
                    decay_descriptor += ' '

                decay_descriptor += ')'
        else :
            selfconj = partprop.selfcc()
            if selfconj :
                ddhead = partname+' => '
            else :
                antipartprop = partprop.anti()
                antipartname = antipartprop.name()
                ddhead = '( '+partname+' | '+antipartname+' ) => '

            ddpart1 = '( '+ddhead
            ddpart2 = '( '+ddhead
            for daug in self.daughters :
                ddpart1 += daug.decayDescriptor( partpropsvc, conj = False )
                ddpart2 += daug.decayDescriptor( partpropsvc, conj = True  )
            ddpart1 += ' )'
            ddpart2 += ' )'
            decay_descriptor = '['+ddpart1+', '+ddpart2+']'

        return decay_descriptor

    def newEvent( self ) :

        self.match = False

        for daug in self.daughters :
            daug.newEvent()


    def fillTuple( self, cand, funcs, conj ) :

        unmatchedbranches = []

        # if we are not matched, add our branch name to the list of unmatched branches
        # and those of all our daughters (since we make no attempt to match them)
        if not self.match :
            unmatchedbranches += self.branch_name
            for daug in self.daughters :
                unmatchedbranches += daug.fillTuple( cand, funcs, conj )
            return unmatchedbranches

        # fill our info in the tuple
        for func in funcs :
            func( cand, self.branch_name )

        # if we have no daughters there is nothing more to do
        if len(self.daughters) == 0 :
            return unmatchedbranches

        # otherwise try and match our daughters with those of the cand
        for cand_daug in cand.children( True ) :
            cand_daug_ID = cand_daug.particleID().pid()
            for daug in self.daughters :
                if ( not conj and (cand_daug_ID == daug.pdgCode) ) or ( conj and (-cand_daug_ID == daug.pdgCode) ) :
                    if not daug.match :
                        daug.match = True
                        unmatchedbranches += daug.fillTuple( cand_daug, funcs, conj )
                        break

        # check whether any of our daughters have not been matched and gather their branch names
        for daug in self.daughters :
            if not daug.match :
                unmatchedbranches += daug.fillTuple( cand_daug, funcs, conj )

        return unmatchedbranches


    def fillTupleHead( self, cand, funcs ) :

        self.newEvent()

        unmatchedbranches = []

        # do we match the head candidate, if not bail out immediately
        candID = cand.particleID().pid()
        if abs(candID) != abs(self.pdgCode) :
            return False

        # set ourselves as matched
        self.match = True

        # do we exactly match or is the candidate our conjugate
        conj = False
        if candID == (-1*self.pdgCode) :
            conj = True

        # now do the recursive fill and match-daughters
        unmatchedbranches = self.fillTuple( cand, funcs, conj )

        # any particles in the decay tree that report as unmatched need their
        # branches to be filled with error values
        for branch in unmatchedbranches :
            for func in funcs :
                func( None, branch )

        return True






