#!/usr/bin/env python

from Bender.Main import *
from Bender.MainMC import *
from ROOT import TLorentzVector
import functools


# Begin GenericMCTruth Class Definition

class GenericMCTruth(AlgoMC) :

    """
    Algorithm to perform ntupling of MC truth information.
    """

    def __init__( self, name, decayHead, isXGen, **kwargs ) :
        super(GenericMCTruth,self).__init__( name, **kwargs )

        self.decay_head = decayHead
        self.xgen = isXGen


    def form_decay_descriptor( self ) :
        """
        Write the decay descriptor for the decay mode of the MC being run over.
        """

        self.par_id        = LHCb.ParticleID( self.decay_head.pdgCode )
        self.par_conj_id   = self.partpropsvc.find( self.par_id ).anti().pid()
        self.par_selfconj  = self.partpropsvc.find( self.par_id ).selfcc()
        self.par_pluscc    = self.decay_head.pluscc

        par_name      = self.partpropsvc.find( self.par_id ).name()
        par_conj_name = self.partpropsvc.find( self.par_id ).anti().name()

        if self.par_selfconj :
            self.Info( 'Forming decay descriptor for decay of self-conjugate particle: %s' % par_name )
        else :
            self.Info( 'Forming decay descriptor for decays of %s and %s' % ( par_name, par_conj_name ) )

        if self.decay_head.special :
            self.decay_descriptor = self.decay_head.decayDescriptor( self.partpropsvc )
        else :
            self.decay_descriptor = '[ '
            self.decay_descriptor += self.decay_head.decayDescriptor( self.partpropsvc )
            if self.par_selfconj :
                self.decay_descriptor += ' ]'
            else :
                self.decay_descriptor += ' ]CC'

        self.Info( 'Will use the decay descriptor '+self.decay_descriptor )


    def initialize( self ) :

        sc = AlgoMC.initialize( self )
        if sc.isFailure() :
            return sc

        # get the particle property service
        self.partpropsvc = self.ppSvc()

        # form the decay descriptor
        self.form_decay_descriptor()

        # set up the reconstrucible/reconstructed tools
        if not self.xgen : 
            self.recible = self.tool( cpp.IMCReconstructible, 'MCReconstructible' )
            self.rected  = self.tool( cpp.IMCReconstructed,   'MCReconstructed'   )

        return SUCCESS


    def reco_status_tuple( self, tuple, mcparticle, name ) :
        """
        Store the reconstructible/reconstructed status of an MC particle
        """

        if self.xgen :
            return

        if not mcparticle :
            tuple.column_int( name + '_Reconstructible', -1 )
            tuple.column_int( name + '_Reconstructed',   -1 )
            return

        cat_ible = self.recible.reconstructible( mcparticle )
        cat_ted  = self.rected.reconstructed( mcparticle )

        tuple.column_int( name + '_Reconstructible', int(cat_ible) )
        tuple.column_int( name + '_Reconstructed',   int(cat_ted)  )


    def mc_p4_tuple( self, tuple, mcparticle, name ) :
        """
        Store the id, charge, 4-momentum, mass, p_t and eta of an MC particle
        """

        if not mcparticle :
            tuple.column_int(    name + '_TRUEID',    -1   )
            tuple.column_int(    name + '_TRUEQ',     -1   )
            tuple.column_double( name + '_TRUEP',     -1.1 )
            tuple.column_double( name + '_TRUEPE',    -1.1 )
            tuple.column_double( name + '_TRUEPX',    -1.1 )
            tuple.column_double( name + '_TRUEPY',    -1.1 )
            tuple.column_double( name + '_TRUEPZ',    -1.1 )
            tuple.column_double( name + '_TRUEPT',    -1.1 )
            tuple.column_double( name + '_TRUEETA',   -1.1 )
            tuple.column_double( name + '_TRUEPHI',   -1.1 )
            tuple.column_double( name + '_TRUETHETA', -1.1 )
            tuple.column_double( name + '_TRUEM',     -1.1 )
            tuple.column_int(    name + '_OSCIL',     -1   )
            tuple.column_int(    name + '_NDAUG',     -1   )
            return

        tuple.column_int(    name + '_TRUEID',    int(MCID(mcparticle))         )
        tuple.column_int(    name + '_TRUEQ',     int(MC3Q(mcparticle)/3)       )
        tuple.column_double( name + '_TRUEP',     MCP(mcparticle)               )
        tuple.column_double( name + '_TRUEPE',    MCE(mcparticle)               )
        tuple.column_double( name + '_TRUEPX',    MCPX(mcparticle)              )
        tuple.column_double( name + '_TRUEPY',    MCPY(mcparticle)              )
        tuple.column_double( name + '_TRUEPZ',    MCPZ(mcparticle)              )
        tuple.column_double( name + '_TRUEPT',    MCPT(mcparticle)              )
        tuple.column_double( name + '_TRUEETA',   MCETA(mcparticle)             )
        tuple.column_double( name + '_TRUEPHI',   MCPHI(mcparticle)             )
        tuple.column_double( name + '_TRUETHETA', MCTHETA(mcparticle)           )
        tuple.column_double( name + '_TRUEM',     MCM(mcparticle)               )
        tuple.column_int(    name + '_OSCIL',     int(MCOSCILLATED(mcparticle)) )
        tuple.column_int(    name + '_NDAUG',     mcparticle.nChildren()        )


    def mc_vtx_tuple( self, tuple, mcparticle, name ) :
        """
        Store vertex and lifetime info for the MC particle
        """

        if not mcparticle :
            tuple.column_double( name + '_TRUEORIGINVERTEX_X', -1.1 )
            tuple.column_double( name + '_TRUEORIGINVERTEX_Y', -1.1 )
            tuple.column_double( name + '_TRUEORIGINVERTEX_Z', -1.1 )
            tuple.column_double( name + '_TRUECTAU'          , -1.1 )
            return

        tuple.column_double( name + '_TRUEORIGINVERTEX_X', MCVFASPF(MCVX)(mcparticle) )
        tuple.column_double( name + '_TRUEORIGINVERTEX_Y', MCVFASPF(MCVY)(mcparticle) )
        tuple.column_double( name + '_TRUEORIGINVERTEX_Z', MCVFASPF(MCVZ)(mcparticle) )
        tuple.column_double( name + '_TRUECTAU'          , MCCTAU(mcparticle)         )


    def analyse( self ) :
        """
        The method called in the event loop
        """

        # select MC particles
        cands  = self.mcselect('cands',  self.decay_descriptor)
        ncands = cands.size()

        if 0 == ncands :
            self.Warning( 'No MC candidates found in this event', SUCCESS )
            return SUCCESS

        # get the event header
        if not self.xgen:
            evthdr = self.get( '/Event/Rec/Header' )
        else :
            evthdr = self.get( '/Event/Gen/Header' )
            
        # create the ntuple
        tuple = self.nTuple( 'tupleMCTruth' )

        # loop through the candidates
        for cand in cands :

            # fill event information
            tuple.column_int( 'runNumber', evthdr.runNumber() )
            tuple.column_int( 'evtNumber', evthdr.evtNumber() )
            tuple.column_int( 'nCands',    ncands             )

            # assemble the list of functions to be called for each candidate in the tree
            funcs = [ functools.partial( self.mc_p4_tuple, tuple ), functools.partial( self.mc_vtx_tuple, tuple ) ]
            if not self.xgen :
                funcs.append( functools.partial( self.reco_status_tuple, tuple ) )

            # fill the tuple
            ok = self.decay_head.fillTupleHead( cand, funcs )

            if ok :
                tuple.write()
            else :
                self.Warning( 'Problem filling tuple, event will not be stored', SUCCESS )


        return SUCCESS

# End of GenericMCTruth Class Definition

