#!/usr/bin/env python

from Bender.Main import * 
from Bender.MainMC import *
import math
from ROOT import ( Double, TMath )
from B2KShh.ThreeBodyKinematics import ThreeBodyKinematics

# Begin B2KShhReco Class Definition

class B2KShhReco(AlgoMC) :

    """
    Algorithm to perform ntupling (and potentially offline selection) for
    B2KShh analyses.
    Reads stripped candidates from the DST and stores useful info in an ntuple.
    Each instance of this algorithm creates a single ntuple, which contains
    information on all (considered) final states, but only for KS's composed of
    either DD, LD or LL tracks.  So one would need 3 instances of this
    algorithm in a job to create ntuples for all possible KS categories.
    """

    def __init__( self, name, reco_daughters, use_extended_hypothesis_set = False, wrongsign = False, simulation = False, signalmc = False, mc_daughters = [], mc_decay_descriptors = [], **kwargs ) :
        super(B2KShhReco,self).__init__( name, **kwargs )

        self.simplenames = {}
        self.simplenames[511]   = 'Bd'
        self.simplenames[531]   = 'Bs'
        self.simplenames[5122]  = 'Lb'
        self.simplenames[310]   = 'KS'
        self.simplenames[321]   = 'K'
        self.simplenames[-321]  = 'K'
        self.simplenames[211]   = 'pi'
        self.simplenames[-211]  = 'pi'
        self.simplenames[2212]  = 'p'
        self.simplenames[-2212] = 'p'
        self.simplenames[3122]  = 'Lz'
        self.simplenames[-3122] = 'Lz'

        self.simulation = simulation
        self.signalmc = False
        if self.simulation :
            self.signalmc = signalmc

        self.wrongsign = wrongsign
        
        self.input_mc_dds = mc_decay_descriptors

        self.set_daug_types( reco_daughters, use_extended_hypothesis_set )

        self.set_mc_daug_types( mc_daughters )


    def set_mc_daug_types( self, mc_daughters ) :
        """
        Define the MC-truth decay mode
        """

        self.mcgdaugnames = {}

        self.mcdaug1ID = None
        self.mcdaug2ID = None
        self.mcdaug3ID = None
        self.mcdaug3ConjID = None

        if not self.signalmc :
            return

        if len(mc_daughters) != 3 :
            self.Warning( 'MC daughters not supplied, will assume they are the same as the reco daughters!', SUCCESS )
            self.mcgdaugnames = dict( self.gdaugnames )
            self.mcdaug1ID = LHCb.ParticleID(self.daug1ID)
            self.mcdaug2ID = LHCb.ParticleID(self.daug2ID)
            self.mcdaug3ID = LHCb.ParticleID(self.daug3ID)
            self.mcdaug3ConjID = LHCb.ParticleID(self.daug3ConjID)
            return

        # determine the V0 type
        if mc_daughters[2] == 'KS' or mc_daughters[2] == 'KS0' :
            self.mcdaug3ID     = LHCb.ParticleID(310)
            self.mcdaug3ConjID = LHCb.ParticleID(310)
            self.mcgdaugnames[ 211] = 'KSpip'
            self.mcgdaugnames[-211] = 'KSpim'

        elif mc_daughters[2] == 'L' or mc_daughters[2] == 'Lambda0' :
            self.mcdaug3ID     = LHCb.ParticleID( 3122)
            self.mcdaug3ConjID = LHCb.ParticleID(-3122)
            self.mcgdaugnames[ 2212] = 'Lzp'
            self.mcgdaugnames[-2212] = 'Lzp'
            self.mcgdaugnames[ 211] = 'Lzpi'
            self.mcgdaugnames[-211] = 'Lzpi'

        else :
            self.Warning( 'MC V0 type ('+mc_daughters[2]+') not recognised, setting to KS0.', SUCCESS )
            self.mcdaug3ID     = LHCb.ParticleID(310)
            self.mcdaug3ConjID = LHCb.ParticleID(310)
            self.mcgdaugnames[ 211] = 'KSpip'
            self.mcgdaugnames[-211] = 'KSpim'

        # determine the charged daughter types
        if mc_daughters[0] == 'pi' and mc_daughters[1] == 'pi':
            self.mcdaug1ID = LHCb.ParticleID( 211)
            self.mcdaug2ID = LHCb.ParticleID(-211)

        elif mc_daughters[0] == 'K' and mc_daughters[1] == 'K':
            self.mcdaug1ID = LHCb.ParticleID( 321)
            self.mcdaug2ID = LHCb.ParticleID(-321)

        elif mc_daughters[0] == 'p' and mc_daughters[1] == 'p':
            self.mcdaug1ID = LHCb.ParticleID( 2212)
            self.mcdaug2ID = LHCb.ParticleID(-2212)

        elif mc_daughters[0] == 'K' and mc_daughters[1] == 'pi':
            self.mcdaug1ID = LHCb.ParticleID( 321)
            self.mcdaug2ID = LHCb.ParticleID(-211)

        elif mc_daughters[0] == 'p' and mc_daughters[1] == 'pi':
            self.mcdaug1ID = LHCb.ParticleID(2212)
            self.mcdaug2ID = LHCb.ParticleID(-211)
            
        elif mc_daughters[0] == 'p' and mc_daughters[1] == 'K':
            self.mcdaug1ID = LHCb.ParticleID(2212)
            self.mcdaug2ID = LHCb.ParticleID(-321)

        else :
            self.Warning( 'MC hh types ('+mc_daughters[0]+' and '+mc_daughters[1]+') not recognised, setting to pions.' , SUCCESS )
            self.mcdaug1ID = LHCb.ParticleID( 211)
            self.mcdaug2ID = LHCb.ParticleID(-211)


    def set_daug_types( self, daughters, use_extended_hypothesis_set ) :
        """
        Define the decay mode to be selected
        """

        self.gdaugnames = {}

        self.selfconj = True

        self.daug1ID = None
        self.daug2ID = None
        self.daug3ID = None
        self.daug3ConjID = None

        self.hypotheses = []

        if len(daughters) != 3 :
            self.Error( 'Expected 3 reco daughters but have %d.  Cannot continue.' % len(daughters), FAILURE )

        if self.wrongsign :
            self.hypotheses = [ (211,211), (321,211), (321,321) ]
            if use_extended_hypothesis_set :
                extra_hypotheses = [ (2212,2212), (2212,211), (2212,321) ]
                self.hypotheses.extend( extra_hypotheses )
        else :
            self.hypotheses = [ (211,-211), (321,-211), (211,-321), (321,-321) ]
            if use_extended_hypothesis_set :
                extra_hypotheses = [ (2212,-2212), (2212,-211), (211,-2212), (2212,-321), (321,-2212) ]
                self.hypotheses.extend( extra_hypotheses )

        # determine the V0 type
        if daughters[2] == 'KS' or daughters[2] == 'KS0' :
            self.daug3ID     = LHCb.ParticleID(310)
            self.daug3ConjID = LHCb.ParticleID(310)
            self.gdaugnames[ 211] = 'KSpip'
            self.gdaugnames[-211] = 'KSpim'
            self.v0name = 'KS'

        elif daughters[2] == 'L' or daughters[2] == 'Lambda0' :
            self.daug3ID     = LHCb.ParticleID( 3122)
            self.daug3ConjID = LHCb.ParticleID(-3122)
            self.gdaugnames[ 2212] = 'Lzp'
            self.gdaugnames[-2212] = 'Lzp'
            self.gdaugnames[ 211] = 'Lzpi'
            self.gdaugnames[-211] = 'Lzpi'
            self.v0name = 'Lz'
            self.selfconj = False

        else :
            self.Warning( 'V0 type ('+daughters[2]+') not recognised, setting to KS0.', SUCCESS )
            self.daug3ID     = LHCb.ParticleID(310)
            self.daug3ConjID = LHCb.ParticleID(310)
            self.gdaugnames[ 211] = 'KSpip'
            self.gdaugnames[-211] = 'KSpim'
            self.v0name = 'KS'

        # determine the charged daughter types
        if  self.wrongsign :
            if daughters[0] == 'pi' and daughters[1] == 'pi':
                self.daug1ID = LHCb.ParticleID( 211 )
                self.daug2ID = LHCb.ParticleID( 211 )
                
            elif daughters[0] == 'K' and daughters[1] == 'K':
                self.daug1ID = LHCb.ParticleID( 321 )
                self.daug2ID = LHCb.ParticleID( 321 )
                
            elif daughters[0] == 'p' and daughters[1] == 'p':
                self.daug1ID = LHCb.ParticleID( 2212)
                self.daug2ID = LHCb.ParticleID( 2212)
                
            elif daughters[0] == 'K' and daughters[1] == 'pi':
                self.daug1ID = LHCb.ParticleID( 321)
                self.daug2ID = LHCb.ParticleID( 211)
                self.selfconj = False
                
            elif daughters[0] == 'p' and daughters[1] == 'pi':
                self.daug1ID = LHCb.ParticleID(2212)
                self.daug2ID = LHCb.ParticleID(211)
                self.selfconj = False

            elif daughters[0] == 'p' and daughters[1] == 'K':
                self.daug1ID = LHCb.ParticleID(2212)
                self.daug2ID = LHCb.ParticleID(321)
                self.selfconj = False
                
            else :
                self.Warning( 'hh types ('+daughters[0]+' and '+daughters[1]+') not recognised, setting to pions.' , SUCCESS )
                self.daug1ID = LHCb.ParticleID( 211)
                self.daug2ID = LHCb.ParticleID(211)
                
        else :
            if daughters[0] == 'pi' and daughters[1] == 'pi':
                self.daug1ID = LHCb.ParticleID( 211)
                self.daug2ID = LHCb.ParticleID(-211)

            elif daughters[0] == 'K' and daughters[1] == 'K':
                self.daug1ID = LHCb.ParticleID( 321)
                self.daug2ID = LHCb.ParticleID(-321)

            elif daughters[0] == 'p' and daughters[1] == 'p':
                self.daug1ID = LHCb.ParticleID( 2212)
                self.daug2ID = LHCb.ParticleID(-2212)

            elif daughters[0] == 'K' and daughters[1] == 'pi':
                self.daug1ID = LHCb.ParticleID( 321)
                self.daug2ID = LHCb.ParticleID(-211)
                self.selfconj = False

            elif daughters[0] == 'p' and daughters[1] == 'pi':
                self.daug1ID = LHCb.ParticleID(2212)
                self.daug2ID = LHCb.ParticleID(-211)
                self.selfconj = False
                
            elif daughters[0] == 'p' and daughters[1] == 'K':
                self.daug1ID = LHCb.ParticleID(2212)
                self.daug2ID = LHCb.ParticleID(-321)
                self.selfconj = False

            else :
                self.Warning( 'hh types ('+daughters[0]+' and '+daughters[1]+') not recognised, setting to pions.' , SUCCESS )
                self.daug1ID = LHCb.ParticleID( 211)
                self.daug2ID = LHCb.ParticleID(-211)


    def check_types( self ) :

        okB = [ -1, 0, 1 ]
        finalB = 0
        for id in ( self.daug1ID, self.daug2ID, self.daug3ID ) :
            if id.isBaryon() :
                if id.pid() > 0 :
                    finalB += 1
                else :
                    finalB -= 1

        if finalB not in okB :
            self.Error( 'Final state baryon number is unexpected value: '+str(finalB) )
            return FAILURE

        # TODO any other checks we can do here?

        return SUCCESS


    def form_mc_decay_descriptors( self ) :
        """
        Create the decay descriptors for reading the MC particles from the MC truth location.
        This is created based on the MC daughter types or uses the ones provided (if available).
        """

        if not self.signalmc :
            return

        if len(self.input_mc_dds) == 4 :
            # use the descriptors provided by the user
            self.mc_decay_descriptor_Xb = self.input_mc_dds[0]
            self.mc_decay_descriptor_hp = self.input_mc_dds[1]
            self.mc_decay_descriptor_hm = self.input_mc_dds[2]
            self.mc_decay_descriptor_V0 = self.input_mc_dds[3]

        else :
            daug1_name = self.partpropsvc.find( self.mcdaug1ID ).name()
            daug2_name = self.partpropsvc.find( self.mcdaug2ID ).name()
            daug3_name = self.partpropsvc.find( self.mcdaug3ID ).name()

            daug1_conj_name = self.partpropsvc.find( self.mcdaug1ID ).anti().name()
            daug2_conj_name = self.partpropsvc.find( self.mcdaug2ID ).anti().name()
            daug3_conj_name = self.partpropsvc.find( self.mcdaug3ID ).anti().name()

            if self.mcdaug1ID.abspid() == self.mcdaug2ID.abspid() and self.mcdaug3ID == self.ksID and not self.wrongsign:

                self.mc_decay_descriptor_Xb = '( Xb & X0 ) =>  %s  %s  %s' % (daug1_name, daug2_name, daug3_name)
                self.mc_decay_descriptor_hp = '( Xb & X0 ) => ^%s  %s  %s' % (daug1_name, daug2_name, daug3_name)
                self.mc_decay_descriptor_hm = '( Xb & X0 ) =>  %s ^%s  %s' % (daug1_name, daug2_name, daug3_name)
                self.mc_decay_descriptor_V0 = '( Xb & X0 ) =>  %s  %s ^%s' % (daug1_name, daug2_name, daug3_name)
            
            else :
                ddpart1 = '( ( Xb & X0 ) =>  %s  %s  %s )' % (daug1_name, daug2_name, daug3_name)
                ddpart2 = '( ( Xb & X0 ) =>  %s  %s  %s )' % (daug1_conj_name, daug2_conj_name, daug3_conj_name)
                self.mc_decay_descriptor_Xb = '[ %s, %s ]' % (ddpart1, ddpart2)

                ddpart1 = '( ( Xb & X0 ) => ^%s  %s  %s )' % (daug1_name, daug2_name, daug3_name)
                ddpart2 = '( ( Xb & X0 ) =>  %s ^%s  %s )' % (daug1_conj_name, daug2_conj_name, daug3_conj_name)
                self.mc_decay_descriptor_hp = '[ %s, %s ]' % (ddpart1, ddpart2)

                ddpart1 = '( ( Xb & X0 ) =>  %s ^%s  %s )' % (daug1_name, daug2_name, daug3_name)
                ddpart2 = '( ( Xb & X0 ) => ^%s  %s  %s )' % (daug1_conj_name, daug2_conj_name, daug3_conj_name)
                self.mc_decay_descriptor_hm = '[ %s, %s ]' % (ddpart1, ddpart2)

                ddpart1 = '( ( Xb & X0 ) =>  %s  %s ^%s )' % (daug1_name, daug2_name, daug3_name)
                ddpart2 = '( ( Xb & X0 ) =>  %s  %s ^%s )' % (daug1_conj_name, daug2_conj_name, daug3_conj_name)
                self.mc_decay_descriptor_V0 = '[ %s, %s ]' % (ddpart1, ddpart2)

        self.Info( 'Will use the MC decay descriptors:' )
        self.Info( self.mc_decay_descriptor_Xb )
        self.Info( self.mc_decay_descriptor_hp )
        self.Info( self.mc_decay_descriptor_hm )
        self.Info( self.mc_decay_descriptor_V0 )


    def form_reco_decay_descriptor( self ) :
        """
        Create the decay descriptor for reading the reconstructed particles from the stripping location.
        This is created based on the daughter types.
        """

        daug1_name = self.partpropsvc.find( self.daug1ID ).name()
        daug2_name = self.partpropsvc.find( self.daug2ID ).name()
        daug3_name = self.partpropsvc.find( self.daug3ID ).name()

        if self.daug1ID.abspid() == self.daug2ID.abspid() and self.daug3ID == self.ksID and not self.wrongsign:
            self.decay_descriptor = '( Xb & X0 ) -> %s %s %s' % (daug1_name, daug2_name, daug3_name)
            
        else :
            self.decay_descriptor = '[ ( Xb & X0 ) -> %s %s %s ]CC' % (daug1_name, daug2_name, daug3_name)

        self.Info( 'Will use the decay descriptor '+self.decay_descriptor )


    def initialize( self ) :

        sc = AlgoMC.initialize( self )
        if sc.isFailure() :
            return sc

        sc = self.check_tes_locations()
        if sc.isFailure() :
            return sc

        self.tistostool   = self.tool( cpp.ITriggerTisTos,   'TriggerTisTos' )
        self.l0tistostool = self.tool( cpp.ITriggerTisTos, 'L0TriggerTisTos' )

        self.bkgtools = []
        if self.simulation :
            # the first of these can't be used in Bender <= v18r3
            self.bkgtools.append( self.tool( cpp.IBackgroundCategory, 'BackgroundCategoryViaRelations' ) )
            self.bkgtools.append( self.tool( cpp.IBackgroundCategory, 'BackgroundCategory' )             )

        self.lifetimetool = self.tool( cpp.ILifetimeFitter, 'PropertimeFitter' )

        self.stateprovider = self.tool( cpp.ITrackStateProvider, 'TrackStateProvider' )

        self.partpropsvc = self.ppSvc()

        self.part2calo = self.tool( cpp.IPart2Calo, 'Part2Calo' )

        # check the validity of the decay
        sc = self.check_types()
        if sc.isFailure() :
            return sc

        # set up the DP kinematics objects
        self.piID = LHCb.ParticleID(211)
        self.kID  = LHCb.ParticleID(321)
        self.pID  = LHCb.ParticleID(2212)
        self.lID  = LHCb.ParticleID(3122)
        self.ksID = LHCb.ParticleID(310)
        self.bdID = LHCb.ParticleID(511)
        self.bsID = LHCb.ParticleID(531)
        self.lbID = LHCb.ParticleID(5122)

        piMass = self.partpropsvc.find( self.piID ).mass()
        kMass  = self.partpropsvc.find( self.kID  ).mass()
        pMass  = self.partpropsvc.find( self.pID  ).mass()
        lMass  = self.partpropsvc.find( self.lID  ).mass()
        ksMass = self.partpropsvc.find( self.ksID ).mass()
        bdMass = self.partpropsvc.find( self.bdID ).mass()
        bsMass = self.partpropsvc.find( self.bsID ).mass()
        lbMass = self.partpropsvc.find( self.lbID ).mass()

        self.kinematics = {}

        self.kinematics[511] = {}
        self.kinematics[511][310] = {}
        self.kinematics[511][310][211] = {}
        self.kinematics[511][310][321] = {}
        self.kinematics[511][310][2212] = {}
        self.kinematics[511][310][211][211]   = ThreeBodyKinematics( piMass, piMass, ksMass, bdMass )
        self.kinematics[511][310][211][321]   = ThreeBodyKinematics( piMass,  kMass, ksMass, bdMass )
        self.kinematics[511][310][321][211]   = ThreeBodyKinematics(  kMass, piMass, ksMass, bdMass )
        self.kinematics[511][310][321][321]   = ThreeBodyKinematics(  kMass,  kMass, ksMass, bdMass )
        self.kinematics[511][310][2212][2212] = ThreeBodyKinematics(  pMass,  pMass, ksMass, bdMass )
        self.kinematics[511][3122] = {}
        self.kinematics[511][3122][211] = {}
        self.kinematics[511][3122][321] = {}
        self.kinematics[511][3122][2212] = {}
        self.kinematics[511][3122][211][2212] = ThreeBodyKinematics( piMass,  pMass,  lMass, bdMass )
        self.kinematics[511][3122][321][2212] = ThreeBodyKinematics(  kMass,  pMass,  lMass, bdMass )
        self.kinematics[511][3122][2212][211] = ThreeBodyKinematics(  pMass, piMass,  lMass, bdMass )
        self.kinematics[511][3122][2212][321] = ThreeBodyKinematics(  pMass,  kMass,  lMass, bdMass )

        self.kinematics[531] = {}
        self.kinematics[531][310] = {}
        self.kinematics[531][310][211] = {}
        self.kinematics[531][310][321] = {}
        self.kinematics[531][310][2212] = {}
        self.kinematics[531][310][211][211]   = ThreeBodyKinematics( piMass, piMass, ksMass, bsMass )
        self.kinematics[531][310][211][321]   = ThreeBodyKinematics( piMass,  kMass, ksMass, bsMass )
        self.kinematics[531][310][321][211]   = ThreeBodyKinematics(  kMass, piMass, ksMass, bsMass )
        self.kinematics[531][310][321][321]   = ThreeBodyKinematics(  kMass,  kMass, ksMass, bsMass )
        self.kinematics[531][310][2212][2212] = ThreeBodyKinematics(  pMass,  pMass, ksMass, bsMass )
        self.kinematics[531][3122] = {}
        self.kinematics[531][3122][211] = {}
        self.kinematics[531][3122][321] = {}
        self.kinematics[531][3122][2212] = {}
        self.kinematics[531][3122][211][2212] = ThreeBodyKinematics( piMass,  pMass,  lMass, bsMass )
        self.kinematics[531][3122][321][2212] = ThreeBodyKinematics(  kMass,  pMass,  lMass, bsMass )
        self.kinematics[531][3122][2212][211] = ThreeBodyKinematics(  pMass, piMass,  lMass, bsMass )
        self.kinematics[531][3122][2212][321] = ThreeBodyKinematics(  pMass,  kMass,  lMass, bsMass )

        self.kinematics[5122] = {}
        self.kinematics[5122][3122] = {}
        self.kinematics[5122][3122][211] = {}
        self.kinematics[5122][3122][321] = {}
        self.kinematics[5122][3122][2212] = {}
        self.kinematics[5122][3122][211][211]   = ThreeBodyKinematics( piMass, piMass,  lMass, lbMass )
        self.kinematics[5122][3122][211][321]   = ThreeBodyKinematics( piMass,  kMass,  lMass, lbMass )
        self.kinematics[5122][3122][321][211]   = ThreeBodyKinematics(  kMass, piMass,  lMass, lbMass )
        self.kinematics[5122][3122][321][321]   = ThreeBodyKinematics(  kMass,  kMass,  lMass, lbMass )
        self.kinematics[5122][3122][2212][2212] = ThreeBodyKinematics(  pMass,  pMass,  lMass, lbMass )
        self.kinematics[5122][310] = {}
        self.kinematics[5122][310][211] = {}
        self.kinematics[5122][310][321] = {}
        self.kinematics[5122][310][2212] = {}
        self.kinematics[5122][310][211][2212] = ThreeBodyKinematics( piMass,  pMass, ksMass, lbMass )
        self.kinematics[5122][310][321][2212] = ThreeBodyKinematics(  kMass,  pMass, ksMass, lbMass )
        self.kinematics[5122][310][2212][211] = ThreeBodyKinematics(  pMass, piMass, ksMass, lbMass )
        self.kinematics[5122][310][2212][321] = ThreeBodyKinematics(  pMass,  kMass, ksMass, lbMass )

        # form the decay descriptors
        self.form_reco_decay_descriptor()
        self.form_mc_decay_descriptors()

        return SUCCESS


    def find_kinematics( self, parentID, h1ID, h2ID, v0ID ) :

        try :
            kine = self.kinematics[abs(parentID)][abs(v0ID)][abs(h1ID)][abs(h2ID)]
        except Exception, e :
            self.Error( 'Problem retrieving kinematics object for %d -> %d %d %d' % ( parentID, h1ID, h2ID, v0ID ) )
            raise e

        return kine


    def check_tes_locations( self ) :
        """
        Called by initialize to check that the TES locations specified are as expected.
        """

        if 1 != len( self.Inputs ) :
            return self.Error( 'Expected 1 TES location in Inputs but found %d' % len( self.Inputs ) )

        loc = self.Inputs[0]
        if loc.find('DD') > -1 :
            if self.name().find('DD') < 0 :
                return self.Error( 'TES location contains "DD" but my name does not' )
        elif loc.find('LL') > -1 :
            if self.name().find('LL') < 0 :
                return self.Error( 'TES location contains "LL" but my name does not' )
        elif loc.find('LD') > -1 :
            if self.name().find('LD') < 0 :
                return self.Error( 'TES location contains "LD" but my name does not' )
        else :
            return self.Error( 'TES location does not contain "LL", "LD" or "DD"' )

        return SUCCESS


    def p4_tuple( self, tuple, particle, name, dtf_params = None ) :
        """
        Store the id, charge, 4-momentum, mass, p_t and eta of a particle.
        """

        p4 = particle.momentum()
        pid = particle.particleID()

        tuple.column_int(    name + '_KEY',   particle.key()             )
        tuple.column_int(    name + '_ID',    pid.pid()                  )
        tuple.column_int(    name + '_Q',     particle.charge()          )
        tuple.column_double( name + '_P',     p4.P()                     )
        tuple.column_double( name + '_PE',    p4.E()                     )
        tuple.column_double( name + '_PX',    p4.Px()                    )
        tuple.column_double( name + '_PY',    p4.Py()                    )
        tuple.column_double( name + '_PZ',    p4.Pz()                    )
        tuple.column_double( name + '_PT',    p4.Pt()                    )
        tuple.column_double( name + '_ETA',   p4.Eta()                   )
        tuple.column_double( name + '_PHI',   p4.Phi()                   )
        tuple.column_double( name + '_THETA', p4.Theta()                 )
        tuple.column_double( name + '_M',     p4.M()                     )
        tuple.column_double( name + '_MM',    particle.measuredMass()    )
        tuple.column_double( name + '_MMERR', particle.measuredMassErr() )
        if dtf_params :
            p4 = dtf_params.momentum()
            tuple.column_double( name + '_DTFM',     p4.m().value() )
            tuple.column_double( name + '_DTFMERR',  p4.m().error() )


    def mc_p4_tuple( self, tuple, mcparticles, mcmatched, name, signal_matched ) :
        """
        Store the id, charge, 4-momentum, mass, p_t and eta of an MC particle
        """

        extra_name = ''
        if signal_matched :
            extra_name = '_SIG'

        if mcparticles.empty() or mcmatched==0 :
            tuple.column_int(    name + extra_name + '_TRUEID',            -1   )
            tuple.column_int(    name + extra_name + '_MOTHER_TRUEID',     -1   )
            tuple.column_int(    name + extra_name + '_GDMOTHER_TRUEID',   -1   )
            tuple.column_int(    name + extra_name + '_GTGDMOTHER_TRUEID', -1   )
            tuple.column_int(    name + extra_name + '_MOTHER_KEY',        -1   )
            tuple.column_int(    name + extra_name + '_GDMOTHER_KEY',      -1   )
            tuple.column_int(    name + extra_name + '_GTGDMOTHER_KEY',    -1   )
            tuple.column_int(    name + extra_name + '_TRUEQ',             -1   )
            tuple.column_double( name + extra_name + '_TRUEP',             -1.1 )
            tuple.column_double( name + extra_name + '_TRUEPE',            -1.1 )
            tuple.column_double( name + extra_name + '_TRUEPX',            -1.1 )
            tuple.column_double( name + extra_name + '_TRUEPY',            -1.1 )
            tuple.column_double( name + extra_name + '_TRUEPZ',            -1.1 )
            tuple.column_double( name + extra_name + '_TRUEPT',            -1.1 )
            tuple.column_double( name + extra_name + '_TRUEETA',           -1.1 )
            tuple.column_double( name + extra_name + '_TRUEPHI',           -1.1 )
            tuple.column_double( name + extra_name + '_TRUETHETA',         -1.1 )
            tuple.column_double( name + extra_name + '_TRUEM',             -1.1 )
            tuple.column_int(    name + extra_name + '_OSCIL',             -1   )
        else :
            mcparticle = mcparticles[0]
            motherid = -1
            gdmotherid = -1
            gtgdmotherid = -1
            motherkey = -1  
            gdmotherkey = -1  
            gtgdmotherkey = -1  
            mcmother = mcparticle.mother()
            if mcmother :
                motherid = int(MCID(mcmother))
                motherkey = mcmother.key()
                mcgrandmother = mcmother.mother()
                if mcgrandmother :
                    gdmotherid = int(MCID(mcgrandmother))
                    gdmotherkey = mcgrandmother.key()
                    mcgreatgrandmother = mcgrandmother.mother()
                    if mcgreatgrandmother :
                        gtgdmotherid = int(MCID(mcgreatgrandmother))
                        gtgdmotherkey = mcgreatgrandmother.key()
            tuple.column_int(    name + extra_name + '_TRUEID',            int(MCID(mcparticle))         )
            tuple.column_int(    name + extra_name + '_MOTHER_TRUEID',     motherid                      )
            tuple.column_int(    name + extra_name + '_GDMOTHER_TRUEID',   gdmotherid                    )
            tuple.column_int(    name + extra_name + '_GTGDMOTHER_TRUEID', gtgdmotherid                  )
            tuple.column_int(    name + extra_name + '_MOTHER_KEY',        motherkey                     )
            tuple.column_int(    name + extra_name + '_GDMOTHER_KEY',      gdmotherkey                   )
            tuple.column_int(    name + extra_name + '_GTGDMOTHER_KEY',    gtgdmotherkey                 )
            tuple.column_int(    name + extra_name + '_TRUEQ',             int(MC3Q(mcparticle)/3)       )
            tuple.column_double( name + extra_name + '_TRUEP',             MCP(mcparticle)               )
            tuple.column_double( name + extra_name + '_TRUEPE',            MCE(mcparticle)               )
            tuple.column_double( name + extra_name + '_TRUEPX',            MCPX(mcparticle)              )
            tuple.column_double( name + extra_name + '_TRUEPY',            MCPY(mcparticle)              )
            tuple.column_double( name + extra_name + '_TRUEPZ',            MCPZ(mcparticle)              )
            tuple.column_double( name + extra_name + '_TRUEPT',            MCPT(mcparticle)              )
            tuple.column_double( name + extra_name + '_TRUEETA',           MCETA(mcparticle)             )
            tuple.column_double( name + extra_name + '_TRUEPHI',           MCPHI(mcparticle)             )
            tuple.column_double( name + extra_name + '_TRUETHETA',         MCTHETA(mcparticle)           )
            tuple.column_double( name + extra_name + '_TRUEM',             MCM(mcparticle)               )
            tuple.column_int(    name + extra_name + '_OSCIL',             int(MCOSCILLATED(mcparticle)) )


    def ip_tuple( self, tuple, particle, name ) :
        """
        Store the impact parameter info for the particle
        """

        primaries = self.vselect('PV', PRIMARY )
        bestPV = self.bestVertex( particle )

        minipfun     = MINIP( primaries, self.geo() )
        minipchi2fun = MINIPCHI2( primaries, self.geo() )
        ipbpvfun     = IP( bestPV, self.geo() )
        ipchi2bpvfun = IPCHI2( bestPV, self.geo() )

        tuple.column_double( name + '_MINIP',         minipfun(particle)     )
        tuple.column_double( name + '_MINIPCHI2',     minipchi2fun(particle) )
        tuple.column_double( name + '_IP_OWNPV',      ipbpvfun(particle)     )
        tuple.column_double( name + '_IPCHI2_OWNPV',  ipchi2bpvfun(particle) )


        bpvpos = bestPV.position()
        tuple.column_double( name + '_OWNPV_X', bpvpos.x() )
        tuple.column_double( name + '_OWNPV_Y', bpvpos.y() )
        tuple.column_double( name + '_OWNPV_Z', bpvpos.z() )

        covMatrix = bestPV.covMatrix()
        tuple.column_double( name + '_OWNPV_XERR', TMath.Sqrt( covMatrix(0,0) ) )
        tuple.column_double( name + '_OWNPV_YERR', TMath.Sqrt( covMatrix(1,1) ) )
        tuple.column_double( name + '_OWNPV_ZERR', TMath.Sqrt( covMatrix(2,2) ) )

        chi2 = bestPV.chi2()
        ndof = bestPV.nDoF()
        chi2ndof = bestPV.chi2PerDoF()
        tuple.column_int(    name + '_OWNPV_NDOF',     ndof                     )
        tuple.column_double( name + '_OWNPV_CHI2',     chi2                     )
        tuple.column_double( name + '_OWNPV_CHI2NDOF', chi2ndof                 )
        tuple.column_double( name + '_OWNPV_PROB',     TMath.Prob( chi2, ndof ) )

        ntrk = bestPV.tracks().size()
        tuple.column_int( name + '_OWNPV_NTRACKS', ntrk )


    def vtx_tuple( self, tuple, bcand, v0cand ) :
        """
        Store vertex info for the B and KS
        """

        bcand_name = 'B'
        v0cand_name = self.simplenames[ v0cand.particleID().pid() ]

        for particle in [bcand, v0cand] :
            if particle is bcand :
                name = bcand_name
            else :
                name = v0cand_name

            vertex = particle.endVertex()

            vtxpos = vertex.position()
            tuple.column_double( name + '_ENDVERTEX_X',    vtxpos.x() )
            tuple.column_double( name + '_ENDVERTEX_Y',    vtxpos.y() )
            tuple.column_double( name + '_ENDVERTEX_Z',    vtxpos.z() )

            covMatrix = vertex.covMatrix()
            tuple.column_double( name + '_ENDVERTEX_XERR', TMath.Sqrt( covMatrix(0,0) ) )
            tuple.column_double( name + '_ENDVERTEX_YERR', TMath.Sqrt( covMatrix(1,1) ) )
            tuple.column_double( name + '_ENDVERTEX_ZERR', TMath.Sqrt( covMatrix(2,2) ) )

            chi2 = vertex.chi2()
            ndof = vertex.nDoF()
            chi2ndof = vertex.chi2PerDoF()
            tuple.column_int(    name + '_ENDVERTEX_NDOF',     ndof                     )
            tuple.column_double( name + '_ENDVERTEX_CHI2',     chi2                     )
            tuple.column_double( name + '_ENDVERTEX_CHI2NDOF', chi2ndof                 )
            tuple.column_double( name + '_ENDVERTEX_PROB',     TMath.Prob( chi2, ndof ) )

            primaries = self.vselect('PV', PRIMARY )
            minvdfun     = MINVVD( primaries )
            minvdchi2fun = MINVVDCHI2( primaries )

            tuple.column_double( name + '_MINVD',     minvdfun(particle)     )
            tuple.column_double( name + '_MINVDCHI2', minvdchi2fun(particle) )

            bestPV = self.bestVertex( particle )
            vdfun     = VD( bestPV )
            vdchi2fun = VDCHI2( bestPV )
            dirafun   = DIRA( bestPV )

            tuple.column_double( name + '_VD_OWNPV',     vdfun(particle)     )
            tuple.column_double( name + '_VDCHI2_OWNPV', vdchi2fun(particle) )
            tuple.column_double( name + '_DIRA_OWNPV',   dirafun(particle)   )

        b_vtx = bcand.endVertex()
        vdfun     = VD(b_vtx)
        vdchi2fun = VDCHI2(b_vtx)
        tuple.column_double( v0cand_name+'_VTX_SEP',     vdfun(v0cand)     )
        tuple.column_double( v0cand_name+'_VTX_SEPCHI2', vdchi2fun(v0cand) )


    def mc_vtx_tuple( self, tuple, mcparticles, mcmatched, name, signal_matched ) :
        """
        Store vertex and lifetime info for the MC particle
        """

        extra_name = ''
        if signal_matched :
            extra_name = '_SIG'

        if mcparticles.empty() or mcmatched==0 :
            tuple.column_double( name + extra_name + '_TRUEORIGINVERTEX_X', -1.1 )
            tuple.column_double( name + extra_name + '_TRUEORIGINVERTEX_Y', -1.1 )
            tuple.column_double( name + extra_name + '_TRUEORIGINVERTEX_Z', -1.1 )
            tuple.column_double( name + extra_name + '_TRUECTAU'          , -1.1 )
        else :
            mcparticle = mcparticles[0]
            tuple.column_double( name + extra_name + '_TRUEORIGINVERTEX_X', MCVFASPF(MCVX)(mcparticle) )
            tuple.column_double( name + extra_name + '_TRUEORIGINVERTEX_Y', MCVFASPF(MCVY)(mcparticle) )
            tuple.column_double( name + extra_name + '_TRUEORIGINVERTEX_Z', MCVFASPF(MCVZ)(mcparticle) )
            tuple.column_double( name + extra_name + '_TRUECTAU'          , MCCTAU(mcparticle)         )


    def trk_tuple( self, tuple, particle, name ) :
        """
        Store track-related quantities:
        - track chi2 per dof
        - PID quantities: PIDk, PIDp, PIDmu and ISMUON
        """

        # track quantities
        tuple.column_double( name + '_TRACK_CHI2NDOF',  TRCHI2DOF(particle)   )
        tuple.column_double( name + '_TRACK_PCHI2',     TRPCHI2(particle)     )
        tuple.column_double( name + '_TRACK_GHOSTPROB', TRGHOSTPROB(particle) )

        # PID quantities
        tuple.column_double( name + '_PIDe',        PIDe(particle)         )
        tuple.column_double( name + '_PIDmu',       PIDmu(particle)        )
        tuple.column_double( name + '_PIDpi',       PIDpi(particle)        )
        tuple.column_double( name + '_PIDK',        PIDK(particle)         )
        tuple.column_double( name + '_PIDp',        PIDp(particle)         )
        tuple.column_double( name + '_PROBNNe',     PROBNNe(particle)      )
        tuple.column_double( name + '_PROBNNmu',    PROBNNmu(particle)     )
        tuple.column_double( name + '_PROBNNpi',    PROBNNpi(particle)     )
        tuple.column_double( name + '_PROBNNK',     PROBNNK(particle)      )
        tuple.column_double( name + '_PROBNNp',     PROBNNp(particle)      )
        tuple.column_double( name + '_PROBNNghost', PROBNNghost(particle)  )

        hasmuon = -1
        ismuon = -1
        hasproto = HASPROTO(particle)
        if hasproto :
            hasmuon = HASMUON(particle)
            if hasmuon :
                ismuon = ISMUON(particle)
        tuple.column_int( name + '_hasProto', int(hasproto) )
        tuple.column_int( name + '_hasMuon',  int(hasmuon)  )
        tuple.column_int( name + '_isMuon',   int(ismuon)   )

        # HCAL info for L0 Hadron correction
        self.part2calo.match( particle, '/dd/Structure/LHCb/DownstreamRegion/Hcal' )

        caloX = self.part2calo.caloState().x()
        caloY = self.part2calo.caloState().y()
        caloZ = self.part2calo.caloState().z()
        caloP = self.part2calo.caloState().p()
        caloM = particle.measuredMass()

        trackET = TMath.Sqrt( caloP*caloP + caloM*caloM ) * TMath.Sqrt( caloX*caloX + caloY*caloY ) / TMath.Sqrt( caloX*caloX + caloY*caloY + caloZ*caloZ )

        region = self.isinside_HCAL( caloX, caloY )

        tuple.column_double( name + '_L0Calo_HCAL_realET',      trackET )
        tuple.column_double( name + '_L0Calo_HCAL_xProjection', caloX   )
        tuple.column_double( name + '_L0Calo_HCAL_yProjection', caloY   )
        tuple.column_int(    name + '_L0Calo_HCAL_region',      region  )


    def isinside_HCAL( self, caloX, caloY ) :
        """
        Determine whether the particle is in the Inner, Outer part of the HCAL
        or outside it completely
        """

        inside = True
        inner = False
        outer = False

        HCAL_CellSize_Inner = 131.3
        HCAL_CellSize_Outer = 262.6
        HCAL_xMax_Inner = 2101
        HCAL_yMax_Inner = 1838
        HCAL_xMax_Outer = 4202
        HCAL_yMax_Outer = 3414

        # projection inside calo
        if TMath.Abs( caloX ) < HCAL_xMax_Outer and TMath.Abs( caloY ) < HCAL_yMax_Outer :
            # projection inside inner calo (else is outer calo)
            if TMath.Abs( caloX ) < HCAL_xMax_Inner and TMath.Abs( caloY ) < HCAL_yMax_Inner :
                # projections outside the beampipe (in x)
                if TMath.Abs( caloX ) > 2*HCAL_CellSize_Inner :
                    inner = True
                elif TMath.Abs( caloY )  > 2*HCAL_CellSize_Inner :
                    inner = True
                else :
                    inside = False
            else :
                outer = True
        else :
            inside = False

        if not inside :
            return -1
        elif inner :
            return 1
        elif outer :
            return 0
        else :
            return -999


    def pid_swap( self, bcand, bpid, h1pid, h2pid ) :
        """
        Change the ID of the B candidate and the IDs of its charged daughters.
        The particle charges will be unchanged, i.e. supplied IDs will be
        treated as unsigned values.
        """

        # first find the "original" IDs
        orig_b_pid = bcand.particleID().pid()
        orig_daug_pids = []
        for daug in bcand.children() :
            daug_pid = daug.particleID().pid()
            orig_daug_pids.append( daug_pid )

        # now swap the ID of the B
        bpid = abs(bpid)
        if bcand.particleID().pid() < 0 :
            bpid = -bpid
        bcand.setParticleID( LHCb.ParticleID(bpid) )

        # swap the charged daughter IDs as well
        h1pid = abs(h1pid)
        h2pid = -abs(h2pid)

        for daug in bcand.children() :
            daug_pid = daug.particleID()

            if daug_pid == self.daug3ID or daug_pid == self.daug3ConjID :
                continue

            if daug.charge() > 0 :
                daug.setParticleID( LHCb.ParticleID(h1pid) )
            else :
                daug.setParticleID( LHCb.ParticleID(h2pid) )

        return (orig_b_pid, orig_daug_pids)


    def pid_swap_back( self, bcand, orig_bpid, orig_daug_pids ) :
        """
        Change the IDs of the B candidate and its charged daughters back to
        their original values.
        """

        bcand.setParticleID( LHCb.ParticleID( orig_bpid ) )

        index = 0
        for daug in bcand.children() :
            daug.setParticleID( LHCb.ParticleID( orig_daug_pids[index] ) )
            index += 1


    def dtf_p4_tuple( self, dpinfo, bcand ) :
        """
        Store the results of a vertex fit where the masses of various particles
        in the decay tree are constrained to their PDG values.
        Values stored are:
        - information on the success (or otherwise) of the fit
        - the vertex chi2, ndof and probability
        - the B lifetime, associated uncertainty and significance
        - the 4-momentum of the B and its 3 daughters
        - the invariant mass-squared pairs m12Sq, m13Sq, m23Sq
        - the square DP co-ordinates mPrime and thPrime
        """

        # get the name of the V0
        v0name = self.v0name

        # perform the fit adding constraints on both the parent and V0 masses
        pv = self.bestVertex( bcand )
        fitter = cpp.DecayTreeFitter.Fitter( bcand, pv, self.stateprovider )
        fitter.setMassConstraint( bcand )
        for daug in bcand.daughters():
            daug_pid = daug.particleID()
            if daug_pid == self.daug3ID or daug_pid == self.daug3ConjID :
                fitter.setMassConstraint( daug )
        fitter.fit()

        bp4 = Gaudi.Math.LorentzVectorWithError()
        bctau = Gaudi.Math.ValueWithError()

        daug_id = [ 0, 0, 0 ]
        daug_p4 = [ Gaudi.Math.LorentzVectorWithError(), Gaudi.Math.LorentzVectorWithError(), Gaudi.Math.LorentzVectorWithError() ]
        daug_names = [ 'h1', 'h2', v0name ]

        m12Sq = -1.1
        m13Sq = -1.1
        m23Sq = -1.1

        mPrime = -1.1
        thPrime = -1.1

        # check if the fit succeeded
        if fitter.status() == 0 :

            # store information on the B candidate
            bparams = fitter.fitParams( bcand )
            bp4 = bparams.momentum()
            bctau = bparams.ctau()

            bcandID = bcand.particleID().pid()

            # loop through 3 B daughters and store their 4-momenta
            if not self.wrongsign :
                h1cand, h2cand, v0cand = self.find_daughters( bcand )
            else :
                h1cand, h2cand, v0cand = self.find_daughters_wrongsign( bcand )

            daughters = [ h1cand, h2cand, v0cand ]

            for i in range(3) :
                params = fitter.fitParams( daughters[i] )
                daug_p4[i] = params.momentum()
                daug_id[i] = daughters[i].particleID().abspid()

            p12 = daug_p4[0] + daug_p4[1]
            p13 = daug_p4[0] + daug_p4[2]
            p23 = daug_p4[1] + daug_p4[2]

            m12Sq = p12.M2()
            m13Sq = p13.M2()
            m23Sq = p23.M2()

            kine = self.find_kinematics( bcandID, daug_id[0], daug_id[1], daug_id[2] )
            if kine.withinDPLimits( m13Sq, m23Sq ) :
                kine.updateKinematics( m13Sq, m23Sq )
                mPrime = kine.mPrime
                thPrime = kine.thPrime

            if m12Sq<0 :     m12Sq=-1.1
            if m12Sq>100e6 : m12Sq=-1.1
            if m13Sq<0 :     m13Sq=-1.1
            if m13Sq>100e6 : m13Sq=-1.1
            if m23Sq<0 :     m23Sq=-1.1
            if m23Sq>100e6 : m23Sq=-1.1

        # store the fit results (sucess or otherwise, lifetime, 4-momenta and DP co-ordinates)
        dpinfo.store_fit_results( fitter, bctau, bp4, daug_p4, m12Sq, m13Sq, m23Sq, mPrime, thPrime )


    def calc_DP_info( self, bcand ) :
        """
        Calculate the DP position and the daughter 4-momenta with the
        B-candidate mass constrained to either the nominal Bd, Bs or Lb mass
        and under various mass assumptions for the daughters
        """

        self.dpinfos = []

        for b_id in [ 511, 531, 5122 ] :
            for h1_id in [ 211, 321, 2212 ] :
                for h2_id in [ 211, 321, 2212 ] :

                    # only process baryon-number conserving modes
                    if (b_id == self.lbID.abspid() and self.daug3ID == self.ksID) or ((b_id == self.bdID.abspid() or b_id == self.bsID.abspid()) and self.daug3ID == self.lID) :
                        if (h1_id == 2212 and h2_id == 2212) or (h1_id != 2212 and h2_id != 2212) :
                            continue
                    elif (h1_id == 2212 and h2_id != 2212) or (h1_id != 2212 and h2_id == 2212) :
                        continue

                    # construct the suffix to be of the form, e.g. Bs2KpiKS
                    suffix = self.simplenames[b_id]+'2'+self.simplenames[h1_id]+self.simplenames[h2_id]+self.simplenames[self.daug3ID.abspid()]

                    # do the PID swap
                    orig_b_pid, orig_daug_pids = self.pid_swap( bcand, b_id, h1_id, h2_id )

                    # create the object to store the information
                    dpinfo = DalitzInfo( self.v0name, suffix )

                    # do the fits and store the information
                    self.dtf_p4_tuple( dpinfo, bcand )

                    # do the swap back to the original IDs
                    self.pid_swap_back( bcand, orig_b_pid, orig_daug_pids )

                    # append the DP info object to the list
                    self.dpinfos.append( dpinfo )


    def store_DP_info( self, tuple ) :
        """
        Store the pre-calculated DP information in the supplied tuple
        """
        
        for info in self.dpinfos :
            info.fill_tuple( tuple )


    def mc_dp_tuple( self, tuple, mcparents ) :
        """
        Store the MC truth DP co-ordinates
        """

        # loop through the B daughters and store their 4-momenta treating the
        # expected 3 daughters and PHOTOS photons separately

        daug_names = [ 'h1', 'h2', self.v0name ]

        # if there is no MC info
        if mcparents.empty() :
            tuple.column_int( 'nPHOTOS', -1 )
            for daug_name in daug_names :
                tuple.column_double( daug_name + '_CORRPE', -1.1 )
                tuple.column_double( daug_name + '_CORRPX', -1.1 )
                tuple.column_double( daug_name + '_CORRPY', -1.1 )
                tuple.column_double( daug_name + '_CORRPZ', -1.1 )
            tuple.column_double( 'm12Sq_MC',   -1.1 )
            tuple.column_double( 'm13Sq_MC',   -1.1 )
            tuple.column_double( 'm23Sq_MC',   -1.1 )
            tuple.column_double( 'mPrime_MC',  -1.1 )
            tuple.column_double( 'thPrime_MC', -1.1 )
            return

        # otherwise take the first entry
        # TODO - can we do something more clever here?
        mcparent = mcparents[0]

        mcparent_id = mcparent.particleID().pid()

        daug_id = [ 0, 0, 0 ]
        daug_p4 = [ Gaudi.LorentzVector(), Gaudi.LorentzVector(), Gaudi.LorentzVector() ]
        gamma_p4 = []

        # python list of children, may initially contain resonances so it is 
        # recursively searched through to find the correct final state particles.
        children = self.mc_find_daug( [ daug for daug in mcparent.children( True ) ] )

        # check that we are left with 3 non-photon daughters
        ndaug = 0
        for daug in children :
            if daug.particleID().pid() != 22 :
                ndaug += 1
        if ndaug != 3 :
            self.Error( 'Parent particle does not have 3 non-photon final-state daughters' )
            e = Exception('Unexpected number of final-state particles')
            raise e

        for daug in children :

            daugID = daug.particleID()

            px = MCPX( daug )
            py = MCPY( daug )
            pz = MCPZ( daug )
            pe = MCE ( daug )

            p4 = Gaudi.LorentzVector( px, py, pz, pe )

            if 22 == daugID.pid() :
                gamma_p4.append( p4 )
            elif self.mcdaug1ID.abspid() == self.mcdaug2ID.abspid() :
                if daugID == self.mcdaug3ID or daugID == self.mcdaug3ConjID :
                    daug_id[2] = daugID.pid()
                    daug_p4[2] = p4
                elif daugID == self.mcdaug1ID :
                    daug_id[0] = daugID.pid()
                    daug_p4[0] = p4
                else :
                    daug_id[1] = daugID.pid()
                    daug_p4[1] = p4
            else :
                if daugID == self.mcdaug3ID or daugID == self.mcdaug3ConjID :
                    daug_id[2] = daugID.pid()
                    daug_p4[2] = p4
                elif daugID.abspid() == self.mcdaug1ID.abspid() :
                    daug_id[0] = daugID.pid()
                    daug_p4[0] = p4
                else :
                    daug_id[1] = daugID.pid()
                    daug_p4[1] = p4

        ngamma = len(gamma_p4)
        tuple.column_int( 'nPHOTOS', ngamma )

        if 0 != ngamma :

            for gamma in gamma_p4 :
                minangle = 1000.0
                mindaug = -1
                for daug in daug_p4 :
                    gammaPHat = gamma.Vect().Unit()
                    daugPHat = daug.Vect().Unit()
                    angle = TMath.ACos( gammaPHat.Dot( daugPHat ) )
                    if abs(angle) < minangle :
                        minangle = angle
                        mindaug = daug_p4.index(daug)

                daug_p4[ mindaug ] += gamma


        p12 = daug_p4[0] + daug_p4[1]
        p13 = daug_p4[0] + daug_p4[2]
        p23 = daug_p4[1] + daug_p4[2]

        for i in range(3) :
            tuple.column_double( daug_names[i] + '_CORRPE',  daug_p4[i].E()  )
            tuple.column_double( daug_names[i] + '_CORRPX',  daug_p4[i].Px() )
            tuple.column_double( daug_names[i] + '_CORRPY',  daug_p4[i].Py() )
            tuple.column_double( daug_names[i] + '_CORRPZ',  daug_p4[i].Pz() )

        m12Sq = p12.M2()
        m13Sq = p13.M2()
        m23Sq = p23.M2()

        mPrime = -1.1
        thPrime = -1.1


        kine = self.find_kinematics( mcparent_id, daug_id[0], daug_id[1], daug_id[2] )
        if kine.withinDPLimits( m13Sq, m23Sq ) :
            kine.updateKinematics( m13Sq, m23Sq )
            mPrime = kine.mPrime
            thPrime = kine.thPrime

        if m12Sq<0 :     m12Sq=-1.1
        if m12Sq>100e6 : m12Sq=-1.1
        if m13Sq<0 :     m13Sq=-1.1
        if m13Sq>100e6 : m13Sq=-1.1
        if m23Sq<0 :     m23Sq=-1.1
        if m23Sq>100e6 : m23Sq=-1.1

        tuple.column_double( 'm12Sq_MC', m12Sq )
        tuple.column_double( 'm13Sq_MC', m13Sq )
        tuple.column_double( 'm23Sq_MC', m23Sq )

        tuple.column_double( 'mPrime_MC',  mPrime  )
        tuple.column_double( 'thPrime_MC', thPrime )


    def mc_find_daug(self, children):
        lower_children = []
        for daug in children:
            daugID = daug.particleID()
            if 22 == daugID.pid() :
                lower_children.append(daug)
                continue
            elif daugID.abspid() in [ self.mcdaug1ID.abspid(), self.mcdaug2ID.abspid(), self.mcdaug3ID.abspid() ] :
                lower_children.append(daug)
                continue
            else :
                # get the daughters of the unknown particle
                daug_children = [ lower_daug for lower_daug in daug.children( True ) ] 
                # check that there are daughters (resonance) and not just unknown
                if len(daug_children)>0:
                    # apply same recursive function to daughter's children
                    lower_children.extend( self.mc_find_daug( daug_children ) )
                    continue
                else :
                    self.Error( 'Daughter does not match any of the expected daughter types and has no children: '+str(daugID.pid()) )
                    e = Exception('Unknown daughter type')
                    raise e
        return lower_children


    def trig_tuple( self, tuple, bcand ) :

        # Find the TCK
        location = 'Hlt/DecReports'
        if self.RootInTES != '' :
            location = self.RootInTES + location
        hdr = self.get(location)
        tck = -1
        if hdr :
            tck = hdr.configuredTCK()
        tuple.column_int("tck", tck )

        # Setup the TISTOS tools
        self.l0tistostool.setOfflineInput( bcand )
        self.tistostool.setOfflineInput( bcand )

        # Get the L0 decisions, plus TIS & TOS info

        l0triggers = self.l0tistostool.triggerSelectionNames('L0.*Decision')
        self.l0tistostool.setTriggerInput( 'L0.*Decision' )
        l0 = self.l0tistostool.tisTosTobTrigger()
        l0dec = l0.decision()
        l0tis = l0.tis()
        l0tos = l0.tos()
        tuple.column_int("L0Global_Dec", l0dec )
        tuple.column_int("L0Global_TIS", l0tis )
        tuple.column_int("L0Global_TOS", l0tos )

        l0declist = self.l0tistostool.triggerSelectionNames( self.tistostool.kTrueRequired, self.tistostool.kAnything,     self.tistostool.kAnything     )
        l0tislist = self.l0tistostool.triggerSelectionNames( self.tistostool.kTrueRequired, self.tistostool.kTrueRequired, self.tistostool.kAnything     )
        l0toslist = self.l0tistostool.triggerSelectionNames( self.tistostool.kTrueRequired, self.tistostool.kAnything,     self.tistostool.kTrueRequired )

        # set the list of L0 decisions to be stored
        l0tuplelist = ['L0DiMuonDecision',
                       'L0MuonDecision',
                       'L0ElectronDecision',
                       'L0ElectronHiDecision',
                       'L0PhotonDecision',
                       'L0HadronDecision']

        for line in l0triggers :
            if line in l0tuplelist :
                l0dec = 0
                l0tis = 0
                l0tos = 0
                if line in l0declist : l0dec = 1
                if line in l0tislist : l0tis = 1
                if line in l0toslist : l0tos = 1
                tuple.column_int(line+"_Dec", l0dec )
                tuple.column_int(line+"_TIS", l0tis )
                tuple.column_int(line+"_TOS", l0tos )

        # Get the HLT decisions, plus TIS & TOS info

        hlt1triggers = self.tistostool.triggerSelectionNames('Hlt1.*Decision')
        self.tistostool.setTriggerInput( 'Hlt1.*Decision' )
        hlt1 = self.tistostool.tisTosTobTrigger()
        hlt1dec = hlt1.decision()
        hlt1tis = hlt1.tis()
        hlt1tos = hlt1.tos()
        tuple.column_int("Hlt1Global_Dec", hlt1dec )
        tuple.column_int("Hlt1Global_TIS", hlt1tis )
        tuple.column_int("Hlt1Global_TOS", hlt1tos )

        hlt1declist = self.tistostool.triggerSelectionNames( self.tistostool.kTrueRequired, self.tistostool.kAnything,     self.tistostool.kAnything     )
        hlt1tislist = self.tistostool.triggerSelectionNames( self.tistostool.kTrueRequired, self.tistostool.kTrueRequired, self.tistostool.kAnything     )
        hlt1toslist = self.tistostool.triggerSelectionNames( self.tistostool.kTrueRequired, self.tistostool.kAnything,     self.tistostool.kTrueRequired )

        #set the list of hlt1 decision to be stored
        hlt1tuplelist = ['Hlt1TrackAllL0Decision',
                         'Hlt1TrackMuonDecision',
                         'Hlt1TrackPhotonDecision']

        for line in hlt1triggers :
            if line in hlt1tuplelist :
                hlt1dec = 0
                hlt1tis = 0
                hlt1tos = 0
                if line in hlt1declist : hlt1dec = 1
                if line in hlt1tislist : hlt1tis = 1
                if line in hlt1toslist : hlt1tos = 1
                tuple.column_int(line+"_Dec", hlt1dec )
                tuple.column_int(line+"_TIS", hlt1tis )
                tuple.column_int(line+"_TOS", hlt1tos )

        hlt2triggers = self.tistostool.triggerSelectionNames('Hlt2.*Decision')
        self.tistostool.setTriggerInput( 'Hlt2.*Decision' )
        hlt2 = self.tistostool.tisTosTobTrigger()
        hlt2dec = hlt2.decision()
        hlt2tis = hlt2.tis()
        hlt2tos = hlt2.tos()
        tuple.column_int("Hlt2Global_Dec", hlt2dec )
        tuple.column_int("Hlt2Global_TIS", hlt2tis )
        tuple.column_int("Hlt2Global_TOS", hlt2tos )

        hlt2declist = self.tistostool.triggerSelectionNames( self.tistostool.kTrueRequired, self.tistostool.kAnything,     self.tistostool.kAnything     )
        hlt2tislist = self.tistostool.triggerSelectionNames( self.tistostool.kTrueRequired, self.tistostool.kTrueRequired, self.tistostool.kAnything     )
        hlt2toslist = self.tistostool.triggerSelectionNames( self.tistostool.kTrueRequired, self.tistostool.kAnything,     self.tistostool.kTrueRequired )

        # set the list of hlt2 decision to be stored
        hlt2tuplelist = ['Hlt2Topo2BodyBBDTDecision',
                         'Hlt2Topo3BodyBBDTDecision',
                         'Hlt2Topo4BodyBBDTDecision',
                         'Hlt2Topo2BodySimpleDecision',
                         'Hlt2Topo3BodySimpleDecision',
                         'Hlt2Topo4BodySimpleDecision',
                         'Hlt2B2HHDecision',
                         'Hlt2B2HHPi0_MergedDecision']

        for line in hlt2triggers :
            if line in hlt2tuplelist :
                hlt2dec = 0
                hlt2tis = 0
                hlt2tos = 0
                if line in hlt2declist : hlt2dec = 1
                if line in hlt2tislist : hlt2tis = 1
                if line in hlt2toslist : hlt2tos = 1
                tuple.column_int(line+"_Dec", hlt2dec )
                tuple.column_int(line+"_TIS", hlt2tis )
                tuple.column_int(line+"_TOS", hlt2tos )


    def store_DTF_fit_results( self, tuple, fitter, suffix ) :
        """
        Fill the supplied tuple with results from the supplied DTF object
        """

        # store info on the success (or otherwise) of the fit
        tuple.column_int( 'B_DTF_STATUS_'+suffix,  fitter.status()  )
        tuple.column_int( 'B_DTF_ERRCODE_'+suffix, fitter.errCode() )
        tuple.column_int( 'B_DTF_NITER_'+suffix,   fitter.nIter()   )

        if fitter.status() != 0 :
            tuple.column_int(    'B_ENDVERTEX_NDOF_'+suffix, -1   )
            tuple.column_double( 'B_ENDVERTEX_CHI2_'+suffix, -1.1 )
            tuple.column_double( 'B_ENDVERTEX_PROB_'+suffix, -1.1 )
            tuple.column_double( 'B_M_'+suffix,              -1.1 )
            if 'PV' in suffix :
                tuple.column_double( 'B_CTAU_'+suffix,    -1.1 )
                tuple.column_double( 'B_CTAUERR_'+suffix, -1.1 )
                tuple.column_double( 'B_CTAUSIG_'+suffix, -1.1 )

        else :
            chi2 = fitter.chiSquare()
            ndof = fitter.nDof()
            prob = TMath.Prob( chi2, ndof )

            tuple.column_int(    'B_ENDVERTEX_NDOF_'+suffix, ndof )
            tuple.column_double( 'B_ENDVERTEX_CHI2_'+suffix, chi2 )
            tuple.column_double( 'B_ENDVERTEX_PROB_'+suffix, prob )

            bcand = fitter.particle()
            bparams = fitter.fitParams( bcand )

            bp4 = bparams.momentum()
            tuple.column_double( 'B_M_'+suffix,    bp4.m().value() )
            tuple.column_double( 'B_MERR_'+suffix, bp4.m().error() )

            if 'PV' in suffix :
                bctau = bparams.ctau()
                tuple.column_double( 'B_CTAU_'+suffix,    bctau.value() )
                tuple.column_double( 'B_CTAUERR_'+suffix, bctau.error() )
                tuple.column_double( 'B_CTAUSIG_'+suffix, bctau.value()/bctau.error() )


    def extra_B_vars_tuple( self, tuple, bcand ) :
        """
        Store the value of the B mass and lifetime (where appropriate)
        resulting from fits with different sets of constraints:
        - V0 mass constraint
        - PV constraint
        - V0 mass and PV constraints
        Also store the vertex chi2, ndof and probability of the fit.
        """

        # TupleToolPropertime method
        bestPV = self.bestVertex( bcand )
        tau = Double(0.0)
        tau_err = Double(0.0)
        tau_chisq = Double(0.0)
        self.lifetimetool.fit( bestPV, bcand, tau, tau_err, tau_chisq )
        tau *= TMath.C()/1e6
        tau_err *= TMath.C()/1e6
        tuple.column_double( 'B_TAU_TT',    tau     )
        tuple.column_double( 'B_TAUERR_TT', tau_err )

        # V0 mass constraint
        fitterV0 = cpp.DecayTreeFitter.Fitter( bcand, self.stateprovider )
        for daug in bcand.daughters():
            daug_pid = daug.particleID()
            if daug_pid == self.daug3ID or daug_pid == self.daug3ConjID :
                fitterV0.setMassConstraint( daug )
        fitterV0.fit()

        self.store_DTF_fit_results( tuple, fitterV0, self.v0name )

        # PV constraint
        fitterPV = cpp.DecayTreeFitter.Fitter( bcand, bestPV, self.stateprovider )
        fitterPV.fit()

        self.store_DTF_fit_results( tuple, fitterPV, 'PV' )

        # V0 mass and PV constraints
        fitterV0PV = cpp.DecayTreeFitter.Fitter( bcand, bestPV, self.stateprovider )
        for daug in bcand.daughters():
            daug_pid = daug.particleID()
            if daug_pid == self.daug3ID or daug_pid == self.daug3ConjID :
                fitterV0PV.setMassConstraint( daug )
        fitterV0PV.fit()

        self.store_DTF_fit_results( tuple, fitterV0PV, 'PV'+self.v0name )


    def bkg_category_tuple( self, tuple, cand, name ) :
        """
        Reproducce information stored by TupleToolMCBackgroundInfo
        """

        category = -1
        for tool in self.bkgtools :
            category = tool.category( cand )
            if category > -1 and category < 1001 :
                break

        if category > 1000 :
            category = -1

        tuple.column_int( name + '_BKGCAT', category )


    def vtx_isolation_tuple( self, tuple, bcand ) :
        """
        Reproduce information stored by TupleToolVtxIsoln
        """

        # 20/04/2015 - updated to access information stored by Stripping21 only
        #            - uses RELINFO functor to retrieve the stored information
        #            - have cone variables for deltaR = 1.0, 1.5 and 1.7
        #            - have vertex isolation information

        # get the TES location of our input particles and remove the trailing 'Particles'
        teslocation = self.RootInTES + self.Inputs[0] + '/'

        angles_location_names = [ 'P2ConeVar10', 'P2ConeVar', 'P2ConeVar17' ]
        angles_variable_names = [ 'CONEANGLE', 'CONEMULT', 'CONEPTASYM' ]
        angles = []

        for name in angles_location_names :
            angle  = '%.1f' % RELINFO( teslocation+name, 'CONEANGLE',  -1.1 )(bcand)
            mult   = int(RELINFO( teslocation+name, 'CONEMULT',   -1.1 )(bcand))
            ptasym = RELINFO( teslocation+name, 'CONEPTASYM', -1.1 )(bcand)
            tuple.column_int( 'B_STRIP_CONEMULT_'+angle.replace('.','_'), mult )
            tuple.column_double( 'B_STRIP_PTASYM_'+angle.replace('.','_'), ptasym )

        vtxiso_location_name = 'VtxIsolationVar'
        vtxiso_variable_names = [ 'VTXISONUMVTX', 'VTXISODCHI2ONETRACK', 'VTXISODCHI2MASSONETRACK', 'VTXISODCHI2TWOTRACK', 'VTXISODCHI2MASSTWOTRACK' ]

        for name in vtxiso_variable_names :
            value = RELINFO( teslocation+vtxiso_location_name, name, -1.1 )(bcand)
            tuple.column_double( 'B_STRIP_'+name, value )


    def mc_match_signal( self, tuple, mcB, mchp, mchm, mcv0, hpcand, hmcand, v0cand ) :
        """
        Do the MC matching for signal MC
        """

        hpname = 'h1'
        hmname = 'h2'
        v0name = self.v0name

        mcmatchhp = 0
        mcmatchhm = 0
        mcmatchv0 = 0

        if not mcB.empty() :
            mcMatcher = self.mcTruth()

            hpFromMC = MCTRUTH( mcMatcher, mchp )
            hmFromMC = MCTRUTH( mcMatcher, mchm )
            v0FromMC = MCTRUTH( mcMatcher, mcv0 )

            if hpFromMC(hpcand) :
                mcmatchhp = 1
            if hmFromMC(hmcand) :
                mcmatchhm = 1
            if v0FromMC(v0cand) :
                mcmatchv0 = 1

        tuple.column_int( hpname+'_SIG_mcMatch', mcmatchhp )
        tuple.column_int( hmname+'_SIG_mcMatch', mcmatchhm )
        tuple.column_int( v0name+'_SIG_mcMatch', mcmatchv0 )

        mcmatched = 0
        if mcmatchhp and mcmatchhm and mcmatchv0 :
            mcmatched = 1
        tuple.column_int( 'B_SIG_mcMatch', mcmatched )

        # store the MC truth info about the B and its daughters
        self.mc_p4_tuple( tuple, mcB, mcmatched, 'B', True )
        self.mc_vtx_tuple( tuple, mcB, mcmatched, 'B', True )
        self.mc_p4_tuple( tuple, mchp, mcmatchhp, hpname, True )
        self.mc_p4_tuple( tuple, mchm, mcmatchhm, hmname, True )
        self.mc_p4_tuple( tuple, mcv0, mcmatchv0, v0name, True )
        self.mc_vtx_tuple( tuple, mcv0, mcmatchv0, v0name, True )


    def mc_match_general( self, tuple, hpcand, hmcand, v0cand ) :
        """
        Do the generialised MC matching
        """

        hpname = 'h1'
        hmname = 'h2'
        v0name = self.v0name

        mcMatcher = self.mcTruth()
        hpfun = RCTRUTH( hpcand, mcMatcher )
        hmfun = RCTRUTH( hmcand, mcMatcher )
        v0fun = RCTRUTH( v0cand, mcMatcher )

        mchp = self.mcselect( 'hpmatches', hpfun & ( ('pi+' == MCABSID) | ('K+' == MCABSID) | ('p+' == MCABSID) | ('mu+' == MCABSID) | ('e+' == MCABSID) ) )
        mchm = self.mcselect( 'hmmatches', hmfun & ( ('pi+' == MCABSID) | ('K+' == MCABSID) | ('p+' == MCABSID) | ('mu+' == MCABSID) | ('e+' == MCABSID) ) )
        mcv0 = self.mcselect( 'v0matches', v0fun & ( ('KS0' == MCABSID) | ('Lambda0' == MCABSID) | ('pi+' == MCABSID) | ('K+' == MCABSID) | ('p+' == MCABSID) | ('mu+' == MCABSID) | ('e+' == MCABSID) ) )

        mcmatchhp = 0
        mcmatchhm = 0
        mcmatchv0 = 0
        if len(mchp) > 0 :
            mcmatchhp = 1
        if len(mchm) > 0 :
            mcmatchhm = 1
        if len(mcv0) > 0 :
            mcmatchv0 = 1
        tuple.column_int( hpname+'_mcMatch', mcmatchhp )
        tuple.column_int( hmname+'_mcMatch', mcmatchhm )
        tuple.column_int( v0name+'_mcMatch', mcmatchv0 )

        # store the MC truth info about the reconstructed daughters
        self.mc_p4_tuple( tuple, mchp, mcmatchhp, hpname, False )
        self.mc_p4_tuple( tuple, mchm, mcmatchhm, hmname, False )
        self.mc_p4_tuple( tuple, mcv0, mcmatchv0, v0name, False )
        self.mc_vtx_tuple( tuple, mcv0, mcmatchv0, v0name, False )


    def unique_cands( self, cands ) :
        """
        Find unique candidates in a list
        """

        unique_cands = []
        evt_keys = []
        for cand in cands :

            cand_keys = []
            for daug in cand.children() :
                daug_pid = daug.particleID()
                if daug_pid == self.daug3ID or daug_pid == self.daug3ConjID :
                    for gdaug in daug.children() :
                        cand_keys.append( -gdaug.key() )
                else :
                    cand_keys.append( daug.key() )

            cand_keys.sort()

            if cand_keys in evt_keys :
                continue
            else :
                unique_cands.append( cand )
                evt_keys.append( cand_keys )

        return unique_cands


    def get_ntracks( self ) :
        """
        Extracts the number of Best and Long tracks in the event
        """

        nbest = 0
        nlong = 0
        nspdhits = 0
        nrich1hits = 0
        nrich2hits = 0

        try :
            summary = self.get('/Event/Rec/Summary')
            nbest = summary.info(summary.nTracks,0)
            nlong = summary.info(summary.nLongTracks,0)
            nspdhits = summary.info(summary.nSPDhits,0)
            nrich1hits = summary.info(summary.nRich1Hits,0)
            nrich2hits = summary.info(summary.nRich2Hits,0)
        except :
            try :
                tracks = self.get( 'Rec/Track/Best' )
                nlong = 0
                nbest = len(tracks)
                for trk in tracks :
                    if 3 == trk.type() :
                        nlong += 1                         
            except :
                self.Error( 'Information about number of tracks not found neither in /Event/Rec/Summary nor in Rec/Track/Best', SUCCESS )

        return nbest, nlong, nspdhits, nrich1hits, nrich2hits


    def find_daughters( self, bcand ) :
        """
        Identify the 3 daughters of the given parent
        """

        h1cand = None
        h2cand = None
        v0cand = None

        for daug in bcand.children() :
            daug_pid = daug.particleID()

            if daug_pid == self.daug3ID or daug_pid == self.daug3ConjID :
                v0cand = daug
            elif daug.charge() > 0 :
                h1cand = daug
            else :
                h2cand = daug

        return h1cand, h2cand, v0cand


    def find_daughters_wrongsign( self, bcand ) :
        """
        Identify the 3 daughters of the given parent (same-sign version)
        """

        h1cand = None
        h2cand = None
        v0cand = None

        gotFirstCand = False
        for daug in bcand.children() :
            daug_pid = daug.particleID()

            if daug_pid == self.daug3ID or daug_pid == self.daug3ConjID :
                v0cand = daug
            elif not gotFirstCand :
                gotFirstCand = True
                h1cand = daug
            else :
                h2cand = daug


        return h1cand, h2cand, v0cand


    def calc_randno( self, runNum, evtNum ) :
        """
        Calculate the "random" numbers used to identify events for the BDT training
        """

        myRandom1 = (( 134*evtNum + runNum ) % 531241)/531241.0
        myRandom2 = (( 134*runNum + evtNum ) % 531241)/531241.0

        return myRandom1, myRandom2


    def analyse( self ) :
        """
        The method called in the event loop
        """

        # select the candidates from the input
        all_cands = self.select( 'candidates', self.decay_descriptor )
        cands = self.unique_cands( all_cands )
        nCands = len(cands)

        if 0 == nCands :
            return SUCCESS

        # select MC particles
        if self.signalmc :
            mcB  = self.mcselect('mcB',  self.mc_decay_descriptor_Xb)
            mchp = self.mcselect('mchp', self.mc_decay_descriptor_hp)
            mchm = self.mcselect('mchm', self.mc_decay_descriptor_hm)
            mcv0 = self.mcselect('mcv0', self.mc_decay_descriptor_V0)

        # get the event header
        evthdr = self.get( '/Event/Rec/Header' )
        runNum = evthdr.runNumber()
        evtNum = evthdr.evtNumber()
        myRdm1, myRdm2 = self.calc_randno( runNum, evtNum )

        # get the track info
        nbest, nlong, nspdhits, nrich1hits, nrich2hits = self.get_ntracks()

        # loop through the candidates
        iCand = 0

        for bcand in cands :

            # check that the candidate has an associated PV
            bestPV = self.bestVertex( bcand )
            if not bestPV :
                self.Warning( 'Candidate %d in event %d has no associated PV!' % (iCand,evtNum), SUCCESS )
                continue

            # calculate all the DP information (common to all ntuples)
            self.calc_DP_info( bcand )

            # get the daughters of this candidate
            if not self.wrongsign :
                orig_h1cand, orig_h2cand, orig_v0cand = self.find_daughters( bcand )
            else :
                orig_h1cand, orig_h2cand, orig_v0cand = self.find_daughters_wrongsign( bcand )

            # re-fit the candidate under the various daughter mass hypotheses
            for hypo in self.hypotheses :

                # do the PID-swap
                orig_h1cand.setParticleID( LHCb.ParticleID( hypo[0] ) )
                orig_h2cand.setParticleID( LHCb.ParticleID( hypo[1] ) )

                # perform the refit
                fitter = cpp.DecayTreeFitter.Fitter( bcand, self.stateprovider )
                fitter.fit()

                # check that the fit succeeded
                if fitter.status() != 0 :
                    continue

                # retrieve the refitted candidate and its parameters
                newtree = fitter.getFittedTree()
                newbcand = newtree.head()
                bparams = fitter.fitParams( bcand )

                # check that the refitted candidate still has an associated PV
                newBestPV = self.bestVertex( newbcand )
                if not newBestPV :
                    self.Warning( 'Refitted version of candidate %d in event %d has no associated PV!' % (iCand,evtNum), SUCCESS )
                    continue

                # apply cut on the B-candidate mass (calculated under the new daughter hypothesis)
                newbmass = bparams.momentum().m()
                if newbmass.value() < 5000.0 or newbmass.value() > 5800.0 :
                    continue

                # get the B daughters
                if not self.wrongsign :
                    h1cand, h2cand, v0cand = self.find_daughters( newbcand )
                else :
                    h1cand, h2cand, v0cand = self.find_daughters_wrongsign( newbcand )

                # apply the cut on the z-separation of the V0 and B vtx
                bvtx = newbcand.endVertex()
                v0vtx = v0cand.endVertex()
                vtx_z_sep = v0vtx.position().z() - bvtx.position().z()
                if vtx_z_sep < 0 :
                    continue

                # get the right ntuple
                tuple_name = 'B2%s%s%s' % ( self.simplenames[hypo[0]], self.simplenames[hypo[1]], self.v0name )
                tuple = self.nTuple( tuple_name )

                # store the event level info
                tuple.column_int( 'runNumber',  runNum     )
                tuple.column_int( 'evtNumber',  evtNum     )
                tuple.column_int( 'nCands',     nCands     )
                tuple.column_int( 'iCand',      iCand      )
                tuple.column_int( 'BestTracks', nbest      )
                tuple.column_int( 'LongTracks', nlong      )
                tuple.column_int( 'SpdHits',    nspdhits   )
                tuple.column_int( 'Rich1Hits',  nrich1hits )
                tuple.column_int( 'Rich2Hits',  nrich2hits )

                tuple.column_double( 'myRandom1', myRdm1 )
                tuple.column_double( 'myRandom2', myRdm2 )

                # store info for the B
                self.p4_tuple( tuple, newbcand, 'B', bparams )
                self.ip_tuple( tuple, newbcand, 'B' )
                self.extra_B_vars_tuple( tuple, newbcand )

                # store trigger info
                self.trig_tuple( tuple, newbcand )

                # store vertex info for the B and KS
                self.vtx_tuple( tuple, newbcand, v0cand )

                # store vertex isolation info for B
                if not self.wrongsign :
                    self.vtx_isolation_tuple( tuple, bcand )

                if self.simulation :

                    if self.signalmc :
                        # store the MC-truth DP info
                        self.mc_dp_tuple( tuple, mcB )

                        # do the MC matching to the signal MC decay
                        self.mc_match_signal( tuple, mcB, mchp, mchm, mcv0, h1cand, h2cand, v0cand )

                    # do the general MC matching
                    self.mc_match_general( tuple, h1cand, h2cand, v0cand )

                    # store the MC background category info
                    self.bkg_category_tuple( tuple, newbcand, 'B'  )
                    self.bkg_category_tuple( tuple, v0cand, self.v0name )


                # store information on the daughters
                self.p4_tuple( tuple, h1cand, 'h1' )
                self.ip_tuple( tuple, h1cand, 'h1' )
                self.trk_tuple( tuple, h1cand, 'h1' )

                self.p4_tuple( tuple, h2cand, 'h2' )
                self.ip_tuple( tuple, h2cand, 'h2' )
                self.trk_tuple( tuple, h2cand, 'h2' )

                self.p4_tuple( tuple, v0cand, self.v0name )
                self.ip_tuple( tuple, v0cand, self.v0name )

                # and the V0 daughters
                for gdaug in v0cand.children() :
                    gdaugID = gdaug.particleID().pid()
                    gdaugname = self.gdaugnames[ gdaugID ]
                    self.p4_tuple( tuple, gdaug, gdaugname )
                    self.trk_tuple( tuple, gdaug, gdaugname )

                # store all the pre-calculated DP information
                self.store_DP_info( tuple )

                # fill the ntuple
                tuple.write()

            # do the PID-swap back
            orig_h1cand.setParticleID( LHCb.ParticleID( self.daug1ID ) )
            orig_h2cand.setParticleID( LHCb.ParticleID( self.daug2ID ) )

            iCand += 1

        return SUCCESS

# End of B2KShhReco Class Definition


# Begin DalitzInfo Class Definition

class DalitzInfo(object) :

    """
    Helper class to store Dalitz-plot fit information and fill a tuple
    """

    def __init__( self, v0name, suffix ) :
        """
        Constructor
        """

        self.v0name = v0name
        self.suffix = suffix
        self.init_values()


    def init_values( self ) :
        """
        Set default values for all variables
        """

        self.status  = -1
        self.errcode = -1
        self.niter   = -1

        self.ndof = -1
        self.chi2 = -1.1
        self.prob = -1.1

        self.b_ctau    = -1.1
        self.b_ctauerr = -1.1
        self.b_ctausig = -1.1

        self.b_p4  = None
        self.daug_p4 = {}
        self.daug_p4['h1'] = None
        self.daug_p4['h2'] = None
        self.daug_p4[self.v0name] = None

        self.m12Sq = -1.1
        self.m13Sq = -1.1
        self.m23Sq = -1.1

        self.mPrime  = -1.1
        self.thPrime = -1.1


    def store_fit_results( self, fitter, bctau, bp4, daug_p4, m12Sq, m13Sq, m23Sq, mPrime, thPrime ) :
        """
        Extract the fit information from the fitter and store it
        """

        self.init_values()

        self.status  = fitter.status()
        self.errcode = fitter.errCode()
        self.niter   = fitter.nIter()

        if self.status != 0 :
            return

        self.chi2 = fitter.chiSquare()
        self.ndof = fitter.nDof()
        self.prob = TMath.Prob( self.chi2, self.ndof )

        self.b_ctau    = bctau.value()
        self.b_ctauerr = bctau.error()
        self.b_ctausig = self.b_ctau/self.b_ctauerr

        self.b_p4 = Gaudi.Math.LorentzVectorWithError(bp4)

        self.daug_p4['h1'] = Gaudi.Math.LorentzVectorWithError(daug_p4[0])
        self.daug_p4['h2'] = Gaudi.Math.LorentzVectorWithError(daug_p4[1])
        self.daug_p4[self.v0name] = Gaudi.Math.LorentzVectorWithError(daug_p4[2])

        self.m12Sq = m12Sq
        self.m13Sq = m13Sq
        self.m23Sq = m23Sq

        self.mPrime  = mPrime
        self.thPrime = thPrime


    def fill_tuple( self, tuple ) :
        """
        Fill the supplied ntuple with the information
        """

        # store info on the success (or otherwise) of the mass constrained fit
        tuple.column_int( 'B_DTF_STATUS_'+self.suffix,  self.status  )
        tuple.column_int( 'B_DTF_ERRCODE_'+self.suffix, self.errcode )
        tuple.column_int( 'B_DTF_NITER_'+self.suffix,   self.niter   )

        # also store the fit chisq, ndof and associated probability
        tuple.column_int(    'B_ENDVERTEX_NDOF_'+self.suffix, self.ndof )
        tuple.column_double( 'B_ENDVERTEX_CHI2_'+self.suffix, self.chi2 )
        tuple.column_double( 'B_ENDVERTEX_PROB_'+self.suffix, self.prob )

        # store information on the B candidate
        tuple.column_double( 'B_CTAU_'+self.suffix,    self.b_ctau    )
        tuple.column_double( 'B_CTAUERR_'+self.suffix, self.b_ctauerr )
        tuple.column_double( 'B_CTAUSIG_'+self.suffix, self.b_ctausig )

        if self.b_p4 :
            tuple.column_double( 'B_M_'+self.suffix,     self.b_p4.m().value()  )
            tuple.column_double( 'B_MERR_'+self.suffix,  self.b_p4.m().error()  )
            tuple.column_double( 'B_PX_'+self.suffix,    self.b_p4.Px()         )
            tuple.column_double( 'B_PY_'+self.suffix,    self.b_p4.Py()         )
            tuple.column_double( 'B_PZ_'+self.suffix,    self.b_p4.Pz()         )
            tuple.column_double( 'B_PE_'+self.suffix,    self.b_p4.E()          )
        else :
            tuple.column_double( 'B_M_'+self.suffix,     -1.1 )
            tuple.column_double( 'B_MERR_'+self.suffix,  -1.1 )
            tuple.column_double( 'B_PX_'+self.suffix,    -1.1 )
            tuple.column_double( 'B_PY_'+self.suffix,    -1.1 )
            tuple.column_double( 'B_PZ_'+self.suffix,    -1.1 )
            tuple.column_double( 'B_PE_'+self.suffix,    -1.1 )

        # loop through 3 B daughters and store their 4-momenta
        for daugname in self.daug_p4.keys() :
            p4 = self.daug_p4[ daugname ]
            if p4 :
                tuple.column_double( daugname+'_M_'+self.suffix,  p4.M()  )
                tuple.column_double( daugname+'_PX_'+self.suffix, p4.Px() )
                tuple.column_double( daugname+'_PY_'+self.suffix, p4.Py() )
                tuple.column_double( daugname+'_PZ_'+self.suffix, p4.Pz() )
                tuple.column_double( daugname+'_PE_'+self.suffix, p4.E()  )
            else :
                tuple.column_double( daugname+'_M_'+self.suffix,  -1.1 )
                tuple.column_double( daugname+'_PX_'+self.suffix, -1.1 )
                tuple.column_double( daugname+'_PY_'+self.suffix, -1.1 )
                tuple.column_double( daugname+'_PZ_'+self.suffix, -1.1 )
                tuple.column_double( daugname+'_PE_'+self.suffix, -1.1 )

        # store the DP co-ordinates
        tuple.column_double( 'm12Sq_'+self.suffix, self.m12Sq )
        tuple.column_double( 'm13Sq_'+self.suffix, self.m13Sq )
        tuple.column_double( 'm23Sq_'+self.suffix, self.m23Sq )

        # store the square DP co-ordinates
        tuple.column_double( 'mPrime_'+self.suffix,  self.mPrime  )
        tuple.column_double( 'thPrime_'+self.suffix, self.thPrime )


# End DalitzInfo Class Definition

