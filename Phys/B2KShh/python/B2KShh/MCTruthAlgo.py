#!/usr/bin/env python

from Bender.Main import *
from Bender.MainMC import *
from ROOT import ( TLorentzVector, TMath )
from B2KShh.ThreeBodyKinematics import ThreeBodyKinematics


# Begin B2KShhMCTruth Class Definition

class B2KShhMCTruth(AlgoMC) :

    """
    Algorithm to perform ntupling of MC truth information for the B2KShh analyses.
    """

    def __init__( self, name, Btype, h1type, h2type, V0type, isXGen, **kwargs ) :
        super(B2KShhMCTruth,self).__init__( name, **kwargs )
        self.set_types( Btype, h1type, h2type, V0type, isXGen )

    def set_types( self, Btype, h1type, h2type, V0type, isXGen ):
        """
        Define the decay mode of the MC being run over:
        Btype = B0, B_s0 or Lambda_b0
        h1type = pi, K or p
        h2type = pi, K or p
        V0type = KS0 or Lambda0
        isXGen = true if xgen is supposed to be analyzed
        """

        self.xgen = isXGen
        self.names = {}
        self.gdaugnames = {}


        # determine the parent type
        self.parentID = None

        if Btype == 'Bd' or Btype == 'B0' :
            self.parentID = LHCb.ParticleID(511)
            self.names[ 511] = 'Bd'
            self.names[-511] = 'Bd'

        elif Btype == 'Bs' or Btype == 'B_s0' :
            self.parentID = LHCb.ParticleID(531)
            self.names[ 531] = 'Bs'
            self.names[-531] = 'Bs'

        elif Btype == 'Lb' or Btype == 'Lambda_b0' :
            self.parentID = LHCb.ParticleID(5122)
            self.names[ 5122] = 'Lb'
            self.names[-5122] = 'Lb'

        else :
            self.Warning( 'B type ('+Btype+') not recognised, setting to B0.', SUCCESS )
            self.parentID = LHCb.ParticleID(511)
            self.names[ 511] = 'Bd'
            self.names[-511] = 'Bd'

        # determine the V0 type
        self.daug3ID = None
        self.daug3ConjID = None

        if V0type == 'KS' or V0type == 'KS0' :
            self.daug3ID = LHCb.ParticleID(310)
            self.daug3ConjID = LHCb.ParticleID(310)
            self.names[310]   = 'KS'
            self.gdaugnames[ 211] = 'KSpip'
            self.gdaugnames[-211] = 'KSpim'

        elif V0type == 'L' or V0type == 'Lambda0' :
            if self.parentID.pid() == 5122 :
                self.daug3ID = LHCb.ParticleID(3122)
                self.daug3ConjID = LHCb.ParticleID(-3122)
            else :
                self.daug3ID = LHCb.ParticleID(-3122)
                self.daug3ConjID = LHCb.ParticleID(3122)
            self.names[ 3122] = 'Lz'
            self.names[-3122] = 'Lz'
            self.gdaugnames[ 2212] = 'Lzp'
            self.gdaugnames[-2212] = 'Lzp'
            self.gdaugnames[ 211] = 'Lzpi'
            self.gdaugnames[-211] = 'Lzpi'

        else :
            self.Warning( 'V0 type ('+V0type+') not recognised, setting to KS0.', SUCCESS )
            self.daug3ID = LHCb.ParticleID(310)
            self.daug3ConjID = LHCb.ParticleID(310)
            self.names[310]   = 'KS'
            self.gdaugnames[ 211] = 'KSpip'
            self.gdaugnames[-211] = 'KSpim'

        # determine the charged daughter types
        self.daug1ID = None
        self.daug2ID = None

        if h1type == 'pi' and h2type == 'pi':
            self.daug1ID = LHCb.ParticleID( 211)
            self.daug2ID = LHCb.ParticleID(-211)
            self.names[ 211] = 'h1'
            self.names[-211] = 'h2'

        elif h1type == 'K' and h2type == 'K':
            self.daug1ID = LHCb.ParticleID( 321)
            self.daug2ID = LHCb.ParticleID(-321)
            self.names[ 321] = 'h1'
            self.names[-321] = 'h2'

        elif h1type == 'p' and h2type == 'p':
            self.daug1ID = LHCb.ParticleID( 2212)
            self.daug2ID = LHCb.ParticleID(-2212)
            self.names[ 2212] = 'h1'
            self.names[-2212] = 'h2'

        elif h1type == 'K' and h2type == 'pi':
            self.daug1ID = LHCb.ParticleID( 321)
            self.daug2ID = LHCb.ParticleID(-211)
            self.names[ 321] = 'h1'
            self.names[-321] = 'h1'
            self.names[ 211] = 'h2'
            self.names[-211] = 'h2'

        elif h1type == 'p' and h2type == 'pi':
            self.daug1ID = LHCb.ParticleID(2212)
            self.daug2ID = LHCb.ParticleID(-211)
            self.names[ 2212] = 'h1'
            self.names[-2212] = 'h1'
            self.names[ 211] = 'h2'
            self.names[-211] = 'h2'

        elif h1type == 'p' and h2type == 'K':
            self.daug1ID = LHCb.ParticleID(2212)
            self.daug2ID = LHCb.ParticleID(-321)
            self.names[ 2212] = 'h1'
            self.names[-2212] = 'h1'
            self.names[ 321] = 'h2'
            self.names[-321] = 'h2'

        else :
            self.Warning( 'hh types ('+h1type+' and '+h2type+') not recognised, setting to pions.' , SUCCESS )
            self.daug1ID = LHCb.ParticleID( 211)
            self.daug2ID = LHCb.ParticleID(-211)
            self.names[ 211] = 'h1'
            self.names[-211] = 'h2'



    def check_types( self ) :

        initB = 0
        if self.parentID.isBaryon() :
            initB = 1

        finalB = 0
        for id in ( self.daug1ID, self.daug2ID, self.daug3ID ) :
            if id.isBaryon() :
                if id.pid() > 0 :
                    finalB += 1
                else :
                    finalB -= 1

        if initB != finalB :
            self.Error( 'Initial and final state baryon numbers do not match, '+str(initB)+' != '+str(finalB) )
            return FAILURE

        daug1Mass = self.partpropsvc.find( self.daug1ID ).mass()
        daug2Mass = self.partpropsvc.find( self.daug2ID ).mass()
        daug3Mass = self.partpropsvc.find( self.daug3ID ).mass()
        parentMass = self.partpropsvc.find( self.parentID ).mass()

        if parentMass < ( daug1Mass + daug2Mass + daug3Mass ) :
            return FAILURE

        self.kinematics = ThreeBodyKinematics( daug1Mass, daug2Mass, daug3Mass, parentMass )

        return SUCCESS


    def form_decay_descriptor( self ) :

        par_name   = self.partpropsvc.find( self.parentID ).name()
        daug1_name = self.partpropsvc.find( self.daug1ID ).name()
        daug2_name = self.partpropsvc.find( self.daug2ID ).name()
        daug3_name = self.partpropsvc.find( self.daug3ID ).name()

        par_conj_name = self.partpropsvc.find( self.parentID ).anti().name()
        daug1_conj_name = self.partpropsvc.find( self.daug1ID ).anti().name()
        daug2_conj_name = self.partpropsvc.find( self.daug2ID ).anti().name()
        daug3_conj_name = self.partpropsvc.find( self.daug3ID ).anti().name()

        if self.parentID.isBaryon() or self.daug1ID.abspid() == self.daug2ID.abspid() :
            self.decay_descriptor = '[ '+par_name+' => '+daug1_name+' '+daug2_name+' '+daug3_name+' ]CC'
        else :
            ddhead = '( '+par_name+' | '+par_conj_name+' ) => '
            ddpart1 = '( '+ddhead+' '+daug1_name+' '+daug2_name+' '+daug3_name+' )'
            ddpart2 = '( '+ddhead+' '+daug1_conj_name+' '+daug2_conj_name+' '+daug3_conj_name+' )'
            self.decay_descriptor = '['+ddpart1+', '+ddpart2+']'

        self.Info( 'Will use the decay descriptor '+self.decay_descriptor )


    def initialize( self ) :

        sc = AlgoMC.initialize( self )
        if sc.isFailure() :
            return sc

        # get the particle property service
        self.partpropsvc = self.ppSvc()

        # check the validity of the decay
        sc = self.check_types()
        if sc.isFailure() :
            return sc

        # form the decay descriptor
        self.form_decay_descriptor()

        # set up the reconstrucible/reconstructed tools
        if not self.xgen : 
            self.recible = self.tool( cpp.IMCReconstructible, 'MCReconstructible' )
            self.rected  = self.tool( cpp.IMCReconstructed,   'MCReconstructed'   )

        return SUCCESS


    def reco_status_tuple( self, tuple, mcparticle, name ) :
        """
        Store the reconstructible/reconstructed status of an MC particle
        """

        if not mcparticle or self.xgen :
            tuple.column_int( name + '_Reconstructible', -1 )
            tuple.column_int( name + '_Reconstructed',   -1 )
            return

        cat_ible = self.recible.reconstructible( mcparticle )
        cat_ted  = self.rected.reconstructed( mcparticle )

        tuple.column_int( name + '_Reconstructible', int(cat_ible) )
        tuple.column_int( name + '_Reconstructed',   int(cat_ted)  )


    def mc_p4_tuple( self, tuple, mcparticle, name ) :
        """
        Store the id, charge, 4-momentum, mass, p_t and eta of an MC particle
        """

        if not mcparticle :
            tuple.column_int( name + '_TRUEID',    -1   )
            tuple.column_int( name + '_TRUEQ',     -1   )
            tuple.column_double( name + '_TRUEP',     -1.1 )
            tuple.column_double( name + '_TRUEPE',    -1.1 )
            tuple.column_double( name + '_TRUEPX',    -1.1 )
            tuple.column_double( name + '_TRUEPY',    -1.1 )
            tuple.column_double( name + '_TRUEPZ',    -1.1 )
            tuple.column_double( name + '_TRUEPT',    -1.1 )
            tuple.column_double( name + '_TRUEETA',   -1.1 )
            tuple.column_double( name + '_TRUEPHI',   -1.1 )
            tuple.column_double( name + '_TRUETHETA', -1.1 )
            tuple.column_double( name + '_TRUEM',     -1.1 )
            tuple.column_int( name + '_OSCIL',     -1   )
            return

        tuple.column_int(    name + '_TRUEID',    int(MCID(mcparticle))         )
        tuple.column_int(    name + '_TRUEQ',     int(MC3Q(mcparticle)/3)       )
        tuple.column_double( name + '_TRUEP',     MCP(mcparticle)               )
        tuple.column_double( name + '_TRUEPE',    MCE(mcparticle)               )
        tuple.column_double( name + '_TRUEPX',    MCPX(mcparticle)              )
        tuple.column_double( name + '_TRUEPY',    MCPY(mcparticle)              )
        tuple.column_double( name + '_TRUEPZ',    MCPZ(mcparticle)              )
        tuple.column_double( name + '_TRUEPT',    MCPT(mcparticle)              )
        tuple.column_double( name + '_TRUEETA',   MCETA(mcparticle)             )
        tuple.column_double( name + '_TRUEPHI',   MCPHI(mcparticle)             )
        tuple.column_double( name + '_TRUETHETA', MCTHETA(mcparticle)           )
        tuple.column_double( name + '_TRUEM',     MCM(mcparticle)               )
        tuple.column_int(    name + '_OSCIL',     int(MCOSCILLATED(mcparticle)) )


    def mc_vtx_tuple( self, tuple, mcparticle, name ) :
        """
        Store vertex and lifetime info for the MC particle
        """

        if not mcparticle :
            tuple.column_double( name + '_TRUEORIGINVERTEX_X', -1.1 )
            tuple.column_double( name + '_TRUEORIGINVERTEX_Y', -1.1 )
            tuple.column_double( name + '_TRUEORIGINVERTEX_Z', -1.1 )
            tuple.column_double( name + '_TRUECTAU'          , -1.1 )
            return

        tuple.column_double( name + '_TRUEORIGINVERTEX_X', MCVFASPF(MCVX)(mcparticle) )
        tuple.column_double( name + '_TRUEORIGINVERTEX_Y', MCVFASPF(MCVY)(mcparticle) )
        tuple.column_double( name + '_TRUEORIGINVERTEX_Z', MCVFASPF(MCVZ)(mcparticle) )
        tuple.column_double( name + '_TRUECTAU'          , MCCTAU(mcparticle)         )


    def mc_dp_tuple( self, tuple, mcparent ) :
        """
        Store the MC truth DP co-ordinates
        """

        # loop through the daughters and store their 4-momenta treating the
        # expected 3 daughters and PHOTOS photons separately

        daug_id = [ 0, 0, 0 ]
        daug_p4 = [ TLorentzVector(), TLorentzVector(), TLorentzVector() ]
        gamma_p4 = []

        parID = mcparent.particleID().pid()

        for daug in mcparent.children( True ) :

            daugID = daug.particleID().pid()

            px = MCPX( daug )
            py = MCPY( daug )
            pz = MCPZ( daug )
            pe = MCE ( daug )

            p4 = TLorentzVector( px, py, pz, pe )

            if daugID == 22 :
                gamma_p4.append( p4 )
            elif self.daug1ID.abspid() == self.daug2ID.abspid() :
                if daugID == self.daug1ID.pid() :
                    daug_p4[0] = p4
                    daug_id[0] = daugID
                elif daugID == self.daug2ID.pid() :
                    daug_p4[1] = p4
                    daug_id[1] = daugID
                elif daugID == self.daug3ID.pid() or daugID == self.daug3ConjID.pid() :
                    daug_p4[2] = p4
                    daug_id[2] = daugID
                else :
                    self.Error( 'Daughter does not match any of the expected types' )
                    e = Exception('Unknown daughter type: '+str(daugID))
                    raise e
            else :
                if abs(daugID) == self.daug1ID.abspid() :
                    daug_p4[0] = p4
                    daug_id[0] = daugID
                elif abs(daugID) == self.daug2ID.abspid() :
                    daug_p4[1] = p4
                    daug_id[1] = daugID
                elif abs(daugID) == self.daug3ID.abspid() :
                    daug_p4[2] = p4
                    daug_id[2] = daugID
                else :
                    self.Error( 'Daughter does not match any of the expected types' )
                    e = Exception('Unknown daughter type: '+str(daugID))
                    raise e

        ngamma = len(gamma_p4)
        tuple.column_int( 'nPHOTOS', ngamma )

        if 0 != ngamma :

            for gamma in gamma_p4 :
                minangle = 1000.0
                mindaug = -1
                for daug in daug_p4 :
                    angle = gamma.Angle( daug.Vect() )
                    if abs(angle) < minangle :
                        minangle = angle
                        mindaug = daug_p4.index(daug)

                daug_p4[ mindaug ] += gamma


        p12 = daug_p4[0] + daug_p4[1]
        p13 = daug_p4[0] + daug_p4[2]
        p23 = daug_p4[1] + daug_p4[2]

        for i in range(3) :
            daug_name = self.names[ daug_id[i] ]
            tuple.column_double( daug_name + '_CORRPE',  daug_p4[i].E()  )
            tuple.column_double( daug_name + '_CORRPX',  daug_p4[i].Px() )
            tuple.column_double( daug_name + '_CORRPY',  daug_p4[i].Py() )
            tuple.column_double( daug_name + '_CORRPZ',  daug_p4[i].Pz() )

        m12Sq = p12.M2()
        m13Sq = p13.M2()
        m23Sq = p23.M2()

        mPrime = -1.1
        thPrime = -1.1

        if self.kinematics.withinDPLimits( m13Sq, m23Sq ) :
            self.kinematics.updateKinematics( m13Sq, m23Sq )
            mPrime = self.kinematics.mPrime
            thPrime = self.kinematics.thPrime

        if m12Sq<0 :     m12Sq = -1.1
        if m12Sq>100e6 : m12Sq = -1.1
        if m13Sq<0 :     m13Sq = -1.1
        if m13Sq>100e6 : m13Sq = -1.1
        if m23Sq<0 :     m23Sq = -1.1
        if m23Sq>100e6 : m23Sq = -1.1

        tuple.column_double( 'm12Sq_MC', m12Sq )
        tuple.column_double( 'm13Sq_MC', m13Sq )
        tuple.column_double( 'm23Sq_MC', m23Sq )

        tuple.column_double( 'mPrime_MC',  mPrime  )
        tuple.column_double( 'thPrime_MC', thPrime )


    def analyse( self ) :
        """
        The method called in the event loop
        """

        # select MC particles
        cands  = self.mcselect('cands',  self.decay_descriptor)
        ncands = cands.size()

        if 0 == ncands :
            self.Warning( 'No MC candidates found in this event', SUCCESS )
            return SUCCESS

        # get the event header
        if not self.xgen:
            evthdr = self.get( '/Event/Rec/Header' )
        else :
            evthdr = self.get( '/Event/Gen/Header' )
            
        # create the ntuple
        tuple = self.nTuple( 'tupleMCTruth' )

        # loop through the candidates
        for cand in cands :

            # fill event information
            tuple.column_int( 'runNumber', evthdr.runNumber() )
            tuple.column_int( 'evtNumber', evthdr.evtNumber() )
            tuple.column_int( 'nCands',    ncands             )

            # get the ID and hance name of the parent
            candID = cand.particleID().pid()
            candname = self.names[ candID ]

            # store parent information
            self.mc_p4_tuple( tuple, cand, candname )
            self.mc_vtx_tuple( tuple, cand, candname )
            self.reco_status_tuple( tuple, cand, candname )

            # store DP information
            self.mc_dp_tuple( tuple, cand )

            # loop through the daughters and store their information
            for daug in cand.children( True ) :

                daugID = daug.particleID().pid()
                if 22 == daugID :
                    continue
                daugname = self.names[ daugID ]

                self.mc_p4_tuple( tuple, daug, daugname )
                self.mc_vtx_tuple( tuple, daug, daugname )
                self.reco_status_tuple( tuple, daug, daugname )

                # if this is the V0 then also find and store information for it's daughters
                if abs(daugID) == self.daug3ID.abspid() :

                    tuple.column_int( daugname+'_NDAUG', daug.nChildren() )

                    gdaug1_name = ''
                    gdaug2_name = ''

                    for gdaug in daug.children( True ) :
                        gdaugID = gdaug.particleID().pid()
                        if gdaugID not in self.gdaugnames :
                            continue

                        gdaugname = self.gdaugnames[gdaugID]
                        if gdaug1_name == '' :
                            gdaug1_name = gdaugname
                        elif gdaug2_name == '' :
                            gdaug2_name = gdaugname
                        else :
                            self.Warning('Unexpected extra daughter of '+daugname, SUCCESS)
                            continue

                        self.mc_p4_tuple( tuple, gdaug, gdaugname )
                        self.mc_vtx_tuple( tuple, gdaug, gdaugname )
                        self.reco_status_tuple( tuple, gdaug, gdaugname )

                    gdaugnames = self.gdaugnames.values()
                    if gdaug1_name in gdaugnames :
                        gdaugnames.remove( gdaug1_name )
                    if gdaug2_name in gdaugnames :
                        gdaugnames.remove( gdaug2_name )

                    for name in gdaugnames :
                        self.mc_p4_tuple( tuple, None, name )
                        self.mc_vtx_tuple( tuple, None, name )
                        self.reco_status_tuple( tuple, None, name )

            tuple.write()


        return SUCCESS

# End of B2KShhMCTruth Class Definition

