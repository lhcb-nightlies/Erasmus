#!/usr/bin/env python

from Bender.Main import *
from Bender.MainMC import *
from ROOT import TLorentzVector
from B2KShh.ThreeBodyKinematics import ThreeBodyKinematics


# Begin B2KShhMCTruthResonance Class Definition

class B2KShhMCTruthResonance(AlgoMC) :

    """
    Algorithm to perform ntupling of MC truth information for the B2KShh analyses.
    """

    def __init__( self, name, Btype, resType, bachType, resDaug1type, resDaug2type, **kwargs ) :
        super(B2KShhMCTruthResonance,self).__init__( name, **kwargs )
        self.set_types( Btype, resType, bachType, resDaug1type, resDaug2type )

    def set_types( self, Btype, resType, bachType, resDaug1type, resDaug2type ) :
        """
        Define the decay mode of the MC being run over:
        Btype = Lambda_b0, B0 or B_s0
        resType = phi, Kst0, Kst~0, Kst+, Kst-, rho0, f0
        bachType = pi+, pi-, K+, K-, p+, p~-, KS0, Lambda0, Lambda~0
        resDaug1type = pi+, pi-, K+, K-, p+, p~-, KS0, Lambda0, Lambda~0
        resDaug2type = pi+, pi-, K+, K-, p+, p~-, KS0, Lambda0, Lambda~0
        One (and only one) of bachType, resDaug1type and resDaug2type must be a KS0/Lambda0/Lambda~0.
        """

        self.parentID = None
        self.daug1ID = None
        self.daug2ID = None
        self.daug3ID = None

        self.resonanceID = None
        self.resDaug1ID = None
        self.resDaug2ID = None
        self.bachelorID = None

        self.names = {}
        self.gdaugnames = {}

        self.selfconj = True

        # determine the B type
        if Btype == 'Bd' or Btype == 'B0' :
            self.parentID = LHCb.ParticleID(511)
            self.names[ 511] = 'Bd'
            self.names[-511] = 'Bd'
        elif Btype == 'Bs' or Btype == 'B_s0' :
            self.parentID = LHCb.ParticleID(531)
            self.names[ 531] = 'Bs'
            self.names[-531] = 'Bs'
        elif Btype == 'Lb' or Btype == 'Lambda_b0' :
            self.parentID = LHCb.ParticleID(5122)
            self.names[ 5122] = 'Lb'
            self.names[-5122] = 'Lb'
            self.selfconj = False
        else :
            self.Warning( 'B type ('+Btype+') not recognised, setting to B0.' , SUCCESS )
            self.parentID = LHCb.ParticleID(511)
            self.names[ 511] = 'Bd'
            self.names[-511] = 'Bd'

        # determine the V0 type
        daug_list = []
        daug_list.append( resDaug1type )
        daug_list.append( resDaug2type )
        daug_list.append( bachType )

        nKS = daug_list.count( 'KS0' )
        nLz = daug_list.count( 'Lambda0' )
        nLzb = daug_list.count( 'Lambda~0' )
        if 1 != ( nKS + nLz + nLzb ) :
            self.Error( 'Wrong number of V0 particles' )
            return

        if nKS == 1 :
            self.daug3ID = LHCb.ParticleID(310)
            self.names[310]   = 'KS'
            self.gdaugnames[ 211] = 'KSpip'
            self.gdaugnames[-211] = 'KSpim'
            daug_list.remove( 'KS0' )
        elif nLz == 1 :
            self.daug3ID = LHCb.ParticleID(3122)
            self.names[ 3122] = 'Lz'
            self.names[-3122] = 'Lz'
            self.gdaugnames[ 2212] = 'Lzp'
            self.gdaugnames[-2212] = 'Lzp'
            self.gdaugnames[ 211] = 'Lzpi'
            self.gdaugnames[-211] = 'Lzpi'
            self.selfconj = False
            daug_list.remove( 'Lambda0' )
        else :
            self.daug3ID = LHCb.ParticleID(-3122)
            self.names[ 3122] = 'Lz'
            self.names[-3122] = 'Lz'
            self.gdaugnames[ 2212] = 'Lzp'
            self.gdaugnames[-2212] = 'Lzp'
            self.gdaugnames[ 211] = 'Lzpi'
            self.gdaugnames[-211] = 'Lzpi'
            self.selfconj = False
            daug_list.remove( 'Lambda~0' )

        # determine the charge daughter types
        daug_list[0] = daug_list[0].replace('+','')
        daug_list[0] = daug_list[0].replace('-','')
        daug_list[0] = daug_list[0].replace('~','')
        daug_list[1] = daug_list[1].replace('+','')
        daug_list[1] = daug_list[1].replace('-','')
        daug_list[1] = daug_list[1].replace('~','')

        if daug_list[0] == 'pi' and daug_list[1] == 'pi':
            self.daug1ID = LHCb.ParticleID( 211)
            self.daug2ID = LHCb.ParticleID(-211)
            self.names[ 211] = 'h1'
            self.names[-211] = 'h2'

        elif daug_list[0] == 'K' and daug_list[1] == 'K':
            self.daug1ID = LHCb.ParticleID( 321)
            self.daug2ID = LHCb.ParticleID(-321)
            self.names[ 321] = 'h1'
            self.names[-321] = 'h2'

        if daug_list[0] == 'p' and daug_list[1] == 'p':
            self.daug1ID = LHCb.ParticleID( 2212)
            self.daug2ID = LHCb.ParticleID(-2212)
            self.names[ 2212] = 'h1'
            self.names[-2212] = 'h2'

        elif (daug_list[0] == 'K' and daug_list[1] == 'pi') or (daug_list[0] == 'pi' and daug_list[1] == 'K'):
            self.daug1ID = LHCb.ParticleID( 321)
            self.daug2ID = LHCb.ParticleID(-211)
            self.names[ 321] = 'h1'
            self.names[-321] = 'h1'
            self.names[ 211] = 'h2'
            self.names[-211] = 'h2'
            self.selfconj = False

        elif (daug_list[0] == 'p' and daug_list[1] == 'pi') or (daug_list[0] == 'pi' and daug_list[1] == 'p'):
            self.daug1ID = LHCb.ParticleID(2212)
            self.daug2ID = LHCb.ParticleID(-211)
            self.names[ 2212] = 'h1'
            self.names[-2212] = 'h1'
            self.names[ 211] = 'h2'
            self.names[-211] = 'h2'
            self.selfconj = False

        elif (daug_list[0] == 'p' and daug_list[1] == 'K') or (daug_list[0] == 'K' and daug_list[1] == 'p'):
            self.daug1ID = LHCb.ParticleID(2212)
            self.daug2ID = LHCb.ParticleID(-321)
            self.names[ 2212] = 'h1'
            self.names[-2212] = 'h1'
            self.names[ 321] = 'h2'
            self.names[-321] = 'h2'
            self.selfconj = False

        else :
            self.Warning( 'hh types ('+daug_list[0]+' and '+daug_list[1]+') not recognised, setting to pions.' , SUCCESS )
            self.daug1ID = LHCb.ParticleID( 211)
            self.daug2ID = LHCb.ParticleID(-211)
            self.names[ 211] = 'h1'
            self.names[-211] = 'h2'

        # determine the resonance type
        if resType == 'phi' :
            self.resonanceID = LHCb.ParticleID(333)
        elif resType == 'Kst0' :
            self.resonanceID = LHCb.ParticleID(313)
        elif resType == 'Kst~0' :
            self.resonanceID = LHCb.ParticleID(-313)
        elif resType == 'Kst+' :
            self.resonanceID = LHCb.ParticleID(323)
        elif resType == 'Kst-' :
            self.resonanceID = LHCb.ParticleID(-323)
        elif resType == 'rho0' :
            self.resonanceID = LHCb.ParticleID(113)
        elif resType == 'f0' :
            self.resonanceID = LHCb.ParticleID(9010221)
        else :
            self.Warning( 'Resonance type ('+resType+') not recognised, setting to rho0.' , SUCCESS )
            self.resonanceID = LHCb.ParticleID(113)

        # determine the resonance daughter types
        if resDaug1type == 'pi+' :
            self.resDaug1ID = LHCb.ParticleID(211)
        elif resDaug1type == 'pi-' :
            self.resDaug1ID = LHCb.ParticleID(-211)
        elif resDaug1type == 'K+' :
            self.resDaug1ID = LHCb.ParticleID(321)
        elif resDaug1type == 'K-' :
            self.resDaug1ID = LHCb.ParticleID(-321)
        elif resDaug1type == 'p+' :
            self.resDaug1ID = LHCb.ParticleID(2212)
        elif resDaug1type == 'p~-' :
            self.resDaug1ID = LHCb.ParticleID(-2212)
        elif resDaug1type == 'KS0' :
            self.resDaug1ID = LHCb.ParticleID(310)
        elif resDaug1type == 'Lambda0' :
            self.resDaug1ID = LHCb.ParticleID(3122)
        elif resDaug1type == 'Lambda~0' :
            self.resDaug1ID = LHCb.ParticleID(-3122)
        else :
            self.Warning( 'Resonance daughter 1 type ('+resDaug1type+') not recognised, setting to pi+.' , SUCCESS )
            self.resDaug1ID = LHCb.ParticleID(211)

        if resDaug2type == 'pi+' :
            self.resDaug2ID = LHCb.ParticleID(211)
        elif resDaug2type == 'pi-' :
            self.resDaug2ID = LHCb.ParticleID(-211)
        elif resDaug2type == 'K+' :
            self.resDaug2ID = LHCb.ParticleID(321)
        elif resDaug2type == 'K-' :
            self.resDaug2ID = LHCb.ParticleID(-321)
        elif resDaug2type == 'p+' :
            self.resDaug2ID = LHCb.ParticleID(2212)
        elif resDaug2type == 'p~-' :
            self.resDaug2ID = LHCb.ParticleID(-2212)
        elif resDaug2type == 'KS0' :
            self.resDaug2ID = LHCb.ParticleID(310)
        elif resDaug2type == 'Lambda0' :
            self.resDaug2ID = LHCb.ParticleID(3122)
        elif resDaug2type == 'Lambda~0' :
            self.resDaug2ID = LHCb.ParticleID(-3122)
        else :
            self.Warning( 'Resonance daughter 2 type ('+resDaug2type+') not recognised, setting to pi-.' , SUCCESS )
            self.resDaug2ID = LHCb.ParticleID(-211)

        # determine the bachelor type
        if bachType == 'pi+' :
            self.bachelorID = LHCb.ParticleID(211)
        elif bachType == 'pi-' :
            self.bachelorID = LHCb.ParticleID(-211)
        elif bachType == 'K+' :
            self.bachelorID = LHCb.ParticleID(321)
        elif bachType == 'K-' :
            self.bachelorID = LHCb.ParticleID(-321)
        elif bachType == 'p+' :
            self.bachelorID = LHCb.ParticleID(2212)
        elif bachType == 'p~-' :
            self.bachelorID = LHCb.ParticleID(-2212)
        elif bachType == 'KS0' :
            self.bachelorID = LHCb.ParticleID(310)
        elif bachType == 'Lambda0' :
            self.bachelorID = LHCb.ParticleID(3122)
        elif bachType == 'Lambda~0' :
            self.bachelorID = LHCb.ParticleID(-3122)
        else :
            self.Warning( 'Bachelor type ('+bachType+') not recognised, setting to KS0.' , SUCCESS )
            self.bachelorID = LHCb.ParticleID(310)



    def check_types( self ) :

        initB = 0
        if self.parentID.isBaryon() :
            initB = 1

        finalB = 0
        for id in ( self.daug1ID, self.daug2ID, self.daug3ID ) :
            if id.isBaryon() :
                if id.pid() > 0 :
                    finalB += 1
                else :
                    finalB -= 1

        if initB != finalB :
            self.Error( 'Initial and final state baryon numbers do not match, '+str(initB)+' != '+str(finalB) )
            return FAILURE

        daug1Mass = self.partpropsvc.find( self.daug1ID ).mass()
        daug2Mass = self.partpropsvc.find( self.daug2ID ).mass()
        daug3Mass = self.partpropsvc.find( self.daug3ID ).mass()
        parentMass = self.partpropsvc.find( self.parentID ).mass()

        if parentMass < ( daug1Mass + daug2Mass + daug3Mass ) :
            return FAILURE

        self.kinematics = ThreeBodyKinematics( daug1Mass, daug2Mass, daug3Mass, parentMass )

        return SUCCESS


    def form_decay_descriptor( self ) :

        parName = self.partpropsvc.find( self.parentID ).name()
        resName = self.partpropsvc.find( self.resonanceID ).name()
        resDaug1Name = self.partpropsvc.find( self.resDaug1ID ).name()
        resDaug2Name = self.partpropsvc.find( self.resDaug2ID ).name()
        bachName = self.partpropsvc.find( self.bachelorID ).name()

        parConjName = self.partpropsvc.find( self.parentID ).antiParticle().name()
        resConjName = self.partpropsvc.find( self.resonanceID ).antiParticle().name()
        resDaug1ConjName = self.partpropsvc.find( self.resDaug1ID ).antiParticle().name()
        resDaug2ConjName = self.partpropsvc.find( self.resDaug2ID ).antiParticle().name()
        bachConjName = self.partpropsvc.find( self.bachelorID ).antiParticle().name()

        self.decay_descriptor = '[ ( '
        self.decay_descriptor += parName
        self.decay_descriptor += ' => ( '
        self.decay_descriptor += resName
        self.decay_descriptor += ' => '
        self.decay_descriptor += resDaug1Name
        self.decay_descriptor += ' '
        self.decay_descriptor += resDaug2Name
        self.decay_descriptor += ' ) '
        self.decay_descriptor += bachName
        self.decay_descriptor += ' ) , ( '
        self.decay_descriptor += parName
        self.decay_descriptor += ' => ( '
        self.decay_descriptor += resConjName
        self.decay_descriptor += ' => '
        self.decay_descriptor += resDaug1ConjName
        self.decay_descriptor += ' '
        self.decay_descriptor += resDaug2ConjName
        self.decay_descriptor += ' ) '
        self.decay_descriptor += bachConjName
        self.decay_descriptor += ' ) , ( '
        self.decay_descriptor += parConjName
        self.decay_descriptor += ' => ( '
        self.decay_descriptor += resName
        self.decay_descriptor += ' => '
        self.decay_descriptor += resDaug1Name
        self.decay_descriptor += ' '
        self.decay_descriptor += resDaug2Name
        self.decay_descriptor += ' ) '
        self.decay_descriptor += bachName
        self.decay_descriptor += ' ) , ( '
        self.decay_descriptor += parConjName
        self.decay_descriptor += ' => ( '
        self.decay_descriptor += resConjName
        self.decay_descriptor += ' => '
        self.decay_descriptor += resDaug1ConjName
        self.decay_descriptor += ' '
        self.decay_descriptor += resDaug2ConjName
        self.decay_descriptor += ' ) '
        self.decay_descriptor += bachConjName
        self.decay_descriptor += ' ) ]'

        self.Info( 'Will use the decay descriptor '+self.decay_descriptor )



    def initialize( self ) :

        sc = AlgoMC.initialize( self )
        if sc.isFailure() :
            return sc

        # set up the kinematics object based on the decay
        self.partpropsvc = self.ppSvc()

        # check the validity of the decay
        sc = self.check_types()
        if sc.isFailure() :
            return sc

        # form the decay descriptor
        self.form_decay_descriptor()

        # set up the reconstrucible/reconstructed tools
        self.recible = self.tool( cpp.IMCReconstructible, 'MCReconstructible' )
        self.rected  = self.tool( cpp.IMCReconstructed,   'MCReconstructed'   )

        return SUCCESS



    def reco_status_tuple( self, tuple, mcparticle, name ) :
        """
        Store the reconstructible/reconstructed status of an MC particle
        """

        if not mcparticle :
            tuple.column_int( name + '_Reconstructible', -1 )
            tuple.column_int( name + '_Reconstructed',   -1 )
            return

        cat_ible = self.recible.reconstructible( mcparticle )
        cat_ted  = self.rected.reconstructed( mcparticle )

        tuple.column_int( name + '_Reconstructible', int(cat_ible) )
        tuple.column_int( name + '_Reconstructed',   int(cat_ted)  )


    def mc_p4_tuple( self, tuple, mcparticle, name ) :
        """
        Store the id, charge, 4-momentum, mass, p_t and eta of an MC particle
        """

        if not mcparticle :
            tuple.column_int( name + '_TRUEID',    -1   )
            tuple.column_int( name + '_TRUEQ',     -1   )
            tuple.column_double( name + '_TRUEP',     -1.1 )
            tuple.column_double( name + '_TRUEPE',    -1.1 )
            tuple.column_double( name + '_TRUEPX',    -1.1 )
            tuple.column_double( name + '_TRUEPY',    -1.1 )
            tuple.column_double( name + '_TRUEPZ',    -1.1 )
            tuple.column_double( name + '_TRUEPT',    -1.1 )
            tuple.column_double( name + '_TRUEETA',   -1.1 )
            tuple.column_double( name + '_TRUEPHI',   -1.1 )
            tuple.column_double( name + '_TRUETHETA', -1.1 )
            tuple.column_double( name + '_TRUEM',     -1.1 )
            tuple.column_int( name + '_OSCIL',     -1   )
            return

        tuple.column_int(    name + '_TRUEID',    int(MCID(mcparticle))         )
        tuple.column_int(    name + '_TRUEQ',     int(MC3Q(mcparticle)/3)       )
        tuple.column_double( name + '_TRUEP',     MCP(mcparticle)               )
        tuple.column_double( name + '_TRUEPE',    MCE(mcparticle)               )
        tuple.column_double( name + '_TRUEPX',    MCPX(mcparticle)              )
        tuple.column_double( name + '_TRUEPY',    MCPY(mcparticle)              )
        tuple.column_double( name + '_TRUEPZ',    MCPZ(mcparticle)              )
        tuple.column_double( name + '_TRUEPT',    MCPT(mcparticle)              )
        tuple.column_double( name + '_TRUEETA',   MCETA(mcparticle)             )
        tuple.column_double( name + '_TRUEPHI',   MCPHI(mcparticle)             )
        tuple.column_double( name + '_TRUETHETA', MCTHETA(mcparticle)           )
        tuple.column_double( name + '_TRUEM',     MCM(mcparticle)               )
        tuple.column_int(    name + '_OSCIL',     int(MCOSCILLATED(mcparticle)) )


    def mc_vtx_tuple( self, tuple, mcparticle, name ) :
        """
        Store vertex and lifetime info for the MC particle
        """

        if not mcparticle :
            tuple.column_double( name + '_TRUEORIGINVERTEX_X', -1.1 )
            tuple.column_double( name + '_TRUEORIGINVERTEX_Y', -1.1 )
            tuple.column_double( name + '_TRUEORIGINVERTEX_Z', -1.1 )
            tuple.column_double( name + '_TRUECTAU'          , -1.1 )
            return

        tuple.column_double( name + '_TRUEORIGINVERTEX_X', MCVFASPF(MCVX)(mcparticle) )
        tuple.column_double( name + '_TRUEORIGINVERTEX_Y', MCVFASPF(MCVY)(mcparticle) )
        tuple.column_double( name + '_TRUEORIGINVERTEX_Z', MCVFASPF(MCVZ)(mcparticle) )
        tuple.column_double( name + '_TRUECTAU'          , MCCTAU(mcparticle)         )


    def mc_dp_tuple( self, tuple, mcparent ) :
        """
        Store the MC truth DP co-ordinates
        """

        # loop through the B daughters and store their 4-momenta treating the
        # expected 3 daughters and PHOTOS photons separately

        bach_p4 = None
        bach_id = -1
        resdaug_p4 = []
        resdaug_id = []
        gammaB_p4 = []
        gammaR_p4 = []

        parID = mcparent.particleID().pid()

        for daug in mcparent.children( True ) :

            daugID = daug.particleID().pid()

            px = MCPX( daug )
            py = MCPY( daug )
            pz = MCPZ( daug )
            pe = MCE ( daug )

            p4 = TLorentzVector( px, py, pz, pe )

            if 22 == daugID :
                gammaB_p4.append( p4 )

            elif daugID not in self.names :

                for resdaug in daug.children( True ) :

                    resdaugID = resdaug.particleID().pid()

                    px = MCPX( resdaug )
                    py = MCPY( resdaug )
                    pz = MCPZ( resdaug )
                    pe = MCE ( resdaug )

                    p4 = TLorentzVector( px, py, pz, pe )

                    if 22 == resdaugID :
                        gammaR_p4.append( p4 )
                    else :
                        resdaug_p4.append( p4 )
                        resdaug_id.append( resdaugID )

            else :
                bach_p4 = p4
                bach_id = daugID

        ngammaB = len(gammaB_p4)
        ngammaR = len(gammaR_p4)
        tuple.column_int( 'nPHOTOS', ngammaB+ngammaR )

        for gamma in gammaB_p4 :
            bach_p4 += gamma

        for gamma in gammaR_p4 :
            minangle = 1000.0
            mindaug = -1
            for daug in resdaug_p4 :
                angle = gamma.Angle( daug.Vect() )
                if abs(angle) < minangle :
                    minangle = angle
                    mindaug = resdaug_p4.index(daug)

            resdaug_p4[ mindaug ] += gamma


        unsorted_daug_p4 = resdaug_p4
        unsorted_daug_p4.append( bach_p4 )
        unsorted_daug_id = resdaug_id
        unsorted_daug_id.append( bach_id )

        daug_id = [ 0, 0, 0 ]
        daug_p4 = [ TLorentzVector(), TLorentzVector(), TLorentzVector() ]

        for i in range(3) :
            id = unsorted_daug_id[i]
            if self.names[ id ] == 'h1' :
                daug_p4[0] = unsorted_daug_p4[i]
                daug_id[0] = id
            elif self.names[ id ] == 'h2' :
                daug_p4[1] = unsorted_daug_p4[i]
                daug_id[1] = id
            else :
                daug_p4[2] = unsorted_daug_p4[i]
                daug_id[2] = id

        p12 = daug_p4[0] + daug_p4[1]
        p13 = daug_p4[0] + daug_p4[2]
        p23 = daug_p4[1] + daug_p4[2]

        for i in range(3) :
            daug_name = self.names[ daug_id[i] ]
            tuple.column_double( daug_name + '_CORRPE',  daug_p4[i].E()  )
            tuple.column_double( daug_name + '_CORRPX',  daug_p4[i].Px() )
            tuple.column_double( daug_name + '_CORRPY',  daug_p4[i].Py() )
            tuple.column_double( daug_name + '_CORRPZ',  daug_p4[i].Pz() )

        m12Sq = p12.M2()
        m13Sq = p13.M2()
        m23Sq = p23.M2()

        mPrime = -1.1
        thPrime = -1.1

        if self.kinematics.withinDPLimits( m13Sq, m23Sq ) :
            self.kinematics.updateKinematics( m13Sq, m23Sq )
            mPrime = self.kinematics.mPrime
            thPrime = self.kinematics.thPrime

        if m12Sq<0 :     m12Sq = -1.1
        if m12Sq>100e6 : m12Sq = -1.1
        if m13Sq<0 :     m13Sq = -1.1
        if m13Sq>100e6 : m13Sq = -1.1
        if m23Sq<0 :     m23Sq = -1.1
        if m23Sq>100e6 : m23Sq = -1.1

        tuple.column_double( 'm12Sq_MC', m12Sq )
        tuple.column_double( 'm13Sq_MC', m13Sq )
        tuple.column_double( 'm23Sq_MC', m23Sq )

        tuple.column_double( 'mPrime_MC',  mPrime  )
        tuple.column_double( 'thPrime_MC', thPrime )


    def analyse( self ) :
        """
        The method called in the event loop
        """

        # select MC particles
        cands  = self.mcselect('cands',  self.decay_descriptor)
        ncands = cands.size()

        if 0 == ncands :
            self.Warning( 'No MC B candidates found in this event', SUCCESS )
            return SUCCESS

        # get the event header
        evthdr = self.get( '/Event/Rec/Header' )

        # create the ntuple
        tuple = self.nTuple( 'tupleMCTruth' )

        # loop through the candidates
        for cand in cands :

            # fill event information
            tuple.column_int( 'runNumber', evthdr.runNumber() )
            tuple.column_int( 'evtNumber', evthdr.evtNumber() )
            tuple.column_int( 'nCands',    ncands             )

            # get the ID and hance name of the parent
            candID = cand.particleID().pid()
            candname = self.names[ candID ]

            # store parent information
            self.mc_p4_tuple( tuple, cand, candname )
            self.mc_vtx_tuple( tuple, cand, candname )
            self.reco_status_tuple( tuple, cand, candname )

            # store DP information
            self.mc_dp_tuple( tuple, cand )

            # find the 3 final-state particles
            daug_list = []
            for daug in cand.children( True ) :
                daugID = daug.particleID().pid()
                if 22 == daugID :
                    continue
                elif daugID not in self.names :
                    for resdaug in daug.children( True ) :
                        resdaugID = resdaug.particleID().pid()
                        if 22 == resdaugID :
                            continue
                        daug_list.append( resdaug )
                else :
                    daug_list.append( daug )

            # loop through the final state particles and store their information
            for daug in daug_list :

                daugID = daug.particleID().pid()
                daugname = self.names[ daugID ]

                self.mc_p4_tuple( tuple, daug, daugname )
                self.mc_vtx_tuple( tuple, daug, daugname )
                self.reco_status_tuple( tuple, daug, daugname )

                # if this is the V0 then also find and store information for it's daughters
                if abs(daugID) == self.daug3ID.abspid() :

                    tuple.column_int( daugname+'_NDAUG', daug.nChildren() )

                    gdaug1_name = ''
                    gdaug2_name = ''

                    for gdaug in daug.children( True ) :
                        gdaugID = gdaug.particleID().pid()
                        if gdaugID not in self.gdaugnames :
                            continue

                        gdaugname = self.gdaugnames[gdaugID]
                        if gdaug1_name == '' :
                            gdaug1_name = gdaugname
                        elif gdaug2_name == '' :
                            gdaug2_name = gdaugname
                        else :
                            self.Warning('Unexpected extra daughter of '+daugname, SUCCESS)
                            continue

                        self.mc_p4_tuple( tuple, gdaug, gdaugname )
                        self.mc_vtx_tuple( tuple, gdaug, gdaugname )
                        self.reco_status_tuple( tuple, gdaug, gdaugname )

                    gdaugnames = self.gdaugnames.values()
                    if gdaug1_name in gdaugnames :
                        gdaugnames.remove( gdaug1_name )
                    if gdaug2_name in gdaugnames :
                        gdaugnames.remove( gdaug2_name )

                    for name in gdaugnames :
                        self.mc_p4_tuple( tuple, None, name )
                        self.mc_vtx_tuple( tuple, None, name )
                        self.reco_status_tuple( tuple, None, name )

            tuple.write()


        return SUCCESS

# End of B2KShhMCTruthResonance Class Definition

