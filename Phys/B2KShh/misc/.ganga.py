#=======================================================================
# User defined ganga functions

import os

# Get bookkeeping information for a given MC event type
def getBKInfo ( evttype )  :

    from subprocess import Popen, PIPE
    
    serr = open ( '/dev/null' )
    pipe = Popen ( [ 'get_bookkeeping_info'  , str(evttype) ] ,
                   env    = os.environ ,
                   stdout = PIPE       ,
                   stderr = serr       )
    
    stdout = pipe.stdout 
    ts = {}
    
    ## Format = tuple 
    result = {} 
    
    for line in stdout :

        try : 
            value = eval ( line )
        except :
            continue
        
        if not isinstance ( value    , tuple ) : continue
        if not isinstance ( value[0] , str   ) : continue
        if not isinstance ( value[1] , str   ) : continue
        if not isinstance ( value[2] , str   ) : continue

        if result.has_key ( value[0] ) : continue
        result [ value[0] ] = value[1:]

    return result 

