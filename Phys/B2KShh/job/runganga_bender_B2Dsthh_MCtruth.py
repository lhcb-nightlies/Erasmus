
eventTypeDict = {
    'B2Dstpipi-sqDalitz-DecProdCut' : 12165080
  , 'B2DstKpi-sqDalitz-DecProdCut'  : 12165081
}

# *************************************************************************************** #
# pick magnet polarities, stripping versions and an event type from the known types above #
# *************************************************************************************** #
magtypes = [ 'MagDown', 'MagUp' ]
strippings = [ 'Stripping20r1', 'Stripping20' ]
eventtypes = eventTypeDict.keys()
# *************************************************************************************** #

import os
import re

path = os.getcwd()
pathList = path.split('/')

match1 = re.search( '/Bender_(v\d+r\d+[p(?=\d)]?[(?<=p)\d]*)/', path )
match2 = re.search( 'cmtuser', path )

if not path.endswith('job') or 'B2KShh' not in pathList or not match1 or not match2 :
    e = Exception('You do not appear to be in the \'job\' directory of the B2KShh package within a Bender project')
    raise e

benderVersion = match1.groups()[0]

modulePath = path.replace('job','options')

userReleaseArea = path[:match2.end()]


for eventtype in eventtypes :

    print 'Creating job(s) for event type: ' + eventtype

    if eventtype not in eventTypeDict.keys() :
        e = Exception('Unknown event type: ' + eventtype )
        raise e

    evtID = eventTypeDict[ eventtype ]

    bkinfo = getBKInfo( evtID )
    bkpaths = bkinfo.keys()

    mode = eventtype.split('-')[0]

    for bkpath in bkpaths :

        stripMatch = False
        for stripping in strippings :
            if stripping in bkpath :
                stripMatch = True
        if not stripMatch :
            continue

        magnetMatch = False
        for magtype in magtypes :
            if magtype in bkpath :
                magnetMatch = True
        if not magnetMatch :
            continue

        print 'Trying BK path: ' + bkpath,
        bkq = BKQuery( type='Path', dqflag='OK', path=bkpath )
        dataset = bkq.getDataset()
        dddbtag = bkinfo[bkpath][0]
        condtag = bkinfo[bkpath][1]

        if len( dataset ) == 0 :
            print 'Could not find any valid data!!  Skipping this configuration!!'
            continue

        # uncomment this if you want to run a quick test on the CERN batch
        #reduced_ds = LHCbDataset()
        #for file in dataset.files :
            #sites = file.getReplicas().keys()
            #for site in sites :
                #if 'CERN' in site :
                    #reduced_ds.files.append( file )
                    #break
            #if len(reduced_ds.files) > 0 :
                #break

        bkparts = bkpath.split('/')
        datatype = bkparts[2]
        conditions = bkparts[3]
        magtype = conditions.split('-')[2]

        params = {}
        params['mode']      = mode
        params['dataType']  = datatype
        params['magType']   = magtype
        params['dddbtag']   = dddbtag
        params['conddbtag'] = condtag

        moduleFile = modulePath+'/bender_'+mode+'_MCtruth.py'

        b = Bender(version=benderVersion)
        b.user_release_area = userReleaseArea
        b.module = File(moduleFile)
        b.params = params

        j=Job()
        j.name = mode+'_'+datatype+'_'+magtype
        j.application = b
        j.backend = Dirac()
        j.inputdata = dataset

        # uncomment this if you want to run a quick test on the CERN batch
        #j.backend = LSF( queue = '8nh' ) 
        #j.inputdata = reduced_ds

        # NB remember to change the tuple name in the Bender script to match this!
        tupleFile = mode+'-'+datatype+'-'+magtype+'-MCTruth.root'

        # can pick if you want the ntuple returned to you immediately (SandboxFile) or stored on the Grid (DiracFile)
        #j.outputfiles = [SandboxFile(tupleFile)]
        j.outputfiles = [DiracFile(tupleFile)]

        # can tweak the Dirac options if you like
        #j.backend.settings['CPUTime'] = 10000 
        #j.backend.settings['Destination'] = 'LCG.CERN.ch' 

        # can change here the number of files you want to run over per job
        j.splitter = SplitByFiles( filesPerJob = 10 ) 

        #j.submit()

