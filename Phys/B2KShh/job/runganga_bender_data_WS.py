
# **********************
# pick a magnet polarity and stripping version
# **********************
magtype = ''
# stripping revision allowed listed in known201(1,2)StrippingVersions below
whichStripping = ''
# **********************

# automatically configured
whichData = ''

known2011StrippingVersions = [ 'Stripping20r1' ]
known2012StrippingVersions = [ 'Stripping20' ]

if whichStripping in known2011StrippingVersions:
  whichData = '2011'
elif whichStripping in known2012StrippingVersions:
  whichData = '2012'
else :
  e = Exception('Unknown Stripping version ' + whichStripping)
  raise e

knownDataTypes = [ '2011' ,'2012' ]
if whichData not in knownDataTypes :
  e = Exception('Unknown data type')
  raise e

knownStrippingVersions = {}
knownStrippingVersions['2011'] = known2011StrippingVersions
knownStrippingVersions['2012'] = known2012StrippingVersions
if whichStripping not in knownStrippingVersions[whichData] :
  e = Exception('Unknown Stripping version for data type '+whichData)
  raise e

beamEnergy = {}
beamEnergy['2011'] = '3500'
beamEnergy['2012'] = '4000'

recoVersion = {}
recoVersion['Stripping20'] = '14'

bkpath = '/LHCb/Collision'+whichData[2:]+'/Beam'+beamEnergy[whichData]+'GeV-VeloClosed-'+magtype+'/Real Data/Reco'+recoVersion[whichStripping[:11]]+'/'+whichStripping+'/90000000/BHADRON.MDST'
bkq = BKQuery( type='Path', dqflag='OK', path=bkpath )
ds = bkq.getDataset()

# uncomment this if you want to run a quick test on the CERN batch
#reduced_ds = LHCbDataset()
#for file in ds.files :
    #sites = file.getReplicas().keys()
    #for site in sites :
        #if 'CERN' in site :
            #reduced_ds.files.append( file )
            #break
    #if len(reduced_ds.files) > 0 :
        #break


moduleFile = 'bender_B2KShh_data_WS_'+whichStripping+'_'+magtype+'.py'
import os
nameList = os.getcwd().split('/')
for name in nameList: 
  if 'Bender' in name:
    versionName = name.partition('_')[2]
b = Bender(version=versionName)
b.user_release_area = '$HOME/cmtuser'
b.module = File(moduleFile)
b.platform = 'x86_64-slc5-gcc46-opt'

j=Job()
j.name = 'Collision'+whichData[2:]+'_'+magtype+'_'+whichStripping+'_WS'
j.application = b
j.backend = Dirac()
j.inputdata = ds

# uncomment this if you want to run a quick test on the CERN batch
#j.backend = LSF( queue = '8nh' ) 
#j.inputdata = reduced_ds

# NB remember to change the tuple name in the Bender script to match this!
tupleFile = 'B2KShh-Collision'+whichData[2:]+'-'+magtype+'-'+whichStripping+'_WS.root'

# can pick if you want the ntuple in your outputsandbox or outputdata
#j.outputsandbox = [tupleFile]
j.outputfiles = [SandboxFile(tupleFile)]

# can tweak the Dirac options if you like
#j.backend.settings['CPUTime'] = 10000 
#j.backend.settings['Destination'] = 'LCG.CERN.ch' 

# can change here the number of files you want to run over per job
# NB need recent version of Ganga to use this splitter with Dirac backend - older versions need DiracSplitter
j.splitter = SplitByFiles( filesPerJob = 25, maxFiles = -1 ) 

j.submit()

