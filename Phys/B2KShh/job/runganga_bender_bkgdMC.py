
eventTypeDict = {
    'Bd2Kst0rho0-KS0pi0pipi'  : 11100001
  , 'Bd2pipiKS0gamma'         : 11100003
  , 'Bd2Kst0phi-KS0pi0KK'     : 11100006
  , 'Bd2etapKS0-pipigamma'    : 11104301
  , 'Bd2etapKS0-etapipi'      : 11104501
  , 'Bd2etaKS0-pipipi0'       : 11104511
  , 'Bu2Kstpipi-KS0pi'        : 12100002
  , 'Bu2Kstphi-KS0piKK'       : 12100004
  , 'Bu2D0K-KS0pipi'          : 12165102
  , 'Bu2D0pi-KS0pipi'         : 12165122
  , 'Bu2D0pi-KS0KK'           : 12165131
  , 'Bu2D0K-KS0KK'            : 12165141
  , 'Bu2Dst0K-D0pi0-KS0pipi'  : 12165501
  , 'Bu2Dst0pi-D0pi0-KS0pipi' : 12165511
  , 'Bs2Kst0phi-KS0pi0KK'     : 13100008
  , 'Bs2KstKst-KS0piKpi0'     : 13106201
  , 'Lb2Lcpi-KS0p'            : 15100009
  , 'Lb2Dsp-KS0K'             : 15100010
}

# *************************************************************************************** #
# pick magnet polarities, stripping versions and an event type from the known types above #
# *************************************************************************************** #
magtypes = [ 'MagDown', 'MagUp' ]
strippings = [ 'Stripping20r1', 'Stripping20', 'Stripping20r1p1', 'Stripping20r0p1' ]
#eventtypes = eventTypeDict.keys()
eventtypes = [ 'Lb2Lcpi-KS0p' ]
# *************************************************************************************** #

import os
import re

knownMagTypes = [ 'MagDown', 'MagUp' ]

known2011StrippingVersions = [ 'Stripping20r1', 'Stripping20r1p1' ]
known2012StrippingVersions = [ 'Stripping20',   'Stripping20r0p1' ]

nativeStrippingVersion = {}
nativeStrippingVersion['2011'] = 'Stripping20r1'
nativeStrippingVersion['2012'] = 'Stripping20'

path = os.getcwd()
pathList = path.split('/')

match1 = re.search( '/Bender_(v\d+r\d+[p(?=\d)]?[(?<=p)\d]*)/', path )
match2 = re.search( 'cmtuser', path )

if not path.endswith('job') or 'B2KShh' not in pathList or not match1 or not match2 :
    e = Exception('You do not appear to be in the \'job\' directory of the B2KShh package within a Bender project')
    raise e

benderVersion = match1.groups()[0]

modulePath = path.replace('job','options')

userReleaseArea = path[:match2.end()]


for eventtype in eventtypes :

    print 'Creating job(s) for event type: ' + eventtype

    if eventtype not in eventTypeDict.keys() :
        e = Exception('Unknown event type: ' + eventtype )
        raise e

    evtID = eventTypeDict[ eventtype ]

    bkinfo = getBKInfo( evtID )
    bkpaths = bkinfo.keys()

    for stripping in strippings :

        print 'With stripping: ' + stripping

        datatype = ''
        if stripping in known2011StrippingVersions:
            datatype = '2011'
        elif stripping in known2012StrippingVersions:
            datatype = '2012'
        else :
            e = Exception('Unsupported Stripping version: ' + stripping)
            raise e

        restrip = False
        if stripping != nativeStrippingVersion[datatype] :
            restrip = True

        for magtype in magtypes :

            print 'With magnet setting: ' + magtype

            if magtype not in knownMagTypes :
                e = Exception('Unsupported magnet setting: ' + magtype)
                raise e

            for pythiaversion in [ 6, 8 ] :

                print 'With Pythia version: %d' % pythiaversion

                datasets = []
                dddbtags = set()
                condtags = set()

                for bkpath in bkpaths :
                    if ('Sim08' not in bkpath) or ('/MC/'+datatype not in bkpath) or (magtype not in bkpath) or ('Pythia'+str(pythiaversion) not in bkpath) :
                        continue
                    print 'Trying BK path: ' + bkpath,
                    bkq = BKQuery( type='Path', dqflag='OK', path=bkpath )
                    dstmp = bkq.getDataset()
                    if len(dstmp) != 0 :
                        print ' - found dataset'
                        datasets.append( dstmp )
                        dddbtags.add( bkinfo[bkpath][0] )
                        condtags.add( bkinfo[bkpath][1] )
                    else :
                        print ' - nothing found'

                if len( datasets ) == 0 :
                    print 'Could not find any valid data!!  Skipping this configuration!!'
                    continue
                elif len(datasets) == 1 :
                    ds = datasets[0]
                elif len(datasets) == 2 :
                    ds = datasets[0].union( datasets[1] )
                else :
                    e = Exception('Found more than two datasets!')
                    raise e

                if len( ds.files ) == 0 :
                    e = Exception('Zero files in this dataset!')
                    raise e

                if len( dddbtags ) != 1 or len( condtags ) != 1 :
                    e = Exception('Found multiple DB tags')
                    raise e

                # uncomment this if you want to run a quick test on the CERN batch
                #reduced_ds = LHCbDataset()
                #for file in ds.files :
                    #sites = file.getReplicas().keys()
                    #for site in sites :
                        #if 'CERN' in site :
                            #reduced_ds.files.append( file )
                            #break
                    #if len(reduced_ds.files) > 0 :
                        #break

                params = {}
                params = {}
                params['mode']      = eventtype
                params['whichMC']   = datatype
                params['stripping'] = stripping
                params['dddbtag']   = dddbtags.pop()
                params['conddbtag'] = condtags.pop()

                moduleFile = modulePath+'/bender_B2KShh_bkgdMC.py'
                if restrip :
                    moduleFile = modulePath+'/bender_B2KShh_stripping_bkgdMC.py'

                b = Bender(version=benderVersion)
                b.user_release_area = userReleaseArea
                b.module = File(moduleFile)
                b.params = params

                j=Job()
                j.name = eventtype+'_MC_'+datatype+'_'+magtype+'_'+stripping+'_Pythia'+str(pythiaversion)
                j.application = b
                j.backend = Dirac()
                j.inputdata = ds

                # uncomment this if you want to run a quick test on the CERN batch
                #j.backend = LSF( queue = '8nh' ) 
                #j.inputdata = reduced_ds

                # NB remember to change the tuple name in the Bender script to match this!
                tupleFile = eventtype+'-MC-'+datatype+'-'+magtype+'-'+stripping+'.root'

                # can pick if you want the ntuple returned to you immediately (SandboxFile) or stored on the Grid (DiracFile)
                #j.outputfiles = [SandboxFile(tupleFile)]
                j.outputfiles = [DiracFile(tupleFile)]

                # can tweak the Dirac options if you like
                #j.backend.settings['CPUTime'] = 10000 
                #j.backend.settings['Destination'] = 'LCG.CERN.ch' 

                # can change here the number of files you want to run over per job
                j.splitter = SplitByFiles( filesPerJob = 5 ) 

                #j.submit()

