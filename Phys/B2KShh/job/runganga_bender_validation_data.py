
bkpath = '/validation/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping20/90000000/BHADRONCOMPLETEEVENT.DST'
bkq = BKQuery( type='Path', path=bkpath )
ds = bkq.getDataset()

moduleFile = '$HOME/cmtuser/Bender_v21r2/Phys/B2KShh/options/bender_B2KShh_data.py'
b = Bender(version="v21r2")
b.user_release_area = '$HOME/cmtuser'
b.module = File(moduleFile)

j=Job()
j.name = 'Stripping20-Validation'
j.application = b
j.backend = Dirac()
j.inputdata = ds

# make sure this matches what's in the Bender options!
tupleFile = 'B2KShh-Collision12-MagDown-Stripping20.root'

# can pick if you want the ntuple in your outputsandbox or outputdata
#j.outputsandbox = [tupleFile]
j.outputdata = [tupleFile]

# can tweak the Dirac options if you like
#j.backend.settings['CPUTime'] = 10000 
#j.backend.settings['Destination'] = 'LCG.CERN.ch' 

# can change here the number of files you want to run over per job
# NB need recent version of Ganga to use this splitter with Dirac backend - older versions need DiracSplitter
j.splitter = SplitByFiles( filesPerJob = 5 ) 

j.submit()

