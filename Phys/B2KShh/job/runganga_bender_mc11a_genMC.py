
eventTypeDict = {
  'Bd2pipiKS-sqDalitz' : 11104122
  , 'Bd2KpiKS-sqDalitz'  : 11304162
  , 'Bd2KKKS-sqDalitz'   : 11104112
  , 'Bs2pipiKS-sqDalitz' : 13104122
  , 'Bs2KpiKS-sqDalitz'  : 13304102
  , 'Bs2KKKS-sqDalitz'   : 13104132
}

# *************************************
# pick a magnet polarity and event type
# *************************************
mode = ''
magtype = ''
# *************************************
# *************************************
# *************************************

whichMC = 'MC11a'
evtID = eventTypeDict[ mode ]
bkpath = '/MC/'+whichMC+'/Beam3500GeV-2011-'+magtype+'-Fix1-EmNoCuts/Gen05d/'+str(evtID)+'/XGEN'
bkq = BKQuery( type='Path', dqflag='OK', path=bkpath )
ds = bkq.getDataset()

# uncomment this if you want to run a quick test on the CERN batch
#reduced_ds = LHCbDataset()
#for file in ds.files :
    #sites = file.getReplicas().keys()
    #for site in sites :
        #if 'CERN' in site :
            #reduced_ds.files.append( file )
            #break
    #if len(reduced_ds.files) > 0 :
        #break


moduleFile = 'bender_B2KShh_generatorMC_'+magtype+'.py'
import os
nameList = os.getcwd().split('/')
for name in nameList:
  if 'Bender' in name:
    versionName = name.partition('_')[2]
b = Bender(version=versionName)
b.user_release_area = '$HOME/cmtuser'
b.module = File(moduleFile)

j=Job()
j.name = mode+'_'+whichMC+'_'+magtype
j.application = b
j.backend = Dirac()
j.inputdata = ds

# uncomment this if you want to run a quick test on the CERN batch
#j.backend = LSF( queue = '8nh' ) 
#j.inputdata = reduced_ds

# NB remember to change the tuple name in the Bender script to match this!
tupleFile = mode+'-'+whichMC[0:4]+'-'+magtype+'-genMCTruth.root'

# can pick if you want the ntuple in your outputsandbox or outputdata
#j.outputsandbox = [tupleFile]
j.outputdata = [tupleFile]

# can tweak the Dirac options if you like
#j.backend.settings['CPUTime'] = 10000 
#j.backend.settings['Destination'] = 'LCG.CERN.ch' 

# can change here the number of files you want to run over per job
# NB need recent version of Ganga to use this splitter with Dirac backend - older versions need DiracSplitter
j.splitter = SplitByFiles( filesPerJob = 10 ) 

#j.submit()

