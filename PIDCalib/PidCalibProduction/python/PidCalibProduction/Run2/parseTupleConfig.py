################################################################################
##                                                                            ##
##  parseTupleConfig.py   -   Setup DaVinci to write nTuples and MicroDSTs    ##
##                                                                            ##
## Example:                                                                   ##
##                                                                            ##
##  configuredAlgorithms = parseConfiguration(                                ##
##      tupleConfiguration,  ## PIDCalib tuple configuration                  ##
##      tesFormat,           ## TES format "/Event/Turbo/<Line>/Particles"    ##
##      mdstOutputFile,      ## MicroDST filename also used as OUTPUT TES!!!  ##
##      mdstOutputPrefix,    ## MicroDST Prefix, to be changed in PRODUCTION  ##
##      varsByType,          ## dictionary of variables per branch type       ##
##      varsByName,          ## dictionary of variables per branch name       ##
##      eventVariables,      ## dictionary of event variables                 ##
##      writeNullWeightCandidates = True,  ## if False filters out sWeight=0  ## 
##    )                                                                       ##
##                                                                            ##
##                                                                            ##
## Example of minimal tupleConfiguration                                      ##
##                                                                            ##
##  tupleConfiguration = {                                                    ##
##    "DSt_PiP" : TupleConfig (                                               ##
##      Decay = "[D*(2010)+ -> ^(D0 -> K- ^pi+) pi+]CC"                       ##
##      , InputLines    = "Hlt2PIDD02KPiTagTurboCalib"                        ##
##      , Filter        = filters.PositiveID                                  ##
##      , Calibration   = "RSDStCalib_Pos"                                    ##
##      , Branches = {                                                        ##
##          "Dst"     : Branch("^([D*(2010)+ -> (D0 -> K- pi+) pi+]CC)", Type='H')
##          , "Dz"    : Branch( "[D*(2010)+ -> ^(D0 -> K- pi+) pi+]CC",  Type='I')
##          , "K"     : Branch( "[D*(2010)+ -> (D0 -> ^K- pi+) pi+]CC",  Type='T')
##          , "probe" : Branch( "[D*(2010)+ -> (D0 -> K- ^pi+) pi+]CC",  Type='T', isAlso=['pi'])
##          , "pis"   : Branch( "[D*(2010)+ -> (D0 -> K- pi+) ^pi+]CC",  Type='T', isAlso=['pi'])
##        }                                                                   ##
##      ),                                                                    ##
##    }                                                                       ##
##                                                                            ##
## Example of varsByType                                                      ##
##   LokiVarsByType = {                                                       ##
##        "HEAD" : {                                                          ##                             
##          "ENDVERTEX_CHI2"   : "VFASPF(VCHI2)"                              ##
##          ,"ENDVERTEX_NDOF" : "VFASPF(VDOF)"                                ##
##          ,"Mass" : "M"                                                     ##
##        },                                                                  ##
##                                                                            ##
##        "INTERMEDIATE" : {                                                  ##
##          ,"BPVLTCHI2" : "BPVLTCHI2()"                                      ##
##          ,"Mass" : "M"                                                     ##
##        },                                                                  ##
##                                                                            ##
##        "TRACK" : {                                                         ##
##          , "Loki_MINIPCHI2": "BPVIPCHI2()"                                 ##
##        },                                                                  ##
##                                                                            ##
##        "NEUTRAL" : {                                                       ##
##        }                                                                   ##
##     }                                                                      ##
##                                                                            ##
##                                                                            ##
##                                                                            ##
## Example of varsByName                                                      ##
##   varsByName = {                                                           ##
##                                                                            ##
##     "Lc" : {'WM_PiKPi': "WM('pi+', 'K-', 'pi+')" # D->Kpipi,               ##
##            ,'WM_KPiPi' : "WM('K-', 'pi+', 'pi+')" # D->Kpipi               ##
##            ,'WM_KKPi'  : "WM('K-', 'K+', 'pi+')" # D->KKpi                 ##
##     },                                                                     ##
##                                                                            ##
##     "Dz" : {'WM_PiPi'    : "WM('pi+','pi-')"                               ##      
##              ,'WM_KK'      : "WM('K+','K-')"                               ##       
##              ,'WM_DCS'     : "WM('pi+','K-')"                              ##
##                                                                            ##
##     },                                                                     ##
##                                                                            ##
##     "K"  : {                                                               ##
##     },                                                                     ##
##   }                                                                        ##
##                                                                            ##
##                                                                            ##
##                                                                            ##
################################################################################

from Gaudi.Configuration import *
from Configurables import DaVinci, MicroDSTWriter
from PhysConf.MicroDST import uDstConf
from PhysSelPython.Wrappers import DataOnDemand
from PhysSelPython.Wrappers import (Selection, 
                                    MergedSelection,
                                    SelectionSequence, 
                                    MultiSelectionSequence)
# make a FilterDesktop
from Configurables import FilterDesktop

from DecayTreeTuple import *
from DecayTreeTuple.Configuration import *
from Configurables import LoKi__Hybrid__EvtTupleTool as LokiEventTool
from Configurables import LoKi__Hybrid__TupleTool as LokiTool
from Configurables import TupleToolTwoParticleMatching as MatcherTool

from Configurables import StoreExplorerAlg
from PidCalibProduction.Configuration import ProbNNRecalibrator as ProbNNcalib
from Configurables import ApplySWeights
from PhysConf.MicroDST import uDstConf
## from Configurables import DaVinci
from copy import copy

# for MicroDST writing
from PhysSelPython.Wrappers import SelectionSequence
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter,stripMicroDSTStreamConf,
                       stripMicroDSTElements, stripCalibMicroDSTStreamConf,
                       stripDSTElements, stripDSTStreamConf)

from Configurables import CopyAndMatchCombination

from TupleConfig import Branch
from TupleConfig import TupleConfig
from Configurables import TupleToolPIDCalib as TTpid

from Configurables import MuonIDPlusTool #added from Giacomo



################################################################################
## Configuration of the MicroDST writer                                       ##
################################################################################
def configureMicroDSTwriter( name, prefix, sequences ):
  # Configure the dst writers for the output
  pack = True

  microDSTwriterInput = MultiSelectionSequence ( name , Sequences = sequences )

  # Configuration of MicroDST
  # per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
  mdstStreamConf = stripMicroDSTStreamConf(pack=pack)#, selectiveRawEvent = False)
  mdstElements   = stripMicroDSTElements(pack=pack)

  # Configuration of SelDSTWriter
  # per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
  SelDSTWriterElements = {'default': mdstElements }
  SelDSTWriterConf = {'default': mdstStreamConf}
#  SelDSTWriterConf = {'default': stripCalibMicroDSTStreamConf(pack=pack, selectiveRawEvent=False)}

  dstWriter = SelDSTWriter( "MyDSTWriter",
                            StreamConf = SelDSTWriterConf,
                            MicroDSTElements = SelDSTWriterElements,
                            OutputFileSuffix = prefix,
                            SelectionSequences = [microDSTwriterInput]
                          )

  from Configurables import StoreExplorerAlg
  return [microDSTwriterInput.sequence(), 
          dstWriter.sequence()]


                     
################################################################################
## CONFIGURATION OF THE JOB                                                   ##
################################################################################
def parseConfiguration(tupleConfig,  # TupleConfig object describing sample
                       tesFormat,    # Input TES with "<line>" placeholder
                       mdstOutputFile,  # MicroDST output extension
                       mdstOutputPrefix,# MicroDST prefix for production
                       varsByType,   # requested variables by type
                       varsByName,   # requested variables by name
                       eventVariables, # event variables
                       writeNullWeightCandidates = True, 
                      ):
  cfg = tupleConfig
  reviveSequences = [] # mark sequences to unpack std particles
  swSequences     = [] # mark sequences to apply sWeights
  filterSequences = [] # mark sequences to be written in tuples
  matchSequences  = [] # mark sequences to be written in tuples
  tupleSequences  = [] # mark tuple sequences
  dstSequences    = [] # sequences writing (Micro)DST files

  for basicPart in ["Muons", "Pions", "Kaons", "Protons", "Electrons"]:
    location = "Phys/StdAllNoPIDs{s}/Particles".format ( s = basicPart )
    reviveSequences += [SelectionSequence("fs_std" + basicPart , 
                          TopSelection = DataOnDemand(location))]
    
  for sample in cfg:
################################################################################
## Configure sWeighting                                                       ##
################################################################################
    for line in cfg[sample].InputLines:
      location = tesFormat.replace('<line>', line) 
      protoLocation = location.replace('/Particles', '/Protos') 

#     swSequences+=[ProbNNcalib ("TurboProbNN" + line, protoLocation).sequence()]
      if cfg[sample].Calibration:
        swSequences += [ 
          ApplySWeights ("ApplySW"+sample,
           InputTes   = location,
           sTableDir  = cfg[sample].Calibration,
           sTableName = "sTableSignal", 
          )
         ]


################################################################################
## Creates filter sequences to fill nTuples                                   ##
################################################################################

    selectionName = sample
    _cut = "DECTREE ('{}')".format ( cfg[sample].Decay.replace("^","") )

    if writeNullWeightCandidates == False:
      _cut += " & ( WEIGHT != 0 ) "

    if cfg[sample].Filter:
      _cut += " & ({}) ".format (  cfg[sample].Filter.cut )

    inputSelection = MergedSelection ( "input" + selectionName ,
          RequiredSelections = [
            DataOnDemand(tesFormat.replace('<line>', line))
              for line in  cfg[sample].InputLines ],
        )

    selection = Selection(selectionName,
        RequiredSelections = [ inputSelection ],
        Algorithm = FilterDesktop("alg_" + selectionName,
                                  Code = _cut),
      )

    filterSequence = SelectionSequence("Seq"+selectionName,
      TopSelection = selection)

    filterSequences += [filterSequence]

################################################################################
## Creates matching selections (used to create the proper locations in mdst)  ##
################################################################################
    matchingSel = Selection("Match" + selectionName,
        Algorithm = CopyAndMatchCombination ( "MatchAlg" + selectionName ),
        RequiredSelections = [selection]
        )

    matchingSeq = SelectionSequence ( "SeqMatch"+ selectionName,
          TopSelection = matchingSel )

    matchSequences += [ matchingSeq ]



################################################################################
## Parses the configuration dictionaries and configure the tuples             ##
################################################################################
    tuple = DecayTreeTuple(sample + "Tuple")
    tuple.Inputs = [filterSequence.outputLocation()]
    tuple.Decay  = cfg[sample].Decay
    tuple.ToolList = [ "TupleToolANNPID" ]

    ttBrem = tuple.addTupleTool("TupleToolBremInfo")
    ttBrem.Particle = ["pi+", "p", "K+", "mu+", "e+"]
    ttBrem.Verbose = True

    ttPPD = tuple.addTupleTool("TupleToolProtoPData")
    ttPPD.DataList = ["VeloCharge","CaloEoverP", "CaloEcalChi2", "CaloPrsE", "CaloHcalE", "EcalPIDe", "PrsPIDe", "HcalPIDe", "CaloEcalE"]

    muidPlus = tuple.addTupleTool("TupleToolMuonPidPlus");
    muidPlus.MuonIDPlusToolName = "MuonIDPlusTool"
    muidPlus.OutputLevel = 4
    muidPlus.addTool(MuonIDPlusTool)
    muidPlus.MuonIDPlusTool.OutputLevel=3
    muidPlus.MuonIDPlusTool.ReleaseObjectOwnership = False
    muidPlus.MuonIDPlusTool.MatchToolName="MuonChi2MatchTool"
    muidPlus.Verbose = True;
                               
    eventTool = tuple.addTupleTool("LoKi::Hybrid::EvtTupleTool/LoKiEvent")
    if 'VOID' in eventVariables.keys():
      eventTool.VOID_Variables = eventVariables['VOID']
    
    if 'ODIN' in eventVariables.keys():
      eventTool.ODIN_Variables = eventVariables['ODIN']

    if 'HLT' in eventVariables.keys():
      eventTool.HLT_Variables = eventVariables['HLT']

    if 'L0DU' in eventVariables.keys():
      eventTool.L0DU_Variables = eventVariables['L0DU']


    eventTool.Preambulo = [
      "from LoKiTracks.decorators import *",
      "from LoKiCore.functions import *"
    ]
    tuple.addTool( eventTool )

    matchingLocation = {
      "mu+"        :  "Phys/StdAllNoPIDsMuons/Particles",
      "pi+"        :  "Phys/StdAllNoPIDsPions/Particles",
      "K+"         :  "Phys/StdAllNoPIDsKaons/Particles",
      "p+"         :  "Phys/StdAllNoPIDsProtons/Particles",
      "e+"         :  "Phys/StdAllNoPIDsElectrons/Particles",
    }

    tupleSequences += [tuple]

    for branchName in cfg[sample].Branches:
      b = tuple.addBranches({branchName : cfg[sample].Branches[branchName].Particle})
      b = b[branchName]
      matcher = b.addTupleTool ( "TupleToolTwoParticleMatching/Matcher_" + branchName )
      matcher.ToolList = []
      matcher.Prefix = ""; matcher.Suffix = "_Brunel"
      matcher.MatchLocations = matchingLocation

      lokitool = b.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_"+branchName)
      vardict = copy(varsByType[ cfg[sample].Branches[branchName].Type ])
      pidcalibtool = b.addTupleTool ( "TupleToolPIDCalib/PIDCalibTool_"+branchName )
      pidcalibtool_matched = TTpid( "PIDCalibTool_match_"+branchName )
      for partName in [branchName] + cfg[sample].Branches[branchName].isAlso:
        if partName in varsByName:
          vardict.update ( varsByName[partName] )
#        if partName == 'e':
#          pidcalibtool.FillBremInfo = True
#          pidcalibtool_matched.FillBremInfo = True

      lokimatchedtool = LokiTool("LoKi_match_"+branchName)
       
      matcher.addTool ( pidcalibtool_matched )
      matcher.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_match_"+branchName ,
                           "TupleToolPIDCalib/PIDCalibTool_match_" + branchName ]

      from Configurables import TupleToolBremInfo, TupleToolProtoPData, TupleToolANNPID, TupleToolMuonPidPlus
      ttbi = TupleToolBremInfo("BremInfoForTheOffline"+branchName)
      ttbi.Particle = ["pi+", "p", "K+", "mu+", "e+"]
      matcher.addTool(ttbi)
      matcher.ToolList += ["TupleToolBremInfo/BremInfoForTheOffline"+branchName]

      ttppd = TupleToolProtoPData("ProtoPDataForTheOffline"+branchName)
      ttppd.DataList = ["VeloCharge","CaloEoverP", "CaloEcalChi2", "CaloPrsE", "CaloHcalE", "EcalPIDe", "PrsPIDe", "HcalPIDe", "CaloEcalE"]
      matcher.addTool(ttppd)
      matcher.ToolList += ["TupleToolProtoPData/ProtoPDataForTheOffline"+branchName]

      ttann = TupleToolANNPID("ANNPIDForTheOffline"+branchName)
      matcher.addTool(ttann)
      matcher.ToolList += ["TupleToolANNPID/ANNPIDForTheOffline"+branchName]

      ttmuidPlus = TupleToolMuonPidPlus("TupleToolMuonPidPlusForTheOffline"+branchName);
      ttmuidPlus.MuonIDPlusToolName = "MuonIDPlusTool"
      ttmuidPlus.OutputLevel = 4
      ttmuidPlus.addTool(MuonIDPlusTool)
      ttmuidPlus.MuonIDPlusTool.OutputLevel=3
      ttmuidPlus.MuonIDPlusTool.ReleaseObjectOwnership = False
      ttmuidPlus.MuonIDPlusTool.MatchToolName="MuonChi2MatchTool"
      ttmuidPlus.Verbose = True;
      matcher.addTool(ttmuidPlus);
      matcher.ToolList += ["TupleToolMuonPidPlus/TupleToolMuonPidPlusForTheOffline"+branchName]
      
      vardict.update (cfg[sample].Branches[branchName].LokiVariables)
      lokimatchedtool.Variables = vardict
      lokitool.Variables        = vardict

      matcher.addTool ( lokimatchedtool )


  print "Input TES: " 
  print "\n".join ( [f.outputLocation() for f in filterSequences] )
  if mdstOutputFile:
    dstSequences += configureMicroDSTwriter ( mdstOutputFile, 
                                              mdstOutputPrefix, 
                                              filterSequences + matchSequences)
      

  return (reviveSequences 
          + swSequences 
          + filterSequences 
          + matchSequences
          + tupleSequences 
          + dstSequences
      )


                    

