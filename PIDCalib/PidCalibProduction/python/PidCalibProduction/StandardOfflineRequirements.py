################################################################################
## Standard Cuts for PIDCalib
## + - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
##  This file contains a set of requirements used around PIDCalib to refine
##  the trigger strategy. They include only cuts to be applied BEFORE filling
##  histograms. These cuts are then written in the sTables to make sure they
##  are ensured again when applying sWeights to candidates.
##  Other selections (as trigger unbias) can be applied at later stage and, 
##  as long as they are uncorrelated with the mass, this is perfectly fine.
################################################################################

TrackGhostProb = "(MAXTREE ( TRGHP , ISBASIC ) < 0.4)"

DstCut = ( TrackGhostProb + 
      "& CHILDCUT ( (abs(WM('pi+','pi-') - PDGMASS) > 25) " + 
                 "& (abs(WM('K+','K-')   - PDGMASS) > 25)   " + 
                 "& (abs(WM('pi+','K-')  - PDGMASS) > 25) , 1 )"  )

PosId = "& (ID > 0)"
NegId = "& (ID < 0)"

def _WMr ( oldId, newId, minMass, maxMass ):
  "Wrong mass for the phi daughters, when coming from a Ds+"
  mass={'p+': 938.272046,'p~-':938.272046, 
        'K+': 493.667,    'K-':493.667, 
        'pi+':139.57018, 'pi-':139.57018}; 

  var = "sqrt( pow( ( E - sqrt(pow({mass},2) + pow(MINTREE(ID == '{oldId}',P),2)) + MINTREE(ID == '{oldId}', E) ),2) - pow(P,2) )"
  return " ( ( {var} < {min} ) | ( {var} > {max} ) )".format (
      min = minMass, max = maxMass,
      var = var.format (newId = newId, oldId = oldId, 
                        mass = mass[newId])
    )


DsPhiVetos =  (TrackGhostProb + 
      " & (  ( (ID > 0) &  {} & {} ) | ( (ID < 0) &  {} & {} ) )" 
        .format ( _WMr ("K+", "pi+", 1830, 1890), _WMr ("K+", "p+" , 2266, 2306),
                  _WMr ("K-", "pi-", 1830, 1890), _WMr ("K-", "p~-", 2266, 2306))
   )



MuonProbe = "(P>3000) & (PT>800) & (BPVIPCHI2()>10) & (TRCHI2DOF<3)"
MuonTag   = "(P>6000) & (PT>1500) & (BPVIPCHI2()>25) & (TRCHI2DOF<3) & (ISMUON)"

Jpsi_Pos  =  "&" + "( INTREE ( (Q > 0) & {probe} ) ) & ( INTREE ( ( Q < 0 ) & {tag} ) ) ".format ( probe = MuonProbe, tag = MuonTag ) 
Jpsi_Neg  =  "&" + "( INTREE ( (Q < 0) & {probe} ) ) & ( INTREE ( ( Q > 0 ) & {tag} ) ) ".format ( probe = MuonProbe, tag = MuonTag ) 
BJpsi_Pos =  "&" + "( INTREE ( (Q > 0) & {probe} ) ) & ( INTREE ( ( Q < 0 ) & {tag} ) ) ".format ( probe = MuonProbe, tag = MuonTag ) 
BJpsi_Neg =  "&" + "( INTREE ( (Q < 0) & {probe} ) ) & ( INTREE ( ( Q > 0 ) & {tag} ) ) ".format ( probe = MuonProbe, tag = MuonTag ) 

MuonProbe_lowpt = "(P>3000) & (BPVIPCHI2()>25) & (TRCHI2DOF<3)"
MuonTag_lowpt   = "(P>6000) & (PT>1500) & (BPVIPCHI2()>45) & (TRCHI2DOF<3) & (ISMUON)"
Jpsi_Pos_lowpt  =  "&" + "( INTREE ( (Q > 0) & {probe} ) ) & ( INTREE ( ( Q < 0 ) & {tag} ) ) ".format ( probe = MuonProbe_lowpt, tag = MuonTag_lowpt )
Jpsi_Neg_lowpt  =  "&" + "( INTREE ( (Q < 0) & {probe} ) ) & ( INTREE ( ( Q > 0 ) & {tag} ) ) ".format ( probe = MuonProbe_lowpt, tag = MuonTag_lowpt )

Jpsi_lowpt = "&" + " & ".join([
      "(BPVVDCHI2 > 225)",
      "(BPVDIRA > 0.9995)",
      "(VFASPF(VCHI2/VDOF)<5)",
        ])

Jpsiee = "&" + " & ".join([
    "(BPVIPCHI2()<9.0) ",
    "(VFASPF(VCHI2/VDOF)<9) ",
    "(NINTREE(('e-'==ABSID) & (BPVIPCHI2()>25)) == 2)"
  ])


DsphiMuonTag   = "(PT>800) & (PROBNNmu>0.8)"

Dsphi_Pos =  "&" + "( INTREE ( ( Q < 0 ) & {tag} ) ) ".format ( tag = DsphiMuonTag )
Dsphi_Neg =  "&" + "( INTREE ( ( Q > 0 ) & {tag} ) ) ".format ( tag = DsphiMuonTag )

Dsphi =  "&" + " & ".join([
  "(VFASPF(VCHI2)<6)",
  "(BPVIPCHI2()<6)",
  "(BPVIP()<0.05)",
  "(inRange(1005, CHILD(MM, 1), 1035))",
  ])

