// $Id: ApplySWeights.h
#ifndef APPLYSWEIGHTS_H
#define APPLYSWEIGHTS_H 1

// Include files
// from DaVinci, this is a specialized GaudiAlgorithm
#include "Kernel/DaVinciTupleAlgorithm.h"
#include "DetDesc/Condition.h"

// =================================
// ROOT
// =================================
#include "TH1.h"
#include "TFile.h"
//
// =================================
// LoKi
// =================================
#include "LoKi/PhysTypes.h"
#include "LoKi/IHybridFactory.h"



class ApplySWeights : public DaVinciTupleAlgorithm {
  public:
    ApplySWeights( const std::string& name, ISvcLocator* pSvcLocator );

    virtual ~ApplySWeights( );

    StatusCode initialize() override;
    StatusCode execute   () override;
    StatusCode finalize  () override;

  protected:

  private:
    std::string m_factory;
    std::string m_sTableMagUpFile, m_sTableMagDownFile;
    std::string m_sTableDir, m_sTableName;
    std::string m_preambulo, m_inputTes, m_condLocation;
    TFile *m_file;
    TH1 *m_map;

    LoKi::PhysTypes::Fun m_headCut, m_cut;
    std::vector < LoKi::PhysTypes::Fun > m_vars;

    Condition *m_condition;
    std::string m_conditionFilePath;

    StatusCode i_updateCache ();

};
#endif // APPLYSWEIGHTS_H

