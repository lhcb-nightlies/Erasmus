// Include files
#include "GaudiKernel/ToolFactory.h"

#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "Event/Particle.h"
#include "Event/Track.h"
#include "Event/GhostTrackInfo.h" 

// local
#include "TupleToolGhostTrackInfo.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolGhostTrackInfo
//
// 2016-09-12 : Paul Seyfert
//-----------------------------------------------------------------------------

DECLARE_TOOL_FACTORY( TupleToolGhostTrackInfo )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  TupleToolGhostTrackInfo::TupleToolGhostTrackInfo( const std::string& type,
                                          const std::string& name,
                                          const IInterface* parent )
    : TupleToolBase ( type, name , parent )
{
  declareInterface<IParticleTupleTool>(this);
}

//=============================================================================
StatusCode TupleToolGhostTrackInfo::initialize()
{
  if( !TupleToolBase::initialize() ) return StatusCode::FAILURE;

  m_ghostClassification = tool<ITrackGhostClassification>("AllTrackGhostClassification");

  return StatusCode::SUCCESS;
}

//=============================================================================
StatusCode TupleToolGhostTrackInfo::fill( const LHCb::Particle*
                                     , const LHCb::Particle* P
                                     , const std::string& head
                                     , Tuples::Tuple& tuple )
{
  const std::string prefix = fullName(head);
  
  bool test = StatusCode::SUCCESS;
  if ( !P ) return StatusCode::FAILURE;

  //first just return if the particle isn't supposed to have a track
  if ( !P->isBasicParticle() ) {
    test &= tuple->column( prefix+"_ghostCat", -996 );
    return StatusCode::SUCCESS;
  }

  const LHCb::ProtoParticle* protop = P->proto();
  if (!protop) {
    test &= tuple->column( prefix+"_ghostCat", -997 );
    return StatusCode::SUCCESS;
  }

  const LHCb::Track* track = protop->track();
  if (!track) {
    test &= tuple->column( prefix+"_ghostCat", -998 );
    return StatusCode::SUCCESS;
  }

  LHCb::GhostTrackInfo gInfo;
  if (m_ghostClassification->info(*track,gInfo).isSuccess()) {
    test &= tuple->column( prefix+"_ghostCat",gInfo.classification() );
  } else {
    test &= tuple->column( prefix+"_ghostCat", -999 );
  }

  return StatusCode(test);
}
