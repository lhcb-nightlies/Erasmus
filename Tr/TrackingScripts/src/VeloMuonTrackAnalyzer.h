#ifndef VELOMUONTRACKANALYZER_H
#define VELOMUONTRACKANALYZER_H 1

#include "GaudiAlg/GaudiAlgorithm.h"
#include "Event/Track.h"
#include "TrackInterfaces/ITrackCloneFinder.h"

/** @class VeloMuonTrackAnalyzer VeloMuonTrackAnalyzer.h
 *
 * @author Paul Seyfert
 * @date   2011-03-03
 *
 * @brief  Analyses VeloMuonTracks: confirms (links) them to tracks from standard reconstruction
 *
 * @code
 * VeloMuonTrackAnalyzer().InputLocation = "Rec/VeloMuon/Tracks"
 * VeloMuonTrackAnalyzer().LinkLocations = [ "Rec/Track/Forward" , "Rec/Track/Best" ]
 * VeloMuonTrackAnalyzer().LinkCriteria  = [ True, True ]
 * VeloMuonTrackAnalyzer().LinkInfos     = [ 5003, 5004 ]
 * ZooWriter.ExtraInfoList               = [ 5003, 5004 ]
 * @endcode
 *
 */
namespace VeloMuonLinkCrit {
 int ancestorclone = 1;
 int directclone = 2;
 int spatialclone = 3;
}

class VeloMuonTrackAnalyzer : public GaudiAlgorithm {
 public:
//The constructor including declaration of properties. Your python configuration should look like this:
/** @example
 * @code
 * std::cout << "Hello World!" << std::endl;
 * @endcode
 */
  VeloMuonTrackAnalyzer(const std::string& name, ISvcLocator* pSvcLocator);
  ~VeloMuonTrackAnalyzer();
  virtual StatusCode initialize();
  virtual StatusCode finalize();
  virtual StatusCode execute();
 private:
  /// where the VeloMuonTracks are to be found on TES
  std::string   m_inlocation;                

  /// unused right now (whether MC truth should be tested)
  bool m_MC;				     

  ITrackCloneFinder* m_clonefinder;

  /// to which TES-Locations should be linked by clone
  std::vector<int> m_linkcriteria;          

  /// to which TES-Locations should be linked at all
  std::vector<std::string> m_linklocations;  

  /// to which extra info the result should be written
  std::vector<int> m_linkinfos;              

  /// only link to certain track types (usually long tracks)
  std::vector<int> m_linktypes;
};

#endif
