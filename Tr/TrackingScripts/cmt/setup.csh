# echo "Setting TrackingScripts v1r0 in /home/hep/decianm/cmtuser/DaVinci_v33r2/Tr"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/lhcb.cern.ch/lib/contrib/CMT/v1r20p20090520
endif
source ${CMTROOT}/mgr/setup.csh

set tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set tempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt setup -csh -pack=TrackingScripts -version=v1r0 -path=/home/hep/decianm/cmtuser/DaVinci_v33r2/Tr  -no_cleanup $* >${tempfile}; source ${tempfile}
/bin/rm -f ${tempfile}

